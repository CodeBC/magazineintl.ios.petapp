//
//  ObjGKey.h
//  zBox
//
//  Created by Zayar Cn on 6/9/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjGKey : NSObject
{
    int status;
    NSString * strKey;
    NSString * strMessage;
}
@property int status;
@property (nonatomic, retain) NSString * strKey;
@property (nonatomic, retain) NSString * strMessage;
@end
