//
//  CustomPetsBarView.m
//  Pet
//
//  Created by Zayar on 6/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "CustomPetsBarView.h"
#import "PetThumbView.h"
#import "ObjPet.h"
#import "StringTable.h"
@implementation CustomPetsBarView
@synthesize owner,intBarType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadThumbScrollBarWith:(NSMutableArray *)arrPets{
    previousIndex = -1;
    intBarType = THUMB_PET;
    for(UIView * v in scroll.subviews){
        if( [v isKindOfClass: [PetThumbView class]]){
            v.hidden = TRUE;
            PetThumbView * customView = (PetThumbView *)v;
            customView = nil;
        }
    }
    if ( [arrPets count] > 0) {
        int x=5;
        int articleCount = 0;
        int xOffset = 5;
        int width = 93;
        int total = 0;
        for (ObjPet * objPet in arrPets) {
            x = (width + xOffset) * articleCount + 10;
            //NSLog(@"custom thumb view x :%d and y:%d",x,0);
            PetThumbView * customView;
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PetThumbView" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[PetThumbView class]]){
                    customView = (PetThumbView *) currentObject;
                    customView.frame = CGRectMake(x,0,width, 118);
                    customView.tag = objPet.pet_id;
                    customView.owner = self;
                    [customView loadThumbView:objPet];
                    break;
                }
            }
            [scroll addSubview:customView];
            
            //[customView release];
            articleCount ++;
            total = x;
        }
        
        total += width + xOffset;
        scroll.pagingEnabled = FALSE;
        [scroll setContentSize:CGSizeMake(total, scroll.frame.size.height)];
        [self bringSubviewToFront:scroll];
        NSLog(@"");
    }
}

- (void)loadThumbScrollBarWithUser:(NSMutableArray *)arrUser{
    previousIndex = -1;
    intBarType = THUMB_USER;
    for(UIView * v in scroll.subviews){
        if( [v isKindOfClass: [PetThumbView class]]){
            v.hidden = TRUE;
            PetThumbView * customView = (PetThumbView *)v;
            customView = nil;
        }
    }
    if ( [arrUser count] > 0) {
        int x=5;
        int articleCount = 0;
        int xOffset = 5;
        int width = 93;
        int total = 0;
        for (ObjUser * obj in arrUser) {
            x = (width + xOffset) * articleCount + 10;
            //NSLog(@"custom thumb view x :%d and y:%d",x,0);
            PetThumbView * customView;
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PetThumbView" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[PetThumbView class]]){
                    customView = (PetThumbView *) currentObject;
                    customView.frame = CGRectMake(x,0,width, 118);
                    customView.tag = obj.idx;
                    customView.owner = self;
                    [customView loadThumbViewWithUser:obj];
                    break;
                }
            }
            [scroll addSubview:customView];
            
            //[customView release];
            articleCount ++;
            total = x;
        }
        
        total += width + xOffset;
        scroll.pagingEnabled = FALSE;
        [scroll setContentSize:CGSizeMake(total, scroll.frame.size.height)];
        [self bringSubviewToFront:scroll];
        NSLog(@"");
    }
}

- (void) thumbScrollViewCleanUp{
    for(UIView * v in scroll.subviews){
        if( [v isKindOfClass: [PetThumbView class]]){
            PetThumbView * customView = (PetThumbView *)v;
            //[customView removeFromSuperview];
            //[customView release];
            customView = nil;
            //[v removeFromSuperview];
            //[v release];
            //v = nil;
        }
        //[v removeFromSuperview];
    }
}

- (void) onThumbImgViewSelected:(ObjPet *)objSPet{
    [owner onThumbScrollBarSelected:objSPet];
}

- (void) onThumbImgViewOnClicked:(ObjPet *)objSPet{
    [owner onThumbScrollBarIConClick:objSPet];
}

- (void) onThumbImgUserViewSelected:(ObjUser *)obj{
    [owner onThumbScrollUserBarSelected:obj];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
