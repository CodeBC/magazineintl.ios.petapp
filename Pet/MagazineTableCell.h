//
//  MagazineTableCell.h
//  Pet
//
//  Created by Zayar on 6/11/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjMagazine.h"
@protocol MagazineTableCellDelegate
- (void) onScribeWith:(ObjMagazine *)objMaga;
- (void) onDownloadWith:(ObjMagazine *)objMaga;
@end
@interface MagazineTableCell : UITableViewCell
{
    IBOutlet UIButton * btnSubscribe;
    IBOutlet UIButton * btnDownload;
    IBOutlet UILabel * lblName;
    id<MagazineTableCellDelegate> owner;
    ObjMagazine * objSelectedMaga;
}
@property id<MagazineTableCellDelegate> owner;
- (void)loadTheCellWith:(ObjMagazine *)obj;
@end
