//
//  DetailLostFoundViewController.h
//  Pet
//
//  Created by Zayar on 6/11/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjLostFound.h"

@interface DetailLostViewController : PetBasedViewController
@property (nonatomic, strong) ObjLostFound * objSelectedLf;
- (IBAction)onThisIsMyPet:(id)sender;
- (IBAction)onIFoundThisPet:(id)sender;
- (IBAction)onAbuse:(id)sender;
@end
