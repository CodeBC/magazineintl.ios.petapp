//
//  VideoTableCell.h
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjVideo.h"
@interface VideoTableCell : UITableViewCell{
    
}
@property (nonatomic, strong) IBOutlet UILabel * lblName;
@property (nonatomic, strong) IBOutlet UIImageView * imgView;
- (void)loadTheCellWith:(ObjVideo *)obj;
@end
