//
//  DBManager.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ObjUser.h"
#import "ObjReminder.h"

@interface DBManager : NSObject{
    
}

- (void) checkAndCreateDatabase;
- (ObjUser *) getUserObj;
- (void) updateUser:(ObjUser *)objUser;

- (NSInteger) insertCachedImage:(NSString *)url path:(NSString *) filePath;
- (NSMutableArray *) getExpiredImages:(double) limit;
- (NSString *) getFilePath:(NSString *) url;
- (void) removeCahcedImage;
- (void) showPreviewWithImage:(UIImage *)img andIsCamera:(BOOL)isCam;
- (void) updateUserPassword:(NSString *)strPass;
- (NSMutableArray *) getAllReminder;
- (void) updateReminder:(ObjReminder *)objReminder;
- (NSInteger) insertReminder:(ObjReminder *) objReminder;
- (ObjReminder *) getLastReminder;
- (NSMutableArray *) getExpiredReminder;
- (void) removeExpiredReminder;
- (void) updateReminderDone:(int)idx andDone:(int)done andSwitch:(int)isSwitch;
- (ObjReminder *) getReminderById:(int)idx;
- (void) deleteReminder:(ObjReminder *)objReminder;
@end
