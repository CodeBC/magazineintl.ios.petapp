//
//  ObjNewsFeed.m
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ObjNewsFeed.h"
#import "ObjUser.h"
#import "ObjPet.h"

@implementation ObjNewsFeed
@synthesize idx,objUser,arrPets,objPet;
- (id) init{
	if( self = [super init] ){
		objUser = [[ObjUser alloc] init];
        objPet = [[ObjPet alloc] init];
	}
	
	return self;
}

@end
