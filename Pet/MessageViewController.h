//
//  MessageViewController.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPullToRefresh.h"
@interface MessageViewController : UITableViewController
- (void)syncMessages;
-(IBAction)popover:(UIBarButtonItem *)sender;
@end
