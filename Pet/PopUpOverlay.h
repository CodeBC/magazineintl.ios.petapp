//
//  PopUpOverlay.h
//  zBox
//
//  Created by Zayar Cn on 6/2/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpOverlay : UIView
{
   IBOutlet UIImageView * imgBgView;
}
- (void)popupOverlayViewDidLoad;
@end
