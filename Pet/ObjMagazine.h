//
//  ObjMagazine.h
//  Pet
//
//  Created by Zayar on 6/11/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjMagazine : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strMagzineFileLink;
@property (nonatomic, strong) NSString * strDate;
@end
