//
//  LostFoundViewController.m
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "LostFoundViewController.h"
#import "ADSlidingViewController.h"
#import "SOAPRequest.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "NavBarButton.h" 
#import "ObjPetType.h"
#import "ObjArea.h"
#import "ObjLostFound.h"
#import "BrowseLostAndFoundViewController.h"
@interface LostFoundViewController ()
{
    SOAPRequest * myLostAndFoundRequest;
    NSMutableArray * arrTypes;
    NSMutableArray * arrArea;
    ObjPetType * objSelectedType;
    ObjArea * objSelectedArea;
    IBOutlet UIImageView * imgBgView;
    
    IBOutlet UIButton *btnFound;
    IBOutlet UIButton *btnLost;
    IBOutlet UIButton *btnBrowse;
}
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedType;
@property int selectedArea;
@property BOOL isTypeSelected;
@property BOOL isAreaSelected;
@property (nonatomic, strong) IBOutlet UIButton * btnType;
@property (nonatomic, strong) IBOutlet UIButton * btnArea;
@end

@implementation LostFoundViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    [self syncLostAndFound];
    
}

- (void) syncLostAndFound{
    [SVProgressHUD show];
    if (myLostAndFoundRequest == nil) {
        myLostAndFoundRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    myLostAndFoundRequest.processId = 11;
    [myLostAndFoundRequest syncLostAndFound];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [SVProgressHUD dismiss];
    if (processId == 11) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strErrorMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            if ([dics objectForKey:@"pet_types"] != [NSNull null]) {
                NSMutableArray * arrPetTypes = [dics objectForKey:@"pet_types"];
                if ([arrPetTypes count] != 0) {
                     arrTypes = [[NSMutableArray alloc]initWithCapacity:[arrPetTypes count] + 1];
                    ObjPetType *allObj = [[ObjPetType alloc] init];
                    allObj.idx = 9999;
                    allObj.strName = @"All Types";
                    [arrTypes addObject: allObj];
                     for(NSInteger i=0;i<[arrPetTypes count];i++){
                         NSDictionary * dicPet = [arrPetTypes objectAtIndex:i];
                         ObjPetType * objPetType = [[ObjPetType alloc]init];
                         if ([dicPet objectForKey:@"pet_type_id"] != [NSNull null]) {
                         objPetType.idx = [[dicPet objectForKey:@"pet_type_id"] intValue];
                         }
                         if ([dicPet objectForKey:@"type_name"] != [NSNull null]) {
                         objPetType.strName = [dicPet objectForKey:@"type_name"];
                         }
                         
                         [arrTypes addObject:objPetType];
                         NSLog(@"arr pet count %d and objPetType name %@ and id %d",[arrTypes count],objPetType.strName,objPetType.idx);
                     }
                
                }
                if ([arrTypes count]>0) {
                    self.selectedType = 0;
                    ObjPetType * objPetType = [arrTypes objectAtIndex:self.selectedType];
                    [self.btnType setTitle:objPetType.strName forState:normal];
                    self.isTypeSelected = TRUE;
                    objSelectedType = objPetType;
                }
            }
            
            NSMutableArray *allAreas = [[NSMutableArray alloc] init];
            
            if ([dics objectForKey:@"areas"] != [NSNull null]) {
                NSMutableArray * arr = [dics objectForKey:@"areas"];
                if ([arr count] != 0) {
                    arrArea = [[NSMutableArray alloc]initWithCapacity:[arr count] + 1];
                    ObjArea *myArea = [[ObjArea alloc] init];
                    myArea.idx = 9999;
                    myArea.strName = @"All Areas";
                    myArea.arrLostFound = allAreas;
                    [arrArea addObject:myArea];
                    for(NSInteger i=0;i<[arr count];i++){
                        NSDictionary * dicPet = [arr objectAtIndex:i];
                        ObjArea * objArea = [[ObjArea alloc]init];
                        if ([dicPet objectForKey:@"area_id"] != [NSNull null]) {
                            objArea.idx = [[dicPet objectForKey:@"area_id"] intValue];
                        }
                        if ([dicPet objectForKey:@"area_name"] != [NSNull null]) {
                            objArea.strName = [dicPet objectForKey:@"area_name"];
                        }
                        if ([dicPet objectForKey:@"lost_founds"] != [NSNull null]) {
                            NSMutableArray * arrT = [dicPet objectForKey:@"lost_founds"];
                            objArea.arrLostFound = [[NSMutableArray alloc]initWithCapacity:[arrT count]];
                            for(NSInteger t=0;t<[arrT count];t++){
                                ObjLostFound * objLf = [[ObjLostFound alloc]init];
                                objLf.lat = 0.0;
                                objLf.lon = 0.0;
                                NSDictionary * dicLF = [arrT objectAtIndex:t];
                                if ([dicPet objectForKey:@"lost_found_id"] != [NSNull null]) {
                                    objLf.idx = [[dicLF objectForKey:@"lost_found_id"] intValue];
                                }
                                
                                if ([dicPet objectForKey:@"lastseen_timestamp"] != [NSNull null]) {
                                    objLf.intLastSeenTimestamp = [[dicLF objectForKey:@"lastseen_timestamp"] intValue];
                                }
                                
                                if ([dicPet objectForKey:@"owner_id"] != [NSNull null]) {
                                    objLf.intOwnerId = [[dicLF objectForKey:@"owner_id"] intValue];
                                }
                                
                                if ([dicLF objectForKey:@"pet_name"] != [NSNull null]) {
                                    objLf.strPetName = [dicLF objectForKey:@"pet_name"];
                                }
                                
                                if ([dicLF objectForKey:@"gender"] != [NSNull null]) {
                                    objLf.strPetGender = [dicLF objectForKey:@"gender"];
                                }
                                
                                if ([dicLF objectForKey:@"pet_image"] != [NSNull null]) {
                                    objLf.strPetImgLink = [dicLF objectForKey:@"pet_image"];
                                }
                                
                                if ([dicLF objectForKey:@"reward"] != [NSNull null]) {
                                    objLf.reward = [[dicLF objectForKey:@"reward"] floatValue];
                                }
                                if ([dicLF objectForKey:@"description"] != [NSNull null]) {
                                    objLf.strDescription = [dicLF objectForKey:@"description"];
                                }
                                
                                if ([dicLF objectForKey:@"lastseen_date"] != [NSNull null]) {
                                    objLf.strLastSeenDate = [dicLF objectForKey:@"lastseen_date"];
                                }
                                /*if ([dicLF objectForKey:@"lost_found_type"] != [NSNull null]) {
                                    objLf.strLostFoundType = [dicLF objectForKey:@"lost_found_type"];
                                }*/
                                if ([dicLF objectForKey:@"lost_found_type"] != [NSNull null]) {
                                    objLf.intLostFoundType = [[dicLF objectForKey:@"lost_found_type"] intValue];
                                }
                                
                                if (objLf.intLostFoundType == 1) {
                                    objLf.strLostFoundType = @"Found";
                                }
                                else if (objLf.intLostFoundType == 0) {
                                    objLf.strLostFoundType = @"Lost";
                                }
                                
                                if ([dicLF objectForKey:@"pet_type_id"] != [NSNull null]) {
                                    objLf.intPetType = [[dicLF objectForKey:@"pet_type_id"] intValue];
                                    NSLog(@"pet type id %d",objLf.intPetType);
                                }
                                
                                if ([dicLF objectForKey:@"pet_breed_type_id"] != [NSNull null]) {
                                    objLf.intBreedType = [[dicLF objectForKey:@"pet_breed_type_id"] intValue];
                                }
                                
                                if ([dicLF objectForKey:@"lat"] != [NSNull null]) {
                                    objLf.lat = [[dicLF objectForKey:@"lat"] floatValue];
                                }
                                
                                if ([dicLF objectForKey:@"long"] != [NSNull null]) {
                                    objLf.lon = [[dicLF objectForKey:@"long"] floatValue];
                                }
                                
                                [objArea.arrLostFound addObject:objLf];
                            }
                        }
                        NSLog(@"obj area lost found count %d",[objArea.arrLostFound count]);
                        [arrArea addObject:objArea];
                        [allAreas addObjectsFromArray:objArea.arrLostFound];
                    }
                    
                    NSLog(@"obj area count %d",[arrArea count]);
                    if ([arrArea count]>0) {
                        self.selectedArea = 0;
                        ObjArea * objArea= [arrArea objectAtIndex:self.selectedArea];
                        [self.btnArea setTitle:objArea.strName forState:normal];
                        self.isAreaSelected = TRUE;
                        objSelectedArea = objArea;
                    }
                    
                }
            }
        }

        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        else if(status == 5){
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
            PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
            //nav.ownner = self;
            [self presentModalViewController:nav animated:YES];
        }
    }
}

- (void) viewReloadWith:(ObjArea *)objA{
    
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Lost & Found";
    self.navigationItem.titleView = lblName;
    [self loadForUIActionView];
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
    }
    
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    [Utility makeCornerRadius:btnBrowse andRadius:5];
    [Utility makeCornerRadius:btnFound andRadius:5];
    [Utility makeCornerRadius:btnLost andRadius:5];
    [Utility makeCornerRadius:self.btnArea andRadius:5];
    [Utility makeCornerRadius:self.btnType andRadius:5];
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    self.selectedArea = 0;
    self.selectedType = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


-(IBAction)onType:(id)sender{
    [self.menu setTitle:@"Select Type"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onBreed:(id)sender{
    [self.menu setTitle:@"Select Area"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedArea inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        ObjPetType * objType = [arrTypes objectAtIndex:row];
        
        return objType.strName;
        
    }
    if (pickerView.tag == 1) {
        ObjArea *objArea = [arrArea objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return objArea.strName;
        
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrTypes count];
    }
    if (pickerView.tag == 1) {
        return [arrArea count];
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        
        self.selectedType = row;
        NSLog(@"selected gender %d",self.selectedType);
    }
    if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedArea = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }

    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }
    
    else if (buttonIndex == 1) {
        //NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            ObjPetType * objPetType = [arrTypes objectAtIndex:self.selectedType];
            [self.btnType setTitle:objPetType.strName forState:normal];
            self.isTypeSelected = TRUE;
            objSelectedType = objPetType;
        }
        if (self.uiPickerView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            ObjArea * objArea= [arrArea objectAtIndex:self.selectedArea];
            [self.btnArea setTitle:objArea.strName forState:normal];
            self.isAreaSelected = TRUE;
            objSelectedArea = objArea;
        }
    }
}

- (IBAction)onBrowse:(id)sender{
    if (self.isTypeSelected && self.isAreaSelected) {
        NSLog(@"SelectedType %d", self.isTypeSelected);
        NSLog(@"SelectedType %d", self.isTypeSelected);
        NSMutableArray * arrResultArr = [[NSMutableArray alloc]init];
        for (ObjLostFound * objLF in objSelectedArea.arrLostFound) {
            NSLog(@"objLf idx: %d and objSelectedtype idx:%d",objLF.intPetType,objSelectedType.idx);
            if (objLF.intPetType == objSelectedType.idx) {
                [arrResultArr addObject:objLF];
            }
            if(objSelectedType.idx == 9999) {
                [arrResultArr addObject:objLF];
            }
        }
        NSLog(@"obj arr result arr %d",[arrResultArr count]);
        [self pushToNextViewWithLostFoundArr:arrResultArr];
    }
}

- (void)pushToNextViewWithLostFoundArr:(NSMutableArray *)arr{
    BrowseLostAndFoundViewController *viewController = [[UIStoryboard storyboardWithName:@"LostAndFound" bundle:nil] instantiateViewControllerWithIdentifier:@"browse"];
    viewController.arrLF = arr;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
