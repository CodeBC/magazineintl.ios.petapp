//
//  AnimateDetailViewController.m
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AnimateDetailViewController.h"
#import "NavBarButton1.h"

@interface AnimateDetailViewController ()
{
    IBOutlet UIImageView * imgView;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIButton * btnClick;
    BOOL isStartAnimate;
    NSTimer *timer;
    int topIndex;
    int prevTopIndex;
    int imageIndex;
}
@end

@implementation AnimateDetailViewController
@synthesize arrImages;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Show Animate";
    self.navigationItem.titleView = lblName;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
        
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    /*CATransition *transition = [CATransition animation];
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [imgView.layer addAnimation:transition forKey:nil];*/
    
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    topIndex = 0;
    prevTopIndex = 1;
    isStartAnimate = FALSE;
    [self loadImageViewWithImages:arrImages];
    
}

- (void)loadImageViewWithImages:(NSMutableArray *)arr{
    imgView.animationImages = arr;
    imgView.animationDuration = 4;
    
    //UIImage * toImage = [UIImage animatedImageWithImages:arr duration:1];
    //[imgView setImage:toImage];
    /*[UIView transitionWithView:imgView
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        imgView.animationImages = arr;
                        //imgView.animationDuration = 1;
                        //imgView.image = toImage;
                    } completion:nil];*/
}

- (IBAction)startAnimation:(id)sender{
    if (!isStartAnimate) {
        //[imgView startAnimating];
        isStartAnimate = TRUE;
        [btnClick setTitle:@"Stop Animate" forState:normal];
        if (timer) {
            [timer invalidate];
        }
        imageIndex=0;
        [imgView setImage:[arrImages objectAtIndex:imageIndex]];
        imageIndex++;
        timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(performTransition) userInfo:nil repeats:YES];
        [timer fire];
    }
    else{
        [imgView stopAnimating];
        isStartAnimate = FALSE;
        [btnClick setTitle:@"Start Animate" forState:normal];
        if (timer) {
            [timer invalidate];
        }
    }

}

//perform image transition
-(void)performTransition
{
    [imgView setImage:[arrImages objectAtIndex:imageIndex]];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.75;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    /*NSString *types[4] = {kCATransitionMoveIn, kCATransitionPush, kCATransitionReveal, kCATransitionFade};
    NSString *subtypes[4] = {kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom};
    int rnd = random() % 4;
    transition.type = types[rnd];
    if(rnd > 3)
    {
        transition.subtype = subtypes[random() % 4];
    }*/
    
    transition.delegate = self;
    
    [imgView.layer addAnimation:transition forKey:nil];
    imageIndex++;
    if(imageIndex == [arrImages count]){
        imageIndex=0;
    }
    
}

-(void)onTimer{
    /*[UIView animateWithDuration:3.0 animations:^{
        imgView.alpha = 0.0;
    }];
    [UIView animateWithDuration:1.0 animations:^{
        imgView.alpha = 1.0;
    }];*/
    [UIView animateWithDuration:1.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        imgView.alpha = !imgView.alpha;
    }completion:^(BOOL done){
        //
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
