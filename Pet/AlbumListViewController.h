//
//  AlbumListViewController.h
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import "ObjNewsFeed.h"
@interface AlbumListViewController : PetBasedViewController
@property (nonatomic, strong) ObjPet * objP;
@property BOOL isFromOther;
@end
