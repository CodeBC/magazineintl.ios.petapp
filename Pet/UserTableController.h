//
//  DemoTableControllerViewController.h
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjUser.h"
@protocol UserTableDelegate
- (void) onUserTableSelected:(ObjUser *)selectedUser;
- (void) onCancel;
@end
@interface UserTableController : UIViewController{
    
  id<UserTableDelegate> owner;
}
@property id<UserTableDelegate> owner;
//@property (nonatomic, strong) IBOutlet UITableView *tableView;
@end
