//
//  ReminderViewController.m
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ReminderViewController.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "MyPetCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ADSlidingViewController.h"
#import "DetailReminderViewController.h"
#import "NavBarButton.h"
#import "NavBarButton2.h"
#import "AddReminderViewController.h"

@interface ReminderViewController (){
    NSMutableArray * arrList;
    IBOutlet UITableView * tbl;
    IBOutlet UIImageView * imgBgView;
    UISwitch * switchDone;
}

@property int selectedType;
@end

@implementation ReminderViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton2 *btnAdd2 = [[NavBarButton2 alloc] init];
	[btnAdd2 addTarget:self action:@selector(rightBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithCustomView:btnAdd2];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = addButton;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    tbl.backgroundColor = [UIColor clearColor];
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Reminder";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTheView) name:@"refreshReminderView" object:nil];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void) refreshTheView{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.db removeExpiredReminder];
    arrList = [delegate.db getAllReminder];
    [self reloadData:YES];
    [delegate clearTheBudgeIcon];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (IBAction) rightBarButton:(UIBarButtonItem *)sender {
    AddReminderViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"addReminder"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    [self refreshTheView];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyPetCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
	/*if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MyPetCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MyPetCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}*/
    
   
    ObjReminder * obj = [arrList objectAtIndex:[indexPath row]];
    cell.textLabel.text = obj.strName;
    cell.backgroundColor = [UIColor clearColor];
    switchDone = [[UISwitch alloc]init];
    cell.accessoryView = switchDone;
    switchDone.tag = indexPath.row;
    /*if (obj.isDone == 1) {
        [switchDone setOn:FALSE];
    }
    else [switchDone setOn:TRUE];
    
    if (obj.isDone == 4) {
        switchDone.enabled = FALSE;
        [switchDone setOn:FALSE];
    }
    else{
        switchDone.enabled = TRUE;
        [switchDone setOn:FALSE];
    }
    
    NSLog(@"name: %@ and Done: %d",obj.strName,obj.isDone);
    
    if(obj.isDone == 99) {
        switchDone.enabled = TRUE;
        [switchDone setOn:FALSE];
    }*/
    if (obj.isSwitch == 1){
         [switchDone setOn:TRUE];
    }
    else{
        [switchDone setOn:FALSE];
    }
    
    if (obj.isDone == 1 || obj.isDone == 4 ) {
        switchDone.enabled = FALSE;
        [switchDone setOn:FALSE];
    }
    else if(obj.isDone == 99){
        switchDone.enabled = TRUE;
    }
    else{
        switchDone.enabled = TRUE;
    }
    
    NSLog(@"done? %d", obj.isDone);
    
    [switchDone addTarget:self action:@selector(onSwitchValueChange:) forControlEvents:UIControlEventValueChanged];
    [self onSwitchValueChange:switchDone];
    
    NSLog(@"first due date %@",obj.strDate);
    
    return cell;
}

- (void) onSwitchValueChange:(UISwitch *)sender{
    NSLog(@"switch index %d and is on %d",sender.tag,sender.isOn);
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjReminder * obj = [arrList objectAtIndex:sender.tag];
    if (sender.isOn) {
        obj.isSwitch = 1;
        [delegate.db updateReminderDone:obj.idx andDone:0 andSwitch:obj.isSwitch];
        obj.isDone = 0;
        [arrList replaceObjectAtIndex:sender.tag withObject:obj];
        [self addLocalReminder:obj];
    }
    else{
        obj.isSwitch = 0;
        [delegate.db updateReminderDone:obj.idx andDone:1 andSwitch:obj.isSwitch];
        obj.isDone = 3;
        [arrList replaceObjectAtIndex:sender.tag withObject:obj];
        [self deleteLocalNotification:obj];
    }
}

- (void) addLocalReminder:(ObjReminder *)obj{
    NSDate * remindDate = [NSDate dateWithTimeIntervalSince1970:obj.reminder_timetick];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                                   fromDate:remindDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
                                                   fromDate:remindDate];
    
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    // Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    switch ([self giveTimeTickforRepeat:obj.strRepeatType]) {
        case 1:
            localNotif.repeatInterval = NSDayCalendarUnit;
            break;
        case 2:
            localNotif.repeatInterval = NSWeekCalendarUnit;
            break;
        case 3:
            localNotif.repeatInterval = NSMonthCalendarUnit;
            break;
        case 4:
            localNotif.repeatInterval = NSYearCalendarUnit;
            break;
        default:
            localNotif.repeatInterval = 0;
            break;
    }
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    localNotif.alertBody = obj.strName;
    // Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    // Specify custom data for the notification
    //NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",obj.idx] forKey:@"ID"];
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d,%@,",obj.idx,obj.strName] forKey:@"ID"];
    localNotif.userInfo = infoDict;
    NSLog(@"notid user info %@",localNotif.userInfo);
    
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (int) giveTimeTickforRepeat:(NSString *)strType{
    
    if ([strType isEqualToString:@"None"]) {
        return 0;
    }
    else if ([strType isEqualToString:@"Every Day"]) {
        return 1;
    }
    else if ([strType isEqualToString:@"Every Week"]) {
        return 2;
    }
    else if ([strType isEqualToString:@"Every Month"]) {
        return 3;
    }
    else if ([strType isEqualToString:@"Every Year"]) {
        return 4;
    }
    
    return -1;
}

- (void) deleteLocalNotification:(ObjReminder *)obj{
    NSString *myIDToCancel = [NSString stringWithFormat:@"%d",obj.idx];
    UILocalNotification *notificationToCancel=nil;
    
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        NSLog(@"noti user info %@",aNotif.userInfo);
        NSArray * commands = nil;
        NSString * notiId = @"";
        NSString * notiName = @"";
        if( [[aNotif.userInfo objectForKey:@"ID"] rangeOfString:@","].location != NSNotFound ){
            
            commands = [[aNotif.userInfo objectForKey:@"ID"] componentsSeparatedByString:@","];
            notiId = [commands objectAtIndex:0];
            notiName = [commands objectAtIndex:1];
        }
        NSLog(@"notid id %@ and obj id %@",notiId,myIDToCancel);
        if([notiId isEqualToString:myIDToCancel]) {
            notificationToCancel=aNotif;
            break;
        }
        
    }
    if (notificationToCancel != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjReminder * objPet = [arrList objectAtIndex:indexPath.row];
    DetailReminderViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"reminderDetail"];
    viewController.objReminder = objPet;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (40*[arrList count]))];
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
}

- (IBAction)onRemove:(id)sender{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

@end