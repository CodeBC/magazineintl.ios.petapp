//
//  EditProfileViewController.h
//  Pet
//
//  Created by Zayar on 4/29/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjUser.h"
#import <TapkuLibrary/TapkuLibrary.h>
@protocol EditProfileViewControllerDelegate <NSObject>

@required
-(void)completedWithViewController:(UIViewController *)viewcontroller;
-(void)dismissWithViewController:(UIViewController *)viewcontroller;
@end

@interface EditProfileViewController : PetBasedViewController<UIImagePickerControllerDelegate>
@property (nonatomic,assign) id<EditProfileViewControllerDelegate> delegate;
@property (nonatomic, strong) ObjUser * user;
- (IBAction)onClose:(id)sender;
-(IBAction)onLevel:(id)sender;
- (IBAction) showPhotoLibrary:(id) sender;
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
-(IBAction)onRegister:(id)sender;
@property (nonatomic,strong) NSArray *cells;
@property (nonatomic,strong) UITableViewCell *typeCell;
@property (nonatomic,strong) UITableViewCell *btnDateCell;
@property (nonatomic,strong) UITableViewCell *btnBreedCell;
@property (nonatomic,strong) UITableViewCell *btnGenderCell;
@property (nonatomic,strong) UITableViewCell *btnCountryCell;
@property (nonatomic,strong) UITableViewCell *btnChangePasswordCell;
@property (nonatomic,strong) UITableViewCell *btnPrivacyCell;
- (IBAction)hideKeyboard:(id)sender;
@end
