//
//  AdoptionDetailViewController.m
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AdoptionDetailViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "petAPIClient.h"
#import "ObjAdoption.h"
#import "UIImageView+AFNetworking.h"
#import "PetAppDelegate.h"
@interface AdoptionDetailViewController ()
{
    IBOutlet UIImageView * imgBgView;
    
    IBOutlet UIImageView * imgProfileView;
    IBOutlet UILabel * lblName;
    IBOutlet UILabel * lblDob;
    IBOutlet UILabel * lblInfo;
    
    TKLabelFieldCell * lblDescripCell;
    TKLabelFieldCell * lblGenderCell;
    TKLabelFieldCell * lblBreedCell;
    TKLabelFieldCell * lblDobCell;
    TKLabelFieldCell * lblOrganizationCell;
    TKLabelFieldCell * lblTypeCell;
    IBOutlet UITableView * tbl;
}
@property (nonatomic,strong) NSArray *cells;
@end

@implementation AdoptionDetailViewController
@synthesize objAdo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    /*UILabel * lblName1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName1.backgroundColor = [UIColor clearColor];
    lblName1.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName1.font = [UIFont fontWithName:strValueFontName size:19];
    lblName1.text= @"Adoptions";
    lblName1.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName1;*/
    
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    if(objAdo.isInterested) {
        self.interestedButton.hidden = YES;
    }
    [self loadTheRequiredCell];
}

- (void) loadTheRequiredCell{
    
    lblTypeCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblTypeCell.label.text = @"Type:";
    lblTypeCell.label.textColor = [UIColor blackColor];
    lblTypeCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblTypeCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblTypeCell.field.text = @"";
    lblTypeCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblDescripCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblDescripCell.label.text = @"Information:";
    lblDescripCell.label.textColor = [UIColor blackColor];
    lblDescripCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblDescripCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblDescripCell.field.text = @"";
    lblDescripCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblGenderCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblGenderCell.label.text = @"Gender:";
    lblGenderCell.label.textColor = [UIColor blackColor];
    lblGenderCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblGenderCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblGenderCell.field.text = @"";
    lblGenderCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblBreedCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblBreedCell.label.text = @"Breed:";
    lblBreedCell.label.textColor = [UIColor blackColor];
    lblBreedCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblBreedCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblBreedCell.field.text = @"";
    lblBreedCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblDobCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblDobCell.label.text = @"Dob:";
    lblDobCell.label.textColor = [UIColor blackColor];
    lblDobCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblDobCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblDobCell.field.text = @"";
    lblDobCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblOrganizationCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblOrganizationCell.label.text = @"Organization:";
    lblOrganizationCell.label.textColor = [UIColor blackColor];
    lblOrganizationCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblOrganizationCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblOrganizationCell.field.text = @"";
    lblOrganizationCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cells = @[lblTypeCell,lblGenderCell,lblDescripCell,lblBreedCell,lblDobCell,lblOrganizationCell];
    //tbl.frame = CGRectMake(tbl.frame.origin.x, tbl.frame.origin.y, 320, [self.cells count]*44);
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    //[tbl reloadData];
    [self loadTheViewWith:objAdo];
}

/*- (void) loadTheViewWith:(ObjAdoption *)obj{
    [imgProfileView setImageWithURL:[NSURL URLWithString:obj.strPetImgLink] placeholderImage:nil];
    lblName.text = obj.strName;
    lblDob.text = obj.strPetDob;
    lblInfo.text = obj.strInfo;
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadTheViewWith:(ObjAdoption *)obj{
    lblDescripCell.field.text = obj.strInfo;
    lblBreedCell.field.text = [NSString stringWithFormat:@"%d",obj.intBreedType];
    lblGenderCell.field.text = obj.strPetGender;
    lblTypeCell.field.text = obj.strPetType;
    lblDobCell.field.text = obj.strPetDob;
    lblOrganizationCell.field.text =  obj.strOrganization;
    //lblRewardCell.field.text = [NSString stringWithFormat:@"%.2f",obj.reward];
    [imgProfileView setImageWithURL:[NSURL URLWithString:obj.strPetImgLink] placeholderImage:nil];
    //self.navigationItem.title = obj.strPetName;
    if (lblName == nil) {
        lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    }
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text=  obj.strName;
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    NSLog(@"navigation title %@",obj.strName);
    [self.view bringSubviewToFront:self.interestedButton];
}

- (IBAction) notifyInterested:(id) sender
{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"adoption[adoption_id]":[NSString stringWithFormat:@"%d",objAdo.idx]};
    [SVProgressHUD show];
    
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",ADOPTIONS_INTERESTED,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:@"Notified"];
            self.interestedButton.hidden = YES;
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];

}

- (IBAction)onAbuse:(id)sender{
    [SVProgressHUD showSuccessWithStatus:@"Reported"];
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
