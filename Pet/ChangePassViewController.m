//
//  ChangePassViewController.m
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ChangePassViewController.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "PetAppDelegate.h"

//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface ChangePassViewController ()
{
    IBOutlet UIToolbar *keyboardToolbar;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    
    TKLabelTextFieldCell *cell3;
    TKLabelTextFieldCell *oldPasswordCell;
    TKLabelTextFieldCell *newPasswordCell;
    TKLabelTextFieldCell *confirmPasswordCell;
}

@end

@implementation ChangePassViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton4 *btnEdit = [[NavBarButton4 alloc] init];
	[btnEdit addTarget:self action:@selector(onChange:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = editButton;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Change Password";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    [self loadTheRequiredCell];
}

- (void)viewWillAppear:(BOOL)animated{
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)hideKeyboard:(id)sender {
	//[self ];
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void) loadTheRequiredCell{
    cell3 = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell3.label.text = @"First Name:";
    cell3.label.textAlignment = UITextAlignmentLeft;
    cell3.label.font = [UIFont boldSystemFontOfSize:12];
    cell3.label.textColor = [UIColor blackColor];
    
    cell3.label.frame = CGRectMake(30 + cell3.label.frame.origin.x, cell3.label.frame.origin.y, cell3.frame.size.width+10, cell3.frame.size.height);
	cell3.field.text = @"";
    [cell3.field setSecureTextEntry:TRUE];
    cell3.field.tag = 0;
    
    oldPasswordCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	oldPasswordCell.label.text = @"Current Password:";
    oldPasswordCell.label.textAlignment = UITextAlignmentLeft;
    oldPasswordCell.label.font = [UIFont boldSystemFontOfSize:12];
    oldPasswordCell.label.textColor = [UIColor blackColor];
    
    oldPasswordCell.label.frame = CGRectMake(30 + oldPasswordCell.label.frame.origin.x, oldPasswordCell.label.frame.origin.y, oldPasswordCell.frame.size.width+10, cell3.frame.size.height);
	oldPasswordCell.field.text = @"";
    oldPasswordCell.field.returnKeyType = UIReturnKeyNext;
    [oldPasswordCell.field setSecureTextEntry:TRUE];
    oldPasswordCell.field.tag = 1;
    oldPasswordCell.field.delegate = self;
    
    newPasswordCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	newPasswordCell.label.text = @"New Password:";
    newPasswordCell.label.textAlignment = UITextAlignmentLeft;
    newPasswordCell.label.font = [UIFont boldSystemFontOfSize:12];
    newPasswordCell.label.textColor = [UIColor blackColor];
    
    newPasswordCell.label.frame = CGRectMake(30 + newPasswordCell.label.frame.origin.x, newPasswordCell.label.frame.origin.y, newPasswordCell.frame.size.width+10, cell3.frame.size.height);
	newPasswordCell.field.text = @"";
    [newPasswordCell.field setSecureTextEntry:TRUE];
    newPasswordCell.field.returnKeyType = UIReturnKeyNext;
    newPasswordCell.field.tag = 2;
    newPasswordCell.field.delegate = self;
    
    confirmPasswordCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	confirmPasswordCell.label.text = @"Confirm Password:";
    confirmPasswordCell.label.textAlignment = UITextAlignmentLeft;
    confirmPasswordCell.label.font = [UIFont boldSystemFontOfSize:12];
    confirmPasswordCell.label.textColor = [UIColor blackColor];
    
    confirmPasswordCell.label.frame = CGRectMake(30 + confirmPasswordCell.label.frame.origin.x, confirmPasswordCell.label.frame.origin.y, confirmPasswordCell.frame.size.width+10, confirmPasswordCell.frame.size.height);
	confirmPasswordCell.field.text = @"";
    [confirmPasswordCell.field setSecureTextEntry:TRUE];
    confirmPasswordCell.field.tag = 3;
    confirmPasswordCell.field.delegate = self;
    
    self.cells = @[oldPasswordCell,newPasswordCell,confirmPasswordCell];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onChange:(id)sender{
    if ([[oldPasswordCell.field.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"Current password cannot be blank."];
        return;
    }
    if ([[newPasswordCell.field.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        [self textValidateAlertShow:@"New password cannot be blank."];
        return;
    }
    
    if (![newPasswordCell.field.text isEqualToString:confirmPasswordCell.field.text]) {
        [self textValidateAlertShow:@"Confirm password does not match"];
        return;
    }
    
    NSLog(@"change pass!");
    [self syncUserPassEdit:oldPasswordCell.field.text and:newPasswordCell.field.text];
}

- (void)syncUserPassEdit:(NSString *)strCurrentPass and:(NSString *)strNewPass{
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    //@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@"
    NSDictionary* params = @{@"user[current_password]":strCurrentPass,@"user[password]":strNewPass};
    
    //,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[phone]":obj.strPhone,@"user[address]":obj.strAddress,@"user[dob]":obj.strDob
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",USER_CHANGE_PASS_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    //[self moveScrollView:textFieldView];
    //UITableViewCell *cell = (UITableViewCell *)[textFieldView superview];
    //NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    //NSIndexPath *myIP = [NSIndexPath indexPathForRow:textFieldView.tag inSection:0];
    //NSLog(@"cell tag %d",textFieldView.tag);
    
    //[tbl scrollToRowAtIndexPath:myIP atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    //[UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    //[UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        [newPasswordCell.field becomeFirstResponder];
        //return NO;
    }
    if(textField.tag == 2) {
        [confirmPasswordCell.field becomeFirstResponder];
        //return NO;
    }
    //[textField resignFirstResponder];
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *) textView{
    //[self moveScrollView:textView];
    //TKLabelTextFieldCell *cell = (TKLabelTextFieldCell *)[textView superview];
    //NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    //[tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void)textViewDidEndEditing:(UITextView *) textFieldView {
    
    //[UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    //[UIView commitAnimations];
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
    
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        // [textView resignFirstResponder];
        return YES;
    }else{
        return YES;
    }
}

//keyboard appear
//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height - 260.0;
	keyboardToolbar.frame = frame;
	
	[UIView commitAnimations];
    
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height + keyboardToolbar.frame.size.height+60, 0);
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height;
	keyboardToolbar.frame = frame;
    
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
	
	[UIView commitAnimations];
}

@end
