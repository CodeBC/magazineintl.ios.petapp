//
//  CustomNewsFeedView.m
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "CustomNewsFeedView.h"
#import "ObjNewsFeed.h"
#import "UIImageView+AFNetworking.h"
#import "TSActionSheet.h"
#import "TSPopoverController.h"
#import "Utility.h"
@implementation CustomNewsFeedView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) loadTheView{
    [Utility makeBorder:lblSmallBg andWidth:1 andColor:[UIColor colorWithHexString:@"cbcbcb"]];
    //[Utility makeBorder:self andWidth:1 andColor:[UIColor darkGrayColor]];
}

-(void) loadTheViewWith:(ObjNewsFeed *)objNewsFeed{
    objNF = objNewsFeed;
    [imgPetView setImageWithURL:[NSURL URLWithString:objNewsFeed.objPet.strImgLink] placeholderImage:nil];
    [imgUserView setImageWithURL:[NSURL URLWithString:objNewsFeed.objUser.strProfileImgLink] placeholderImage:nil];
    NSString * strNormalFont=@"Agfa Rotis Sans Serif";
    NSString * strValueFontName=@"SerifaStd-Black";
    lblUser.font = [UIFont fontWithName:strValueFontName size:14];
    lblPetName.font = [UIFont fontWithName:strValueFontName size:14];
    lblPetType.font = [UIFont fontWithName:strNormalFont size:14];
    lblDob.font = [UIFont fontWithName:strNormalFont size:14];
    lblUser.text = [NSString stringWithFormat:@"%@ has added new pet: %@",objNewsFeed.objUser.strFName,objNewsFeed.objPet.strName];
    
    lblPetName.text = objNewsFeed.objPet.strName;
    
    lblPetType.text = [NSString stringWithFormat:@"Breed: %@",objNewsFeed.objPet.strBreed];
    lblDob.text = [NSString stringWithFormat:@"%@",objNewsFeed.objPet.strDob];
    
    imgUserView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgUserView.layer.borderWidth = 3.0;
    
    imgPetView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgPetView.layer.borderWidth = 3.0;
}

- (void)setThemeGrayAndWhite:(BOOL)isGray{
    if (isGray) {
        [self setBackgroundColor:[UIColor colorWithHexString:@"e4e4e4"]];
        [lblSmallBg setBackgroundColor:[UIColor colorWithHexString:@"e4e4e4"]];
    }
    else{
        [self setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
        [lblSmallBg setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    }
}

- (IBAction)onPetSelected:(id)sender{
    [owner onPetSelected:objNF.objPet andNewsFeed:objNF];
}

- (IBAction)onUserSelected:(id)sender{
    [owner onUserSelected:objNF.objUser];
}

- (IBAction)onMenu:(UIButton *)sender {
    [owner onMenuSelectedPet:objNF.objPet andButton:sender];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
@end
