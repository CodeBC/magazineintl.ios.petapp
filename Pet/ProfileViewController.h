//
//  ProfileViewController.h
//  Pet
//
//  Created by Zayar on 4/29/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditProfileViewController.h"
#import "WebImageOperations.h"

@interface ProfileViewController : PetBasedViewController<EditProfileViewControllerDelegate>
- (IBAction) rightBarButton:(UIBarButtonItem *)sender;
- (void) onNeibourhoodClick:(id) sender;
@property BOOL isFromOther;
@property (nonatomic,strong) ObjUser * user;
- (IBAction) onNeibourhoodClick:(id) sender;
- (IBAction)onProfilePic:(id)sender;
@end
