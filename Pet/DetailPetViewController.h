//
//  DetailPetViewController.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import "ObjNewsFeed.h"
@interface DetailPetViewController : PetBasedViewController
@property (nonatomic,retain) ObjPet * objPet;
@property (nonatomic,retain) ObjNewsFeed * objNewsFeed;
- (IBAction)onAlbumSelected:(id)sender;
@property BOOL isFromOther;
- (void) syncDetailPetWithId:(int)idx;
@end
