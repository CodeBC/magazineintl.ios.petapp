//
//  ObjUser.h
//  zBox
//
//  Created by Zayar Cn on 6/10/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjUser : NSObject
{
    int idx;
    int userId;
    NSString * strFName;
    NSString * strLName;
    NSString * strGender;
    NSString * strName;
    NSString * strPassword;
    NSString * strSession;
    NSString * strNewPassword;
    NSString * strEmail;
    NSString * strProfileImgLink;
    NSString * strFbLink;
    NSString * strWebLink;
    NSString * strPhone;
    NSString * strAddress;
    NSString * strDob;
    NSString * strUDID;
    NSString * strFbID;
    NSString * strDescription;
    NSString * strDistance;
    NSString * strCountry;
    NSString * strPostalCode;
    NSMutableArray * arrPets;
    NSMutableArray * arrNeigbours;
    NSMutableArray * arrNeigbourPostal;
    int intActive;
    int hasLocationProvided;
    int isFBLogin;
    int neighbor_type;
    int privacy_id;
}
@property int idx;
@property int userId;
@property int hasLocationProvided;
@property int neighbor_type;
@property int privacy_id;
@property (nonatomic,retain) NSString * strName;
@property (nonatomic,retain) NSString * strPassword;
@property (nonatomic,retain) NSString * strSession;
@property (nonatomic,retain) NSString * strNewPassword;
@property (nonatomic,retain) NSString * strFName;
@property (nonatomic,retain) NSString * strLName;
@property (nonatomic,retain) NSString * strEmail;
@property (nonatomic,retain) NSString * strGender;
@property (nonatomic,retain) NSString * strProfileImgLink;
@property (nonatomic,retain) NSString * strFbLink;
@property (nonatomic,retain) NSString * strWebLink;
@property (nonatomic,retain) NSString * strPhone;
@property (nonatomic,retain) NSString * strAddress;
@property (nonatomic,retain) NSString * strDob;
@property (nonatomic,retain) NSString * strUDID;
@property (nonatomic,retain) NSString * strFbID;
@property (nonatomic,retain) NSString * strCountry;
@property (nonatomic,retain) NSString * strPostalCode;
@property (nonatomic,retain) NSString * strDistance;
@property (nonatomic,retain) NSMutableArray * arrPets;
@property (nonatomic,retain) NSMutableArray * arrNeigbours;
@property (nonatomic,retain) NSMutableArray * arrNeigbourPostal;
@property int intActive;
@property int isFBLogin;
@property (nonatomic,retain) NSString * strDescription;

@end
