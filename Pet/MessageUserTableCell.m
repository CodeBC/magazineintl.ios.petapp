//
//  MessageUserTableCell.m
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MessageUserTableCell.h"
#import "ObjUser.h" 
#import "UIImageView+AFNetworking.h"

@implementation MessageUserTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadTheViewWith:(ObjUser *)objUser{
    lblName.text = objUser.strFName;
    [imgThumbView setImageWithURL:[NSURL URLWithString:objUser.strProfileImgLink] placeholderImage:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
