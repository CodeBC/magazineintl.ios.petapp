//
//  CustomPetsBarView.h
//  Pet
//
//  Created by Zayar on 6/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import "ObjUser.h"
@protocol ThumbScrollBarDelegate
- (void) onThumbScrollBarSelected:(ObjPet *)selectedObjPet;
- (void) onThumbScrollUserBarSelected:(ObjUser *)selectedObjUser;
- (void) onThumbScrollBarIConClick:(ObjPet *)selectedObjPet;
- (void) onHideClick;
@end
@interface CustomPetsBarView : UIView
{
    IBOutlet UIScrollView * scroll;
    id<ThumbScrollBarDelegate> owner;
    int previousIndex;
    int intBarType;
}

@property id<ThumbScrollBarDelegate> owner;
@property int intBarType;
- (void) thumbScrollViewCleanUp;
- (void)loadThumbScrollBarWith:(NSMutableArray *)arrPets;
- (void)loadThumbScrollBarWithUser:(NSMutableArray *)arrUser;
@end
