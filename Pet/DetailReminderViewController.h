//
//  DetailReminderViewController.h
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjReminder.h"
@interface DetailReminderViewController : PetBasedViewController
@property (nonatomic, retain) ObjReminder * objReminder;
-(IBAction)onAdd:(id)sender;
-(IBAction)onDelete:(id)sender;
@end
