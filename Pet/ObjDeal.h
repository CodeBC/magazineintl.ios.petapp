//
//  ObjDeal.h
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjDeal : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strInfo;
@property (nonatomic, strong) NSString * strDescription;
@property (nonatomic, strong) NSString * strImgLink;
@property (nonatomic, strong) NSString * strPrice;
@end
