//
//  MagazineTableCell.m
//  Pet
//
//  Created by Zayar on 6/11/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MagazineTableCell.h"

@implementation MagazineTableCell
@synthesize owner;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadTheCellWith:(ObjMagazine *)obj{
    //btnDownload.hidden = TRUE;
    //btnSubscribe.hidden = TRUE;
    objSelectedMaga = obj;
    lblName.text = obj.strName;
}

- (IBAction)onSubscribe:(id)sender{
    [owner onScribeWith:objSelectedMaga];
}

- (IBAction)onDownload:(id)sender{
    [owner onDownloadWith:objSelectedMaga];
}

@end
