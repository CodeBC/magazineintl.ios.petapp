//
//  DBManager.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "DBManager.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjGKey.h"
#import "ObjUser.h"
#import "ObjReminder.h"

@implementation DBManager

- (void) checkAndCreateDatabase{
    PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString * databasePath = [documentsDir stringByAppendingPathComponent: DBNAME];
	
	NSLog(@"checking at %@", databasePath);
	BOOL success;
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
	
	// If the database already exists then return without doing anything
	if(success){
		NSLog(@"db found");
		
		delegate.databasePath = databasePath;
		[delegate.databasePath retain];
        
		return;
	}
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBNAME];
	
	NSLog(@"copying db...");
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];	
	[fileManager release];
	
	delegate.databasePath = databasePath;
	[delegate.databasePath retain];
	
	NSLog(@"db transfered!");
}

/**** Cached Image DbMethod ****/
- (NSInteger) insertCachedImage:(NSString *)url path:(NSString *) filePath{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	NSInteger result;
	BOOL opSuccessful = FALSE;
	
	sqlite3 *database;
	sqlite3_stmt *insert_statement;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		static char *sql = "INSERT INTO cached_images (url,filePath,downloadTime) VALUES(?,?,?)";
		if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_double(insert_statement, 3, [[NSDate date] timeIntervalSince1970]);
		sqlite3_bind_text(insert_statement, 2, [filePath UTF8String], -1, SQLITE_TRANSIENT); 
		sqlite3_bind_text(insert_statement, 1, [url UTF8String], -1, SQLITE_TRANSIENT); 
		
		
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"image inserted!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		}
		
		sqlite3_finalize(insert_statement);      
	}
	
	if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
}

- (NSString *) getFilePath:(NSString *) url{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSString * strPath = @"";
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT filePath FROM cached_images WHERE url = ?";
		sqlite3_stmt *compiledStatement;
		
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_bind_text(compiledStatement, 1, [url UTF8String], -1, SQLITE_TRANSIENT);
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				strPath = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
				//NSLog(@"file path %@", strPath);
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return strPath;
}

- (NSMutableArray *) getExpiredImages:(double) limit{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSMutableArray * list = [[NSMutableArray alloc] init];
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT filePath FROM cached_images WHERE downloadTime < ?";
		sqlite3_stmt *compiledStatement;
		
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(compiledStatement, 1, limit);
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				NSString *aCat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
				[list addObject:aCat];
				[aCat release];
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return list;
}

- (void) removeCahcedImage{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	double expiredLimit = [[NSDate date] timeIntervalSince1970]- CACHE_DURATION;
	
	NSMutableArray * files = [self getExpiredImages: expiredLimit];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	for(NSString * f in files){
		if( f != nil && (NSNull *) f != [NSNull null] && ![f isEqualToString:@""] )
			//NSLog(@"Removing cached: %@", f);
			[fileManager removeItemAtPath:f error:NULL];
	}
	
	sqlite3 *database;
	sqlite3_stmt * delete_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "DELETE FROM cached_images WHERE downloadTime < ?";
		if (sqlite3_prepare_v2(database, sql, -1, &delete_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_double(delete_statment, 1, expiredLimit);
		int success = sqlite3_step(delete_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_reset(delete_statment);
	}
	
	sqlite3_close(database);
}

/**** End Cached Image DbMethod ****/

-(NSInteger) checkUserIfActive{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSInteger total=0;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT COUNT(*) FROM tbl_user WHERE active = 1";
        
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				total = sqlite3_column_int(compiledStatement, 0);
			}
		}
		
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
	return total;
}

- (ObjUser *) getUserObj{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	ObjUser * objUser = nil;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		
		const char *sqlStatement = "SELECT * FROM tbl_user WHERE idx = 1";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSLog(@"found...");
                
				objUser = [[[ObjUser alloc] init] autorelease];
				objUser.idx = sqlite3_column_int(compiledStatement, 0);
                
                objUser.strName = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 1) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 1) != [NSNull null]){
					objUser.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				}
                
				objUser.strPassword = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 2) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 2) != [NSNull null]){
					objUser.strPassword = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
				}
                
                objUser.strSession = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
					objUser.strSession = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
				}
                
                objUser.intActive = sqlite3_column_int(compiledStatement, 4);
                
                objUser.strFName = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 5) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 5) != [NSNull null]){
					objUser.strFName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
				}
                
                objUser.strLName = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 6) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 6) != [NSNull null]){
					objUser.strLName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
				}
                
                objUser.strEmail = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 7) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 7) != [NSNull null]){
					objUser.strEmail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
				}
                
                objUser.strGender = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 8) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 8) != [NSNull null]){
					objUser.strGender = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
				}
                
                objUser.userId = sqlite3_column_int(compiledStatement, 9);
                
                objUser.isFBLogin = sqlite3_column_int(compiledStatement, 10);
                
                objUser.strProfileImgLink = @"";
				if( (char *)sqlite3_column_text(compiledStatement, 11) != nil && (NSNull *)sqlite3_column_text(compiledStatement,11) != [NSNull null]){
					objUser.strProfileImgLink = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement,11)];
				}
			}
		}
		
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return objUser;
}

- (void) updateUser:(ObjUser *)objUser{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_user SET name = ?,session_key = ?,password = ?,l_name = ?,s_name=?,email=?,gender=?,user_id=?,is_fblogin=?,profile_link = ? WHERE idx = 1";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_text(update_statment, 1, [objUser.strName UTF8String], -1, SQLITE_TRANSIENT);
		
		sqlite3_bind_text(update_statment, 2, [objUser.strSession UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 3, [objUser.strPassword UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 4, [objUser.strFName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 5, [objUser.strLName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 6, [objUser.strEmail UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 7, [objUser.strGender UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(update_statment, 8, objUser.userId);
        
        sqlite3_bind_int(update_statment, 9, objUser.isFBLogin);
        
        sqlite3_bind_text(update_statment, 10, [objUser.strProfileImgLink UTF8String], -1, SQLITE_TRANSIENT);
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (void) updateUserPassword:(NSString *)strPass{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_user SET password = ? WHERE idx = 1";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_text(update_statment, 1, [strPass UTF8String], -1, SQLITE_TRANSIENT);
		
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (NSInteger) insertReminder:(ObjReminder *)objReminder{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	NSInteger result;
	BOOL opSuccessful = FALSE;
	
	sqlite3 *database;
	sqlite3_stmt *insert_statement;
    NSLog(@"Opening database");	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		static char *sql = "INSERT INTO tbl_reminder (is_done,name,repeat,date,remind_date,timetick,date_timetick,remind_timetick,is_switch) VALUES(?,?,?,?,?,?,?,?,?)";
        NSLog(@"Adding remidner");
		if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(insert_statement, 1, objReminder.isDone);
		sqlite3_bind_text(insert_statement, 2, [objReminder.strName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(insert_statement, 3, [objReminder.strRepeatType UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insert_statement, 4, [objReminder.strDate UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(insert_statement, 5, [objReminder.strReminderDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(insert_statement, 6, objReminder.timetick);
        sqlite3_bind_int(insert_statement, 7, objReminder.date_timetick);
        sqlite3_bind_int(insert_statement, 8, objReminder.reminder_timetick);
        sqlite3_bind_int(insert_statement, 9, objReminder.isSwitch);
		
		int success = sqlite3_step(insert_statement);
		
		sqlite3_reset(insert_statement);
		if (success != SQLITE_ERROR) {
			NSLog(@"Reminder inserted!");
			result = sqlite3_last_insert_rowid(database);
			opSuccessful = TRUE;
		} else {
            NSLog(@"Reminder error!");
        }
		
		sqlite3_finalize(insert_statement);
	}
	
	if( opSuccessful ){
		sqlite3_close(database);
		
		return result;
	}
	
	NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
	sqlite3_close(database);
	
	return -1;
}

- (void) updateReminder:(ObjReminder *)objReminder{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	NSLog(@"update reminder is done %d",objReminder.isDone);
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_reminder SET is_done = ?,name=?,repeat=?,date=?,remind_date =?,timetick=?,date_timetick=?,is_switch=? WHERE idx = ?";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_int(update_statment, 1, objReminder.isDone);
		sqlite3_bind_text(update_statment, 2, [objReminder.strName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(update_statment, 3, [objReminder.strRepeatType UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(update_statment, 4, [objReminder.strDate UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(update_statment, 5, [objReminder.strReminderDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(update_statment, 6, objReminder.timetick);
        sqlite3_bind_int(update_statment, 7, objReminder.date_timetick);
        sqlite3_bind_int(update_statment, 8, objReminder.isSwitch);
        
		sqlite3_bind_int(update_statment, 9, objReminder.idx);
        
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (void) deleteReminder:(ObjReminder *)objReminder{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * delete_statment;
	NSLog(@"reminder for %d ",objReminder.idx);
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "DELETE FROM tbl_reminder WHERE idx = ?";
		if (sqlite3_prepare_v2(database, sql, -1, &delete_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_double(delete_statment, 1, objReminder.idx);
		int success = sqlite3_step(delete_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
		
		sqlite3_reset(delete_statment);
	}
	
	sqlite3_close(database);
}

- (void) updateReminderDone:(int)idx andDone:(int)done andSwitch:(int)isSwitch{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    sqlite3 *database;
	sqlite3_stmt * update_statment;
	
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sql = "UPDATE tbl_reminder SET is_done = ?,is_switch = ? WHERE idx = ?";
		if (sqlite3_prepare_v2(database, sql, -1, &update_statment, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
		}
        sqlite3_bind_int(update_statment, 1, done);
        sqlite3_bind_int(update_statment, 2, isSwitch);
		sqlite3_bind_int(update_statment, 3, idx);
        int success = sqlite3_step(update_statment);
		
		if (success != SQLITE_DONE) {
			NSAssert1(0, @"Error: failed to save priority with message '%s'.", sqlite3_errmsg(database));
		}
        //NSLog(@"update statement for this %s ",update_statment);
        sqlite3_reset(update_statment);
	}
	
	sqlite3_close(database);
}

- (NSMutableArray *) getAllReminder{
    
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSMutableArray * list = [[NSMutableArray alloc] init];
	
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "SELECT * FROM tbl_reminder";
        //const char *sqlStatement = "SELECT * FROM tbl_reminder";
        
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            //sqlite3_bind_int(compiledStatement, 1, catId);
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                ObjReminder * objRemind = [[ObjReminder alloc] init];
                
                objRemind.idx = sqlite3_column_int(compiledStatement, 0);
                
                objRemind.isDone = sqlite3_column_int(compiledStatement, 1);
                
                objRemind.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 2) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 2) != [NSNull null]){
                    objRemind.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
                
                objRemind.strRepeatType = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
                    objRemind.strRepeatType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                }
                
                objRemind.strDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 4) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 4) != [NSNull null]){
                    objRemind.strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                }
                
                objRemind.strReminderDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 5) != [NSNull null]){
                    objRemind.strReminderDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objRemind.timetick = sqlite3_column_int(compiledStatement, 6);
                
                objRemind.date_timetick = sqlite3_column_int(compiledStatement, 7);
                
                objRemind.reminder_timetick = sqlite3_column_int(compiledStatement, 8);
                
                objRemind.isSwitch = sqlite3_column_int(compiledStatement, 9);
                
                [list addObject: objRemind];
                [objRemind release];
            }
        }
        
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return list;
}

- (ObjReminder *) getReminderById:(int)idx{
    
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	ObjReminder * objRemind;
	
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "SELECT * FROM tbl_reminder WHERE idx = ?";
        
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            sqlite3_bind_int(compiledStatement, 1, idx);
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                objRemind = [[ObjReminder alloc] init];
                
                objRemind.idx = sqlite3_column_int(compiledStatement, 0);
                
                objRemind.isDone = sqlite3_column_int(compiledStatement, 1);
                
                objRemind.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 2) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 2) != [NSNull null]){
                    objRemind.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
                
                objRemind.strRepeatType = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
                    objRemind.strRepeatType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                }
                
                objRemind.strDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 4) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 4) != [NSNull null]){
                    objRemind.strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                }
                
                objRemind.strReminderDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 5) != [NSNull null]){
                    objRemind.strReminderDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objRemind.timetick = sqlite3_column_int(compiledStatement, 6);
                
                objRemind.date_timetick = sqlite3_column_int(compiledStatement, 7);
                
                objRemind.reminder_timetick = sqlite3_column_int(compiledStatement, 8);
                
                objRemind.isSwitch = sqlite3_column_int(compiledStatement, 9);
            }
        }
        
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return objRemind;
}

- (ObjReminder *) getLastReminder{
    
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	ObjReminder * objRemind;
	
    if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatement = "SELECT MAX(idx),name FROM tbl_reminder";
        //SELECT MAX(idx),name, district_Manager_Name, mr_id, district_Manager_Code,unique_ID FROM tbl_user
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                objRemind = [[ObjReminder alloc] init];
                
                objRemind.idx = sqlite3_column_int(compiledStatement, 0);
                
        
                objRemind.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 1) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 1) != [NSNull null]){
                    objRemind.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                }
                
                objRemind.strRepeatType = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
                    objRemind.strRepeatType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                }
                
                objRemind.strDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 4) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 4) != [NSNull null]){
                    objRemind.strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                }
                
                objRemind.strReminderDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 5) != [NSNull null]){
                    objRemind.strReminderDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objRemind.timetick = sqlite3_column_int(compiledStatement, 6);
                
                objRemind.date_timetick = sqlite3_column_int(compiledStatement, 7);
                
                objRemind.reminder_timetick = sqlite3_column_int(compiledStatement, 8);
                
                objRemind.isSwitch = sqlite3_column_int(compiledStatement, 9);
                
            }
        }
        
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return objRemind;
}

- (NSMutableArray *) getExpiredReminder{
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	sqlite3 *database;
	NSMutableArray * list = [[NSMutableArray alloc] init];
	double limit = [[NSDate date] timeIntervalSince1970];
    NSLog(@"limit %f",limit);
	if(sqlite3_open([delegate.databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT * FROM tbl_reminder WHERE date_timetick < ?";
		sqlite3_stmt *compiledStatement;
		
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_bind_double(compiledStatement, 1, limit);
			
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                ObjReminder * objRemind = [[ObjReminder alloc] init];
                
                objRemind.idx = sqlite3_column_int(compiledStatement, 0);
                
                objRemind.isDone = sqlite3_column_int(compiledStatement, 1);
                
                objRemind.strName = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 2) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 2) != [NSNull null]){
                    objRemind.strName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
                
                objRemind.strRepeatType = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 3) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 3) != [NSNull null]){
                    objRemind.strRepeatType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                }
                
                objRemind.strDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 4) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 4) != [NSNull null]){
                    objRemind.strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                }
                
                objRemind.strReminderDate = @"";
                if( (char *)sqlite3_column_text(compiledStatement, 5) != nil && (NSNull *)sqlite3_column_text(compiledStatement, 5) != [NSNull null]){
                    objRemind.strReminderDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                }
                
                objRemind.timetick = sqlite3_column_int(compiledStatement, 6);
                
                objRemind.date_timetick = sqlite3_column_int(compiledStatement, 7);
                
                objRemind.reminder_timetick = sqlite3_column_int(compiledStatement, 8);
                
                objRemind.isSwitch = sqlite3_column_int(compiledStatement, 9);
                
                [list addObject: objRemind];
                [objRemind release];
            }
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
	return list;
}

- (void) removeExpiredReminder{
	NSMutableArray * reminderFiles = [self getExpiredReminder];
    NSLog(@"reminder file count %d",[reminderFiles count]);
	for(ObjReminder * objReminder in reminderFiles){
        //if (objReminder.isDone != 99) {
            objReminder.isDone = 1;
            [self updateReminder:objReminder];
            [self deleteLocalNotification:objReminder];
        //}
	}
}

- (void)deleteLocalNotification:(ObjReminder *)obj{
    NSString *myIDToCancel = [NSString stringWithFormat:@"%d",obj.idx];
    UILocalNotification *notificationToCancel=nil;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        NSArray * commands = nil;
        NSString * notiId = @"";
        NSString * notiName = @"";
        if( [[aNotif.userInfo objectForKey:@"ID"] rangeOfString:@","].location != NSNotFound ){
            
            commands = [[aNotif.userInfo objectForKey:@"ID"] componentsSeparatedByString:@","];
            notiId = [commands objectAtIndex:0];
            notiName = [commands objectAtIndex:1];
        }
        if([notiId isEqualToString:myIDToCancel]) {
            notificationToCancel=aNotif;
            break;
        }
    }
    if (notificationToCancel != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
    }
}

@end