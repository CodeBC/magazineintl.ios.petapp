//
//  AddPetViewController.m
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "EditPetViewController.h"
#import "SOAPRequest.h"
#import "ObjPet.h"
#import "Utility.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "NavBarButton1.h"
#import "NavBarButton3.h"
#import "petAPIClient.h"
#import "UIImageView+AFNetworking.h"
#import "petAPIClient.h"
#import "ObjPetType.h"
#import "ObjBreedType.h"
#import "NavBarButton4.h"
#import "TSActionSheet.h"
#import "ObjPetType.h"
#import "ObjBreedType.h"

//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface EditPetViewController ()
{
    IBOutlet UITextField * txtName;
    IBOutlet UITextField * txtDob;

    IBOutlet UIImageView * imgProfile;
    IBOutlet UITextView * txtVDescription;
    IBOutlet UIScrollView * scrollView;
    IBOutlet UIButton * btnType;
    IBOutlet UIButton * btnBreed;
    IBOutlet UIButton * btnGender;
    IBOutlet UIButton * btnDate;
    NSString * strGender;
    NSString * strType;
    NSString * strBreed;
    SOAPRequest * editPetRequest;
    BOOL isFromPhotoLibrary;
    int fromCamera;
    NSDate * date;
    NSString * strDate;

    IBOutlet UITableView * tbl;

    TKLabelTextFieldCell *cell3;
    TKLabelTextViewCell *cell2;

    IBOutlet UIToolbar *keyboardToolbar;
    IBOutlet UIImageView * imgBgView;
    UIImagePickerController *imagePicker;
    ObjPetType * objSelectedPetType;
    ObjBreedType * objSelectedPetBreed;
    
    UILabel * lblbtnCapType;
    UILabel * lblbtnCapDob;
    UILabel * lblbtnCapBreed;
    UILabel * lblbtnCapGender;
}
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIDatePicker *uiDateView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedLevel;
@property int selectedType;
@property int selectedBreed;
@property (nonatomic, strong) NSMutableArray *arrGender;
@property (nonatomic, strong) NSMutableArray *arrType;
@property (nonatomic, strong) NSMutableArray *arrBreed;
@property BOOL isSelected;
@end

@implementation EditPetViewController
@synthesize objPet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    if (!isFromPhotoLibrary) {
        [self loadPetView:objPet];
        [self syncTypeAndBreed];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.navigationItem.title = @"Edit Pet";

    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Edit Pet";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;

    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];

    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        scrollView.frame  = CGRectMake(0, 0, 320, 548);
        //[imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }
    else{
        scrollView.frame  = CGRectMake(0, 0, 320, 460);
        scrollView.contentSize = CGSizeMake(320, 548);
        //[imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
    }

    scrollView.frame = CGRectMake(0, 44, scrollView.frame.size.width, scrollView.frame.size.height);
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    

    UIBarButtonItem * btnAdd = [[UIBarButtonItem alloc]initWithTitle:@"Update" style:UIBarButtonItemStyleBordered target:self action:@selector(onAdd:)];
    self.navigationItem.rightBarButtonItem = btnAdd;

    NSLog(@"view did load!!");

    NSLog(@"scroll width %f and scroll hight %f",scrollView.contentSize.width,scrollView.contentSize.height);
    [self hardCodeList];
    isFromPhotoLibrary = FALSE;
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];

	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;

    NavBarButton4 *btnEdit = [[NavBarButton4 alloc] init];
	[btnEdit addTarget:self action:@selector(onAdd:) forControlEvents:UIControlEventTouchUpInside];

	UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = editButton;

    

    cell3 = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell3.label.text = @"Name:";
    cell3.label.textAlignment = UITextAlignmentLeft;
    cell3.label.font = [UIFont boldSystemFontOfSize:14];
    cell3.label.textColor = [UIColor blackColor];
    cell3.label.frame = CGRectMake(30 + cell3.label.frame.origin.x, cell3.label.frame.origin.y, cell3.frame.size.width, cell3.frame.size.height);
	cell3.field.text = @"";
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    cell3.field.textColor = [UIColor blackColor];

    self.typeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
	//self.typeCell.textLabel.text = @"Select Type";
    // self.typeCell.textLabel.textColor = [UIColor blackColor];
    //self.typeCell.textLabel.textAlignment = UITextAlignmentLeft;
    if (!lblbtnCapType) {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapType  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapType  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapType.text = @"Select Type";
    lblbtnCapType.textColor = [UIColor blackColor];
    lblbtnCapType.textAlignment = UITextAlignmentLeft;
    lblbtnCapType.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCapType.tag = 1;
    lblbtnCapType.backgroundColor = [UIColor clearColor];
    self.typeCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.typeCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.typeCell addSubview:lblbtnCapType];

    self.btnDateCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapDob) {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapDob  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapDob = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapDob.text = @"DOB";
    lblbtnCapDob.textColor = [UIColor blackColor];
    lblbtnCapDob.textAlignment = UITextAlignmentLeft;
    lblbtnCapDob.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCapDob.backgroundColor = [UIColor clearColor];
    lblbtnCapDob.tag = 1;
    self.btnDateCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     self.btnDateCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnDateCell addSubview:lblbtnCapDob];

    self.btnBreedCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapBreed) {
        //lblbtnCapBreed = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapBreed  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapBreed = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapBreed.text = @"Select Breed";
    lblbtnCapBreed.textColor = [UIColor blackColor];
    lblbtnCapBreed.textAlignment = UITextAlignmentLeft;
    lblbtnCapBreed.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCapBreed.backgroundColor = [UIColor clearColor];
    lblbtnCapBreed.tag = 1;
    self.btnBreedCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnBreedCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnBreedCell addSubview:lblbtnCapBreed];

    self.btnGenderCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapGender) {
        //lblbtnCapGender = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapGender  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapGender = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapGender.text = @"Select Gender";
    lblbtnCapGender.textColor = [UIColor blackColor];
    lblbtnCapGender.textAlignment = UITextAlignmentLeft;
    lblbtnCapGender.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCapGender.backgroundColor = [UIColor clearColor];
    lblbtnCapGender.tag = 1;
    self.btnGenderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnGenderCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnGenderCell addSubview:lblbtnCapGender];

    UITableViewCell *cell4 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	UILabel * lblCap;
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        lblCap =[[UILabel alloc]initWithFrame:CGRectMake(8, 7, 70, 30)];
    }
    else if([Utility isGreaterOREqualOSVersion:@"6"])
    {
        lblCap =[[UILabel alloc]initWithFrame:CGRectMake(20, 7, 70, 30)];
    }
    
    lblCap.textColor = [UIColor blackColor];
    lblCap.backgroundColor = [UIColor clearColor];
    lblCap.text=@"Profile:";
    lblCap.font = [UIFont boldSystemFontOfSize:14];

    imgProfile = [[UIImageView alloc]initWithFrame:CGRectMake(210, 7, 90, 90)];
    imgProfile.backgroundColor = [UIColor darkGrayColor];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell4 addSubview:lblCap];
    [cell4 addSubview:imgProfile];

    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Description: ";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:14];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    /*CGRect textViewNewFrame = cell2.textView.frame;
    textViewNewFrame.origin.x  += 20;
    cell2.textView.frame = textViewNewFrame;*/

    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.textView.textColor = [UIColor blackColor];
    self.cells = @[self.typeCell,cell3,self.btnDateCell,self.btnBreedCell,self.btnGenderCell,cell4,cell2];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
        [tbl setFrame:CGRectMake(0, -40, 320, self.view.frame.size.height)];
    }
    
    [self loadForUIActionView];
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    self.uiDateView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    self.selectedLevel = 0;
    self.selectedBreed = 0;
    self.selectedType = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}

-(void) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    [self.view endEditing:YES];
    UITableViewCell * cell = (UITableViewCell *)sender;
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Camera" block:^{
        NSLog(@"Camera");
        [self showCamera];
    }];
    
    [actionSheet addButtonWithTitle:@"Library" block:^{
        NSLog(@"Library");
       // [self showPhotoLibrary];
        [self showPhotoLibrary:nil];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    //actionSheet.

    [actionSheet showWithCell:cell];
}

-(void)showCamera{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;

        // Delegate is self
        imagePicker.delegate = self;

        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;

        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        fromCamera = 1;

    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {

    }
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;

    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;

    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;

    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);

    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 5) return 104;
    else if (indexPath.row == 6) return 120;
    else return 44;
	return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        [self onType:nil];
    }
    else if (indexPath.row == 2) {
        [self onDate:nil];
    }
    else if (indexPath.row == 3) {
        [self onBreed:nil];
    }
    else if (indexPath.row == 4) {
        [self onGender:nil];
    }
    else if (indexPath.row == 5) {
        //[self showPhotoLibrary:nil];
        [self showActionSheet:cell forEvent:nil];
        //NSLog(@"on row show photo!!");
    }
}

- (void) loadPetView:(ObjPet * )pet{
    //txtName.text = pet.strName;
    cell3.field.text = pet.strName;
    //txtDob.text = pet.strDob;
    //txtVDescription.text = pet.strDescription;
    cell2.textView.text = pet.strDescription;
    NSLog(@"pet name:%@, pet type:%@, pet bType:%@,pet Gender: %@,pet Dob: %@, pet Des:%@",pet.strName,pet.strType,pet.strBreed,pet.strGender,pet.strDob,pet.strDescription);
    if (![pet.strGender isEqualToString:@""]) {
        strGender = pet.strGender;

        [self setGenderText:strGender];
        self.isSelected = TRUE;
    }
    else {
        strGender = @"";
        //[btnGender setTitle:@"Select Gender" forState:normal];
    }
    if (objSelectedPetType == nil) {
        objSelectedPetType = [[ObjPetType alloc]init];
    }
    objSelectedPetType.idx = pet.type_id;
    objSelectedPetType.strName = pet.strType;

    if (![pet.strBreed isEqualToString:@""]) {
        strBreed = pet.strBreed;
        
        [self setBreedText:strBreed];
    }
    else {
        strBreed = @"";
        //[btnBreed setTitle:@"Select Breed" forState:normal];
    }
    
    if (objSelectedPetBreed == nil) {
        objSelectedPetBreed = [[ObjBreedType alloc]init];
    }
    objSelectedPetBreed.idx = pet.breed_type_id;
    objSelectedPetBreed.strName = pet.strBreed;
    if (![pet.strType isEqualToString:@""]) {
        strType = pet.strType;
        lblbtnCapType.text = strType;
        [self setTypeText:strType];
    }
    else {
        strBreed = @"";
        //[btnType setTitle:@"Select Type" forState:normal];
    }

    if (![pet.strDob isEqualToString:@""]) {
        //strDate = pet.strDob;
        //[btnDate setTitle:strDate forState:normal];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * dobDate = [dateFormatter dateFromString:pet.strDob];
        NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc]init];

        [dateFormatter1 setDateFormat:@"dd MMM yyyy"];

        strDate = [dateFormatter1 stringFromDate:dobDate];
        
        //lblbtnCapDob.text = strDate;
        [self setDoBText:strDate];
    }
    else {
        strDate = @"";
        //[btnType setTitle:@"Select Date" forState:normal];
    }

    [imgProfile setImageWithURL:[NSURL URLWithString:pet.strImgLink] placeholderImage:nil];
}

- (void)setBreedText:(NSString *)strValue{
    lblbtnCapBreed.text = [NSString stringWithFormat:@"Breed: %@",strValue];
}

- (void)setTypeText:(NSString *)strValue{
    lblbtnCapType.text = [NSString stringWithFormat:@"Type: %@",strValue];
}

- (void)setGenderText:(NSString *)strValue{
    lblbtnCapGender.text = [NSString stringWithFormat:@"Gender: %@",strValue];
}

- (void)setDoBText:(NSString *)strValue{
    lblbtnCapDob.text = [NSString stringWithFormat:@"DOB: %@",strValue];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {

    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        //[txtVDescription becomeFirstResponder];
        //return NO;
    }
    //[textField resignFirstResponder];
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *) textView{
    //[self moveScrollView:textView];
    UITableViewCell *cell = (UITableViewCell *)[textView superview];
    NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    [tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];

}

-(void)textViewDidEndEditing:(UITextView *) textFieldView {

    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
       // [textView resignFirstResponder];
        return YES;
    }else{
        return YES;
    }
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];

    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];

	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height - 260.0;
	keyboardToolbar.frame = frame;

	[UIView commitAnimations];

    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height + keyboardToolbar.frame.size.height+60, 0);
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];

}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];

	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height;
	keyboardToolbar.frame = frame;

    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];

	[UIView commitAnimations];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if (pickerView.tag == 0) {
        NSString *strName = [self.arrGender objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);

        return strName;

    }
    if (pickerView.tag == 1) {
        ObjPetType * objPT = [self.arrType objectAtIndex:row];
        NSString *strName = objPT.strName;
        return strName;

    }
    if (pickerView.tag == 2) {
        if ([self.arrType count]>0) {
            ObjPetType * objType = [self.arrType objectAtIndex:self.selectedType];
            ObjBreedType * objBT = [objType.arrBreed objectAtIndex:row];
            NSString *strName = objBT.strName;
            return strName;
        }
        return @"";

    }

    if (self.uiDateView.tag == 1){
        //Date picker click
        //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
        date = [self.uiDateView date];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        strDate = [dateFormatter stringFromDate:date];
        [btnDate setTitle:[NSString stringWithFormat:@"%@",strDate] forState:normal];
        //remindDate = nil;

    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrGender count];
    }
    if (pickerView.tag == 1) {
        return [self.arrType count];
    }
    if (pickerView.tag == 2) {
        if ([self.arrType count]>0) {
            ObjPetType * objType = [self.arrType objectAtIndex:self.selectedType];
            return [objType.arrBreed count];
        }
        return 0;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {

        self.selectedLevel = row;
        NSLog(@"selected gender %d",self.selectedLevel);
    }
    if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedType = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
        self.selectedBreed = 0;
    }

    if (pickerView.tag == 2) {
        self.selectedBreed = row;
        NSLog(@"selected gender %d",self.selectedBreed);
    }

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];

    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
    }

    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrGender objectAtIndex:self.selectedLevel];
        
            [self setGenderText:strName];
            self.isSelected = TRUE;
            strGender = strName;
        }
        if (self.uiPickerView.tag == 1){
            ObjPetType * objPT = [self.arrType objectAtIndex:self.selectedType];
            objSelectedPetType = objPT;
            NSString * strName = objPT.strName;
            [self setTypeText:strName];
            self.isSelected = TRUE;
            strType = strName;

            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:self.selectedBreed];
            objSelectedPetBreed = objBT;
            NSString * strBreedName;
            if([objPT.arrBreed count]>0){
                strBreedName = objBT.strName;
            }
            else{
                strBreedName = @"No Breed Type";
            }

            [self setBreedText:strBreedName];
            strBreed = strBreedName;
        }
        if (self.uiPickerView.tag == 2){
            ObjPetType * objPT = [self.arrType objectAtIndex:self.selectedType];
            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:self.selectedBreed];
            objSelectedPetBreed = objBT;
            NSString * strName = objBT.strName;
            [self setBreedText:strName];
            self.isSelected = TRUE;
            strBreed = strName;
        }

        if (self.uiDateView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            date = [self.uiDateView date];
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            strDate = [dateFormatter stringFromDate:date];
    
            lblbtnCapDob.text = strDate;
            [self setDoBText:strDate];
        }
    }
}

- (void)hardCodeList{
    self.arrGender = [[NSMutableArray alloc]initWithCapacity:2];
    [self.arrGender addObject:@"Male"];
    [self.arrGender addObject:@"Female"];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onGender:(id)sender{
    [self.menu setTitle:@"Select Gender"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onType:(id)sender{
    [self.menu setTitle:@"Select Type"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onDate:(id)sender{
    [self.menu setTitle:@"DOB"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiDateView.tag = 1;
    self.uiDateView.datePickerMode = UIDatePickerModeDate;
    self.uiDateView.maximumDate = [NSDate date];
    //self.uiDateView.delegate = self;
    //[self.uiDateView reloadAllComponents];
    //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiDateView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onBreed:(id)sender{
    [self.menu setTitle:@"Select Breed"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 2;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedBreed inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (IBAction)onAdd:(id)sender{
    NSString * strName = [cell3.field.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * strDob = [txtDob.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([strName isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Name is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strDob isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"DOB is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strGender isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Gender is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strBreed isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Breed is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strType isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Type is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        //ObjPetType * objPT = [self.arrType objectAtIndex:self.selectedType];
        //ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:self.selectedBreed];

        ObjPet * pet = [[ObjPet alloc]init];
        pet.pet_id = objPet.pet_id;
        pet.strName = cell3.field.text;
        pet.strDob = strDate;
        pet.strGender = strGender;
        pet.strBreed = [NSString stringWithFormat:@"%d",objSelectedPetBreed.idx];
        pet.strType = [NSString stringWithFormat:@"%d",objSelectedPetType.idx];
        pet.strDescription = cell2.textView.text;

        NSData *imageData = UIImageJPEGRepresentation(imgProfile.image,0.4);
        NSString *base64image = [self base64forData:imageData];
        pet.strImgLink = base64image;
        //[self syncEditPet:pet];
        if ([self stringIsEmpty:pet.strName shouldCleanWhiteSpace:YES]) {
            pet.strName = @"";
        }
        if ([self stringIsEmpty:pet.strDob shouldCleanWhiteSpace:YES]) {
            pet.strDob = @"";
        }
        if ([self stringIsEmpty:pet.strGender shouldCleanWhiteSpace:YES]) {
            pet.strGender = @"";
        }
        if ([self stringIsEmpty:pet.strBreed shouldCleanWhiteSpace:YES]) {
            pet.strBreed = @"";
        }
        if ([self stringIsEmpty:pet.strType shouldCleanWhiteSpace:YES]) {
            pet.strType = @"";
        }
        if ([self stringIsEmpty:pet.strDescription shouldCleanWhiteSpace:YES]) {
            pet.strDescription = @"";
        }
        [self syncPetEdit:pet];
        [self.view endEditing:YES];
    }

}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {

    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }

    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }

    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }

    return NO;
}

- (void)syncPetEdit:(ObjPet *)pet{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    //pet[gender]=%@&pet[pet_name]=%@&pet[profile_image]=%@&pet[dob]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@
    NSDictionary* params = @{@"pet[pet_name]":pet.strName,@"pet[gender]":pet.strGender,@"pet[pet_image]":pet.strImgLink,@"pet[dob]":pet.strDob,@"pet[pet_breed_type_id]":pet.strBreed,@"pet[pet_type_id]":pet.strType,@"pet[description]":pet.strDescription};
    NSLog(@"params %@ and full link %@",params,[NSString stringWithFormat:@"%@/%d?auth_token=%@",EDITPET_LINK,pet.pet_id,strSession]);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",EDITPET_LINK,pet.pet_id,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (IBAction) showPhotoLibrary:(id) sender{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }

    // Set source to the camera
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;

        // Delegate is self
        imagePicker.delegate = self;

        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;

        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        fromCamera = 0;
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Fuction is not suported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {

    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self deleteAllFilesInDocuments];
    NSLog(@">>>>>>>>>didFinishPickingMediaWithInfo");
    //iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];

    // Access the uncropped image from info dictionary
    //UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];

    UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImage * orginalImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage * resizedImage=[Utility imageByScalingProportionallyToSize:editedImage newsize:CGSizeMake(90, 90) resizeFrame:TRUE];

    //UIImage * resizedImageFromLib=[Utility forceImageResize:editedImage newsize:CGSizeMake(90, 90)];

    if (fromCamera == 1) {
        // Save image;

        /*UIImageWriteToSavedPhotosAlbum(thumbImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
         NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];
         //delegate.pathSelectedPic = seletedPicPath;
         NSData *imageData = UIImagePNGRepresentation(thumbImage);
         [imageData writeToFile:seletedPicPath atomically:NO];*/
        NSLog(@"here is photo selected!!");
        //[self onCrop:resizedImage];
        if (resizedImage != nil) {
            //[arrThumbImages addObject:thumbImage];
            //[arrImages addObject:editedImage];
            //[self reloadTheScrollImageViewWithThumbArr:arrThumbImages];

            NSData *imageData = UIImageJPEGRepresentation(editedImage,0.4);
            NSString *base64image = [self base64forData:imageData];
            imgProfile.image = editedImage;
            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil);
        }
    }
    else if(fromCamera == 0){

        /*NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];*/
        //delegate.pathSelectedPic = seletedPicPath;

        //NSData *imageData = UIImagePNGRepresentation(resizedImageFromLib);
        //[imageData writeToFile:seletedPicPath atomically:NO];

        //[delegate showPhoto];
        //[self onCrop:resizedImageFromLib];

        //[delegate showGamePlayDetail:CUSTOM_PHOTO_VIEW];
        //[imgProfileView setImage:resizedImage];
        if (resizedImage != nil) {
            //[arrThumbImages addObject:thumbImage];
            //[arrImages addObject:editedImage];
            //[self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
            NSLog(@"here is library selected!!");

            NSData *imageData = UIImageJPEGRepresentation(editedImage,0.4);
            //NSString *base64image = [self base64forData:imageData];
            imgProfile.image = editedImage;
        }

    }
    [self imagePickerControllerDidCancel:picker];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    isFromPhotoLibrary = TRUE;
    [picker dismissModalViewControllerAnimated:YES];
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        // iOS 7
        //self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+20, self.navigationController.navigationBar.frame.size.width, 44);
        //self.navigationController.navigationBar.translucent = NO;
        // for iOS7
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        }
        PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        [delegate windowViewAdjust];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    isFromPhotoLibrary = FALSE;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //[self deleteAllFilesInDocuments];
    // iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];

    UIAlertView *alert;
    // Unable to save the image
    if (error){

        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
        // [alert release];
    }
    else{
        /*alert = [[UIAlertView alloc] initWithTitle:@"Success"
         message:@"Image saved to Photo Album."
         delegate:self cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/

    }

}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];

    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;

    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;

            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }

        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }

    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

/*- (void) syncEditPet:(ObjPet *)pet{
    [SVProgressHUD show];
    if (editPetRequest == nil) {
        editPetRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    editPetRequest.processId = 9;
    [editPetRequest syncEditPet:pet];
}*/

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 9) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
    }
}

- (void) syncTypeAndBreed{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",PET_TYPE_BREED_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];

        if (status == STATUS_ACTION_SUCCESS) {
            NSMutableArray *  arr = [dics objectForKey:@"pet_types"];
            if (self.arrType == nil) {
                self.arrType = [[NSMutableArray alloc]initWithCapacity:[arr count]];
            }
            [self.arrType removeAllObjects];

            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicType = [arr objectAtIndex:i];
                ObjPetType * objPT = [[ObjPetType alloc]init];
                objPT.idx = [[dicType objectForKey:@"pet_type_id"]intValue];
                objPT.strName = [dicType objectForKey:@"type_name"];
                NSMutableArray *  arrBreedType = [dicType objectForKey:@"pet_breed_types"];
                objPT.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];

                /*if (self.arrBreed == nil) {
                 self.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];
                 }
                 [self.arrBreed removeAllObjects];*/

                for(NSInteger b=0;b<[arrBreedType count];b++){
                    NSDictionary * dicType = [arrBreedType objectAtIndex:b];
                    ObjBreedType * objBT = [[ObjBreedType alloc]init];
                    objBT.idx = [[dicType objectForKey:@"pet_breed_type_id"]intValue];
                    objBT.strName = [dicType objectForKey:@"pet_breed"];
                    [objPT.arrBreed addObject:objBT];
                }
                [self.arrType addObject:objPT];
            }

           /* NSMutableArray *  arrBreedType = [dics objectForKey:@"pet_breed_types"];
            if (self.arrBreed == nil) {
                self.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];
            }
            [self.arrBreed removeAllObjects];

            for(NSInteger b=0;b<[arrBreedType count];b++){
                NSDictionary * dicType = [arrBreedType objectAtIndex:b];
                ObjBreedType * objBT = [[ObjBreedType alloc]init];
                objBT.idx = [[dicType objectForKey:@"pet_breed_type_id"]intValue];
                objBT.strName = [dicType objectForKey:@"pet_breed"];
                [self.arrBreed addObject:objBT];
            }
            */
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
