//
//  EditProfileViewController.m
//  Pet
//
//  Created by Zayar on 4/29/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "EditProfileViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "NavBarButton1.h"
#import "NavBarButton3.h"
#import <QuartzCore/QuartzCore.h>
#import "WebImageOperations.h"
#import "UIImage+Category.h"
#import "petAPIClient.h"
#import "NavBarButton4.h"
#import "TSActionSheet.h"
#import "ChangePassViewController.h"
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface EditProfileViewController ()
{
    UITextField * txtFName;
    UITextField * txtLName;
    UITextField * txtDob;
    UITextField * txtUserName;
    UITextField * txtEmail;
    UIButton * btnGender;
    
    UITextField * txtPhone;
    UITextField * txtFb;
    UITextField * txtWeb;
    NSString * strGender;
    NSString * strCountry;
    int fromCamera;
    SOAPRequest * profileEditRequest;
    BOOL isFromPhotoLibrary;
    
    NSDate * date;
    NSString * strDate;
    
    IBOutlet UITableView * tbl;
    
    TKLabelTextFieldCell *cell3;
    TKLabelTextFieldCell *lastNamecell;
    TKLabelTextFieldCell *userNameCell;
    TKLabelTextFieldCell *emailCell;
    TKLabelTextFieldCell *fbCell;
    TKLabelTextFieldCell *websiteCell;
    TKLabelTextFieldCell *contactCell;
    TKLabelTextFieldCell *postalCell;
    TKLabelTextViewCell *cell2;
    
    IBOutlet UIToolbar *keyboardToolbar;
    IBOutlet UIImageView * imgBgView;
    UIImage * selectedImage;
    UIImagePickerController *imagePicker;
    
    ChangePassViewController * changePassController;
    
    UILabel * lblbtnCapDob;
    UILabel * lblbtnCapGender;
    UILabel * lblbtnCapCountry;
    UILabel * lblbtnCapPrivacy;
}

@property BOOL isSelected;
@property BOOL isCountrySelected;
@property (nonatomic, strong) UIDatePicker *uiDateView;
@property (nonatomic,strong) NSMutableArray * arrGender;
@property (nonatomic,strong) NSMutableArray * arrCountry;
@property (nonatomic,strong) NSMutableArray * arrPrivacy;
@property int selectedLevel;
@property int selectedCountry;
@property int selectedPrivacy;
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic, strong) UIActionSheet *menu;
@property (nonatomic, strong) IBOutlet UIImageView * imgProfileView;
@end

@implementation EditProfileViewController
@synthesize user,typeCell,btnDateCell,btnBreedCell,btnGenderCell,btnCountryCell,btnChangePasswordCell,btnPrivacyCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"Scroll view x:%f y:%f",self.scrollView.frame.origin.x,self.scrollView.frame.origin.y);
    self.navigationItem.title = @"Edit";
    self.scrollView.frame = CGRectMake(0, 44, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    
    scrollViewOriginalSize = self.scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    [self hardCodeGenderList];
    isFromPhotoLibrary = FALSE;
    
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton4 *btnEdit = [[NavBarButton4 alloc] init];
	[btnEdit addTarget:self action:@selector(onRegister:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = editButton;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
        
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Edit";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    [self loadTheRequiredCell];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
        [tbl setFrame:CGRectMake(0, -40, 320, self.view.frame.size.height+40)];
    }
    
    [self loadForUIActionView];
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    self.uiDateView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    
    self.selectedLevel = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


- (void)loadTheRequiredCell{
    
    cell3 = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell3.label.text = @"First Name:";
    cell3.label.textAlignment = UITextAlignmentLeft;
    cell3.label.font = [UIFont boldSystemFontOfSize:12];
    cell3.label.textColor = [UIColor blackColor];
    
    cell3.label.frame = CGRectMake(30 + cell3.label.frame.origin.x, cell3.label.frame.origin.y, cell3.frame.size.width+10, cell3.frame.size.height);
	cell3.field.text = @"";
    cell3.field.tag = 0;
    
    lastNamecell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lastNamecell.label.text = @"Last Name:";
    lastNamecell.label.textAlignment = UITextAlignmentLeft;
    lastNamecell.label.font = [UIFont boldSystemFontOfSize:12];
    lastNamecell.label.textColor = [UIColor blackColor];
    lastNamecell.label.frame = CGRectMake(30 + lastNamecell.label.frame.origin.x, lastNamecell.label.frame.origin.y+10, lastNamecell.frame.size.width, lastNamecell.frame.size.height);
	lastNamecell.field.text = @"";
    lastNamecell.field.tag = 1;
    
    userNameCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	userNameCell.label.text = @"Username:";
    userNameCell.label.textAlignment = UITextAlignmentLeft;
    userNameCell.label.font = [UIFont boldSystemFontOfSize:12];
    userNameCell.label.textColor = [UIColor blackColor];
    userNameCell.label.frame = CGRectMake(30 + userNameCell.label.frame.origin.x, userNameCell.label.frame.origin.y, userNameCell.frame.size.width, userNameCell.frame.size.height);
	userNameCell.field.text = @"";
    userNameCell.field.delegate = self;
    userNameCell.field.tag = 2;
    
    emailCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	emailCell.label.text = @"Email:";
    emailCell.label.textAlignment = UITextAlignmentLeft;
    emailCell.label.font = [UIFont boldSystemFontOfSize:12];
    emailCell.label.textColor = [UIColor blackColor];
    emailCell.label.frame = CGRectMake(30 + emailCell.label.frame.origin.x, emailCell.label.frame.origin.y, emailCell.frame.size.width, emailCell.frame.size.height);
	emailCell.field.text = @"";
    emailCell.field.delegate = self;
    
    emailCell.field.tag = 3;
    
    
    fbCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	fbCell.label.text = @"Facebook:";
    fbCell.label.textAlignment = UITextAlignmentLeft;
    fbCell.label.font = [UIFont boldSystemFontOfSize:12];
    fbCell.label.textColor = [UIColor blackColor];
    fbCell.label.frame = CGRectMake(30 + fbCell.label.frame.origin.x, fbCell.label.frame.origin.y, fbCell.frame.size.width, fbCell.frame.size.height);
	fbCell.field.text = @"";
    fbCell.field.delegate = self;
    
    fbCell.field.tag = 4;
    
    
    websiteCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	websiteCell.label.text = @"Website:";
    websiteCell.label.textAlignment = UITextAlignmentLeft;
    websiteCell.label.font = [UIFont boldSystemFontOfSize:12];
    websiteCell.label.textColor = [UIColor blackColor];
    websiteCell.label.frame = CGRectMake(30 + websiteCell.label.frame.origin.x, websiteCell.label.frame.origin.y, websiteCell.frame.size.width, websiteCell.frame.size.height);
	websiteCell.field.text = @"";
    websiteCell.field.delegate = self;
    
    websiteCell.field.tag = 5;
    
    contactCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	contactCell.label.text = @"Contact Number:";
    contactCell.label.textAlignment = UITextAlignmentLeft;
    contactCell.label.font = [UIFont boldSystemFontOfSize:12];
    contactCell.label.textColor = [UIColor blackColor];
    contactCell.label.frame = CGRectMake(30 + contactCell.label.frame.origin.x, contactCell.label.frame.origin.y, contactCell.frame.size.width, contactCell.frame.size.height);
	contactCell.field.text = @"";
    contactCell.field.delegate = self;
    
    contactCell.field.tag = 6;
    
    postalCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	postalCell.label.text = @"Postal:";
    postalCell.label.textAlignment = UITextAlignmentLeft;
    postalCell.label.font = [UIFont boldSystemFontOfSize:12];
    postalCell.label.textColor = [UIColor blackColor];
    postalCell.label.frame = CGRectMake(30 + postalCell.label.frame.origin.x, postalCell.label.frame.origin.y, postalCell.frame.size.width, postalCell.frame.size.height);
	postalCell.field.text = @"";
    postalCell.field.delegate = self;
    
    postalCell.field.tag = 7;
    
    self.btnDateCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapDob) {
        //lblbtnCap2  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapDob  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapDob = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapDob.text = @"DOB";
    lblbtnCapDob.textColor = [UIColor blackColor];
    lblbtnCapDob.textAlignment = UITextAlignmentLeft;
    lblbtnCapDob.font = [UIFont boldSystemFontOfSize:12];
    lblbtnCapDob.backgroundColor = [UIColor clearColor];
    lblbtnCapDob.tag = 1;
    self.btnDateCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnDateCell addSubview:lblbtnCapDob];
    
    self.btnGenderCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapGender) {
        //lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapGender  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapGender = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapGender.text = @"Select Gender";
    lblbtnCapGender.textColor = [UIColor blackColor];
    lblbtnCapGender.textAlignment = UITextAlignmentLeft;
    lblbtnCapGender.font = [UIFont boldSystemFontOfSize:12];
    lblbtnCapGender.backgroundColor = [UIColor clearColor];
    lblbtnCapGender.tag = 1;
    self.btnGenderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnGenderCell addSubview:lblbtnCapGender];
    
    self.btnCountryCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapCountry) {
        //lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapCountry  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCapCountry = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapCountry.text = @"Select Country";
    lblbtnCapCountry.textColor = [UIColor blackColor];
    lblbtnCapCountry.textAlignment = UITextAlignmentLeft;
    lblbtnCapCountry.font = [UIFont boldSystemFontOfSize:12];
    lblbtnCapCountry.backgroundColor = [UIColor clearColor];
    lblbtnCapCountry.tag = 1;
    self.btnCountryCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnCountryCell addSubview:lblbtnCapCountry];
    
    self.btnChangePasswordCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap6;
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        lblbtnCap6  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
    }
    else if([Utility isGreaterOREqualOSVersion:@"6"])
    {
        lblbtnCap6 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    }
    
    lblbtnCap6.text = @"Change Password";
    lblbtnCap6.textColor = [UIColor blackColor];
    lblbtnCap6.textAlignment = UITextAlignmentLeft;
    lblbtnCap6.font = [UIFont boldSystemFontOfSize:12];
    lblbtnCap6.backgroundColor = [UIColor clearColor];
    lblbtnCap6.tag = 1;
    self.btnChangePasswordCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnChangePasswordCell addSubview:lblbtnCap6];

    self.btnPrivacyCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCapPrivacy) {
        
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCapPrivacy  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
           lblbtnCapPrivacy  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCapPrivacy.text = @"Privacy";
    lblbtnCapPrivacy.textColor = [UIColor blackColor];
    lblbtnCapPrivacy.textAlignment = UITextAlignmentLeft;
    lblbtnCapPrivacy.font = [UIFont boldSystemFontOfSize:12];
    lblbtnCapPrivacy.backgroundColor = [UIColor clearColor];
    lblbtnCapPrivacy.tag = 1;
    self.btnPrivacyCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnPrivacyCell addSubview:lblbtnCapPrivacy];
    /*self.btnBreedCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap3.text = @"Select Breed";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 1;
    self.btnBreedCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnBreedCell addSubview:lblbtnCap3];
    
    self.btnGenderCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap4.text = @"Select Gender";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:14];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 1;
    self.btnGenderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnGenderCell addSubview:lblbtnCap4];*/
    
    UITableViewCell *cell4 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    UILabel * lblCap;
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        lblCap  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
    }
    else if([Utility isGreaterOREqualOSVersion:@"6"])
    {
        lblCap  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    }
	
    lblCap.textColor = [UIColor blackColor];
    lblCap.backgroundColor = [UIColor clearColor];
    lblCap.text=@"Profile:";
    lblCap.font = [UIFont boldSystemFontOfSize:12];
    
    self.imgProfileView = [[UIImageView alloc]initWithFrame:CGRectMake(210, 7, 90, 90)];
    self.imgProfileView.backgroundColor = [UIColor darkGrayColor];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell4 addSubview:lblCap];
    [cell4 addSubview:self.imgProfileView];
    
    //static NSString *CellIdentifier = @"TableTextViewCell";
    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Address:";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:12];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
    cell2.textView.font = [UIFont systemFontOfSize:11];
    cell2.textView.textColor = [UIColor blackColor];
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    
    self.cells = @[cell3,lastNamecell,userNameCell,emailCell,fbCell,websiteCell,contactCell,self.btnGenderCell,self.btnCountryCell,postalCell,self.btnDateCell,cell4,cell2,self.btnChangePasswordCell,self.btnPrivacyCell];
}

- (void)loadTheTableViewWith:(ObjUser *)objUser{
    //objUser.str
    cell3.field.text = objUser.strFName;
    lastNamecell.field.text = objUser.strLName;
    userNameCell.field.text = objUser.strName;
    emailCell.field.text = objUser.strEmail;
    fbCell.field.text = objUser.strFbLink;
    websiteCell.field.text = objUser.strWebLink;
    contactCell.field.text = objUser.strPhone;
    cell2.textView.text = objUser.strAddress;
    postalCell.field.text = objUser.strPostalCode;
    
    emailCell.field.enabled = FALSE;
    userNameCell.field.enabled = FALSE;
    fbCell.field.enabled = FALSE;
    
    if (![objUser.strGender isEqualToString:@""]) {
        strGender = objUser.strGender;
        
        [self setGenderText:strGender];
        self.isSelected = TRUE;
    }
    else {
        strGender = @"";
        //[btnGender setTitle:@"Select Gender" forState:normal];
    }
    
    if (![objUser.strDob isEqualToString:@""]) {
        ///strDate = objUser.strDob;
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * dobDate = [dateFormatter dateFromString:objUser.strDob];
        NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc]init];
        
        [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
        
        strDate = [dateFormatter1 stringFromDate:dobDate];
        [self setDoBText:strDate];
    }
    else {
        strDate = @"";
        //[btnType setTitle:@"Select Date" forState:normal];
    }
    
    if (![objUser.strCountry isEqualToString:@""]) {
        strCountry = objUser.strCountry;
        [self setCountryText:strCountry];
        self.isCountrySelected = TRUE;
    }
    else {
        strCountry = @"";
        //[btnGender setTitle:@"Select Gender" forState:normal];
    }
    
    if (objUser.privacy_id != -1) {
        //strCountry = objUser.strCountry;
        //[btnGender setTitle:strGender forState:normal];
        NSString * strPrivate = [self.arrPrivacy objectAtIndex:objUser.privacy_id];
        
    
        [self setPrivacyText:strPrivate];
        //self.isCountrySelected = TRUE;
    }
    else {
        NSString * strPrivate = @"Privacy";
    
        lblbtnCapPrivacy.text = strPrivate;
        [self setPrivacyText:strPrivate];
        //[btnGender setTitle:@"Select Gender" forState:normal];
    }
    
    [self.imgProfileView setImageWithURL:[NSURL URLWithString:objUser.strProfileImgLink] placeholderImage:nil];
}

- (void)setGenderText:(NSString *)strValue{
    lblbtnCapGender.text = [NSString stringWithFormat:@"Gender: %@",strValue];
}

- (void)setDoBText:(NSString *)strValue{
    lblbtnCapDob.text = [NSString stringWithFormat:@"DOB: %@",strValue];
}

- (void)setCountryText:(NSString *)strValue{
    lblbtnCapCountry.text = [NSString stringWithFormat:@"Country: %@",strValue];
}

- (void)setPrivacyText:(NSString *)strValue{
    lblbtnCapPrivacy.text = [NSString stringWithFormat:@"Privacy: %@",strValue];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)hardCodeGenderList{
    self.arrGender = [[NSMutableArray alloc]init];
    [self.arrGender addObject:@"Male"];
    [self.arrGender addObject:@"Female"];
    
    self.arrPrivacy = [[NSMutableArray alloc]init];
    [self.arrPrivacy addObject:@"Profile is public now"];
    [self.arrPrivacy addObject:@"Profile is private now"];
    
    self.arrCountry = [[NSMutableArray alloc]init];
    NSArray * commands = nil;
    if( [COUNTRY_STRING_ARRAY rangeOfString:@","].location != NSNotFound ){
        
        commands = [COUNTRY_STRING_ARRAY componentsSeparatedByString:@","];
        
        for(NSString * strWifiName in commands){
            [self.arrCountry addObject:strWifiName];
        }
    }
}

- (void)onCountry:(id)sender{
    [self.menu setTitle:@"Select Country"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 2;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedCountry inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        NSString *strName = [self.arrGender objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return strName;
    }
    else if (pickerView.tag == 2) {
        NSString *strName = [self.arrCountry objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return strName;
    }
    else if (pickerView.tag == 4) {
        NSString *strName = [self.arrPrivacy objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        return strName;
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrGender count];
    }
    else if (pickerView.tag == 2) {
        return [self.arrCountry count];
    }
    else if (pickerView.tag == 4) {
        return [self.arrPrivacy count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    else if (pickerView.tag == 2) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedCountry = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    else if (pickerView.tag == 4) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedPrivacy = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    
    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrGender objectAtIndex:self.selectedLevel];
    
            [self setGenderText:strName];
            self.isSelected = TRUE;
            strGender = strName;
        }
         if (self.uiPickerView.tag == 2){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrCountry objectAtIndex:self.selectedCountry];
             [self setCountryText:strName];
            self.isCountrySelected = TRUE;
            strCountry = strName;
        }
        if (self.uiDateView.tag == 3){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSLog(@"date selected dob %@",strDate);
            date = [self.uiDateView date];
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            strDate = [dateFormatter stringFromDate:date];
            [self setDoBText:strDate];
            //remindDate = nil;
        }
        if (self.uiPickerView.tag == 4){
            NSString * strPrivacy = [self.arrPrivacy objectAtIndex:self.selectedPrivacy];
            
            /*for (UIView * v in self.btnDateCell.subviews) {
             if ([v isKindOfClass:[UILabel class]]) {
             if (v.tag == 1) {
             UILabel * lbl = (UILabel *)v;
             lbl.text = strDate;
             }
             }
             }*/
            lblbtnCapPrivacy.text = strPrivacy;
            [self setPrivacyText:strPrivacy];
            //remindDate = nil;
        }
    }
}

- (IBAction) showPhotoLibrary:(id) sender{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    
    // Set source to the camera
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        fromCamera = 0;
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Fuction is not suported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self deleteAllFilesInDocuments];
    NSLog(@">>>>>>>>>didFinishPickingMediaWithInfo");
    //iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    // Access the uncropped image from info dictionary
    //UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImage * orginalImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage * resizedImage=[Utility imageByScalingProportionallyToSize:editedImage newsize:CGSizeMake(90, 90) resizeFrame:TRUE];
    
    //UIImage * resizedImageFromLib=[Utility forceImageResize:editedImage newsize:CGSizeMake(90, 90)];
    
    if (fromCamera == 1) {
        // Save image;
        
        /*UIImageWriteToSavedPhotosAlbum(thumbImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
         NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];
         //delegate.pathSelectedPic = seletedPicPath;
         NSData *imageData = UIImagePNGRepresentation(thumbImage);
         [imageData writeToFile:seletedPicPath atomically:NO];*/
        NSLog(@"here is photo selected!!");
        //[self onCrop:resizedImage];
        /*if (resizedImage != nil) {
            //[arrThumbImages addObject:thumbImage];
            //[arrImages addObject:editedImage];
            //[self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
            
            NSData *imageData = UIImageJPEGRepresentation(editedImage,0.4);
            NSString *base64image = [self base64forData:imageData];
            imgProfile.image = editedImage;
            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil);
        }*/
        
        if (resizedImage != nil) {
            self.imgProfileView.image = nil;
            [self.imgProfileView setImage:resizedImage];
            NSLog(@"here is photo %@ and img profile %@",resizedImage,self.imgProfileView.image);
            selectedImage = resizedImage;
            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil);
        }
    }
    else if(fromCamera == 0){
        
        /*NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];*/
        //delegate.pathSelectedPic = seletedPicPath;
        
        //NSData *imageData = UIImagePNGRepresentation(resizedImageFromLib);
        //[imageData writeToFile:seletedPicPath atomically:NO];
        
        //[delegate showPhoto];
        //[self onCrop:resizedImageFromLib];
        
        //[delegate showGamePlayDetail:CUSTOM_PHOTO_VIEW];
        //[imgProfileView setImage:resizedImage];
        if (resizedImage != nil) {
            self.imgProfileView.image = nil;
            [self.imgProfileView setImage:resizedImage];
            NSLog(@"here is photo %@ and img profile %@",resizedImage,self.imgProfileView.image);
            selectedImage = resizedImage;
            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil);
        }
    }
    [self imagePickerControllerDidCancel:picker];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //[self deleteAllFilesInDocuments];
   // iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    UIAlertView *alert;
    // Unable to save the image
    if (error){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
       // [alert release];
    }
    else{
        /*alert = [[UIAlertView alloc] initWithTitle:@"Success"
         message:@"Image saved to Photo Album."
         delegate:self cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/
        
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    isFromPhotoLibrary = TRUE;
    [picker dismissModalViewControllerAnimated:YES];
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        // iOS 7
        //self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+20, self.navigationController.navigationBar.frame.size.width, 44);
        //self.navigationController.navigationBar.translucent = NO;
        // for iOS7
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        }
        PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        [delegate windowViewAdjust];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    isFromPhotoLibrary = FALSE;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    
    
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    if (!isFromPhotoLibrary) {
        //[self loadViewProfile];
        //[self reloadProfileTheViewWith:user];
        [self loadTheTableViewWith:user];
    }
}

- (IBAction)hideKeyboard:(id)sender {
	//[self ];
    [self.view endEditing:YES];
}

- (void)loadViewProfile{
    txtFName.text = user.strFName;
    txtLName.text = user.strLName;
    txtDob.text = user.strDob;
    [self.imgProfileView setImageWithURL:[NSURL URLWithString:user.strProfileImgLink] placeholderImage:[UIImage imageNamed:@"img_profile_default.png"]];
    //[self.imgProfileView setImage:[UIImage imageNamed:@"img_profile_default.png"]];
    if (![user.strGender isEqualToString:@""]) {
        [btnGender setTitle:user.strGender forState:normal];
    }
    txtUserName.text = user.strName;
    txtEmail.text = user.strEmail;
    strGender = user.strGender;
    txtPhone.text = user.strPhone;
    txtFb.text = user.strFbLink;
    txtWeb.text = user.strWebLink;
}

- (void) reloadProfileTheViewWith:(ObjUser *)obj{
    for (UIView * v in self.scrollView.subviews) {
        [v removeFromSuperview];
    }
    float y=0.0;
    self.imgProfileView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 203)];
    NSString * strValueFontName=@"AvenirNext-Bold";
    float fontSizeOfValue = 16;
    [self.imgProfileView setBackgroundColor:[UIColor grayColor]];
    [self.scrollView addSubview:self.imgProfileView];
    if (obj.strProfileImgLink != nil) {
        NSLog(@"user strProfile image link %@",obj.strProfileImgLink);
        //[activity startAnimating];
        [WebImageOperations processImageDataWithURLString:obj.strProfileImgLink andBlock:^(NSData *imageData) {
            if (self.view) {
                UIImage *image = [UIImage imageWithData:imageData];
                UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(self.imgProfileView.frame.size.width,self.imgProfileView.frame.size.height) resizeFrame:NO];
                //[self o]
                [self.imgProfileView setImage:[cropedImage overlayWith:[UIImage imageNamed: @"img_gradient_overlay.png"] overlayPoint: CGPointMake(0,0)]];
                //[activity stopAnimating];
                //[self.imgProfile setImage:image];
            }
        }];
        
        UILabel * lblTap = [[UILabel alloc]initWithFrame:CGRectMake(60,80,200,42)];
        lblTap.font = [UIFont fontWithName:@"Avenir Next" size:16];
        lblTap.textColor = [UIColor whiteColor];
        lblTap.text = @"Tap to add profile!";
        lblTap.textAlignment = UITextAlignmentCenter;
        lblTap.backgroundColor = [UIColor clearColor];
        
        [self.scrollView addSubview:lblTap];
        
        UIButton * btnTap = [UIButton buttonWithType:UIButtonTypeCustom];
        btnTap.frame = CGRectMake(0, 0, 320, 203);
        [btnTap addTarget:self action:@selector(showPhotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
        btnTap.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:btnTap];
        
        
        /*UILabel * lblAddre = [[UILabel alloc]initWithFrame:CGRectMake(5,149,181,42)];
        lblAddre.font = [UIFont fontWithName:@"Avenir Next" size:16];
        lblAddre.textColor = [UIColor whiteColor];
        lblAddre.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:lblAddre];
        
        if (![obj.strAddress isEqualToString:@""]) {
            lblAddre.text =[NSString stringWithFormat:@"Live in %@",obj.strAddress ];
        }*/
        
        UILabel * capOfTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,211,166,21)];
        capOfTitle.font = [UIFont fontWithName:@"Avenir Next" size:14];
        capOfTitle.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        //capOfTitle.textColor = [UIColor blackColor];
        capOfTitle.backgroundColor = [UIColor clearColor];
        capOfTitle.numberOfLines = 0;
        capOfTitle.text = @"Personal Information";
        [self.scrollView addSubview:capOfTitle];
        
        UILabel * bgOfContact = [[UILabel alloc]initWithFrame:CGRectMake(10,232,300,76)];
        bgOfContact.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfContact.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfContact.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfContact];
        [self makeRoundandBorder:bgOfContact];
        UILabel * titleOfContact = [[UILabel alloc]initWithFrame:CGRectMake(20,240,280,24)];
        titleOfContact.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfContact.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfContact.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        titleOfContact.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfContact];
        
        txtPhone = [[UITextField alloc]initWithFrame:CGRectMake(20,272,280,30)];
        txtPhone.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtPhone.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtPhone.tag = 1;
        txtPhone.backgroundColor = [UIColor clearColor];
        txtPhone.delegate = self;
        [self makeRoundandBorderForText:txtPhone];
        if (![obj.strPhone isEqualToString:@""]) {
            txtPhone.text = obj.strPhone;
        }
        [self.scrollView addSubview:txtPhone];
        
        UILabel * bgOfWebsite = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfContact.frame.origin.y+bgOfContact.frame.size.height,300,76)];
        bgOfWebsite.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfWebsite.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfWebsite.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfWebsite];
        [self makeRoundandBorder:bgOfWebsite];
        
        UILabel * titleOfWebsite = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfWebsite.frame.origin.y+8,280,24)];
        titleOfWebsite.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfWebsite.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfWebsite.text = @"Website";
        //bgOfContact.text = @"Personal Information";
        titleOfWebsite.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfWebsite];
        
        txtWeb = [[UITextField alloc]initWithFrame:CGRectMake(20,bgOfWebsite.frame.origin.y+40,280,30)];
        txtWeb.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtWeb.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtWeb.tag = 2;
        txtWeb.backgroundColor = [UIColor clearColor];
        txtWeb.delegate = self;
        [self.scrollView addSubview:txtWeb];
        if (![obj.strWebLink isEqualToString:@""]) {
            txtWeb.text = obj.strWebLink;
        }
        [self makeRoundandBorderForText:txtWeb];
        
        UILabel * bgOfFB = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfWebsite.frame.origin.y+bgOfWebsite.frame.size.height,300,76)];
        bgOfFB.font = [UIFont fontWithName:@"Avenir Next" size:17];
        
        bgOfFB.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfFB.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfFB];
        [self makeRoundandBorder:bgOfFB];
        
        UILabel * titleOfFb = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFB.frame.origin.y+8,280,24)];
        titleOfFb.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfFb.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfFb.text = @"Facebook";
        //bgOfContact.text = @"Personal Information";
        titleOfFb.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfFb];
        
        txtFb = [[UITextField alloc]initWithFrame:CGRectMake(20,bgOfFB.frame.origin.y+40,280,30)];
        txtFb.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtFb.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtFb.tag = 3;
        txtFb.backgroundColor = [UIColor clearColor];
        txtFb.delegate = self;
        [self makeRoundandBorderForText:txtFb];
        if (![obj.strFbLink isEqualToString:@""]) {
            txtFb.text = obj.strFbLink;
        }
        [self.scrollView addSubview:txtFb];
        
        UILabel * bgOfFBName = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfFB.frame.origin.y+bgOfFB.frame.size.height,300,76)];
        bgOfFBName.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfFBName.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfFBName.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfFBName];
        [self makeRoundandBorder:bgOfFBName];
        
        UILabel * titleOfFbName = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFBName.frame.origin.y+8,280,24)];
        titleOfFbName.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfFbName.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfFbName.text = @"Name";
        //bgOfContact.text = @"Personal Information";
        titleOfFbName.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfFbName];
        
        txtFName = [[UITextField alloc]initWithFrame:CGRectMake(20,bgOfFBName.frame.origin.y+40,280,30)];
        txtFName.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtFName.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtFName.tag = 4;
        txtFName.backgroundColor = [UIColor clearColor];
        txtFName.delegate = self;
        [self makeRoundandBorderForText:txtFName];
        if (![obj.strFName isEqualToString:@""]) {
            txtFName.text = obj.strFName;
        }
        [self.scrollView addSubview:txtFName];
        
        UILabel * bgOfGender = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfFBName.frame.origin.y+bgOfFBName.frame.size.height,300,76)];
        bgOfGender.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfGender.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfGender.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfGender];
        [self makeRoundandBorder:bgOfGender];
        
        UILabel * titleOfGender = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfGender.frame.origin.y+8,280,24)];
        titleOfGender.font = [UIFont fontWithName:@"Avenir Next" size:fontSizeOfValue];
        titleOfGender.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfGender.text = @"Gender";
        //bgOfContact.text = @"Personal Information";
        titleOfGender.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfGender];
        
        /*UILabel * lblGender = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfGender.frame.origin.y+40,280,31)];
        lblGender.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblGender.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblGender.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:lblGender];
        if (![obj.strGender isEqualToString:@""]) {
            lblGender.text = obj.strGender;
        }*/
        
        btnGender = [UIButton buttonWithType:UIButtonTypeCustom];
        btnGender.frame = CGRectMake(20,bgOfGender.frame.origin.y+40,280,31);
        [btnGender addTarget:self action:@selector(onLevel:) forControlEvents:UIControlEventTouchUpInside];
        //btnGender.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:btnGender];
        [btnGender setTitleColor:[UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f] forState:normal];
        if (![obj.strGender isEqualToString:@""]) {
            [btnGender setTitle:obj.strGender forState:normal];
        }
        else [btnGender setTitle:@"Select Gender" forState:normal];
        [self makeRoundandBorderForText:btnGender];
        
        
        UILabel * bgOfDob = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfGender.frame.origin.y+bgOfGender.frame.size.height,300,76)];
        bgOfDob.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfDob.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfDob.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfDob];
        [self makeRoundandBorder:bgOfDob];
        
        UILabel * titleOfDob = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfDob.frame.origin.y+8,280,24)];
        titleOfDob.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfDob.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfDob.text = @"Date of Birth";
        //bgOfContact.text = @"Personal Information";
        titleOfDob.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfDob];
        
        txtDob = [[UITextField alloc]initWithFrame:CGRectMake(20,bgOfDob.frame.origin.y+40,280,30)];
        txtDob.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtDob.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtDob.tag = 5;
        txtDob.backgroundColor = [UIColor clearColor];
        txtDob.delegate = self;
        [self makeRoundandBorderForText:txtDob];
        if (![obj.strDob isEqualToString:@""]) {
            txtDob.text = obj.strDob;
        }
        [self.scrollView addSubview:txtDob];
        
        UILabel * bgOfEmail = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfDob.frame.origin.y+bgOfDob.frame.size.height,300,76)];
        bgOfEmail.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfEmail.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfEmail.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [self.scrollView addSubview:bgOfEmail];
        [self makeRoundandBorder:bgOfEmail];
        
        UILabel * titleOfEmail = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfEmail.frame.origin.y+8,280,24)];
        titleOfEmail.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfEmail.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfEmail.text = @"Email";
        //bgOfContact.text = @"Personal Information";
        titleOfEmail.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:titleOfEmail];
        
        txtEmail = [[UITextField alloc]initWithFrame:CGRectMake(20,bgOfEmail.frame.origin.y+40,280,30)];
        txtEmail.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        txtEmail.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //txtWeb.textInputView.frame = CGRectMake(2, 5, txtWeb.frame.size.width,5);
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        txtEmail.backgroundColor = [UIColor clearColor];
        txtEmail.delegate = self;
        txtEmail.tag = 6;
        [self makeRoundandBorderForText:txtEmail];
        if (![obj.strEmail isEqualToString:@""]) {
            txtEmail.text = obj.strEmail;
        }
        [self.scrollView addSubview:txtEmail];
        
        y += bgOfEmail.frame.origin.y + bgOfEmail.frame.size.height;
        [self.scrollView setContentSize:CGSizeMake(320, y+44)];
    }
}

- (void) makeRoundandBorder:(UIView *)v{
    [v.layer setCornerRadius:2.0f];
    [v.layer setMasksToBounds:YES];
    
    v.layer.borderColor = [UIColor lightGrayColor].CGColor;
    v.layer.borderWidth = 1.0;
}

- (void) makeRoundandBorderForText:(UIView *)v{
    [v.layer setCornerRadius:2.0f];
    [v.layer setMasksToBounds:YES];
    
    v.layer.borderColor = [UIColor colorWithRed:253.0f/255.0f green:110.0f/255.0f blue:1.0f/255.0f alpha:1.0f].CGColor;
    v.layer.borderWidth = 1.0;
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    self.scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [self.scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    //[self moveScrollView:textFieldView];
    //UITableViewCell *cell = (UITableViewCell *)[textFieldView superview];
    //NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:textFieldView.tag inSection:0];
    NSLog(@"cell tag %d",textFieldView.tag);
    
    [tbl scrollToRowAtIndexPath:myIP atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        //[txtVDescription becomeFirstResponder];
        //return NO;
    }
    //[textField resignFirstResponder];
	return YES;
}

- (void) textViewDidBeginEditing:(UITextView *) textView{
    //[self moveScrollView:textView];
    TKLabelTextFieldCell *cell = (TKLabelTextFieldCell *)[textView superview];
    NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    [tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void) textViewDidEndEditing:(UITextView *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        // [textView resignFirstResponder];
        return YES;
    }else{
        return YES;
    }
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height - 260.0;
	keyboardToolbar.frame = frame;
	
	[UIView commitAnimations];
    
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height + keyboardToolbar.frame.size.height+60, 0);
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height;
	keyboardToolbar.frame = frame;
    
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
	
	[UIView commitAnimations];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (IBAction)onClose:(id)sender{
    if([self.delegate respondsToSelector:@selector(dismissWithViewController:)])
    {
        [self.delegate dismissWithViewController:self];
    }
}

- (void)showLibrary:(id)sender{
    NSLog(@"here is tappppppp!!!");
}

-(IBAction)onLevel:(id)sender{
    [self.menu setTitle:@"Select Gender"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onDate:(id)sender{
    [self.menu setTitle:@"DOB"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiDateView.tag = 3;
    self.uiDateView.datePickerMode = UIDatePickerModeDate;
    self.uiDateView.maximumDate = [NSDate date];
    //self.uiDateView.delegate = self;
    //[self.uiDateView reloadAllComponents];
    //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiDateView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onPrivacy:(id)sender{
    [self.menu setTitle:@"Privacy"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 4;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onRegister:(id)sender{
    NSData *imageData = UIImageJPEGRepresentation(self.imgProfileView.image,0.4);
    NSString *base64image = [self base64forData:imageData];
    ObjUser * objUser = [[ObjUser alloc]init];
    
    objUser.strFName = cell3.field.text;
    objUser.strLName = lastNamecell.field.text;
    objUser.strDob = strDate;
    //objUser.strDescription = cell2.textView.text;
    if (strGender != nil) {
        objUser.strGender = strGender;
    }
    else objUser.strGender = @"";
    
    if (strCountry != nil) {
        objUser.strCountry = strCountry;
    }
    else objUser.strCountry = @"";
    
    objUser.strProfileImgLink = base64image;
    
    objUser.strPhone = contactCell.field.text;
    if (contactCell.field.text != nil) {
        objUser.strPhone = contactCell.field.text;
    }
    else objUser.strPhone = @"";
    
    if (websiteCell.field.text != nil) {
        objUser.strWebLink =websiteCell.field.text;
    }
    else objUser.strWebLink = @"";
    
    if (fbCell.field.text != nil) {
        objUser.strFbLink = fbCell.field.text;
    }
    else objUser.strFbLink = @"";
    
    objUser.userId = user.userId;
    
    if (userNameCell.field.text != nil) {
        objUser.strName = userNameCell.field.text;
    }
    else objUser.strName = @"";
    
    if (emailCell.field.text != nil) {
        objUser.strEmail = emailCell.field.text;
    }
    else objUser.strEmail = @"";
    
    if (cell2.textView.text != nil) {
        objUser.strAddress = cell2.textView.text;
    }
    else objUser.strAddress = @"";
    
    if (postalCell.field.text != nil) {
        objUser.strPostalCode = postalCell.field.text;
    }
    else objUser.strPostalCode = @"";
    
    objUser.userId = user.userId;
    
    if ([self stringIsEmpty:objUser.strName shouldCleanWhiteSpace:YES]) {
        objUser.strName = @"";
    }
    if ([self stringIsEmpty:objUser.strFName shouldCleanWhiteSpace:YES]) {
        objUser.strFName = @"";
        [Utility showAlert:APP_TITLE message:ERRMSG1];
        return;
    }
    if ([self stringIsEmpty:objUser.strLName shouldCleanWhiteSpace:YES]) {
        objUser.strLName = @"";
        [Utility showAlert:APP_TITLE message:ERRMSG2];
        return;
    }
    if ([self stringIsEmpty:objUser.strDob shouldCleanWhiteSpace:YES]) {
        objUser.strDob = @"";
    }
    if ([self stringIsEmpty:objUser.strEmail shouldCleanWhiteSpace:YES]) {
        objUser.strEmail = @"";
    }
    if ([self stringIsEmpty:objUser.strPhone shouldCleanWhiteSpace:YES]) {
        objUser.strPhone = @"";
    }
    if ([self stringIsEmpty:objUser.strAddress shouldCleanWhiteSpace:YES]) {
        objUser.strAddress = @"";
    }
    if ([self stringIsEmpty:objUser.strPostalCode shouldCleanWhiteSpace:YES]) {
        objUser.strPostalCode = @"";
    }
    if ([self stringIsEmpty:objUser.strFbLink shouldCleanWhiteSpace:YES]) {
        objUser.strFbLink = @"";
    }
    if ([self stringIsEmpty:objUser.strWebLink shouldCleanWhiteSpace:YES]) {
        objUser.strWebLink = @"";
    }
    if ([self stringIsEmpty:objUser.strGender shouldCleanWhiteSpace:YES]) {
        objUser.strGender = @"";
    }
    if ([self stringIsEmpty:objUser.strDescription shouldCleanWhiteSpace:YES]) {
        objUser.strDescription = @"";
    }
    
    [self syncPetEdit:objUser];
    [self hideKeyboard:nil];
}

- (BOOL) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

- (void) syncPetEdit:(ObjUser *)obj{
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    //@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@"
    NSLog(@"profile sex %@",obj.strGender);
    NSLog(@"str country %@ and postal code %@",obj.strCountry,obj.strPostalCode);
    NSDictionary* params = @{@"user[first_name]":obj.strFName,@"user[last_name]":obj.strLName,@"user[gender]":obj.strGender,@"user[username]":obj.strName,@"user[profile_image]":obj.strProfileImgLink,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[address]":obj.strAddress,@"user[phone]":obj.strPhone,@"user[dob]":obj.strDob,@"user[country_name]":obj.strCountry,@"user[postal_code]":obj.strPostalCode,@"user[privacy]":[NSString stringWithFormat:@"%d",self.selectedPrivacy ]};
    
//,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[phone]":obj.strPhone,@"user[address]":obj.strAddress,@"user[dob]":obj.strDob
    NSLog(@"edited params %@",params);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",PROFILE_EDIT_LINK,obj.userId,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    //PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 6) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:@"Sucessfully update!"];
        }
        else if(status == STATUS_ACTION_FAILED){
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        else if(status == 4){
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
    }
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 11) return 104;
    else if (indexPath.row == 12) return 120;
    else return 44;
	return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 7) {
        [self onLevel:nil];
    }
    else if (indexPath.row == 8) {
        [self onCountry:nil];
    }
    else if (indexPath.row == 10) {
        [self onDate:nil];
    }
    else if (indexPath.row == 11) {
        //[self showPhotoLibrary:nil];
        [self showActionSheet:cell forEvent:nil];
    }
    else if (indexPath.row == 14) {
        //[self showPhotoLibrary:nil];
        [self onPrivacy:cell];
    }
    else if (indexPath.row == 13) {
        //[self showPhotoLibrary:nil];
        [self onChangePassword];
    }
}

-(void) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    [self.view endEditing:YES];
    UITableViewCell * cell = (UITableViewCell *)sender;
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Camera" block:^{
        NSLog(@"Camera");
        [self showCamera];
    }];
    
    [actionSheet addButtonWithTitle:@"Library" block:^{
        NSLog(@"Library");
        // [self showPhotoLibrary];
        [self showPhotoLibrary:nil];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    //actionSheet.
    
    [actionSheet showWithCell:cell];
}

-(void) onChangePassword{
    if (changePassController == nil) {
        changePassController = [[UIStoryboard storyboardWithName:@"settings" bundle:nil] instantiateViewControllerWithIdentifier:@"changePassword"];;
    }
    [self.navigationController pushViewController:changePassController animated:YES];
}

-(void)showCamera{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        fromCamera = 1;
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}
@end
