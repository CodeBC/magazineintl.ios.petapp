//
//  DealViewController.m
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DealViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjDeal.h"
#import <QuartzCore/QuartzCore.h>
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "DealTableCell.h"
#import "DealDetailViewController.h"
#import "NavBarButton1.h"
#import "Utility.h"
@interface DealViewController ()
{
    IBOutlet UITableView * tbl;
    SOAPRequest * dealRequest;
    NSMutableArray * arrDeal;
    IBOutlet UIImageView * imgBgView;
}

@end

@implementation DealViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Pet Deals";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncDeal];
}

- (void)syncDeal{
    [SVProgressHUD show];
    if (dealRequest == nil) {
        dealRequest = [[SOAPRequest alloc]initWithOwner:self];
    }
    dealRequest.processId = 14;
    [dealRequest syncDeal];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    //PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 14) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSLog(@"status of my pet %d",status);
        if (status == STATUS_ACTION_SUCCESS) {
            if ([dics objectForKey:@"deals"] != [NSNull null]) {
                NSMutableArray * arrRawPets = [dics objectForKey:@"deals"];
                if ([arrRawPets count] != 0) {
                    arrDeal = [[NSMutableArray alloc]initWithCapacity:[arrRawPets count]];
                    for(NSInteger i=0;i<[arrRawPets count];i++){
                        NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                        ObjDeal * objVideo = [[ObjDeal alloc]init];
                        if ([dicPet objectForKey:@"deal_id"] != [NSNull null]) {
                            objVideo.idx = [[dicPet objectForKey:@"deal_id"] intValue];
                        }
                        if ([dicPet objectForKey:@"deal_name"] != [NSNull null]) {
                            objVideo.strName = [dicPet objectForKey:@"deal_name"];
                        }
                        if ([dicPet objectForKey:@"info"] != [NSNull null]) {
                            objVideo.strInfo = [dicPet objectForKey:@"info"];
                        }
                        if ([dicPet objectForKey:@"description"] != [NSNull null]) {
                            objVideo.strDescription = [dicPet objectForKey:@"description"];
                        }
                        if ([dicPet objectForKey:@"deal_image"] != [NSNull null]) {
                            objVideo.strImgLink = [dicPet objectForKey:@"deal_image"];
                        }
                        if ([dicPet objectForKey:@"price"] != [NSNull null]) {
                            objVideo.strPrice = [dicPet objectForKey:@"price"];
                        }
                        
                        [arrDeal addObject:objVideo];
                        
                    }
                    NSLog(@"arr pet count %d",[arrDeal count]);
                    [self reloadData:YES];
                    
                }
            }
            [SVProgressHUD dismiss];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        else
        {
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            if (status == STATUS_ACTION_SUCCESS) {
                //ObjUser * objUser = [[ObjUser alloc]init];
                //objUser.strSession = [dics objectForKey:@"session_id"];
                //[delegate.db updateUser:objUser];
                [SVProgressHUD dismiss];
            }
            else if(status == STATUS_ACTION_FAILED){
                
                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
            }
            else if(status == 5){
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
                PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
                //nav.ownner = self;
                [self presentModalViewController:nav animated:YES];
            }
        }
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDeal count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DealTableCell";
	DealTableCell *cell = (DealTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DealTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (DealTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjDeal * obj = [arrDeal objectAtIndex:[indexPath row]];
    [cell loadTheCellWith:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjDeal * objD = [arrDeal objectAtIndex:indexPath.row];
    DealDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"dealDetail"];
    viewController.objDeal = objD;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    [tbl reloadData];
    /*if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
