  //
//  main.m
//  Pet
//
//  Created by Zayar on 4/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PetAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PetAppDelegate class]));
    }
}
