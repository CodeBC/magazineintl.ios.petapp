//
//  SideMenuViewController.h
//  Feel
//
//  Created by Zayar on 12/11/12.
//
//

#import <UIKit/UIKit.h>
#import "ADSlidingViewController.h"

@interface SideMenuViewController : UIViewController{
    
    IBOutlet UITableView * tbl;
    NSMutableArray * arrList;
    NSMutableArray * arrSecList;
    IBOutlet UIImageView * imgBgView;
}
- (IBAction)onCoupon:(id)sender;
@end
