//
//  ReporLostViewController.m
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ReporLostViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "petAPIClient.h"
#import "ObjArea.h"
#import "ProfileMapViewController.h"
#import "Utility.h"

//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface ReporLostViewController ()
{
    SOAPRequest * myLostPetRequest;
    NSMutableArray * arrPet;
    NSMutableArray * arrArea;
    IBOutlet UIButton * btnPet;
    IBOutlet UITextField * txtArea;
    IBOutlet UITextView * txtDescription;
    IBOutlet UITextField * txtLastSeen;
    IBOutlet UITextField * txtReward;
    ObjPet * objPet;
    
    IBOutlet UIImageView * imgBgView;
    TKLabelTextFieldCell *cell3;
    TKLabelTextViewCell *cell2;
    TKLabelTextFieldCell * areaTextCell;
    TKLabelTextFieldCell * rewardTextCell;
    TKLabelTextFieldCell * lastSeenTextCell;
    IBOutlet UITableView *tbl;
    IBOutlet UIToolbar *keyboardToolbar;
    NSDate * date;
    NSString * strDate;
    BOOL isFromMap;
    float lat;
    float lon;
    UILabel * lblbtnCap2;
    UILabel * lblbtnCap3;
    UILabel * lblbtnCap4;
    UILabel * lblbtnCap5;
}
@property int selectedLevel;
@property int selectedArea;
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property (nonatomic,strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic,strong) UITableViewCell *btnPet;
@property (nonatomic,strong) UITableViewCell *btnArea;
@property (nonatomic,strong) UITableViewCell *btnLastDate;
@property (nonatomic,strong) UITableViewCell *btnLocation;
@property (nonatomic,strong) NSArray *cells;
@property (nonatomic, strong) UIDatePicker *uiDateView;
@end

@implementation ReporLostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //[self syncPets];
    if (!isFromMap) {
        [self syncLostInfo];
        lat = 0.0;
        lon = 0.0;
    }
}

- (void) loadTheRequiredCell{
    self.btnPet = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap2) {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap2  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
        
    }
    lblbtnCap2.text = @"Select Pet";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
    self.btnPet.selectionStyle = UITableViewCellSelectionStyleNone;
    self.btnPet.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnPet addSubview:lblbtnCap2];
    
    self.btnArea = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap3) {
        //lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap3  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap3.text = @"Select Area";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 1;
    self.btnArea.selectionStyle = UITableViewCellSelectionStyleNone;
    self.btnArea.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnArea addSubview:lblbtnCap3];
    
    self.btnLastDate = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap4) {
        //lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap4.text = @"Last Seen date";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 1;
    self.btnLastDate.selectionStyle = UITableViewCellSelectionStyleNone;
    self.btnLastDate.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnLastDate addSubview:lblbtnCap4];
    
    self.btnLocation = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap5) {
        //lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap5  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap5.text = @"Location";
    lblbtnCap5.textColor = [UIColor blackColor];
    lblbtnCap5.textAlignment = UITextAlignmentLeft;
    lblbtnCap5.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap5.backgroundColor = [UIColor clearColor];
    lblbtnCap5.tag = 1;
    self.btnLocation.selectionStyle = UITableViewCellSelectionStyleNone;
    self.btnLocation.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnLocation addSubview:lblbtnCap5];
    /*lastSeenTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lastSeenTextCell.label.text = @"Last Seen:";
    lastSeenTextCell.label.textAlignment = UITextAlignmentLeft;
    lastSeenTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    lastSeenTextCell.label.textColor = [UIColor blackColor];
    
    lastSeenTextCell.label.frame = CGRectMake(30 + lastSeenTextCell.label.frame.origin.x, lastSeenTextCell.label.frame.origin.y, cell3.frame.size.width+10, lastSeenTextCell.frame.size.height);
	lastSeenTextCell.field.text = @"";
    lastSeenTextCell.field.tag = 0;
    lastSeenTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    areaTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	areaTextCell.label.text = @"Area:";
    areaTextCell.label.textAlignment = UITextAlignmentLeft;
    areaTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    areaTextCell.label.textColor = [UIColor blackColor];
    
    areaTextCell.label.frame = CGRectMake(30 + areaTextCell.label.frame.origin.x, areaTextCell.label.frame.origin.y, areaTextCell.frame.size.width+10, areaTextCell.frame.size.height);
	areaTextCell.field.text = @"";
    areaTextCell.field.tag = 0;
    areaTextCell.selectionStyle = UITableViewCellSelectionStyleNone;*/
    
    rewardTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	rewardTextCell.label.text = @"Reward(Optional):";
    rewardTextCell.label.textAlignment = UITextAlignmentLeft;
    rewardTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    rewardTextCell.label.textColor = [UIColor blackColor];
        
    rewardTextCell.label.frame = CGRectMake(30 + rewardTextCell.label.frame.origin.x, rewardTextCell.label.frame.origin.y, rewardTextCell.frame.size.width+30, rewardTextCell.frame.size.height);
	rewardTextCell.field.text = @"";
    rewardTextCell.field.tag = 0;
    rewardTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
    rewardTextCell.field.keyboardType = UIKeyboardTypeNumberPad;
    rewardTextCell.field.delegate = self;
    
    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Description:";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:13];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cells = @[self.btnArea,self.btnPet,self.btnLastDate,rewardTextCell,cell2,self.btnLocation];
}

-(IBAction)onDate:(id)sender{
    [self.menu setTitle:@"Select Date"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiDateView.tag = 1;
    self.uiDateView.datePickerMode = UIDatePickerModeDate;
    self.uiDateView.maximumDate = [NSDate date];
    //self.uiDateView.delegate = self;
    //[self.uiDateView reloadAllComponents];
    //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiDateView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (void) syncPets{
    [SVProgressHUD show];
    if (myLostPetRequest == nil) {
        myLostPetRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    myLostPetRequest.processId = 7;
    [myLostPetRequest syncMyPet];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 7) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        if ([dics objectForKey:@"pets"] != [NSNull null]) {
            NSMutableArray * arrRawPets = [dics objectForKey:@"pets"];
            if ([arrRawPets count] != 0) {
                arrPet = [[NSMutableArray alloc]initWithCapacity:[arrRawPets count]];
                for(NSInteger i=0;i<[arrRawPets count];i++){
                    NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                    ObjPet * objPet = [[ObjPet alloc]init];
                    if ([dicPet objectForKey:@"pet_id"] != [NSNull null]) {
                        objPet.pet_id = [[dicPet objectForKey:@"pet_id"] intValue];
                    }
                    if ([dicPet objectForKey:@"pet_name"] != [NSNull null]) {
                        objPet.strName = [dicPet objectForKey:@"pet_name"];
                    }
                    if ([dicPet objectForKey:@"pet_image"] != [NSNull null]) {
                        objPet.strImgLink = [dicPet objectForKey:@"pet_image"];
                    }
                    if ([dicPet objectForKey:@"pet_type"] != [NSNull null]) {
                        objPet.strType = [dicPet objectForKey:@"pet_type"];
                    }
                    [arrPet addObject:objPet];
                    NSLog(@"arr pet count %d and objPet name %@ and id %d",[arrPet count],objPet.strName,objPet.pet_id);
                }
                ///[self reloadData:YES];
                [SVProgressHUD dismiss];
            }
            else
            {
                int status = [[dics objectForKey:@"status"] intValue];
                NSString * strMsg = [dics objectForKey:@"message"];
                if (status == STATUS_ACTION_SUCCESS) {
                    [SVProgressHUD showSuccessWithStatus:strMsg];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
                else if(status == STATUS_ACTION_FAILED){
                    
                    //[self textValidateAlertShow:strErrorMsg];
                    [SVProgressHUD showErrorWithStatus:strMsg];
                }
            }
        }
        
        
        
    }
    if (processId == 12) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //self.scrollView.frame = CGRectMake(0, 44, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    
    //scrollViewOriginalSize = self.scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    [self loadForUIActionView];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton4 *btnSave = [[NavBarButton4 alloc] init];
	[btnSave addTarget:self action:@selector(onAdd:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithCustomView:btnSave];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = saveButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Report Lost";
    self.navigationItem.titleView = lblName;
    
    [self loadTheRequiredCell];
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    CGRect newframe = self.view.frame;
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        
    }
    else {
        newframe.origin.y -= 20;
    }
    tbl.frame = newframe;
    
    UIView * v = [[UIView alloc] init];
    tbl.tableFooterView = v;
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    self.uiDateView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    self.selectedArea = 0;
    self.selectedLevel = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


- (void)syncLostInfo{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    [self hideKeyboard:nil];
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",REPORT_LOST_INFO_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            
            NSMutableArray *  arr = [dics objectForKey:@"pets"];
            [arrPet removeAllObjects];
            if(arrPet == nil){
                arrPet = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjPet * obj = [[ObjPet alloc]init];
                if ([dicNewsFeed objectForKey:@"id"] != [NSNull null]) {
                    obj.pet_id = [[dicNewsFeed objectForKey:@"id"] intValue];
                }
                if ([dicNewsFeed objectForKey:@"name"] != [NSNull null]) {
                    obj.strName = [dicNewsFeed objectForKey:@"name"];
                }
                [arrPet addObject:obj];
            }
            
            NSLog(@"arrPet count %d",[arrPet count]);
            
            NSMutableArray *  arrTemp = [dics objectForKey:@"areas"];
            [arrArea removeAllObjects];
            if(arrArea == nil){
                arrArea = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arrTemp count];i++){
                NSDictionary * dicNewsFeed = [arrTemp objectAtIndex:i];
                ObjArea * objA = [[ObjArea alloc]init];
                if ([dicNewsFeed objectForKey:@"id"] != [NSNull null]) {
                    objA.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                }
                if ([dicNewsFeed objectForKey:@"name"] != [NSNull null]) {
                    objA.strName = [dicNewsFeed objectForKey:@"name"];
                }
                [arrArea addObject:objA];
            }
            if ([arrArea count]>0) {
                self.selectedArea = 0;
                ObjArea * obj = [arrArea objectAtIndex:self.selectedArea];
                //[btnPet setTitle:objPet.strName forState:normal];
                for (UIView * v in self.btnArea.subviews) {
                    if ([v isKindOfClass:[UILabel class]]) {
                        if (v.tag == 1) {
                            UILabel * lbl = (UILabel *)v;
                            lbl.text = obj.strName;
                        }
                    }
                }
            }
            
            NSLog(@"arrArea count %d",[arrArea count]);
            
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onAdd:(id)sender{
    //NSString * strArea = /*[ stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]]*/;
    ObjArea * objA = [arrArea objectAtIndex:self.selectedArea];
    //NSString * strArea = objA.strName;
    
    NSString * strLastSeen = [strDate stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSString * strDes = cell2.textView.text;
    
    if([strLastSeen isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Last Seen is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(objPet ==nil){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Pet is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (lat == 0.0 && lon == 0.0){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Pet lost location is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        ObjPet * pet = [[ObjPet alloc]init];
        pet = objPet;
        pet.strArea = [NSString stringWithFormat:@"%d",objA.idx];
        pet.strDescription = cell2.textView.text;
        pet.strLastSeen = strLastSeen;
        pet.strReward =  rewardTextCell.field.text;
        pet.lat  = lat;
        pet.lon  = lon;
        if ([self stringIsEmpty:pet.strDescription shouldCleanWhiteSpace:YES]) {
            pet.strDescription = @"";
        }
        if ([self stringIsEmpty:pet.strReward shouldCleanWhiteSpace:YES]) {
            pet.strReward = @"";
        }
        if ([self stringIsEmpty:pet.strLastSeen shouldCleanWhiteSpace:YES]) {
            pet.strLastSeen = @"";
        }
        
        [self syncLostPet:pet];
    }
}

- (void)syncLostPet:(ObjPet *)pet{
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    NSDictionary* params = @{@"lost[pet_id]":[NSString stringWithFormat:@"%d",pet.pet_id],@"lost[description]":pet.strDescription,@"lost[reward]":pet.strReward,@"lost[area_id]":pet.strArea,@"lost[lastseen_date]":pet.strLastSeen,@"lost[lat]":[NSString stringWithFormat:@"%f",pet.lat],@"lost[long]":[NSString stringWithFormat:@"%f",pet.lon]};
    
    NSLog(@"Submitting : %@",params);
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",LOST_REPORT_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"%@    ",strMsg]];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: APP_TITLE
                                  message: strMsg
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

/*- (void)syncLostPet:(ObjPet *)pet{
   
        [SVProgressHUD show];
        if (myLostPetRequest == nil) {
            myLostPetRequest = [[SOAPRequest alloc] initWithOwner:self];
        }
        myLostPetRequest.processId = 12;
        [myLostPetRequest syncReportLostPet:pet];
}*/

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        ObjPet * obj = [arrPet objectAtIndex:row];
        return obj.strName;
    }
    if (pickerView.tag == 1) {
        ObjArea * objA = [arrArea objectAtIndex:row];
        return objA.strName;
    }
    
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrPet count];
    }
    else if (pickerView.tag == 1){
        return [arrArea count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedArea = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    
    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiPickerView removeFromSuperview];
        [self.uiDateView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        [self.uiPickerView removeFromSuperview];
        [self.uiDateView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            objPet = [arrPet objectAtIndex:self.selectedLevel];
        
            
            [self setPetText:objPet.strName];
        }
        if (self.uiPickerView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            ObjArea * obj = [arrArea objectAtIndex:self.selectedArea];
            //[btnPet setTitle:objPet.strName forState:normal];
            
            [self setAreaText:obj.strName];
        }
        
        if (self.uiDateView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            date = [self.uiDateView date];
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            strDate = [dateFormatter stringFromDate:date];
            
            [self setLastSDText:strDate];
            
        }
    }
}

-(IBAction)onPet:(id)sender{
    [self.menu setTitle:@"Select Pet"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (void)onArea{
    [self.menu setTitle:@"Select Area"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedArea inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 4) {
        return 120;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*if (indexPath.row == 1) {
        [self onType:nil];
    }
    else if (indexPath.row == 2) {
        [self onDate:nil];
    }
    else if (indexPath.row == 3) {
        [self onReminderDate:nil];
    }*/
    if (indexPath.row == 0) {
        [self onArea];
    }
    if (indexPath.row == 1) {
         [self onPet:nil];
    }
    if (indexPath.row == 2) {
        [self onDate:nil];
    }
    if (indexPath.row == 5) {
        [self onLocation];
    }
}

- (void)onLocation{
    ProfileMapViewController * mapViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"profileMap"];
    mapViewController.owner = self;
    mapViewController.seletedLat = lat;
    mapViewController.seletedLon = lon;
    mapViewController.isEditable = TRUE;
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:mapViewController];
    nav.navigationBar.tintColor = [UIColor blackColor];
    
    [self.navigationController presentModalViewController:nav animated:YES];
    NSLog(@"here is on map click");
    isFromMap = TRUE;
}

- (void) finishWithCoordinate:(float)late andlong:(float)lng{
    NSLog(@"home town location %f and long %f",late,lng);
    lat = late;
    lon = lng;
    for (UIView * v in self.btnLocation.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 1) {
                UILabel * lbl = (UILabel *)v;
                lbl.text = [NSString stringWithFormat:@"%0.6f, %0.6f",late,lng];
            }
        }
    }
    isFromMap = TRUE;
}

- (void) closeMap{
    isFromMap = TRUE;
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    //scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    //[scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    //[self moveScrollView:textFieldView];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:textFieldView.tag inSection:0];
    NSLog(@"cell tag %d",textFieldView.tag);
    
    [tbl scrollToRowAtIndexPath:myIP atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        //[txtVDescription becomeFirstResponder];
        //return NO;
    }
    //[textField resignFirstResponder];
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *) textView{
    //[self moveScrollView:textView];
    UITableViewCell *cell = (UITableViewCell *)[textView superview];
    NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    [tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void)textViewDidEndEditing:(UITextView *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (IBAction)hideKeyboard:(id)sender {
	//[self ];
    [self.view endEditing:YES];
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height - 260.0;
	keyboardToolbar.frame = frame;
	
	[UIView commitAnimations];
    
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height;
	keyboardToolbar.frame = frame;
    
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
	
	[UIView commitAnimations];
}


- (void)setPetText:(NSString *)strValue{
    lblbtnCap2.text = [NSString stringWithFormat:@"Pet: %@",strValue];
}

- (void)setLastSDText:(NSString *)strValue{
    lblbtnCap4.text = [NSString stringWithFormat:@"Last Seen Date: %@",strValue];
}

- (void)setAreaText:(NSString *)strValue{
    lblbtnCap3.text = [NSString stringWithFormat:@"Area: %@",strValue];
}

- (void)setGenderText:(NSString *)strValue{
    lblbtnCap3.text = [NSString stringWithFormat:@"Gender: %@",strValue];
}

@end
