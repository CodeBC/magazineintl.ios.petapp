//
//  ObjOrganization.h
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjOrganization : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@end
