//
//  AlbumListCell.m
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AnimateAlbumListCell.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "UIImageView+AFNetworking.h"

@implementation AnimateAlbumListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loatTheCellWith:(ObjAlbum *)objAlbum{
    lblName.text = objAlbum.strName;
    if ([objAlbum.arrAlbumPhoto count]>0) {
        ObjAlbumPhoto * objPhoto = [objAlbum.arrAlbumPhoto objectAtIndex:0];
        [imgThumbView setImageWithURL:[NSURL URLWithString:objPhoto.strImgURL] placeholderImage:nil];
    }
}

- (void)loatTheCellWithPet:(ObjPet *)objPet{
    lblName.text = objPet.strName;
    [imgThumbView setImageWithURL:[NSURL URLWithString:objPet.strImgLink] placeholderImage:nil];
}

@end
