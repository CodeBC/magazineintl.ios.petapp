//
//  AlbumViewController.h
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAlbum.h"

@interface AlbumViewController : PetBasedViewController
@property (nonatomic, retain) ObjAlbum * objAlbum;
@property BOOL isFromOther;
@end
