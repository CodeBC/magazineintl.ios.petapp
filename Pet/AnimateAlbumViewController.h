//
//  AnimateAlbumViewController.h
//  Pet
//
//  Created by Zayar on 6/15/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAlbum.h"
@interface AnimateAlbumViewController : PetBasedViewController
@property (nonatomic, retain) ObjAlbum * objAlbum;
@end
