//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "NeigbourViewController.h"
#import "StringTable.h"
#import "petAPIClient.h"
#import "PetAppDelegate.h"
#import "ObjUser.h"
#import "MessageUserTableCell.h"
#import "NavBarButton1.h"
#import "ProfileViewController.h"
@interface NeigbourViewController ()
{
    IBOutlet UITableView * tbl;
    IBOutlet UIImageView * imgBgView;
}

@end

@implementation NeigbourViewController
@synthesize arrUsers;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = @"Popover Title";
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0,0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    tbl.backgroundColor = [UIColor clearColor];
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Neighbours";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = @"Neighbour";
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    //[self reloadPerson];
    for (ObjUser * objUser in self.arrUsers) {
        NSLog(@"objuer name %@",objUser.strName);
    }
    [tbl reloadData];
}

- (void)reloadPerson{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",ALL_USER_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        
        if (status == STATUS_ACTION_SUCCESS) {
            NSMutableArray *  arr = [dics objectForKey:@"users"];
            arrUsers = [[NSMutableArray alloc]initWithCapacity:[arr count]];
            for(NSInteger i=0;i<[arr count];i++){
                /*
                 {
                 "id": 3,
                 "first_name": "Justina Bogisich",
                 "last_name": "Arne Weimann",
                 "profile_image": ""
                 },
                 */
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjUser * objUser = [[ObjUser alloc]init];
                objUser.userId = [[dicNewsFeed objectForKey:@"id"] intValue];
                objUser.strFName = [dicNewsFeed objectForKey:@"first_name"];
                objUser.strLName = [dicNewsFeed objectForKey:@"last_name"];
                objUser.strProfileImgLink = [dicNewsFeed objectForKey:@"profile_image"];
                
                [arrUsers addObject:objUser];
            }
            [tbl reloadData];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageUserTableCell";
	MessageUserTableCell *cell = (MessageUserTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageUserTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MessageUserTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    //cell.textLabel.text = [NSString stringWithFormat:@"cell %d",indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    ObjUser * objU = [self.arrUsers objectAtIndex:indexPath.row];
    NSLog(@"objuer cell name %@ and ph %@ and fb %@ and dob %@ and email %@",objU.strName,objU.strPhone,objU.strFbLink,objU.strDob,objU.strEmail);
    [cell loadTheViewWith:objU];
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* NewsHubAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
     if([indexPath row]==0){
     
     [delegate closeFPPopover];
     
     }
     else if([indexPath row]==1){
     
     //        SettingViewController *settingViewController=[[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:[NSBundle mainBundle]];
     //        [self.navigationController pushViewController:settingViewController animated:YES];
     //        [settingViewController release];
     //        settingViewController=nil;
     //[delegate showSetting];
     
     }*/
    ObjUser * objUser = [self.arrUsers objectAtIndex:indexPath.row];
    //[owner onUserTableSelected:objUser];
    //uprofile
    ProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"uprofile"];
    
    //viewController.pe = objAdo;
    viewController.user = objUser;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
