//
//  VideoTableCell.h
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjLostFound.h"
@interface BrowseLFTableCell : UITableViewCell{
    
}
@property (nonatomic, strong) IBOutlet UIImageView * imgView;
@property (nonatomic, strong) IBOutlet UILabel * lblName;
@property (nonatomic, strong) IBOutlet UILabel * lblType;
@property (nonatomic, strong) IBOutlet UILabel * lblReportType;
- (void)loadTheCellWith:(ObjLostFound *)obj;
@end
