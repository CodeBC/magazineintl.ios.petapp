//
//  VideoTableCell.m
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "VideoTableCell.h"
#import "ObjVideo.h"
#import "UIImageView+AFNetworking.h"

@implementation VideoTableCell
@synthesize imgView,lblName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadTheCellWith:(ObjVideo *)obj{
    //http://www.youtube.com/watch?v=vffwoZgFdDU
   /* NSString * correctedUrl = [self getYTUrlStr:obj.strLink];
    //obj.strLink = @"http://www.youtube.com/v/vffwoZgFdDU";
    NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    
    NSString *html = [NSString stringWithFormat:embedHTML, correctedUrl, self.webView.frame.size.width,  self.webView.frame.size.height];
    [self.webView loadHTMLString:html baseURL:nil];
    self.webView.delegate = self;
    //[self.webView reload];*/
    
    NSString * videoID = [self extractYoutubeID:obj.strLink];
    NSString * strImgLinkFormat = @"http://img.youtube.com/vi/%@/1.jpg";
    NSString * strUTubeLink = [NSString stringWithFormat:strImgLinkFormat,videoID];
    [imgView setImageWithURL:[NSURL URLWithString:strUTubeLink] placeholderImage:nil];
    self.lblName.text = obj.strName;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"VTC: Web view finish load!!");
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"VTC: Web view start load!!");
}

- (NSString*)getYTUrlStr:(NSString*)url
{
    if (url == nil)
        return nil;
    
    NSString *retVal = [url stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
    
    NSRange pos=[retVal rangeOfString:@"version"];
    if(pos.location == NSNotFound)
    {
        retVal = [retVal stringByAppendingString:@"?version=3&hl=en_EN"];
    }
    return retVal;
}

- (NSString *)extractYoutubeID:(NSString *)youtubeURL
{
    
    NSError *error = nil;
    NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:regexString
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                    options:0
                                                      range:NSMakeRange(0, [youtubeURL length])];
    if (match) {
        NSRange videoIDRange = [match rangeAtIndex:0];
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
        return substringForFirstMatch;
    }
    return nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
