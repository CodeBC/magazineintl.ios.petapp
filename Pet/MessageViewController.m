//
//  MessageViewController.m
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MessageViewController.h"
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "ObjConversation.h"
#import "MessageTBCell.h"
#import "MessageDetailViewController.h"
#import "CustomPullToRefresh.h"
#import "UserTableController.h"
#import "TSPopoverController.h"
#import "NavBarSearchButton.h"
#import "PetAppDelegate.h"
#import "petAPIClient.h"
@interface MessageViewController ()
{
    NSMutableArray * arrMessages;
    IBOutlet UITableView * tbl;
    CustomPullToRefresh *_ptr;
    UserTableController *controller;
    //RecipeBookViewController * controller;
    TSPopoverController *popoverController;
    UIButton * btnNoMessage;
    UILabel * lblNoMessage;
}
@end

@implementation MessageViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"e4e4e4"]];
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"e95401"];
    }
    else {
        //self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"Navbar"];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    //UIBarButtonItem *addButton2 = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStyle target:self action:@selector(showPopover:forEvent:)];
    NavBarSearchButton *btnAdd = [[NavBarSearchButton alloc] init];
	[btnAdd addTarget:self action:@selector(showPopover:forEvent:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = addButton;

    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Messages";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;

    _ptr = [[CustomPullToRefresh alloc] initWithScrollView:tbl delegate:self];
    
    //[tbl setEditing:YES animated:YES];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    
   /* if (!btnNoMessage) {
        btnNoMessage = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    [btnNoMessage setFrame:CGRectMake(0, self.view.frame.size.height/2 -15, 320, 30)];
    //[btnNoMessage setFrame:tbl.frame];
    [btnNoMessage setTitle:@"You have no message. Tap here to start" forState:normal];
    [btnNoMessage addTarget:self action:@selector(onNoMessage) forControlEvents:UIControlEventTouchUpInside];
    btnNoMessage.hidden = TRUE;
    btnNoMessage.backgroundColor = [UIColor whiteColor];
    btnNoMessage.titleLabel.textColor = [UIColor blackColor];
    [self.view addSubview:btnNoMessage];*/
    
    CGRect newFrame = tbl.frame;
    newFrame.size.height -= 44;
    if (!lblNoMessage) {
        lblNoMessage = [[UILabel alloc] initWithFrame:newFrame];
    }
    lblNoMessage.text = @"You have no message.\n Click search button to start!";
    lblNoMessage.numberOfLines = 2;
    lblNoMessage.backgroundColor = [UIColor whiteColor];
    lblNoMessage.textColor = [UIColor blackColor];
    lblNoMessage.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblNoMessage];
    
    //self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"e95401"];
    
}

- (void) startRefresh{
    
}

#pragma mark - CustomPullToRefresh Delegate Methods
- (void) customPullToRefreshShouldRefresh:(CustomPullToRefresh *)ptr {
    //[self performSelectorInBackground:@selector(findNextPrime) withObject:nil];
    //[self performSelector:@selector(endRefresh) withObject:nil afterDelay:2];
    [self syncMessages];
}

- (void) endRefresh{
    [_ptr endRefresh];
}

- (void) viewWillAppear:(BOOL)animated{
    [self syncMessages];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [arrMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageTBCell";
	MessageTBCell *cell = (MessageTBCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageTBCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MessageTBCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjConversation * obj = [arrMessages objectAtIndex:[indexPath row]];
    [cell loadTheCellWith:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(void)showPopover:(id)sender forEvent:(UIEvent*)event
{
    //UITableViewController *tableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    if (controller == nil) {
        controller = [[UIStoryboard storyboardWithName:@"message" bundle:nil] instantiateViewControllerWithIdentifier:@"userTable"];
    }
    
    controller.view.frame = CGRectMake(0,0, 320, 350);
    controller.owner = self;
    if (popoverController == nil) {
        popoverController = [[TSPopoverController alloc] initWithContentViewController:controller];
    }
    //[popoverController setContentViewController:controller];
    popoverController.cornerRadius = 5;
    popoverController.titleText = @"Select A Person";
    popoverController.popoverBaseColor = [UIColor darkGrayColor];
    popoverController.popoverGradient= NO;
    //    popoverController.arrowPosition = TSPopoverArrowPositionHorizontal;
    [popoverController showPopoverWithTouch:event];
}

- (void)closePopover{
    [popoverController dismissPopoverAnimatd:YES];
}

- (void) onUserTableSelected:(ObjUser *)selectedUser{
    [self closePopover];
    NSLog(@"user id %d",selectedUser.userId);
    if (controller) {
        [controller dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    ObjConversation * objConvers=[[ObjConversation alloc]init];
    objConvers.receiverId = selectedUser.userId;
    objConvers.strReciverName = selectedUser.strFName;
    objConvers.strReciverImgLink = selectedUser.strProfileImgLink;
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    ObjUser * objUser2 = [delegate.db getUserObj];
    
    MessageDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"message" bundle:nil] instantiateViewControllerWithIdentifier:@"messageDetail"];
    viewController.objConv = objConvers;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) onCancel{
    if (controller) {
        [controller dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        ObjConversation * obj = [arrMessages objectAtIndex:indexPath.row];
        [self syncDeleteConversation:obj.idx and:indexPath];
        //[arrMessages removeObjectAtIndex:indexPath.row];
        //[tableView deleteRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)syncDeleteConversation:(int)con_id and:(NSIndexPath *)indexPath{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    //@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@"
    //NSDictionary* params = @{@"user[first_name]":obj.strFName,@"user[last_name]":obj.strLName,@"user[gender]":user.strGender,@"user[username]":obj.strName,@"user[profile_image]":obj.strProfileImgLink,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[address]":obj.strAddress,@"user[phone]":obj.strPhone,@"user[dob]":obj.strDob};
    
    //,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[phone]":obj.strPhone,@"user[address]":obj.strAddress,@"user[dob]":obj.strDob
    //NSLog(@"edited params %@",params);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",MESSAGE_CONVERSATION_DELETE_LINK,con_id,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [arrMessages removeObjectAtIndex:indexPath.row];
            [tbl deleteRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    ObjConversation * objConv = [arrMessages objectAtIndex:indexPath.row];
    MessageDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"message" bundle:nil] instantiateViewControllerWithIdentifier:@"messageDetail"];
    viewController.objConv = objConv;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)syncMessages{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",MESSAGE_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [self endRefresh];
            [self showTable:YES];
            NSMutableArray *  arr = [dics objectForKey:@"conversations"];
            [arrMessages removeAllObjects];
            if(arrMessages == nil){
                arrMessages = [[NSMutableArray alloc]initWithCapacity:[arr count]];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjConversation * objConv = [[ObjConversation alloc]init];
                objConv.idx = [[dicNewsFeed objectForKey:@"conversation_id"] intValue];
                objConv.strMessage = [dicNewsFeed objectForKey:@"message"];
                objConv.strReciverImgLink = [dicNewsFeed objectForKey:@"profile_image"];
                 objConv.strReciverName = [dicNewsFeed objectForKey:@"name"];
                objConv.timetick = [[dicNewsFeed objectForKey:@"timestamp"] intValue];
                [arrMessages addObject:objConv];
            }
            NSLog(@"arrMessages count %d",[arrMessages count]);
            [tbl reloadData];
            [_ptr endRefresh];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [self showTable:NO];
           // [SVProgressHUD showErrorWithStatus:strMsg];
            [_ptr endRefresh];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
            [_ptr endRefresh];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
        [_ptr endRefresh];
    }];
}

- (void)showTable:(BOOL)show{
    if (show) {
        tbl.hidden = FALSE;
        //btnNoMessage.hidden = TRUE;
        lblNoMessage.hidden = TRUE;
        [self.view bringSubviewToFront:tbl];
    }
    else{
        //tbl.hidden = TRUE;
        //btnNoMessage.hidden = FALSE;
        lblNoMessage.hidden = FALSE;
        [self.tableView bringSubviewToFront:lblNoMessage];
        //[self.tableView bringSubviewToFront:btnNoMessage];
    }
}

- (void)onNoMessage{
    NSLog(@"no message");
    if (controller == nil) {
        controller = [[UIStoryboard storyboardWithName:@"message" bundle:nil] instantiateViewControllerWithIdentifier:@"userTable"];
    }
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        controller.view.frame = CGRectMake(0, 20, controller.view.frame.size.width, controller.view.frame.size.height);
    }
    controller.owner = self;
    [self presentViewController:controller animated:YES completion:^{
        
    }];
}

@end
