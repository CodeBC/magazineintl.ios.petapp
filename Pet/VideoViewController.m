//
//  VideoViewController.m
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "VideoViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjVideo.h"
#import <QuartzCore/QuartzCore.h>
#import "VideoTableCell.h"
#import "SVModalWebViewController.h"
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "XCDYouTubeVideoPlayerViewController.h"
#import "Utility.h"

@interface VideoViewController ()
{
    IBOutlet UITableView * tbl;
    SOAPRequest * videoRequest;
    NSMutableArray * arrVideo;
    IBOutlet UIImageView * imgBgView;
}
@end

@implementation VideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Videos";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    tbl.backgroundColor = [UIColor clearColor];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncVideo];
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [delegate windowViewAdjust];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)syncVideo{
    [SVProgressHUD show];
    if (videoRequest == nil) {
        videoRequest = [[SOAPRequest alloc]initWithOwner:self];
    }
    videoRequest.processId = 13;
    [videoRequest syncVideo];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    //PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 13) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSLog(@"status of my pet %d",status);
        if (status == STATUS_ACTION_SUCCESS) {
            if ([dics objectForKey:@"videos"] != [NSNull null]) {
                NSMutableArray * arrRawPets = [dics objectForKey:@"videos"];
                if ([arrRawPets count] != 0) {
                    arrVideo = [[NSMutableArray alloc]initWithCapacity:[arrRawPets count]];
                    for(NSInteger i=0;i<[arrRawPets count];i++){
                        NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                        ObjVideo * objVideo = [[ObjVideo alloc]init];
                        if ([dicPet objectForKey:@"video_id"] != [NSNull null]) {
                            objVideo.idx = [[dicPet objectForKey:@"video_id"] intValue];
                        }
                        if ([dicPet objectForKey:@"video_name"] != [NSNull null]) {
                            objVideo.strName = [dicPet objectForKey:@"video_name"];
                        }
                        if ([dicPet objectForKey:@"video_url"] != [NSNull null]) {
                            objVideo.strLink = [dicPet objectForKey:@"video_url"];
                        }
                        
                        [arrVideo addObject:objVideo];
                        
                    }
                    NSLog(@"arr pet count %d",[arrVideo count]);
                    [self reloadData:YES];
                    
                }
            }
            [SVProgressHUD dismiss];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        else
        {
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            if (status == STATUS_ACTION_SUCCESS) {
                //ObjUser * objUser = [[ObjUser alloc]init];
                //objUser.strSession = [dics objectForKey:@"session_id"];
                //[delegate.db updateUser:objUser];
                [SVProgressHUD dismiss];
            }
            else if(status == STATUS_ACTION_FAILED){
                
                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
            }
            else if(status == 5){
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
                PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
                //nav.ownner = self;
                [self presentModalViewController:nav animated:YES];
            }
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrVideo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VideoTableCell";
	VideoTableCell *cell = (VideoTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VideoTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (VideoTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjVideo * obj = [arrVideo objectAtIndex:[indexPath row]];
    [cell loadTheCellWith:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjVideo * obj = [arrVideo objectAtIndex:indexPath.row];
    
   
    
    //NSString * strCorrectedLink = [self getYTUrlStr:obj.strLink];
    //NSLog(@"load string %@ and corrected link %@",obj.strLink,strCorrectedLink);
    //NSURL *URL = [NSURL URLWithString:obj.strLink];
    //http://www.youtube.com/v/B8_SE4JvFlc
    /*NSURL *URL = [NSURL URLWithString:@"http://www.youtube.com/watch?v=O7wfVu8Y4cc"];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    [self presentModalViewController:webViewController animated:YES];*/
    NSString * strUTubeId = [Utility extractYoutubeID:obj.strLink];
     NSLog(@"load string id %@",strUTubeId);
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:strUTubeId];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}

- (NSString*)getYTUrlStr:(NSString*)url
{
    if (url == nil)
        return nil;
    
    NSString *retVal = [url stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
    
    NSRange pos=[retVal rangeOfString:@"version"];
    if(pos.location == NSNotFound)
    {
        retVal = [retVal stringByAppendingString:@"?version=3&hl=en_EN"];
    }
    return retVal;
}

- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    [tbl reloadData];
    /*if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.0];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }*/
}

@end
