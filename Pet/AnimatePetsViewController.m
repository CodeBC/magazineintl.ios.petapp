//
//  MyPetViewController.m
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AnimatePetsViewController.h"
#import "ADSlidingViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "ObjPet.h"
#import "StringTable.h"
#import "MyPetCell.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#import "AddPetViewController.h"
#import "DetailPetViewController.h"
#import "NavBarButton.h"
#import "NavBarButton2.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "AlbumListCell.h"
#import "AnimateAlbumListViewController.h"
@interface AnimatePetsViewController ()
{
    IBOutlet UILabel * lblText;
    IBOutlet UITableView * tbl;
    SOAPRequest * myPetRequest;
    NSMutableArray * arrPet;
    IBOutlet UILabel * lblInstruction;
    IBOutlet UIButton * btnAddPet;
    IBOutlet UIImageView * imgBgView;
}
@end

@implementation AnimatePetsViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    /*UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 80, 44)];
     lblName.backgroundColor = [UIColor clearColor];
     lblName.textColor = [UIColor whiteColor];
     // NSString * strValueFontName=@"SerifaStd-Black";
     lblName.font = [UIFont systemFontOfSize:18];
     lblName.text= @"My Pets";*/
    self.navigationItem.title =  @"My Pets";
    
}

- (IBAction)onCancel:(id)sender{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)showInstruction:(BOOL)show{
    if (show) {
        lblInstruction.hidden = FALSE;
        btnAddPet.hidden =FALSE;
        tbl.hidden = TRUE;
    }
    else{
        lblInstruction.hidden = TRUE;
        btnAddPet.hidden =TRUE;
        tbl.hidden = FALSE;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncPets];
}

- (void)viewWillDisappear:(BOOL)animated{
    
}

- (void) syncPets{
    [SVProgressHUD show];
    if (myPetRequest == nil) {
        myPetRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    myPetRequest.processId = 7;
    [myPetRequest syncMyPet];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 7) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSLog(@"status of my pet %d",status);
        if (status == STATUS_ACTION_SUCCESS) {
            if ([dics objectForKey:@"pets"] != [NSNull null]) {
                NSMutableArray * arrRawPets = [dics objectForKey:@"pets"];
                if ([arrRawPets count] != 0) {
                    arrPet = [[NSMutableArray alloc]initWithCapacity:[arrRawPets count]];
                    for(NSInteger i=0;i<[arrRawPets count];i++){
                        NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                        ObjPet * objPet = [[ObjPet alloc]init];
                        if ([dicPet objectForKey:@"pet_id"] != [NSNull null]) {
                            objPet.pet_id = [[dicPet objectForKey:@"pet_id"] intValue];
                        }
                        if ([dicPet objectForKey:@"pet_name"] != [NSNull null]) {
                            objPet.strName = [dicPet objectForKey:@"pet_name"];
                        }
                        if ([dicPet objectForKey:@"pet_image"] != [NSNull null]) {
                            objPet.strImgLink = [dicPet objectForKey:@"pet_image"];
                            //objPet.strImgLink = @"http://swipetelecom.com/blog/wp-content/uploads/pets.jpg";
                        }
                        if ([dicPet objectForKey:@"pet_type"] != [NSNull null]) {
                            objPet.type_id = [[dicPet objectForKey:@"pet_type"] intValue];
                        }
                        if ([dicPet objectForKey:@"pet_breed_type"] != [NSNull null]) {
                            objPet.breed_type_id = [[dicPet objectForKey:@"pet_breed_type"] intValue];
                        }
                        if ([dicPet objectForKey:@"pet_type_name"] != [NSNull null]) {
                            objPet.strType = [dicPet objectForKey:@"pet_type_name"];
                        }
                        if ([dicPet objectForKey:@"pet_breed_type_name"] != [NSNull null]) {
                            objPet.strBreed = [dicPet objectForKey:@"pet_breed_type_name"];
                        }
                        if ([dicPet objectForKey:@"dob"] != [NSNull null]) {
                            objPet.strDob = [dicPet objectForKey:@"dob"];
                        }
                        if ([dicPet objectForKey:@"gender"] != [NSNull null]) {
                            objPet.strGender = [dicPet objectForKey:@"gender"];
                        }
                        if ([dicPet objectForKey:@"description"] != [NSNull null]) {
                            objPet.strDescription = [dicPet objectForKey:@"description"];
                        }
                        if ([dicPet objectForKey:@"albums"] != [NSNull null]) {
                            NSMutableArray * arrTempAlbums = [dicPet objectForKey:@"albums"];
                            objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrTempAlbums count]];
                            for(NSInteger y=0;y<[arrTempAlbums count];y++){
                                ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                                NSDictionary * dicAlbum = [arrTempAlbums objectAtIndex:y];
                                objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                                objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                                NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                                objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                                for(NSInteger m=0;m<[arrTempImages count];m++){
                                    NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                    ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                    objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];
                                    
                                    objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];
                                    
                                    objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];
                                    
                                    objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];
                                    
                                    [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                                }
                                
                                [objPet.arrAlbums addObject:objAlbum];
                            }
                        }
                        
                        
                        [arrPet addObject:objPet];
                        NSLog(@"arr pet count %d and objPet name %@ and id %d and album count %d",[arrPet count],objPet.strName,objPet.pet_id,[objPet.arrAlbums count]);
                    }
                    if ([arrPet count]>0) {
                        [self showInstruction:FALSE];
                        [self reloadData:YES];
                    }
                }
            }
            [SVProgressHUD dismiss];
        }
        else
        {
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            if (status == STATUS_ACTION_SUCCESS) {
                //ObjUser * objUser = [[ObjUser alloc]init];
                //objUser.strSession = [dics objectForKey:@"session_id"];
                //[delegate.db updateUser:objUser];
                [SVProgressHUD dismiss];
            }
            else if(status == STATUS_ACTION_FAILED){
                
                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
            }
            else if(status == 5){
                [SVProgressHUD showErrorWithStatus:strErrorMsg];
                PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
                //nav.ownner = self;
                [self presentModalViewController:nav animated:YES];
            }
        }
        
    }
    [SVProgressHUD dismiss];
}

#pragma mark - UITableViewDataSource

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 return [NSString stringWithFormat:@"Section %d", section];
 }*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrPet count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlbumListCell";
	AlbumListCell *cell = (AlbumListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AlbumListCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (AlbumListCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjPet * pet = [arrPet objectAtIndex:[indexPath row]];
    [cell loatTheCellWithPet:pet];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjPet * objPet = [arrPet objectAtIndex:indexPath.row];
    AnimateAlbumListViewController  *viewController = [[UIStoryboard storyboardWithName:@"AnimateMe" bundle:nil] instantiateViewControllerWithIdentifier:@"animateAlbums"];
    viewController.objP = objPet;
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
}

@end
