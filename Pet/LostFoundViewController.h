//
//  LostFoundViewController.h
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LostFoundViewController : PetBasedViewController
- (IBAction) leftBarButton:(UIBarButtonItem *)sender;
-(IBAction)onType:(id)sender;
-(IBAction)onBreed:(id)sender;
- (IBAction)onBrowse:(id)sender;
@end
