//
//  MessageTBCell.m
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MessageTBCell.h"
#import "ObjConversation.h"
#import "UIImageView+AFNetworking.h"

@implementation MessageTBCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) loadTheCellWith:(ObjConversation *)objConv{
    [imgProfileView setImageWithURL:[NSURL URLWithString:objConv.strReciverImgLink] placeholderImage:nil];
    lblName.text = objConv.strReciverName;
    lblMessage.text = objConv.strMessage;
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:objConv.timetick];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    lblDTime.text = [dateFormatter stringFromDate:date];
}

@end
