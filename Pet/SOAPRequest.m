//
//  SOAPRequest.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//
#import <CommonCrypto/CommonDigest.h>
#import "SOAPRequest.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "JSONKit.h"
#import "ObjUser.h"
#import "NSData+Base64.h"
#import "ObjPet.h"
#import "ObjReminder.h"
@implementation SOAPRequest

@synthesize responseData;
@synthesize owner, processId, rType;

- (id)initWithOwner:(id)del {
	if (self = [super init]) {
        self.owner = del;
    }
	return self;
}

- (NSString *) urlencode: (NSString *) url{
    NSArray *escapeChars = [NSArray arrayWithObjects:@";" , @"/" , @"?" , @":" ,
                            @"@" , @"&" , @"=" , @"+" ,
                            @"$" , @"," , @"[" , @"]",
                            @"#", @"!", @"'", @"(",
                            @")", @"*", nil];

    NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B" , @"%2F" , @"%3F" ,
                             @"%3A" , @"%40" , @"%26" ,
                             @"%3D" , @"%2B" , @"%24" ,
                             @"%2C" , @"%5B" , @"%5D",
                             @"%23", @"%21", @"%27",
                             @"%28", @"%29", @"%2A", nil];

    int len = [escapeChars count];

    NSMutableString *temp = [url mutableCopy];

    int i;
    for(i = 0; i < len; i++)
    {

        [temp replaceOccurrencesOfString: [escapeChars objectAtIndex:i]
                              withString:[replaceChars objectAtIndex:i]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, [temp length])];
    }

    NSString *out = [NSString stringWithString: temp];

    return out;
}

- (void) syncGenerateKey{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_GENR_KEY;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,GENR_KEY_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"client_platform=I&device_token=%@",delegate.strUdid];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncGenerateKeyWithToken:(NSString *) token{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    rType = REQUEST_TYPE_GENR_KEY;
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,GENR_KEY_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"client_platform=I&device_token=%@",token];
    responseData = [[NSMutableData alloc] init];
    NSURL * url = [NSURL URLWithString: fullURL];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSLog(@"loading The Key: %@", fullURL);
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncLoginWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_LOGIN;

    NSString * strParamPlatform = [NSString stringWithFormat:@"user_login[email]=%@&user_login[password]=%@",objUser.strName,objUser.strPassword];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,LOGIN_LINK];
    NSLog(@"link %@ and param %@",fullURL,strParamPlatform);
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,objUser.strPassword);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncRegisterWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_LOGIN;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"user[username]=%@&user[password]=%@&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[email]=%@&user[device_token]=%@&user[password_confirmation]=%@&user[postal_code]=%@&user[country_name]=%@",objUser.strName,objUser.strPassword,objUser.strFName,objUser.strLName,objUser.strGender,objUser.strEmail,objUser.strUDID,objUser.strPassword,objUser.strPostalCode,objUser.strCountry];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,REGISTER_LINK];
     NSLog(@"link %@ and param %@",fullURL,strParamPlatform);
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncEditProfileWithUser:(ObjUser *)objUser{
    rType = REQUEST_TYPE_PROFILE_EDIT;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    NSString * strParamPlatform = [NSString stringWithFormat:@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@",objUser.userId,objUser.strFName,objUser.strLName,objUser.strGender,objUser.strName,objUser.strProfileImgLink,objUser.strEmail,objUser.strFbLink,objUser.strWebLink,objUser.strPhone,objUser.strAddress,objUser.strDob];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@/%d?auth_token=%@",BASE_LINK,PROFILE_EDIT_LINK,objUser.userId,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"PUT"];
    NSLog(@"param %@",strParamPlatform);
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAddPet:(ObjPet *)objRemind{
    rType = REQUEST_TYPE_ADDPET;

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    NSString * strParamPlatform = [NSString stringWithFormat:@"pet[gender]=%@&pet[pet_name]=%@&pet[pet_image]=%@&pet[gender]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@",objRemind.strGender,objRemind.strName,objRemind.strImgLink,objRemind.strDob,objRemind.strBreed,objRemind.strType,objRemind.strDescription];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,ADDPET_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"link %@ and param %@",fullURL,strParamPlatform);
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAddRemind:(ObjReminder *)objRemind{
    rType = REQUEST_TYPE_REMINDER;

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    NSString * strParamPlatform = [NSString stringWithFormat:@"reminder[reminder_name]=%@&reminder[repeat_type]=%@&reminder[due_date]=%@",objRemind.strName,objRemind.strRepeatType,objRemind.strDate];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,REMINDER_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"param %@ and url %@",strParamPlatform,fullURL);
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncEditPet:(ObjPet *)objPet{
    rType = REQUEST_TYPE_EDITPET;

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"pet[gender]=%@&pet[pet_name]=%@&pet[profile_image]=%@&pet[dob]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@",objPet.strGender,objPet.strName,objPet.strImgLink,objPet.strDob,objPet.strBreed,objPet.strType,objPet.strDescription];

    //NSString * strParamPlatform = [NSString stringWithFormat:@"pet[pet_name]=%@",@"mayyar"];

    NSString * fullURL = [NSString stringWithFormat:@"%@%@/%d?auth_token=%@",BASE_LINK,EDITPET_LINK,objPet.pet_id,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"PUT"];
    NSLog(@"url %@ and param %@",fullURL,strParamPlatform);
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];

	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

// NSString * strParamPlatform = [NSString stringWithFormat:@"pet[gender]=%@&pet[pet_name]=%@&pet[profile_image]=%@&pet[dob]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@",objPet.strGender,objPet.strName,objPet.strImgLink,objPet.strDob,objPet.strBreed,objPet.strType,objPet.strDescription];

- (void) syncReportLostPet:(ObjPet *)objRemind{
    rType = REQUEST_TYPE_REPORT_LOST;

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
   // NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];

    NSString * strParamPlatform = [NSString stringWithFormat:@"lost[pet_id]=%d&lost[area_id]=%@&breed_type=%@&lost[lastseen_date]=%@&lost[description]=%@&lost[reward]=%@",objUser.idx,objRemind.strArea,objRemind.strBreed,objRemind.strLastSeen,objRemind.strDescription,objRemind.strReward];

    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,LOST_REPORT_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"param %@",strParamPlatform);
    //NSLog(@"loading The Key: %@ and sha1+salt %@", fullURL,encodedPass);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncAndCheckTheSessionKey{
    rType = REQUEST_TYPE_CHECK_SESSION;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@",strSession,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@",BASE_LINK,CHECK_LOGIN_SESSION_LINK];

    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"loading The Key: %@ and para: %@", fullURL,strParamPlatform);
    [request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncProfile{
    rType = REQUEST_TYPE_PROFILE;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    //NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@",strSession,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@/%d?auth_token=%@",BASE_LINK,PROFILE_LINK,objUser.userId,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"loading The Key: %@ ", fullURL);
    //[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncVideo{
    rType = REQUEST_TYPE_VEDIO;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,VIDEO_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"loading The Key: %@ ", fullURL);
    //[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncDeal{
    rType = REQUEST_TYPE_VEDIO;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,DEAL_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"loading The Key: %@ ", fullURL);
    //[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncLostAndFound{
    rType = REQUEST_TYPE_LOST_FOUND;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    //NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@",strSession,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,LOST_FOUND_BROWSE_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"loading The Key: %@ ", fullURL);
    //[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) syncMyPet{
    rType = REQUEST_TYPE_MYPET;
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    //NSString * strParamPlatform = [NSString stringWithFormat:@"session_id=%@&key=%@",strSession,strKey];
    NSString * fullURL = [NSString stringWithFormat:@"%@%@?auth_token=%@",BASE_LINK,MYPET_LINK,strSession];
    responseData = [[NSMutableData alloc] init];
	NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: fullURL]];
    [request setHTTPMethod:@"GET"];
    NSLog(@"loading The Key: %@", fullURL);
    //[request setHTTPBody:[strParamPlatform dataUsingEncoding:NSUTF8StringEncoding]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (NSString *)encodePasswordWithSHA1:(NSString *)str{
    NSString *hashkey = str;
    // PHP uses ASCII encoding, not UTF
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];

    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);

    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4 bytes
    NSString *hash = [out description];
    hash = [hash stringByReplacingOccurrencesOfString:@"Not specified" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];

    return hash;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];

    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;

    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;

            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }

        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }

    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
    intResponseStatusCode = 0;
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	/*UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message: [NSString stringWithFormat:@"Error: %@", [error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alert show];
     [alert release];*/
	/*NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	[connection release];
	responseData = nil;
	[responseData release];
    NSLog(@"error  %@",error);
    [owner onErrorLoad: processId];

    NSLog(@"responseString: %@", responseString);*/
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {

	//PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	responseData = nil;

   if( rType == REQUEST_TYPE_GENR_KEY ){
        NSLog(@"responseString: %@", responseString);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;

        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }

        NSDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];
        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];

    }
   else if( rType == REQUEST_TYPE_CHECK_SESSION ){
        NSLog(@"responseString: %@", responseString);
        NSLog(@"status code %d",intResponseStatusCode);
        NSError *error = nil;

        NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if (soapData == nil)
        {
            NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
            abort();
        }

        NSDictionary *dictionary;
        JSONDecoder* decoder = [[JSONDecoder alloc]
                                initWithParseOptions:JKParseOptionNone];
        dictionary = [decoder objectWithData:soapData];
        NSLog(@"Feed dictionary count %d",[dictionary count]);
        [owner onJsonLoaded:dictionary withProcessId:processId];

    }
   else if( rType == REQUEST_TYPE_LOGIN ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];

   }
   else if( rType == REQUEST_TYPE_REGISTER ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_PROFILE ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_PROFILE_EDIT ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_MYPET ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_ADDPET ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_EDITPET ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_REMINDER ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_LOST_FOUND ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_LOST_FOUND ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_VEDIO ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
   else if( rType == REQUEST_TYPE_DEAL ){
       NSLog(@"responseString: %@", responseString);
       NSLog(@"status code %d",intResponseStatusCode);
       NSError *error = nil;

       NSData *soapData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
       if (soapData == nil)
       {
           NSLog(@"Error reading file at %@\n%@", soapData, [error localizedFailureReason]);
           abort();
       }

       NSDictionary *dictionary;
       JSONDecoder* decoder = [[JSONDecoder alloc]
                               initWithParseOptions:JKParseOptionNone];
       dictionary = [decoder objectWithData:soapData];
       NSLog(@"Feed dictionary count %d",[dictionary count]);
       [owner onJsonLoaded:dictionary withProcessId:processId];
   }
}

- (void) readResults:(NSMutableDictionary*)dics processId:(NSString *) pid{
	//int proId = [pid intValue];
}

- (void) failedParsing:(NSError*)error processId:(NSString *) pid{
	//[owner onErrorLoad: self.processId];
}

@end
