//
//  BrowseLostAndFoundViewController.m
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "BrowseLostAndFoundViewController.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "NavBarButton1.h"
#import "BrowseLFTableCell.h"
#import "ObjLostFound.h"
#import <QuartzCore/QuartzCore.h>
#import "ObjPet.h"
#import "DetailFoundViewController.h"
#import "DetailLostViewController.h"
#import "TSActionSheet.h"
#import "NavBarButton8.h"
#import "Utility.h"

@interface BrowseLostAndFoundViewController ()
{
    IBOutlet UITableView * tbl;
    NSArray * arrFoundPet;
    NSArray * arrLostPet;
}

@end

@implementation BrowseLostAndFoundViewController
@synthesize arrLF;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton8 *btnSave = [[NavBarButton8 alloc] init];
	[btnSave addTarget:self action:@selector(showActionSheet:forEvent:) forControlEvents:UIControlEventTouchUpInside];
	[btnSave setImageWithName:@"btn_nav_filter"];
	UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithCustomView:btnSave];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = saveButton;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)setNavTitleViewWithName:(NSString *)strName{
    self.navigationItem.titleView = nil;
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= strName;
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"browser arr count %d",[arrLF count]);
    
    [self reloadAllData:YES];
}

- (NSMutableArray *)sortWithTimetick:(NSMutableArray *)arr{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intLastSeenTimestamp"
                                                 ascending:NO selector:nil];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [arr sortedArrayUsingDescriptors:sortDescriptors];
    //[arr sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return (NSMutableArray *)sortedArray;
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tbl.tag == 0) {
        return [arrLF count];
    }
    else if(tbl.tag == 1){
        return [arrLostPet count];
    }
    else if(tbl.tag == 2){
        return [arrFoundPet count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BrowseLFTableCell";
	BrowseLFTableCell *cell = (BrowseLFTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BrowseLFTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (BrowseLFTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjLostFound * obj = nil;
    if (tbl.tag == 0) {
        obj = [arrLF objectAtIndex:[indexPath row]];
    }
    else if(tbl.tag == 1){
        obj = [arrLostPet objectAtIndex:[indexPath row]];
    }
    else if(tbl.tag == 2){
        obj = [arrFoundPet objectAtIndex:[indexPath row]];
    }
    
    [cell loadTheCellWith:obj];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjLostFound * objLf = nil;
    
    if (tbl.tag == 0) {
        objLf = [arrLF objectAtIndex:[indexPath row]];
    }
    else if(tbl.tag == 1){
        objLf = [arrLostPet objectAtIndex:[indexPath row]];
    }
    else if(tbl.tag == 2){
        objLf = [arrFoundPet objectAtIndex:[indexPath row]];
    }
    
    if (objLf.intLostFoundType == 0) {
        DetailFoundViewController *viewController = [[UIStoryboard storyboardWithName:@"LostAndFound" bundle:nil] instantiateViewControllerWithIdentifier:@"detailFound"];
        viewController.objSelectedLf = objLf;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (objLf.intLostFoundType == 1) {
        DetailLostViewController *viewController = [[UIStoryboard storyboardWithName:@"LostAndFound" bundle:nil] instantiateViewControllerWithIdentifier:@"detailLost"];
        viewController.objSelectedLf = objLf;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

- (void) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Filter"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"All" block:^{
        NSLog(@"All");
        [self reloadAllData:YES];
    }];
    [actionSheet addButtonWithTitle:@"Lost Pet" block:^{
        NSLog(@"Lost");
        [self reloadLostPetData:YES];
    }];
    [actionSheet addButtonWithTitle:@"Found Pet" block:^{
        NSLog(@"Found");
        [self reloadFoundPetData:YES];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    //actionSheet.
    
    [actionSheet showWithTouch:event];
}

- (void)reloadAllData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    arrLF = [self sortWithTimetick:arrLF];
    tbl.tag = 0;
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
    
    [self setNavTitleViewWithName:@"All"];
}

- (void)reloadLostPetData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    if (arrLostPet != nil ) {
        arrLostPet = nil;
    }
    
    NSPredicate *preda = [NSPredicate predicateWithFormat:
                          @"strLostFoundType beginswith[c] %@",@"Lost"];
    arrLostPet = [arrLF filteredArrayUsingPredicate:preda];
    arrLostPet = [self sortWithTimetick:arrLostPet];
    tbl.tag = 1;
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
    
    [self setNavTitleViewWithName:@"Lost Pet"];
}

- (void)reloadFoundPetData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    
    if (arrFoundPet != nil ) {
        arrFoundPet = nil;
    }
    
    NSPredicate *preda = [NSPredicate predicateWithFormat:
                          @"strLostFoundType beginswith[c] %@",@"Found"];
    arrFoundPet = [arrLF filteredArrayUsingPredicate:preda];
    arrFoundPet = [self sortWithTimetick:arrFoundPet];
    tbl.tag = 2;
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
    
    [self setNavTitleViewWithName:@"Found Pet"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
