//
//  AlbumListViewController.m
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AnimateAlbumListViewController.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "UIImageView+AFNetworking.h"
#import "EditPetViewController.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "NavBarButton2.h"
#import "NavBarButton1.h"
#import "AnimateAlbumListCell.h"
#import "petAPIClient.h"
#import "AlbumViewController.h"

@interface AnimateAlbumListViewController ()
{
    NSMutableArray * arrAlbum;
    IBOutlet UITableView * tbl;
    IBOutlet UIImageView * imgBgView;
    UITextField * txtParentPassword;
}

@end

@implementation AnimateAlbumListViewController
@synthesize objP;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    
    //arrAlbum = [[NSMutableArray alloc]init];
    self.navigationItem.title = @"Albums";
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    // [obj]
    [self loadTheViewWith:objP];
}

- (void)loadTheViewWith:(ObjPet *)objPet{
    arrAlbum = objPet.arrAlbums;
    [self reloadData:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrAlbum count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AnimateAlbumListCell";
	AnimateAlbumListCell *cell = (AnimateAlbumListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AnimateAlbumListCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (AnimateAlbumListCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    ObjAlbum * objA = [arrAlbum objectAtIndex:[indexPath row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell loatTheCellWith:objA];
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjAlbum * objAlbum = [arrAlbum objectAtIndex:indexPath.row];
    AlbumViewController *viewController = [[UIStoryboard storyboardWithName:@"AnimateMe" bundle:nil] instantiateViewControllerWithIdentifier:@"AnimateAlbumView"];
    viewController.objAlbum = objAlbum;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
}

- (void) onAdd:(id)sender{
    [self dialogForNewAlbum];
}

- (void)dialogForNewAlbum{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Album" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];
    
    txtParentPassword.placeholder = @"Name";
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];
    
    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    
}

- (void)synNewAlbum:(NSString *)strName andPetId:(int)idx{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"album[pet_id]":[NSString stringWithFormat:@"%d", idx],@"album[album_name]":strName};
    NSLog(@"params %@",params);
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",ALBUM_NEW_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncPushMessagesWith:(NSString *)strMessage{
    
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            [self synNewAlbum:txtParentPassword.text andPetId:objP.pet_id];
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
        }
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
