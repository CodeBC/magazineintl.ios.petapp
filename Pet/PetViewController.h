//
//  PetViewController.h
//  Pet
//
//  Created by Zayar on 4/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADSlidingViewController.h"
#import "PetLoginViewController.h"
@interface PetViewController : ADSlidingViewController<petLoginViewControllerDelegate>

@end
