//
//  ProfileViewController.m
//  Pet
//
//  Created by Zayar on 4/29/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ProfileViewController.h"
#import "ADSlidingViewController.h"
#import "SOAPRequest.h"
#import "ObjUser.h"
#import "ObjPet.h"
#import "ObjAlbum.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "EditProfileViewController.h"
#import "ObjUser.h"
#import "UIImageView+AFNetworking.h"
#import "NavBarButton.h"
#import "NavBarButton3.h"
#import "Utility.h"
#import "UIImage+Category.h"
#import <QuartzCore/QuartzCore.h>
#import "WebImageOperations.h"
#import "CustomPetsBarView.h"
#import "DetailPetViewController.h"
#import "NavBarButton1.h"
#import "ProfileMapViewController.h"
#import "NeigbourViewController.h"
#import "petAPIClient.h"
#import "TTTAttributedLabel.h"
#import "NavBarButton7Message.h"
#import "SVModalWebViewController.h"
#import "FGalleryViewController.h"
#import "AddPetViewController.h"

@interface ProfileViewController ()
{
    SOAPRequest * profileRequest;
    IBOutlet UILabel * lblFnameLname;
    IBOutlet UILabel * lblGender;
    IBOutlet UILabel * lblAddress;
    IBOutlet UILabel * lblBod;
    IBOutlet UILabel * lblEmail;
    IBOutlet UILabel * lblUsername;
    IBOutlet UILabel * lblPhone;
    IBOutlet TTTAttributedLabel * lblFb;
    IBOutlet UILabel * lblWeb;
    IBOutlet UILabel * lblContactBg;
    IBOutlet UILabel * lblWebBg;
    IBOutlet UILabel * lblCountry;
    IBOutlet UILabel * lblPostal;
    IBOutlet UIScrollView * scrollView;
    IBOutlet UIActivityIndicatorView * activity;
    IBOutlet UILabel * lblLastedObject;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblBasicContestBg;
    IBOutlet UILabel * capBasicInfo;
    IBOutlet UILabel * capContacInfo;
    IBOutlet UILabel * capPh;
    IBOutlet UILabel * capFb;
    IBOutlet UILabel * capWeb;
    IBOutlet UILabel * capPets;
    IBOutlet UILabel * capCountry;
    IBOutlet UILabel * capPostal;
    IBOutlet UILabel * capPetNeigour;
    IBOutlet UILabel * capPetNeigourPostal;
    IBOutlet UILabel * capGender;
    IBOutlet UILabel * capAddress;
    IBOutlet UILabel * capBod;
    IBOutlet UILabel * capEmail;
    IBOutlet UILabel * capUsername;
    IBOutlet UILabel * lblPetNeigourBg;
    IBOutlet UIButton * btnLocationProvide;
    IBOutlet UILabel * lblNeigbourNotProvided;
    IBOutlet UILabel * lblNeigbour;
    IBOutlet UIButton * btnNeigbourLish;
    UILabel * lblNoPet;
    UITextField * txtParentPassword;

    UILabel * lblTitleName;

    CustomPetsBarView * thumbScrollBar;
    CustomPetsBarView * thumbUserScrollBar;
    CustomPetsBarView * thumbNeighborPostalUserScrollBar;
    BOOL isFromMap;

    NSMutableArray *networkCaptions;
    NSMutableArray *networkImages;
    FGalleryViewController *networkGallery;
    UIButton * btnHaveNoPet;
    
    AddPetViewController * addPetViewController;
}

@property (nonatomic,strong) IBOutlet UIImageView * imgProfile;
@end

@implementation ProfileViewController
@synthesize user;
@synthesize isFromOther;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) showLocationNotProvidedViews:(BOOL)show{
    if (show) {
        lblNeigbourNotProvided.hidden = FALSE;
        btnLocationProvide.hidden = FALSE;
        lblPetNeigourBg.hidden = FALSE;
        lblNeigbour.hidden = TRUE;
        btnNeigbourLish.hidden = TRUE;
        thumbUserScrollBar.hidden = TRUE;
        thumbNeighborPostalUserScrollBar.hidden = TRUE;

    }
    else{
        lblNeigbourNotProvided.hidden = TRUE;
        btnLocationProvide.hidden = TRUE;
        lblNeigbour.hidden = TRUE;
        btnNeigbourLish.hidden = TRUE;
        lblPetNeigourBg.hidden = TRUE;
        thumbUserScrollBar.hidden = FALSE;
        thumbNeighborPostalUserScrollBar.hidden = TRUE;
    }
}

- (IBAction) onNeibourList:(id)sender{
     NSLog(@"neigbour list");
        NeigbourViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"neigbour"];

        viewController.arrUsers = self.user.arrNeigbours;
        [self.navigationController pushViewController:viewController animated:YES];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self updatePressed:nil];
    //ADSlidingViewController *slidingViewController = [self slidingViewController];
    //[slidingViewController setLeftViewAnchorWidth:200];

    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [scrollView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
         //[imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [scrollView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        //[imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    [scrollView setContentSize:CGSizeMake(320, lblPetNeigourBg.frame.origin.y  + lblPetNeigourBg.frame.size.height)];
    self.imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgProfile.layer.borderWidth = 2.0;

    NSString * strValueFontName=@"SerifaStd-Black";
    //NSString * strTitleFont=@"Rotis Sans Serif Bold 65";
    NSString * strNormalFont=@"Agfa Rotis Sans Serif";
    capBasicInfo.font = [UIFont fontWithName:strValueFontName size:15];
    capContacInfo.font = [UIFont fontWithName:strValueFontName size:15];
    capPets.font = [UIFont fontWithName:strValueFontName size:16];
    capPetNeigour.font = [UIFont fontWithName:strValueFontName size:16];
    capPetNeigourPostal.font = [UIFont fontWithName:strValueFontName size:16];
    lblAddress.font = [UIFont fontWithName:strNormalFont size:12];
    //self.navigationController.navigationItem.title = @"Profile";
    capPh.font = [UIFont fontWithName:strNormalFont size:14];
    capFb.font = [UIFont fontWithName:strNormalFont size:14];
    capWeb.font = [UIFont fontWithName:strNormalFont size:14];
    capCountry.font = [UIFont fontWithName:strNormalFont size:14];
    capPostal.font = [UIFont fontWithName:strNormalFont size:14];
    capGender.font = [UIFont fontWithName:strNormalFont size:14];
    capAddress.font = [UIFont fontWithName:strNormalFont size:14];
    capBod.font = [UIFont fontWithName:strNormalFont size:14];
    capEmail.font = [UIFont fontWithName:strNormalFont size:14];
    capUsername.font = [UIFont fontWithName:strNormalFont size:14];
    
    lblPhone.font = [UIFont fontWithName:strNormalFont size:14];
    lblFb.font = [UIFont fontWithName:strNormalFont size:14];
    lblWeb.font = [UIFont fontWithName:strNormalFont size:14];
    lblCountry.font = [UIFont fontWithName:strNormalFont size:14];
    lblPostal.font = [UIFont fontWithName:strNormalFont size:14];
    lblUsername.font = [UIFont fontWithName:strNormalFont size:14];
    lblEmail.font = [UIFont fontWithName:strNormalFont size:14];
    lblAddress.font = [UIFont fontWithName:strNormalFont size:14];
    lblGender.font = [UIFont fontWithName:strNormalFont size:14];
    lblBod.font = [UIFont fontWithName:strNormalFont size:14];
    lblNeigbour.font = [UIFont fontWithName:strNormalFont size:14];
    lblNeigbourNotProvided.font = [UIFont fontWithName:strNormalFont size:14];
    lblFb.delegate = self;
    networkImages = [[NSMutableArray alloc] initWithCapacity:1];
    networkCaptions = [[NSMutableArray alloc] initWithCapacity:1];
}

- (void) manageNavBarButton{
    if (!isFromOther) {
        NavBarButton *btnBack = [[NavBarButton alloc] init];
        [btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = backButton;

        NavBarButton3 *btnEdit = [[NavBarButton3 alloc] init];
        [btnEdit addTarget:self action:@selector(rightBarButton:) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = editButton;

        btnNeigbourLish.hidden = FALSE;
        btnLocationProvide.hidden = FALSE;
        lblPetNeigourBg.hidden = FALSE;
        lblNeigbourNotProvided.hidden = FALSE;
        lblNeigbour.hidden = FALSE;
        capPetNeigour.hidden = FALSE;
        capPetNeigourPostal.hidden = FALSE;
        [scrollView setContentSize:CGSizeMake(320, capPetNeigourPostal.frame.origin.y  + capPetNeigourPostal.frame.size.height + 118 + 30 + 118)];
    }
    else{
        NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
        [btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = backButton;
        self.navigationItem.rightBarButtonItem = nil;

        NavBarButton7Message *btnEdit = [[NavBarButton7Message alloc] init];
        [btnEdit addTarget:self action:@selector(messageButton:) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = editButton;

        btnNeigbourLish.hidden = TRUE;
        btnLocationProvide.hidden = TRUE;
        lblPetNeigourBg.hidden = TRUE;
        lblNeigbourNotProvided.hidden = TRUE;
        lblNeigbour.hidden = TRUE;
        capPetNeigour.hidden = TRUE;
        capPetNeigourPostal.hidden = TRUE;
        thumbUserScrollBar.hidden = TRUE;
        thumbNeighborPostalUserScrollBar.hidden = TRUE;

        [scrollView setContentSize:CGSizeMake(320, capPets.frame.origin.y  + capPets.frame.size.height + 180)];
    }
}

- (void) goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)customLongBornDate:(NSString *)strDate{
    //NSDate * date = [self dateFromInternetDateTimeString:strDate];
     NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-d"];
    NSDate * date = [formatter dateFromString:strDate];
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];
    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:@"MMMM dd, yyyy"];
    /*NSString *prefixDateString = [prefixDateFormatter stringFromDate:date];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    //NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSString *suffix_string= @"";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    NSString *dateString = [prefixDateString stringByAppendingString:suffix];
    NSLog(@"%@", dateString);*/
    NSString *dateString = [prefixDateFormatter stringFromDate:date];
    return dateString;
}

- (NSDate *)dateFromInternetDateTimeString:(NSString *)dateString {

    // Setup Date & Formatter
    NSDate *date = nil;
    static NSDateFormatter *formatter = nil;
    if (!formatter) {
        NSLocale *en_US_POSIX = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:en_US_POSIX];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];

    }

    /*
     *  RFC3339
     */

    NSString *RFC3339String = [[NSString stringWithString:dateString] uppercaseString];
    RFC3339String = [RFC3339String stringByReplacingOccurrencesOfString:@"Z" withString:@"-0000"];

    // Remove colon in timezone as iOS 4+ NSDateFormatter breaks
    // See https://devforums.apple.com/thread/45837
    if (RFC3339String.length > 20) {
        RFC3339String = [RFC3339String stringByReplacingOccurrencesOfString:@":"
                                                                 withString:@""
                                                                    options:0
                                                                      range:NSMakeRange(20, RFC3339String.length-20)];
    }

    if (!date) { // 1996-12-19T16:39:57-0800
        [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"];
        date = [formatter dateFromString:RFC3339String];
    }
    if (!date) { // 1937-01-01T12:00:27.87+0020
        [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZZZ"];
        date = [formatter dateFromString:RFC3339String];
    }
    if (!date) { // 1937-01-01T12:00:27
        [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
        date = [formatter dateFromString:RFC3339String];
    }
    if (date) return date;

    /*
     *  RFC822
     */

    NSString *RFC822String = [[NSString stringWithString:dateString] uppercaseString];
    if (!date) { // Sun, 19 May 02 15:21:36 GMT
        [formatter setDateFormat:@"EEE, d MMM yy HH:mm:ss zzz"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) { // Sun, 19 May 2002 15:21:36 GMT
        [formatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // Sun, 19 May 2002 15:21 GMT
        [formatter setDateFormat:@"EEE, d MMM yyyy HH:mm zzz"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // 19 May 2002 15:21:36 GMT
        [formatter setDateFormat:@"d MMM yyyy HH:mm:ss zzz"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // 19 May 2002 15:21 GMT
        [formatter setDateFormat:@"d MMM yyyy HH:mm zzz"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // 19 May 2002 15:21:36
        [formatter setDateFormat:@"d MMM yyyy HH:mm:ss"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // 19 May 2002 15:21
        [formatter setDateFormat:@"d MMM yyyy HH:mm"];
        date = [formatter dateFromString:RFC822String];
    }
    if (!date) {  // 2012-05-11
        [formatter setDateFormat:@"yyyy-MM-d"];
        date = [formatter dateFromString:RFC822String];
    }
    if (date) return date;

    // Failed
    return nil;

}

- (void) showPetThumbBar:(NSMutableArray *)arrPet{
    if ([arrPet count]>0) {
        if( thumbScrollBar == nil ){
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPetsBarView" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[CustomPetsBarView class]]){
                    thumbScrollBar = (CustomPetsBarView *) currentObject;
                    thumbScrollBar.bounds = self.view.bounds;
                    thumbScrollBar.frame = CGRectMake(10,capPets.frame.origin.y + capPets.frame.size.height, 300, 118);
                    thumbScrollBar.owner = self;
                    
                    [scrollView addSubview: thumbScrollBar];
                    
                    break;
                }
            }
        }
        NSLog(@"here is scroll loaded!!");
        [thumbScrollBar loadThumbScrollBarWith:arrPet];
        thumbScrollBar.hidden = FALSE;
        [self.view bringSubviewToFront:thumbScrollBar];
        lblNoPet.hidden = TRUE;
         btnHaveNoPet.hidden = TRUE;
    }
    else if([arrPet count] == 0){
        NSLog(@"here is no pet!!");
        if (!lblNoPet) {
            lblNoPet = [[UILabel alloc] initWithFrame:CGRectMake(10,capPets.frame.origin.y + capPets.frame.size.height, 300, 118)];
        }
        lblNoPet.hidden = FALSE;
        lblNoPet.backgroundColor = [UIColor clearColor];
        NSString * strNormalFont=@"Agfa Rotis Sans Serif";
        lblNoPet.font = [UIFont fontWithName:strNormalFont size:15];
        lblNoPet.textAlignment = NSTextAlignmentCenter;
        lblNoPet.text = @"You have no pet. Tap here to add pet.";
        lblNoPet.textColor = [UIColor blackColor];
        [scrollView addSubview:lblNoPet];
        
        if (!btnHaveNoPet) {
            btnHaveNoPet = [UIButton buttonWithType:UIButtonTypeCustom];
        }
        btnHaveNoPet.hidden = FALSE;
        [btnHaveNoPet setFrame:lblNoPet.frame];
        [btnHaveNoPet setTitle:@"" forState:normal];
        [btnHaveNoPet addTarget:self action:@selector(onNoPet:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:btnHaveNoPet];
    }
    
}

- (void)onNoPet:(UIButton *)sender{
    if (!addPetViewController) {
        addPetViewController  = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"addpet"];
    }
    [self.navigationController pushViewController:addPetViewController animated:YES];
}

- (void) showUserThumbBar:(NSMutableArray *)arrUser{
    if( thumbUserScrollBar == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPetsBarView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CustomPetsBarView class]]){
				thumbUserScrollBar = (CustomPetsBarView *) currentObject;
				thumbUserScrollBar.bounds = self.view.bounds;
				thumbUserScrollBar.frame = CGRectMake(10,capPetNeigour.frame.origin.y + capPetNeigour.frame.size.height + 5, 300, 118);
                thumbUserScrollBar.owner = self;

				[scrollView addSubview: thumbUserScrollBar];

				break;
			}
		}
	}
    NSLog(@"here is scroll loaded!!");
    [thumbUserScrollBar loadThumbScrollBarWithUser:arrUser];
    if (isFromOther) {
        thumbUserScrollBar.hidden = TRUE;
    }
    else{
        thumbUserScrollBar.hidden = FALSE;
    }

    [self.view bringSubviewToFront:thumbUserScrollBar];
}

- (void) showNeighborPostalThumbBar:(NSMutableArray *)arrUser{
    if( thumbNeighborPostalUserScrollBar == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomPetsBarView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[CustomPetsBarView class]]){
				thumbNeighborPostalUserScrollBar = (CustomPetsBarView *) currentObject;
				thumbNeighborPostalUserScrollBar.bounds = self.view.bounds;
				thumbNeighborPostalUserScrollBar.frame = CGRectMake(10,capPetNeigourPostal.frame.origin.y + capPetNeigourPostal.frame.size.height + 5, 300, 118);
                thumbNeighborPostalUserScrollBar.owner = self;

				[scrollView addSubview: thumbNeighborPostalUserScrollBar];

				break;
			}
		}
	}
    NSLog(@"here is scroll loaded!!");
    [thumbNeighborPostalUserScrollBar loadThumbScrollBarWithUser:arrUser];
    if (isFromOther) {
        thumbNeighborPostalUserScrollBar.hidden = TRUE;
    }
    else{
        thumbNeighborPostalUserScrollBar.hidden = FALSE;
    }
    [self.view bringSubviewToFront:thumbNeighborPostalUserScrollBar];
}

- (void) onThumbScrollBarSelected:(ObjPet *)selectedObjPet{
    NSLog(@"obj pet selected!!");

}

- (void) onThumbScrollBarIConClick:(ObjPet *)selectedObjPet{
    // NSLog(@"obj pet selected!!");
    DetailPetViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"petDetail"];
    viewController.objPet = selectedObjPet;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) onThumbScrollUserBarSelected:(ObjUser *)selectedObjUser{
    NSLog(@"user thumb selected");
    ProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"uprofile"];

    //viewController.pe = objAdo;
    viewController.user = selectedObjUser;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    if (!isFromMap) {
        if (!isFromOther) {
            //[self.imgProfile setFrame:CGRectMake(0, 0, 320, 203)];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            ObjUser * user1 = [delegate.db getUserObj];
            [self reloadProfileView:user1];

            //[self reloadProfileTheViewWith:user];
            [self syncProfile];
             capPets.text = @"My Pets";
        }
        else{
            //[self reloadProfileView:self.user];
            [self syncProfileWith:self.user.idx];
            capPets.text = @"Pets";
        }

        [self.imgProfile setImage:[UIImage imageNamed:@"img_profile_default.png"]];
        [self manageNavBarButton];

    }
}

/*- (void)viewWillDisappear:(BOOL)animated{
    isFromHome = FALSE;
}*/

- (void) reloadProfileTheViewWith:(ObjUser *)objUser{
    for (UIView * v in scrollView.subviews) {
        [v removeFromSuperview];
    }
    float y=0.0;
    UIImageView * imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 203)];
    NSString * strValueFontName=@"AvenirNext-Bold";
    float fontSizeOfValue = 16;
    [imgView setBackgroundColor:[UIColor grayColor]];
    [scrollView addSubview:imgView];
    if (objUser) {
        NSLog(@"user strProfile image link %@",objUser.strProfileImgLink);
        //[activity startAnimating];
        /*[WebImageOperations processImageDataWithURLString:objUser.strProfileImgLink andBlock:^(NSData *imageData) {
            if (self.view) {
                UIImage *image = [UIImage imageWithData:imageData];
                UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(imgView.frame.size.width,imgView.frame.size.height) resizeFrame:NO];
                //[self o]
                [imgView setImage:[cropedImage overlayWith:[UIImage imageNamed: @"img_gradient_overlay.png"] overlayPoint: CGPointMake(0,0)]];
                //[activity stopAnimating];
                //[self.imgProfile setImage:image];
            }
        }];*/

        UILabel * lblAddre = [[UILabel alloc]initWithFrame:CGRectMake(5,149,181,42)];
        lblAddre.font = [UIFont fontWithName:@"Avenir Next" size:16];
        lblAddre.textColor = [UIColor whiteColor];
        lblAddre.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblAddre];

        lblAddre.text =[NSString stringWithFormat:@"%@ %@",objUser.strFName, objUser.strLName];

        UILabel * capOfTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,211,166,21)];
        capOfTitle.font = [UIFont fontWithName:@"Avenir Next" size:14];
        capOfTitle.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        //capOfTitle.textColor = [UIColor blackColor];
        capOfTitle.backgroundColor = [UIColor clearColor];
        capOfTitle.numberOfLines = 0;
        capOfTitle.text = @"Personal Information";
        [scrollView addSubview:capOfTitle];

        UILabel * bgOfContact = [[UILabel alloc]initWithFrame:CGRectMake(10,232,300,76)];
        bgOfContact.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfContact.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfContact.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfContact];
        [self makeRoundandBorder:bgOfContact];
        UILabel * titleOfContact = [[UILabel alloc]initWithFrame:CGRectMake(20,240,280,24)];
        titleOfContact.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfContact.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfContact.text = @"Contact:";
        //bgOfContact.text = @"Personal Information";
        titleOfContact.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfContact];
        UILabel * lblContactNo = [[UILabel alloc]initWithFrame:CGRectMake(20,272,280,31)];
        lblContactNo.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblContactNo.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblContactNo.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblContactNo];
        if (![objUser.strPhone isEqualToString:@""]) {
            lblContactNo.text = objUser.strPhone;
        }

        UILabel * bgOfWebsite = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfContact.frame.origin.y+bgOfContact.frame.size.height,300,76)];
        bgOfWebsite.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfWebsite.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfWebsite.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfWebsite];
        [self makeRoundandBorder:bgOfWebsite];

        UILabel * titleOfWebsite = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfWebsite.frame.origin.y+8,280,24)];
        titleOfWebsite.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfWebsite.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfWebsite.text = @"Website:";
        //bgOfContact.text = @"Personal Information";
        titleOfWebsite.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfWebsite];

        UILabel * lblWebsite = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfWebsite.frame.origin.y+40,280,31)];
        lblWebsite.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblWebsite.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblWebsite.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblWebsite];
        if (![objUser.strWebLink isEqualToString:@""]) {
            lblWebsite.text = objUser.strWebLink;
        }

        UILabel * bgOfFB = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfWebsite.frame.origin.y+bgOfWebsite.frame.size.height,300,76)];
        bgOfFB.font = [UIFont fontWithName:@"Avenir Next" size:17];

        bgOfFB.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfFB.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfFB];
        [self makeRoundandBorder:bgOfFB];

        UILabel * titleOfFb = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFB.frame.origin.y+8,280,24)];
        titleOfFb.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfFb.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfFb.text = @"Facebook:";
        //bgOfContact.text = @"Personal Information";
        titleOfFb.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfFb];

        UILabel * lblFbLink = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFB.frame.origin.y+40,280,31)];
        lblFbLink.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblFbLink.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblFbLink.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblFbLink];
        if (![objUser.strFbLink isEqualToString:@""]) {
            lblFbLink.text = objUser.strFbLink;
        }

        UILabel * bgOfFBName = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfFB.frame.origin.y+bgOfFB.frame.size.height,300,76)];
        bgOfFBName.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfFBName.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfFBName.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfFBName];
        [self makeRoundandBorder:bgOfFBName];

        UILabel * titleOfFbName = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFBName.frame.origin.y+8,280,24)];
        titleOfFbName.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfFbName.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfFbName.text = @"Facebook Name:";
        //bgOfContact.text = @"Personal Information";
        titleOfFbName.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfFbName];

        UILabel * lblFbName = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfFBName.frame.origin.y+40,280,31)];
        lblFbName.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        //lblFbName.font = [UIFont boldSystemFontOfSize:16];
        lblFbName.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblFbName.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblFbName];
        if (![objUser.strFName isEqualToString:@""]) {
            lblFbName.text = objUser.strFName;
        }

        UILabel * bgOfGender = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfFBName.frame.origin.y+bgOfFBName.frame.size.height,300,76)];
        bgOfGender.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfGender.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfGender.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfGender];
        [self makeRoundandBorder:bgOfGender];

        UILabel * titleOfGender = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfGender.frame.origin.y+8,280,24)];
        titleOfGender.font = [UIFont fontWithName:@"Avenir Next" size:fontSizeOfValue];
        titleOfGender.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfGender.text = @"Gender:";
        //bgOfContact.text = @"Personal Information";
        titleOfGender.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfGender];

        UILabel * lblGender = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfGender.frame.origin.y+40,280,31)];
        lblGender.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblGender.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblGender.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblGender];
        if (![objUser.strGender isEqualToString:@""]) {
            lblGender.text = objUser.strGender;
        }

        UILabel * bgOfDob = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfGender.frame.origin.y+bgOfGender.frame.size.height,300,76)];
        bgOfDob.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfDob.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfDob.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfDob];
        [self makeRoundandBorder:bgOfDob];

        UILabel * titleOfDob = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfDob.frame.origin.y+8,280,24)];
        titleOfDob.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfDob.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfDob.text = @"Date of Birth:";
        //bgOfContact.text = @"Personal Information";
        titleOfDob.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfDob];

        UILabel * lblDOB1 = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfDob.frame.origin.y+40,280,31)];
        lblDOB1.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblDOB1.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblDOB1.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblDOB1];
        if (![objUser.strDob isEqualToString:@""]) {
            lblDOB1.text = objUser.strDob;
        }

        UILabel * bgOfEmail = [[UILabel alloc]initWithFrame:CGRectMake(10,bgOfDob.frame.origin.y+bgOfDob.frame.size.height,300,76)];
        bgOfEmail.font = [UIFont fontWithName:@"Avenir Next Medium" size:17];
        bgOfEmail.textColor = [UIColor colorWithRed:123/255 green:123/255 blue:123/255 alpha:1.0f];
        bgOfEmail.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
        [scrollView addSubview:bgOfEmail];
        [self makeRoundandBorder:bgOfEmail];

        UILabel * titleOfEmail = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfEmail.frame.origin.y+8,280,24)];
        titleOfEmail.font = [UIFont fontWithName:@"Avenir Next" size:16];
        titleOfEmail.textColor = [UIColor colorWithRed:123.0f/255 green:123.0f/255 blue:123.0f/255 alpha:1.0f];
        titleOfEmail.text = @"Email:";
        //bgOfContact.text = @"Personal Information";
        titleOfEmail.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:titleOfEmail];

        UILabel * lblEmail = [[UILabel alloc]initWithFrame:CGRectMake(20,bgOfEmail.frame.origin.y+40,280,31)];
        lblEmail.font = [UIFont fontWithName:strValueFontName size:fontSizeOfValue];
        lblEmail.textColor = [UIColor colorWithRed:100.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
        //lblContactNo.text = @"Contact";
        //bgOfContact.text = @"Personal Information";
        lblEmail.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:lblDOB1];
        if (![objUser.strEmail isEqualToString:@""]) {
            lblEmail.text = objUser.strEmail;
        }

        y += lblEmail.frame.origin.y + lblEmail.frame.size.height;
        [scrollView setContentSize:CGSizeMake(320, y)];
    }
}

- (void) syncProfile{
    [SVProgressHUD show];
    if (profileRequest == nil) {
        profileRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    profileRequest.processId = 5;
    [profileRequest syncProfile];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 5) {
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strErrorMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            NSDictionary * dic = [dics objectForKey:@"user"];
            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strSession = [dics objectForKey:@"session_id"];
            if (dic != nil) {
                ObjUser * obj = [[ObjUser alloc]init];
                if ([dic objectForKey:@"user_id"] != [NSNull null]) {
                    obj.userId = [[dic objectForKey:@"user_id"] intValue];
                }
                else obj.userId = 0;

                if ([dic objectForKey:@"first_name"] != [NSNull null]) {
                    obj.strFName = [dic objectForKey:@"first_name"];
                }
                else obj.strFName = @"";

                if ([dic objectForKey:@"last_name"] != [NSNull null]) {
                    obj.strLName = [dic objectForKey:@"last_name"];
                }
                else obj.strLName = @"";

                if ([dic objectForKey:@"gender"] != [NSNull null]) {
                    obj.strGender = [dic objectForKey:@"gender"];
                }
                else obj.strGender = @"";

                if ([dic objectForKey:@"username"] != [NSNull null]) {
                    obj.strName = [dic objectForKey:@"username"];
                }
                else obj.strName = @"";

                if ([dic objectForKey:@"profile_image"] != [NSNull null]) {
                    obj.strProfileImgLink = [dic objectForKey:@"profile_image"];
                }
                else obj.strProfileImgLink = @"";

                if ([dic objectForKey:@"email"] != [NSNull null]) {
                    obj.strEmail = [dic objectForKey:@"email"];
                }
                else obj.strEmail = @"";

                if ([dic objectForKey:@"facebook"] != [NSNull null]) {
                    obj.strFbLink = [dic objectForKey:@"facebook"];
                }
                else obj.strFbLink = @"";

                if ([dic objectForKey:@"website"] != [NSNull null]) {
                    obj.strWebLink = [dic objectForKey:@"website"];
                }
                else obj.strWebLink = @"";

                if ([dic objectForKey:@"phone"] != [NSNull null]) {
                    obj.strPhone = [dic objectForKey:@"phone"];
                }
                else obj.strPhone = @"";

                obj.strAddress = @"";
                if ([dic objectForKey:@"address"] != [NSNull null]) {
                    obj.strAddress = [dic objectForKey:@"address"];
                }
                else obj.strAddress = @"";

                if ([dic objectForKey:@"dob"] != [NSNull null]) {
                    if([[dic objectForKey:@"dob"] length] == 0) {
                        obj.strDob = @"";
                    } else {
                        obj.strDob = [dic objectForKey:@"dob"];
                    }
                }
                else obj.strDob = @"";

                if ([dic objectForKey:@"country_name"] != [NSNull null]) {
                    obj.strCountry = [dic objectForKey:@"country_name"];
                }
                else obj.strCountry = @"";

                if ([dic objectForKey:@"postal_code"] != [NSNull null]) {
                    obj.strPostalCode = [dic objectForKey:@"postal_code"];
                }
                else obj.strPostalCode = @"";
                
                if ([dic objectForKey:@"privacy"] != [NSNull null]) {
                    obj.privacy_id = [[dic objectForKey:@"privacy"] intValue];
                }
                else obj.privacy_id = 0;

                if ([dic objectForKey:@"has_location_provided"] != [NSNull null]) {
                    obj.hasLocationProvided = [[dic objectForKey:@"has_location_provided"]intValue];
                    NSLog(@"has location %d",obj.hasLocationProvided);
                }
                if ([dic objectForKey:@"pets"] != [NSNull null]) {
                    NSMutableArray * arrRawPets = [dic objectForKey:@"pets"];
                    obj.arrPets = [[NSMutableArray alloc]initWithCapacity:arrRawPets.count];
                    for(NSInteger i=0;i<[arrRawPets count];i++){
                        NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                        ObjPet * objPet = [[ObjPet alloc]init];
                        if ([dicPet objectForKey:@"pet_id"] != [NSNull null]) {
                            objPet.pet_id = [[dicPet objectForKey:@"pet_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"pet_name"] != [NSNull null]) {
                            objPet.strName = [dicPet objectForKey:@"pet_name"];
                        }
                        else objPet.strName = @"";

                        if ([dicPet objectForKey:@"pet_image"] != [NSNull null]) {
                            objPet.strImgLink = [dicPet objectForKey:@"pet_image"];
                        }
                        else objPet.strImgLink = @"";

                        if ([dicPet objectForKey:@"pet_type_name"] != [NSNull null]) {
                            objPet.strType = [dicPet objectForKey:@"pet_type_name"];
                        }
                        else objPet.strType = @"";

                        if ([dicPet objectForKey:@"pet_breed_type_name"] != [NSNull null]) {
                            objPet.strBreed = [dicPet objectForKey:@"pet_breed_type_name"];
                        }
                        else objPet.strBreed = @"";

                        if ([dicPet objectForKey:@"dob"] != [NSNull null]) {
                            objPet.strDob = [dicPet objectForKey:@"dob"];
                        }
                        else objPet.strDob = @"";

                        if ([dicPet objectForKey:@"description"] != [NSNull null]) {
                            objPet.strDescription = [dicPet objectForKey:@"description"];
                        }
                        else objPet.strDescription = @"";

                        if ([dicPet objectForKey:@"pet_type_id"] != [NSNull null]) {
                            objPet.type_id = [[dicPet objectForKey:@"pet_type_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"pet_breed_type_id"] != [NSNull null]) {
                            objPet.breed_type_id = [[dicPet objectForKey:@"pet_breed_type_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"albums"] != [NSNull null]) {
                            NSMutableArray * arrTempAlbums = [dicPet objectForKey:@"albums"];
                            objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrTempAlbums count]];
                            for(NSInteger y=0;y<[arrTempAlbums count];y++){
                                ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                                NSDictionary * dicAlbum = [arrTempAlbums objectAtIndex:y];
                                objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                                objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                                NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                                objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                                for(NSInteger m=0;m<[arrTempImages count];m++){
                                    NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                    ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                    objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];

                                    objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];

                                    objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];

                                    objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];

                                    [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                                }

                                [objPet.arrAlbums addObject:objAlbum];
                            }
                        }

                        [obj.arrPets addObject:objPet];
                    }

                    //[self reloadProfileTheViewWith:user];
                }
                if (obj.hasLocationProvided == 1) {
                    if ([dic objectForKey:@"neighbours"] != [NSNull null] || ![self stringIsEmpty:[dic objectForKey:@"neighbours"] shouldCleanWhiteSpace:YES ]) {
                        NSMutableArray * arrRawNeigbour = [dic objectForKey:@"neighbours"];
                        obj.arrNeigbours = [[NSMutableArray alloc]init];
                        obj.arrNeigbourPostal = [[NSMutableArray alloc]init];
                        for(NSInteger n=0;n<[arrRawNeigbour count];n++){
                            NSDictionary * dicNeig = [arrRawNeigbour objectAtIndex:n];
                            ObjUser * objNeig = [[ObjUser alloc]init];
                            if ([dicNeig objectForKey:@"user_id"] != [NSNull null]) {
                                objNeig.userId = [[dicNeig objectForKey:@"user_id"] intValue];
                            }
                            if ([dicNeig objectForKey:@"id"] != [NSNull null]) {
                                objNeig.idx = [[dicNeig objectForKey:@"id"] intValue];
                            }
                            if ([dicNeig objectForKey:@"first_name"] != [NSNull null]) {
                                objNeig.strFName = [dicNeig objectForKey:@"first_name"];
                            }
                            else objNeig.strFName = @"";

                            if ([dicNeig objectForKey:@"last_name"] != [NSNull null]) {
                                objNeig.strLName = [dicNeig objectForKey:@"last_name"];
                            }
                            else objNeig.strLName = @"";

                            if ([dicNeig objectForKey:@"gender"] != [NSNull null]) {
                                objNeig.strGender = [dic objectForKey:@"gender"];
                            }
                            else objNeig.strGender = @"";

                            if ([dicNeig objectForKey:@"username"] != [NSNull null]) {
                                objNeig.strName = [dicNeig objectForKey:@"username"];
                            }
                            else objNeig.strName = @"";

                            if ([dicNeig objectForKey:@"profile_image"] != [NSNull null]) {
                                objNeig.strProfileImgLink = [dicNeig objectForKey:@"profile_image"];
                            }
                            else objNeig.strProfileImgLink = @"";

                            if ([dicNeig objectForKey:@"email"] != [NSNull null]) {
                                objNeig.strEmail = [dicNeig objectForKey:@"email"];
                            }
                            if ([dicNeig objectForKey:@"facebook"] != [NSNull null]) {
                                objNeig.strFbLink = [dicNeig objectForKey:@"facebook"];
                            }
                            else objNeig.strFbLink = @"";

                            if ([dicNeig objectForKey:@"website"] != [NSNull null]) {
                                objNeig.strWebLink = [dicNeig objectForKey:@"website"];
                            }
                            else objNeig.strWebLink = @"";

                            if ([dicNeig objectForKey:@"phone"] != [NSNull null]) {
                                objNeig.strPhone = [dicNeig objectForKey:@"phone"];
                            }
                            else objNeig.strPhone = @"";

                            if ([dicNeig objectForKey:@"address"] != [NSNull null]) {
                                objNeig.strAddress = [dicNeig objectForKey:@"address"];
                            }
                            else objNeig.strAddress = @"";

                            if ([dicNeig objectForKey:@"dob"] != [NSNull null]) {
                                objNeig.strDob = [dicNeig objectForKey:@"dob"];
                            }
                            else objNeig.strDob = @"";

                            if ([dicNeig objectForKey:@"distance"] != [NSNull null]) {
                                objNeig.strDistance = [dicNeig objectForKey:@"distance"];
                            }
                            else objNeig.strDistance = @"";

                            if ([dicNeig objectForKey:@"type"] != [NSNull null]) {
                                objNeig.neighbor_type = [[dicNeig objectForKey:@"type"] intValue];
                            }
                            else objNeig.neighbor_type = 0;

                            if ([dicNeig objectForKey:@"pets"] != [NSNull null]) {
                                NSMutableArray * arrRawNeigMyPets = [dicNeig objectForKey:@"pets"];
                                objNeig.arrPets = [[NSMutableArray alloc]initWithCapacity:arrRawNeigMyPets.count];
                                for(NSInteger i=0;i<[arrRawNeigMyPets count];i++){
                                    NSDictionary * dicNeighPet = [arrRawNeigMyPets objectAtIndex:i];
                                    ObjPet * objPet = [[ObjPet alloc]init];
                                    if ([dicNeighPet objectForKey:@"pet_id"] != [NSNull null]) {
                                        objPet.pet_id = [[dicNeighPet objectForKey:@"pet_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"pet_name"] != [NSNull null]) {
                                        objPet.strName = [dicNeighPet objectForKey:@"pet_name"];
                                    }
                                    else objPet.strName = @"";

                                    if ([dicNeighPet objectForKey:@"pet_image"] != [NSNull null]) {
                                        objPet.strImgLink = [dicNeighPet objectForKey:@"pet_image"];
                                    }
                                    else objPet.strImgLink = @"";

                                    if ([dicNeighPet objectForKey:@"pet_type_name"] != [NSNull null]) {
                                        objPet.strType = [dicNeighPet objectForKey:@"pet_type_name"];
                                    }
                                    else objPet.strType = @"";

                                    if ([dicNeighPet objectForKey:@"pet_breed_type_name"] != [NSNull null]) {
                                        objPet.strBreed = [dicNeighPet objectForKey:@"pet_breed_type_name"];
                                    }
                                    else objPet.strBreed = @"";

                                    if ([dicNeighPet objectForKey:@"dob"] != [NSNull null]) {
                                        objPet.strDob = [dicNeighPet objectForKey:@"dob"];
                                    }
                                    else objPet.strDob = @"";

                                    if ([dicNeighPet objectForKey:@"description"] != [NSNull null]) {
                                        objPet.strDescription = [dicNeighPet objectForKey:@"description"];
                                    }
                                    else objPet.strDescription = @"";

                                    if ([dicNeighPet objectForKey:@"pet_type_id"] != [NSNull null]) {
                                        objPet.type_id = [[dicNeighPet objectForKey:@"pet_type_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"pet_breed_type_id"] != [NSNull null]) {
                                        objPet.breed_type_id = [[dicNeighPet objectForKey:@"pet_breed_type_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"albums"] != [NSNull null]) {
                                        NSMutableArray * arrNeighPetTempAlbums = [dicNeighPet objectForKey:@"albums"];
                                        objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrNeighPetTempAlbums count]];
                                        for(NSInteger y=0;y<[arrNeighPetTempAlbums count];y++){
                                            ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                                            NSDictionary * dicAlbum = [arrNeighPetTempAlbums objectAtIndex:y];
                                            objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                                            objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                                            NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                                            objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                                            for(NSInteger m=0;m<[arrTempImages count];m++){
                                                NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                                ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                                objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];

                                                objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];

                                                objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];

                                                objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];

                                                [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                                            }

                                            [objPet.arrAlbums addObject:objAlbum];
                                        }
                                    }

                                    [objNeig.arrPets addObject:objPet];
                                }
                                //[self reloadProfileTheViewWith:user];
                            }

                            NSLog(@"obj arrNeigbour dob %@",objNeig.strDob);
                            NSLog(@"obj neighbor type %d",objNeig.neighbor_type);
                            if (objNeig.neighbor_type == 1) {
                                [obj.arrNeigbours addObject:objNeig];
                            }
                            else if(objNeig.neighbor_type == 2){
                                [obj.arrNeigbourPostal addObject:objNeig];
                            }

                        }
                        //NSLog(@"obj arrNeigbour count %d",[obj.arrNeigbours count]);

                        //[self reloadProfileTheViewWith:user];
                    }
                }

                NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
                //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
                NSString * strSession = [prefs objectForKey:LOGIN_LINK];
                obj.strSession = strSession;
                [self reloadProfileView:obj];
                ObjUser * objUser = [delegate.db getUserObj];
                obj.isFBLogin = objUser.isFBLogin;
                [delegate.db updateUser:obj];
                self.user = obj;
                [SVProgressHUD dismiss];
            }
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];*/
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
            PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
            //nav.ownner = self;
            [self presentModalViewController:nav animated:YES];
            //self.window.rootViewController
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        NSLog(@"arr count %d",[dics count]);

    }
}

- (void) reloadProfileView:(ObjUser *)objUser{
    NSLog(@"reloading view!!");
    user = objUser;
    if (objUser != nil) {
        lblUsername.text = [NSString stringWithFormat:@"%@", objUser.strName];
        lblFnameLname.text =[NSString stringWithFormat:@"%@ %@",objUser.strFName, objUser.strLName];
        
        lblAddress.text =[NSString stringWithFormat:@"%@",objUser.strAddress];

        if (![objUser.strGender isEqualToString:@""]) {
            lblGender.text = objUser.strGender;
        }
        else lblGender.text = @"Not specified";

        if (![objUser.strDob isEqualToString:@""]) {
            lblBod.text = [NSString stringWithFormat:@"%@",[self  customLongBornDate:objUser.strDob]];
        }
        else lblBod.text = @"Not specified";

        if (![objUser.strPhone isEqualToString:@""]) {
            lblPhone.text = objUser.strPhone;
        }
        else lblPhone.text = @"Not specified";

        if (![objUser.strFbLink isEqualToString:@""]) {
            lblFb.text = objUser.strFbLink;

            NSRange r = [objUser.strFbLink rangeOfString:lblFb.text];
            [lblFb addLinkToURL:[NSURL URLWithString:objUser.strFbLink] withRange:r];

        }
        else lblFb.text = @"Not specified";

        if (![objUser.strWebLink isEqualToString:@""]) {
            lblWeb.text = objUser.strWebLink;
        }
        else lblWeb.text = @"Not specified";

        if (![objUser.strCountry isEqualToString:@""]) {
            lblCountry.text = objUser.strCountry;
        }
        else lblCountry.text = @"Not specified";

        if (![objUser.strPostalCode isEqualToString:@""]) {
            lblPostal.text = objUser.strPostalCode;
        }
        else lblPostal.text = @"Not specified";
        
        if (![objUser.strEmail isEqualToString:@""]) {
            lblEmail.text = [NSString stringWithFormat:@"%@",objUser.strEmail];
        }
        else lblEmail.text = @"Not specified";

        NSLog(@"img profile x: %f and y: %f",self.imgProfile.frame.origin.x,self.imgProfile.frame.origin.y);
        [self.view bringSubviewToFront:self.imgProfile];

        if ([networkImages count]>0) {
            [networkImages removeAllObjects];
        }
        if ([networkCaptions count]>0) {
            [networkCaptions removeAllObjects];
        }

        if (objUser.strProfileImgLink != nil) {
            NSLog(@"user strProfile image link %@",objUser.strProfileImgLink);
            [networkImages addObject:objUser.strProfileImgLink];
            [networkCaptions addObject:objUser.strName];

            [self.imgProfile setImageWithURL:[NSURL URLWithString:objUser.strProfileImgLink ] placeholderImage:[UIImage imageNamed:@"img_profile_default.png"]];

            //[activity startAnimating];
            /*[WebImageOperations processImageDataWithURLString:user.strProfileImgLink andBlock:^(NSData *imageData) {
                if (self.view) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(self.imgProfile.frame.size.width,self.imgProfile.frame.size.height) resizeFrame:NO];
                    //[self o]
                    //[self.imgProfile setImage:[cropedImage overlayWith:[UIImage imageNamed: @"img_gradient_overlay.png"] overlayPoint: CGPointMake(0,0)]];
                    [self.imgProfile setImage:cropedImage];
                    [activity stopAnimating];
                    //[self.imgProfile setImage:image];
                }
            }];*/
            //self.imgProfile.image = nil;


        }

        if (objUser.hasLocationProvided == 0) {
            [self showLocationNotProvidedViews:YES];
            NSLog(@"not provided");
        }
        else{
            NSLog(@"yes provided");
            [self showLocationNotProvidedViews:NO];
            if ([objUser.arrNeigbours count]>0) {
                //lblNeigbour.text = [NSString stringWithFormat:@"%d users registered in your area.",[objUser.arrNeigbours count]];
                [self showUserThumbBar:objUser.arrNeigbours];

            }
            if ([objUser.arrNeigbourPostal count]>0) {
                [self showNeighborPostalThumbBar:objUser.arrNeigbourPostal];
            }
        }

        //self.navigationItem.title = objUser.strFName;
        if (lblTitleName == nil) {
            lblTitleName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
        }

        lblTitleName.backgroundColor = [UIColor clearColor];
        lblTitleName.textColor = [UIColor whiteColor];
        NSString * strValueFontName=@"AvenirNext-Regular";
        lblTitleName.font = [UIFont fontWithName:strValueFontName size:19];
        lblTitleName.text= @"Owner";
        lblTitleName.textAlignment = UITextAlignmentCenter;
        self.navigationItem.titleView = lblTitleName;
        if(isFromOther) {
            lblTitleName.text = @"Neighbour";
        }
    }
    NSLog(@"user arr pets account %d",[objUser.arrPets count]);
    [self showPetThumbBar:objUser.arrPets];
}

- (IBAction) onNeibourhoodClick:(id) sender{
    ProfileMapViewController * mapViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"profileMap"];
    mapViewController.owner = self;
    mapViewController.seletedLat = 0.0;
    mapViewController.seletedLon = 0.0;
    mapViewController.isEditable = TRUE;
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:mapViewController];
    nav.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationController presentModalViewController:nav animated:YES];
    NSLog(@"here is on map click");
    isFromMap = TRUE;
}

- (void) finishWithCoordinate:(float)late andlong:(float)lng{
    NSLog(@"home town location %f and long %f",late,lng);
    [self syncUserLocation:late andlong:lng];
    isFromMap = FALSE;
}

- (void) closeMap{
    isFromMap = TRUE;
}

- (void) syncUserLocation:(float)late andlong:(float)lng{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    //@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@"
    NSDictionary* params = @{@"user[latitude]":[NSString stringWithFormat:@"%f",late],@"user[longitude]":[NSString stringWithFormat:@"%f",lng]};

    //,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[phone]":obj.strPhone,@"user[address]":obj.strAddress,@"user[dob]":obj.strDob

    [SVProgressHUD show];
    [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",PROFILE_EDIT_LINK,self.user.userId,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            isFromOther = FALSE;
            [self viewWillAppear:YES];
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)makeRoundandBorder:(UIView *)v{
    [v.layer setCornerRadius:2.0f];
    [v.layer setMasksToBounds:YES];

    v.layer.borderColor = [UIColor lightGrayColor].CGColor;
    v.layer.borderWidth = 1.0;
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (IBAction) rightBarButton:(UIBarButtonItem *)sender {
	//[[self slidingViewController] anchorTopViewTo:ADAnchorSideLeft];
    EditProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"editprofile"];
    viewController.delegate =self;
    viewController.user = self.user;
    [self.navigationController pushViewController:viewController animated:YES];
    NSLog(@"here is");
}

- (IBAction) messageButton:(UIBarButtonItem *)sender {
	//[[self slidingViewController] anchorTopViewTo:ADAnchorSideLeft];
    [self showDialog];
}

-(void)completedWithViewController:(UIViewController *)viewcontroller{
    [viewcontroller dismissModalViewControllerAnimated:YES];
}

-(void)dismissWithViewController:(UIViewController *)viewcontroller{
    [viewcontroller dismissModalViewControllerAnimated:YES];
}

- (BOOL) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {

    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }

    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }

    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }

    return NO;
}

- (IBAction) updatePressed:(UIButton *)sender {
	/*ADSlidingViewController *slidingViewController = [self slidingViewController];

     [slidingViewController setLeftViewAnchorWidth:[[self leftAnchorAmountStepper] value]];
     [slidingViewController setRightViewAnchorWidth:[[self rightAnchorAmountStepper] value]];

     [slidingViewController setLeftViewAnchorWidthType:[[self leftAnchorWidthType] selectedSegmentIndex]];
     [slidingViewController setRightViewAnchorWidthType:[[self rightAnchorWidthType] selectedSegmentIndex]];

     [slidingViewController setLeftMainAnchorType:[[self leftAnchorLayoutType] selectedSegmentIndex]];
     [slidingViewController setRightMainAnchorType:[[self rightAnchorLayoutType] selectedSegmentIndex]];

     [slidingViewController setLeftUnderAnchorType:[[self leftSecondaryLayoutType] selectedSegmentIndex]];
     [slidingViewController setRightUnderAnchorType:[[self rightSecondaryLayoutType] selectedSegmentIndex]];

     [slidingViewController setUndersidePersistencyType:[[self undersidePersistencyControl] selectedSegmentIndex]];*/
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)syncProfileWith:(int)idx{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",PROFILE_LINK,idx,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];

        if (status == STATUS_ACTION_SUCCESS) {
            NSDictionary * dic = [dics objectForKey:@"user"];
            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strSession = [dics objectForKey:@"session_id"];

            if (dic != nil) {
                ObjUser * obj = [[ObjUser alloc]init];
                obj.strCountry = @"";
                obj.strPostalCode = @"";
                if ([dic objectForKey:@"user_id"] != [NSNull null]) {
                    obj.userId = [[dic objectForKey:@"user_id"] intValue];
                }
                else obj.userId = 0;

                if ([dic objectForKey:@"first_name"] != [NSNull null]) {
                    obj.strFName = [dic objectForKey:@"first_name"];
                }
                else obj.strFName = @"";

                if ([dic objectForKey:@"last_name"] != [NSNull null]) {
                    obj.strLName = [dic objectForKey:@"last_name"];
                }
                else obj.strLName = @"";

                if ([dic objectForKey:@"gender"] != [NSNull null]) {
                    obj.strGender = [dic objectForKey:@"gender"];
                }
                else obj.strGender = @"";

                if ([dic objectForKey:@"username"] != [NSNull null]) {
                    obj.strName = [dic objectForKey:@"username"];
                }
                else obj.strName = @"";

                if ([dic objectForKey:@"profile_image"] != [NSNull null]) {
                    obj.strProfileImgLink = [dic objectForKey:@"profile_image"];
                }
                else obj.strProfileImgLink = @"";

                if ([dic objectForKey:@"email"] != [NSNull null]) {
                    obj.strEmail = [dic objectForKey:@"email"];
                }
                else obj.strEmail = @"";

                if ([dic objectForKey:@"facebook"] != [NSNull null]) {
                    obj.strFbLink = [dic objectForKey:@"facebook"];
                }
                else obj.strFbLink = @"";

                if ([dic objectForKey:@"website"] != [NSNull null]) {
                    obj.strWebLink = [dic objectForKey:@"website"];
                }
                else obj.strWebLink = @"";

                if ([dic objectForKey:@"phone"] != [NSNull null]) {
                    obj.strPhone = [dic objectForKey:@"phone"];
                }
                else obj.strPhone = @"";

                if ([dic objectForKey:@"address"] != [NSNull null]) {
                    obj.strAddress = [dic objectForKey:@"address"];
                }
                else obj.strAddress = @"";

                if ([dic objectForKey:@"dob"] != [NSNull null]) {
                    obj.strDob = [dic objectForKey:@"dob"];
                }
                else obj.strDob = @"";

                if ([dic objectForKey:@"country_name"] != [NSNull null]) {
                    obj.strCountry = [dic objectForKey:@"country_name"];
                }
                else obj.strCountry = @"";

                if ([dic objectForKey:@"postal_code"] != [NSNull null]) {
                    obj.strPostalCode = [dic objectForKey:@"postal_code"];
                }
                else obj.strPostalCode = @"";
                
                if ([dic objectForKey:@"privacy"] != [NSNull null]) {
                    obj.privacy_id = [[dic objectForKey:@"privacy"] intValue];
                }
                else obj.privacy_id = 0;

                if ([dic objectForKey:@"has_location_provided"] != [NSNull null]) {
                    obj.hasLocationProvided = [[dic objectForKey:@"has_location_provided"]intValue];
                    NSLog(@"has location %d",obj.hasLocationProvided);
                }
                if ([dic objectForKey:@"pets"] != [NSNull null]) {
                    NSMutableArray * arrRawPets = [dic objectForKey:@"pets"];
                    obj.arrPets = [[NSMutableArray alloc]initWithCapacity:arrRawPets.count];
                    for(NSInteger i=0;i<[arrRawPets count];i++){
                        NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                        ObjPet * objPet = [[ObjPet alloc]init];
                        if ([dicPet objectForKey:@"pet_id"] != [NSNull null]) {
                            objPet.pet_id = [[dicPet objectForKey:@"pet_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"pet_name"] != [NSNull null]) {
                            objPet.strName = [dicPet objectForKey:@"pet_name"];
                        }
                        else objPet.strName = @"";

                        if ([dicPet objectForKey:@"pet_image"] != [NSNull null]) {
                            objPet.strImgLink = [dicPet objectForKey:@"pet_image"];
                        }
                        else objPet.strImgLink = @"";

                        if ([dicPet objectForKey:@"pet_type_name"] != [NSNull null]) {
                            objPet.strType = [dicPet objectForKey:@"pet_type_name"];
                        }
                        else objPet.strType = @"";

                        if ([dicPet objectForKey:@"pet_breed_type_name"] != [NSNull null]) {
                            objPet.strBreed = [dicPet objectForKey:@"pet_breed_type_name"];
                        }
                        else objPet.strBreed = @"";

                        if ([dicPet objectForKey:@"dob"] != [NSNull null]) {
                            objPet.strDob = [dicPet objectForKey:@"dob"];
                        }
                        else objPet.strDob = @"";

                        if ([dicPet objectForKey:@"description"] != [NSNull null]) {
                            objPet.strDescription = [dicPet objectForKey:@"description"];
                        }
                        else objPet.strDescription = @"";

                        if ([dicPet objectForKey:@"pet_type_id"] != [NSNull null]) {
                            objPet.type_id = [[dicPet objectForKey:@"pet_type_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"pet_breed_type_id"] != [NSNull null]) {
                            objPet.breed_type_id = [[dicPet objectForKey:@"pet_breed_type_id"] intValue];
                        }

                        if ([dicPet objectForKey:@"albums"] != [NSNull null]) {
                            NSMutableArray * arrTempAlbums = [dicPet objectForKey:@"albums"];
                            objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrTempAlbums count]];
                            for(NSInteger y=0;y<[arrTempAlbums count];y++){
                                ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                                NSDictionary * dicAlbum = [arrTempAlbums objectAtIndex:y];
                                objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                                objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                                NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                                objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                                for(NSInteger m=0;m<[arrTempImages count];m++){
                                    NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                    ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                    objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];

                                    objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];

                                    objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];

                                    objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];

                                    [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                                }

                                [objPet.arrAlbums addObject:objAlbum];
                            }
                        }

                        [obj.arrPets addObject:objPet];
                    }

                    //[self reloadProfileTheViewWith:user];
                }
                if (obj.hasLocationProvided == 1) {
                    if ([dic objectForKey:@"neighbours"] != [NSNull null] || ![self stringIsEmpty:[dic objectForKey:@"neighbours"] shouldCleanWhiteSpace:YES ]) {
                        NSMutableArray * arrRawNeigbour = [dic objectForKey:@"neighbours"];
                        obj.arrNeigbours = [[NSMutableArray alloc]initWithCapacity:arrRawNeigbour.count];
                        for(NSInteger n=0;n<[arrRawNeigbour count];n++){
                            NSDictionary * dicNeig = [arrRawNeigbour objectAtIndex:n];
                            ObjUser * objNeig = [[ObjUser alloc]init];
                            if ([dicNeig objectForKey:@"user_id"] != [NSNull null]) {
                                objNeig.userId = [[dicNeig objectForKey:@"user_id"] intValue];
                            }
                            if ([dicNeig objectForKey:@"first_name"] != [NSNull null]) {
                                objNeig.strFName = [dicNeig objectForKey:@"first_name"];
                            }
                            else objNeig.strFName = @"";

                            if ([dicNeig objectForKey:@"last_name"] != [NSNull null]) {
                                objNeig.strLName = [dicNeig objectForKey:@"last_name"];
                            }
                            else objNeig.strLName = @"";

                            if ([dicNeig objectForKey:@"gender"] != [NSNull null]) {
                                objNeig.strGender = [dic objectForKey:@"gender"];
                            }
                            else objNeig.strGender = @"";

                            if ([dicNeig objectForKey:@"username"] != [NSNull null]) {
                                objNeig.strName = [dicNeig objectForKey:@"username"];
                            }
                            else objNeig.strName = @"";

                            if ([dicNeig objectForKey:@"profile_image"] != [NSNull null]) {
                                objNeig.strProfileImgLink = [dicNeig objectForKey:@"profile_image"];
                            }
                            else objNeig.strProfileImgLink = @"";

                            if ([dicNeig objectForKey:@"email"] != [NSNull null]) {
                                objNeig.strEmail = [dicNeig objectForKey:@"email"];
                            }
                            if ([dicNeig objectForKey:@"facebook"] != [NSNull null]) {
                                objNeig.strFbLink = [dicNeig objectForKey:@"facebook"];
                            }
                            else objNeig.strFbLink = @"";

                            if ([dicNeig objectForKey:@"distance"] != [NSNull null]) {
                                objNeig.strDistance = [dicNeig objectForKey:@"distance"];
                            }
                            else objNeig.strDistance = @"";

                            if ([dicNeig objectForKey:@"type"] != [NSNull null]) {
                                objNeig.neighbor_type = [[dicNeig objectForKey:@"type"] intValue];
                            }
                            else objNeig.neighbor_type = 0;

                            if ([dicNeig objectForKey:@"website"] != [NSNull null]) {
                                objNeig.strWebLink = [dicNeig objectForKey:@"website"];
                            }
                            else objNeig.strWebLink = @"";

                            if ([dicNeig objectForKey:@"phone"] != [NSNull null]) {
                                objNeig.strPhone = [dicNeig objectForKey:@"phone"];
                            }
                            else objNeig.strPhone = @"";

                            if ([dicNeig objectForKey:@"address"] != [NSNull null]) {
                                objNeig.strAddress = [dicNeig objectForKey:@"address"];
                            }
                            else objNeig.strAddress = @"";

                            if ([dicNeig objectForKey:@"dob"] != [NSNull null]) {
                                objNeig.strDob = [dicNeig objectForKey:@"dob"];
                            }
                            else objNeig.strDob = @"";

                            if ([dicNeig objectForKey:@"pets"] != [NSNull null]) {
                                NSMutableArray * arrRawNeigMyPets = [dicNeig objectForKey:@"pets"];
                                objNeig.arrPets = [[NSMutableArray alloc]initWithCapacity:arrRawNeigMyPets.count];
                                for(NSInteger i=0;i<[arrRawNeigMyPets count];i++){
                                    NSDictionary * dicNeighPet = [arrRawNeigMyPets objectAtIndex:i];
                                    ObjPet * objPet = [[ObjPet alloc]init];
                                    if ([dicNeighPet objectForKey:@"pet_id"] != [NSNull null]) {
                                        objPet.pet_id = [[dicNeighPet objectForKey:@"pet_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"pet_name"] != [NSNull null]) {
                                        objPet.strName = [dicNeighPet objectForKey:@"pet_name"];
                                    }
                                    else objPet.strName = @"";

                                    if ([dicNeighPet objectForKey:@"pet_image"] != [NSNull null]) {
                                        objPet.strImgLink = [dicNeighPet objectForKey:@"pet_image"];
                                    }
                                    else objPet.strImgLink = @"";

                                    if ([dicNeighPet objectForKey:@"pet_type_name"] != [NSNull null]) {
                                        objPet.strType = [dicNeighPet objectForKey:@"pet_type_name"];
                                    }
                                    else objPet.strType = @"";

                                    if ([dicNeighPet objectForKey:@"pet_breed_type_name"] != [NSNull null]) {
                                        objPet.strBreed = [dicNeighPet objectForKey:@"pet_breed_type_name"];
                                    }
                                    else objPet.strBreed = @"";

                                    if ([dicNeighPet objectForKey:@"dob"] != [NSNull null]) {
                                        objPet.strDob = [dicNeighPet objectForKey:@"dob"];
                                    }
                                    else objPet.strDob = @"";

                                    if ([dicNeighPet objectForKey:@"description"] != [NSNull null]) {
                                        objPet.strDescription = [dicNeighPet objectForKey:@"description"];
                                    }
                                    else objPet.strDescription = @"";

                                    if ([dicNeighPet objectForKey:@"pet_type_id"] != [NSNull null]) {
                                        objPet.type_id = [[dicNeighPet objectForKey:@"pet_type_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"pet_breed_type_id"] != [NSNull null]) {
                                        objPet.breed_type_id = [[dicNeighPet objectForKey:@"pet_breed_type_id"] intValue];
                                    }

                                    if ([dicNeighPet objectForKey:@"albums"] != [NSNull null]) {
                                        NSMutableArray * arrNeighPetTempAlbums = [dicNeighPet objectForKey:@"albums"];
                                        objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrNeighPetTempAlbums count]];
                                        for(NSInteger y=0;y<[arrNeighPetTempAlbums count];y++){
                                            ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                                            NSDictionary * dicAlbum = [arrNeighPetTempAlbums objectAtIndex:y];
                                            objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                                            objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                                            NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                                            objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                                            for(NSInteger m=0;m<[arrTempImages count];m++){
                                                NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                                ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                                objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];

                                                objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];

                                                objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];

                                                objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];

                                                [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                                            }

                                            [objPet.arrAlbums addObject:objAlbum];
                                        }
                                    }

                                    [objNeig.arrPets addObject:objPet];
                                }
                                //[self reloadProfileTheViewWith:user];
                            }

                            NSLog(@"obj arrNeigbour dob %@",objNeig.strDob);
                            [obj.arrNeigbours addObject:objNeig];
                        }
                        //NSLog(@"obj arrNeigbour count %d",[obj.arrNeigbours count]);

                        //[self reloadProfileTheViewWith:user];
                    }
                }
                NSLog(@"obj country %@ and postal code %@",obj.strCountry,obj.strPostalCode);
                [self reloadProfileView:obj];

                [SVProgressHUD dismiss];
            }
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)showDialog{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"                        \n " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextView alloc]initWithFrame:CGRectMake(30, 45, 215, 40)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentLeft;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];
    txtParentPassword.backgroundColor = [UIColor whiteColor];

    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 360);
    alert.delegate = self;
    [alert show];
}

- (void)syncPushMessagesWith:(NSString *)strMessage andSenderId:(int)intSenderId{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"msg[receiver_id]":[NSString stringWithFormat:@"%d", intSenderId],@"msg[message]":strMessage};
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",MESSAGE_PUSH_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:@"Sent"];
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Info"
                                      message:strMsg
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            [self syncPushMessagesWith:txtParentPassword.text andSenderId:user.userId];
		}else if (buttonIndex == 0){

            NSLog(@"button index tag1");
        }
	}
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [delegate windowViewReAdjust];
    [[[UIActionSheet alloc] initWithTitle:[url absoluteString] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Facebook", nil), nil] showInView:self.view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strId = @"";
    NSNumber * strId = [prefs objectForKey:@"fbid"];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionSheet.title]];
    NSLog(@"facebook name %@",strId);
    
    if (strId) {
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: [NSString stringWithFormat:@"fb://profile/%@",strId]]];
    }
    else{
        NSURL *url = [NSURL URLWithString:actionSheet.title];
        [[UIApplication sharedApplication] openURL:url];
    }
    //NSLog(@"facebook url %@",actionSheet.title);
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [delegate windowViewAdjust];
}

- (IBAction)onProfilePic:(id)sender{
    /*PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    NSURL *URL = [NSURL URLWithString:objUser.strProfileImgLink];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    [self presentModalViewController:webViewController animated:YES];*/
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    if(networkGallery == nil)
        networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];

    [networkGallery setCurrentIndex:0];
    networkGallery.hideTitle = YES;
    networkGallery.hideEdit = YES;
    [self.navigationController pushViewController:networkGallery animated:YES];
}

#pragma mark - FGalleryViewControllerDelegate Methods
- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
    int num;
    num = [networkImages count];
	return num;
}

- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
	return FGalleryPhotoSourceTypeNetwork;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
    NSString *caption;
    if( gallery == networkGallery ) {
        caption = [networkCaptions objectAtIndex:index];
    }
	return caption;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery urlForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return [networkImages objectAtIndex:index];
}

@end
