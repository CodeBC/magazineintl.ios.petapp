//
//  CustomNavigationBar.m
//  GroovyMap
//
//  Created by Tonytoons on 1/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "PetAppDelegate.h"
#import "StringTable.h"

@implementation CustomNavigationBar

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
        NSLog(@"initWithFrame Initialization");
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
	PetAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	UIImage *image = [UIImage imageNamed:delegate.header_image];
    NSLog(@"hearder name %@ and rect %f and %f",delegate.header_image,rect.size.width,rect.size.height);
    if ([Utility isGreaterOSVersion:@"7.0"]){
        rect.size = CGSizeMake(320, 64);
    }
    else rect.size = CGSizeMake(320, 44);
	[image drawInRect:rect];
}

#pragma mark -
#pragma mark UINavigationDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
	NSString *title = viewController.navigationItem.title;
    NSLog(@"willShowViewController selected language:::%d",selectedLanguage);
	UILabel *myTitleView = [[UILabel alloc] init];
//    if(selectedLanguage==ENGLISH){
//        [myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
//        [myTitleView setFont:[UIFont fontWithName:@"Helvetica Neue" size:18.0]];
//    }
//    else if(selectedLanguage==MYANMAR){
//        [myTitleView setFont:[UIFont boldSystemFontOfSize:16]];
//        [myTitleView setFont:[UIFont fontWithName:@"Zawgyi-One" size:16.0]];
//        
//    }
    [myTitleView setFont:[UIFont boldSystemFontOfSize:16]];
    [myTitleView setFont:[UIFont fontWithName:@"Zawgyi-One" size:16.0]];
    
    myTitleView.text = title;
	[myTitleView setTextColor: [UIColor whiteColor]];
	[myTitleView setShadowColor: [UIColor blackColor]];
    myTitleView.backgroundColor = [UIColor clearColor];
	[myTitleView sizeToFit];
	viewController.navigationItem.titleView = myTitleView;
   
	viewController.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.16f green:0.36f blue:0.46 alpha:0.8];
    
    
}

- (void) updateTitle:(UIViewController *)viewController{
    
    NSLog(@"updateTitle>>>>>viewController");
	NSString *title = viewController.navigationItem.title;
	
	UILabel *myTitleView = [[UILabel alloc] init];
    
//    if(selectedLanguage==ENGLISH){
//        [myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
//        [myTitleView setFont:[UIFont fontWithName:@"Helvetica Neue" size:18.0]];
//    }
//    else if(selectedLanguage==MYANMAR){
//        [myTitleView setFont:[UIFont boldSystemFontOfSize:18]];
//        [myTitleView setFont:[UIFont fontWithName:@"Zawgyi-One" size:18.0]];
//    }
    
    [myTitleView setFont:[UIFont boldSystemFontOfSize:16]];
    [myTitleView setFont:[UIFont fontWithName:@"Zawgyi-One" size:16.0]];
    
	[myTitleView setTextColor: [UIColor blackColor] ];
	[myTitleView setShadowColor: [UIColor whiteColor]];
	myTitleView.text = title;
    myTitleView.backgroundColor = [UIColor clearColor];
	[myTitleView sizeToFit];
	viewController.navigationItem.titleView = myTitleView;
	
	viewController.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.16f green:0.36f blue:0.46 alpha:0.8];
}

#pragma mark TextField

- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
}


@end
