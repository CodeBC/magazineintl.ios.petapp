//
//  ObjConversation.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjConversation : NSObject
@property int idx;
@property int receiverId;
@property int timetick;
@property int userId;
@property int message_id;
@property (nonatomic, strong) NSString * strReciverName;
@property (nonatomic, strong) NSString * strReciverImgLink;
@property (nonatomic, strong) NSString * strMessage;
@property (nonatomic, strong) NSString * strSenderImgLink;
@end
