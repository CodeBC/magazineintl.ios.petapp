//
//  ObjAlertType.h
//  Pet
//
//  Created by Zayar on 8/26/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjAlertType : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property int timeInterval;
@end
