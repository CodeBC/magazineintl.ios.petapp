//
//  PetLoginViewController.h
//  Pet
//
//  Created by Zayar on 4/24/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"
@class PetLoginViewController;
@protocol petLoginViewControllerDelegate <NSObject>
@required
-(void)completedWithViewController:(PetLoginViewController *)viewcontroller;
-(void)dismissWithViewController:(PetLoginViewController *)viewcontroller;
-(void) completedWithViewController;
@end
/*@protocol LoginViewControllerDelegate
-(void) onSMQViewCancel;
-(void) completedWithViewController;
-(void) onSMQViewSuccessClose:(BOOL)isCamera;
@end*/
@interface PetLoginViewController : UIViewController{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIScrollView * scrollView;
    IBOutlet UITextField * txtLoginName;
    IBOutlet UITextField * txtPassword;
    IBOutlet UIButton * btnSignin;
    
    NSMutableArray * entryFields;
    SOAPRequest * loginRequest;
    //id<petLoginViewControllerDelegate> owner;
    UIButton *loginButton;
    NSArray *permissions;
}
-(IBAction)onLogin:(id)sender;
@property (assign) id<petLoginViewControllerDelegate> delegate;

@property (nonatomic, retain) NSArray *permissions;
- (void) hideLogin;
//@property id<LoginViewControllerDelegate> owner;
-(IBAction)close:(id)sender;
- (IBAction)fblogin:(id)sender;
@end
