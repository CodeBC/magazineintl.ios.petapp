//
//  AnimateDetailViewController.h
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>s
@interface AnimateDetailViewController : PetBasedViewController
@property (nonatomic,strong) NSMutableArray * arrImages;
- (IBAction)startAnimation:(id)sender;
@end
