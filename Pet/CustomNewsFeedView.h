//
//  CustomNewsFeedView.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjNewsFeed.h"
#import "ObjUser.h"
#import "ObjPet.h"
@protocol CustomNewsFeedViewDelegate
- (void) onPetSelected:(ObjPet *)objSPet;
- (void) onPetSelected:(ObjPet *)objSPet andNewsFeed:(ObjNewsFeed *)objNewsFeed;
- (void) onUserSelected:(ObjUser *)objSPet;
- (void) onMenuSelectedPet:(ObjPet *)objSPet andButton:(UIButton *)sender;
@end
@interface CustomNewsFeedView : UIView
{
    IBOutlet UIImageView * imgUserView;
    IBOutlet UIImageView * imgPetView;
    IBOutlet UILabel * lblUser;
    IBOutlet UILabel * lblPetName;
    IBOutlet UILabel * lblPetType;
    IBOutlet UILabel * lblDob;
    id<CustomNewsFeedViewDelegate> owner;
    ObjNewsFeed * objNF;
    IBOutlet UILabel *lblSmallBg;
}
@property id<CustomNewsFeedViewDelegate> owner;
-(void) loadTheViewWith:(ObjNewsFeed *)objNewsFeed;
- (IBAction)onPetSelected:(id)sender;
- (IBAction)onUserSelected:(id)sender;
- (void)loadTheView;
- (void)setThemeGrayAndWhite:(BOOL)isGray;
@end
