//
//  ObjBreedType.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjBreedType : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@end
