//
//  ObjAdoption.h
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjAdoption : NSObject
@property int idx;
@property int intBreedType;
@property int intPetType;
@property int intOrganization;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strPetName;
@property (nonatomic, strong) NSString * strPetGender;
@property (nonatomic, strong) NSString * strPetBreed;
@property (nonatomic, strong) NSString * strPetType;
@property (nonatomic, strong) NSString * strPetDob;
@property (nonatomic, strong) NSString * strDescription;
@property (nonatomic, strong) NSString * strPetImgLink;
@property (nonatomic, strong) NSString * strOrganization;
@property (nonatomic, strong) NSString * strInfo;
@property int * isInterested;
@end
