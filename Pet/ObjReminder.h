//
//  ObjReminder.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjReminder : NSObject
@property int idx;
@property int isDone;
@property int isSwitch;
@property int timetick;
@property int date_timetick;
@property int reminder_timetick;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strRepeatType;
@property (nonatomic, strong) NSString * strDate;
@property (nonatomic, strong) NSString * strReminderDate;
@end
