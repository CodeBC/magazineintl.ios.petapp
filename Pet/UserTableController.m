//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "UserTableController.h"
#import "StringTable.h"
#import "petAPIClient.h"
#import "PetAppDelegate.h"
#import "ObjUser.h"
#import "MessageUserTableCell.h"
@interface UserTableController ()
{
    NSMutableArray * arrUsers;
    IBOutlet UISearchBar * searchBar2;
    NSArray * arrSearchResult;
    NSMutableDictionary *sections;
    UITableView * tableView;
}
@property (nonatomic,strong)NSMutableDictionary *sections;
@property (nonatomic,strong) IBOutlet UITableView * tableView;
@end

@implementation UserTableController
@synthesize owner,sections,tableView;
//@synthesize tableView = _tableView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = @"Popover Title";
    
    //[self.tableView addSubview:searchBar];
    //[self.tableView setFrame:<#(CGRect)#>]
    self.sections = [[NSMutableDictionary alloc] init];
    //searchBar2 = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    searchBar2.delegate = self;
    searchBar2.showsCancelButton = YES;
    searchBar2.showsSearchResultsButton = YES;
    //[self.view addSubview:searchBar2];
    
    //[self.tableView setFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 40)];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tableView respondsToSelector:@selector(separatorInset)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self reloadPerson];
}

- (void)reloadPerson{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    if (arrUsers != nil) {
        if ([arrUsers count]>0) {
            [arrUsers removeAllObjects];
        }
    }
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",ALL_USER_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        
        if (status == STATUS_ACTION_SUCCESS) {
            NSMutableArray *  arr = [dics objectForKey:@"users"];
            arrUsers = [[NSMutableArray alloc]init];
            for(NSInteger i=0;i<[arr count];i++){
                /* 
                 {
                 "id": 3,
                 "first_name": "Justina Bogisich",
                 "last_name": "Arne Weimann",
                 "profile_image": ""
                 },
                 */
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjUser * objUser = [[ObjUser alloc]init];
                objUser.userId = [[dicNewsFeed objectForKey:@"id"] intValue];
                objUser.strFName = [dicNewsFeed objectForKey:@"first_name"];
                NSString *str = objUser.strFName;
                
                str = [NSString stringWithFormat:@"%@%@",[[str substringToIndex:1] uppercaseString],[str substringFromIndex:1]];
                objUser.strFName = str;
                objUser.strLName = [dicNewsFeed objectForKey:@"last_name"];
                objUser.strProfileImgLink = [dicNewsFeed objectForKey:@"profile_image"];
                
                [arrUsers addObject:objUser];
            }
            [self reloadTheTableViewForNormal];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)reloadTheTableViewForNormal{
    //arrUsers = [self sortWithName:arrUsers];
    [self loadThetableSideList:arrUsers];
    self.tableView.tag = 0;
    [self.tableView reloadData];
}

- (void) loadThetableSideList:(NSMutableArray *)arr{
    BOOL found;
    if (self.sections != nil) {
        if ([[self.sections allKeys] count]>0) {
            [self.sections removeAllObjects];
        }
    }
    // Loop through the books and create our keys
    for (ObjUser * obj in arr)
    {
        NSString *c = [obj.strFName substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [self.sections allKeys])
        {
            /*if ([str isEqualToString:c])
            {
                found = YES;
            }*/
            NSLog(@"key string: %@",str);
            if ( [str caseInsensitiveCompare:c] == NSOrderedSame)
            {
                NSLog(@"arr string: %@",c);
                found = YES;
            }
            
        }
        
        if (!found)
        {
            [self.sections setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (ObjUser * obj in arr)
    {
        [[self.sections objectForKey:[obj.strFName substringToIndex:1]] addObject:obj];
    }
    
    // Sort each section array
    for (NSString *key in [self.sections allKeys])
    {
        [[self.sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"strFName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    }
    
    NSLog(@"section count %d and section 1 count %d",[[self.sections allKeys] count],[[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:1]] count]);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if (self.tableView.tag == 0) {
        NSLog(@"section count %d",[[self.sections allKeys] count]);
       return [[self.sections allKeys] count];
    }
    else if (self.tableView.tag == 1){
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.tableView.tag == 0) {
        NSLog(@"section row count %d",[[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count]);
        return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
    }
    else if (self.tableView.tag == 1){
        return [arrSearchResult count];
    }
    return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.tableView.tag == 0) {
        return [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageUserTableCell";
	MessageUserTableCell *cell = (MessageUserTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageUserTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MessageUserTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    //cell.textLabel.text = [NSString stringWithFormat:@"cell %d",indexPath.row];
    NSLog(@"obj table u row %d",[indexPath row]);
    ObjUser * objU = nil;
    if (tableView.tag == 0) {
        objU = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
    }
    else if(tableView.tag == 1){
        objU = [arrSearchResult objectAtIndex:indexPath.row];
    }
    
    [cell loadTheViewWith:objU];
    return cell;
}

/*- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    searchBar2 = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
    searchBar2.delegate = self;
    searchBar2.showsCancelButton = YES;
    searchBar2.showsSearchResultsButton = YES;
    return searchBar2;
}*/

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    NSPredicate *preda = [NSPredicate predicateWithFormat:
                          @"strFName beginswith[c] %@",searchText];
    arrSearchResult = [arrUsers filteredArrayUsingPredicate:preda];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", searchText];
    //arrSearchResult = [arrUsers filteredArrayUsingPredicate:predicate];
    for (ObjUser * user in arrSearchResult) {
        NSLog(@"arr search count %d and name %@",[arrSearchResult count],user.strFName);
    }
    self.tableView.tag = 1;
    [self.tableView reloadData];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* NewsHubAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    if([indexPath row]==0){
        
        [delegate closeFPPopover];
        
    }
    else if([indexPath row]==1){
        
//        SettingViewController *settingViewController=[[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:[NSBundle mainBundle]];
//        [self.navigationController pushViewController:settingViewController animated:YES];
//        [settingViewController release];
//        settingViewController=nil;
        //[delegate showSetting];
        
    }*/
    NSLog(@"table selected!");
    if (self.tableView.tag == 0) {
        ObjUser * objUser = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        [owner onUserTableSelected:objUser];
    }
    else if (self.tableView.tag == 1) {
        ObjUser * objUser = [arrSearchResult objectAtIndex:indexPath.row];
        [owner onUserTableSelected:objUser];
    }
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSArray *) getFirstNameFromArrar:(NSMutableArray *)arr{
    NSMutableArray * arrTemp = nil;
    if ([arr count]>0) {
        arrTemp = [[NSMutableArray alloc]initWithCapacity:[arr count]];
        
        for (ObjUser * obj in arr) {
            NSString * str = [self uppercaseFirstLetterOfName:obj.strName];
            [arrTemp addObject:str];
        }
    }
    return (NSArray *)arrTemp;
}

- (NSString *)uppercaseFirstLetterOfName:(NSString *)str {
    NSString *stringToReturn = [[str uppercaseString] substringToIndex:1];
    return stringToReturn;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString
                                                                             *)title atIndex:(NSInteger)index {
    NSLog(@"%@",title);
    return index;
}

- (NSMutableArray *)sortWithName:(NSMutableArray *)arr{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"strFName"
                                                  ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [arr sortedArrayUsingDescriptors:sortDescriptors];
    //[arr sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return (NSMutableArray *)sortedArray;
}

#pragma mark - Search view delegate
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    //[self filterContentForSearchText:searchBar.text scope:nil];
    NSLog(@"txt search %@",searchBar.text);
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.showsSearchResultsButton = TRUE;
    /*self.tableView.tag = 0;
    [self.tableView reloadData];*/
    [self reloadTheTableViewForNormal];
    [self.view endEditing:YES];
    [owner onCancel];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self filterContentForSearchText:searchBar.text scope:nil];
    searchBar.showsSearchResultsButton = TRUE;
    [self.view endEditing:YES];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar{
    searchBar.showsSearchResultsButton = TRUE;
    /*self.tableView.tag = 0;
    [self.tableView reloadData];*/
    [self reloadTheTableViewForNormal];
}

@end
