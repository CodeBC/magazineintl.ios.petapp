//
//  PetAppDelegate.h
//  Pet
//
//  Created by Zayar on 4/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "SOAPRequest.h"
#import "DBManager.h"
#import "Facebook.h"
#import "FBConnect.h"
#import "PetLoginViewController.h"
#import "PetRegisterViewController.h"
#import "ObjUser.h"
#import "ObjAlbumPhoto.h"
#import "ObjLostFound.h"
#import "ZDialogView.h"
#import "PopUpOverlay.h"
extern NSString *const FBSessionStateChangedNotification;
@interface PetAppDelegate : UIResponder <UIApplicationDelegate,FBDialogDelegate>
{
    NSString * databasePath;
    DBManager * db;
    
    NetworkStatus remoteHostStatus;
    NetworkStatus internetConnectionStatus;
    NetworkStatus localWiFiConnectionStatus;
    
    SOAPRequest * generateKeyRequest;
    SOAPRequest * checkSessionRequest;
    SOAPRequest * userPointRequest;
    
    BOOL getConnection;
    BOOL isNowInLogin;
    BOOL isFromMain;
    BOOL isFromLogin;
    NSString * strGenKey;
    NSString * strUdid;
    
    Facebook * facebook;
    NSMutableDictionary *userPermissions;
    NSArray *permissions;
    BOOL isFromFBRegister;
    BOOL isFromFBLogin;
    BOOL isFromFBShare;
    BOOL isFBParamRequest;
    BOOL isFBLoginRequest;
    PetLoginViewController * petLoginViewController;
    PetRegisterViewController * petRegisterViewController;
    ObjUser * objUser;
    //int fbProcessId;
    ObjAlbumPhoto * objAPhoto;
    ObjLostFound * objSelectedLF;
    ZDialogView * zDialogCustomView;
    PopUpOverlay * popUpOverlayView;
    UIViewController * current_View_Controller;
    NSString * strCountry;
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentGeo;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
    
    NSString * strCurrentPlace;
    NSTimer * myLocationUpdateTimer;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString * header_image;
@property (nonatomic, retain) NSString * databasePath;
@property (nonatomic, retain) NSString * strUdid;
@property (nonatomic, retain) DBManager * db;
@property BOOL getConnection;
@property BOOL isFromMain;
@property BOOL isNowInLogin;
@property BOOL isFromLogin;
@property (nonatomic, retain) NSString * strGenKey;
@property (strong, nonatomic) Facebook *facebook;
@property (nonatomic, retain) NSMutableDictionary *userPermissions;
@property (nonatomic, retain) NSArray *permissions;
@property (nonatomic, retain) ObjAlbumPhoto * objAPhoto;

@property double currentLat;
@property double currentLng;

@property NetworkStatus remoteHostStatus;
@property NetworkStatus internetConnectionStatus;
@property NetworkStatus localWiFiConnectionStatus;

@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property int selectedCountry;
@property (nonatomic, strong) NSMutableArray * arrCountry;
- (void) windowViewReAdjust;

- (void)apiFQLIMeForRegister;
- (void)showLoginFromMain;
- (void)showMainFromLogin;
- (void)showLoadingScreen;
- (void)hideLoadingScreen;
- (void)updateStatus;
- (void)updateCarrierDataNetworkWarning;
- (void)syncAndCheckTheSession;
- (void) clearTheBudgeIcon;
- (void)fbLogin:(PetLoginViewController *)pLoginViewController;
- (void)fbRegister:(PetRegisterViewController *)pRegisterViewController;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void)fbShare:(ObjLostFound *)objLF;
- (void) logout:(UIViewController *)viewController;
- (void) fbSessionKill;
- (void) deleteLocalNotification:(ObjReminder *)obj;
- (void) preSetReminders;
- (void) windowViewAdjust;
@end
