//
//  AnimateMeViewController.m
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AnimateMeViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjDeal.h"
#import <QuartzCore/QuartzCore.h>
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "NavBarButton1.h"
#import "NavBarButton2.h"
#import "Utility.h"
#import "AnimateDetailViewController.h"
#import "TSActionSheet.h"
@interface AnimateMeViewController ()
{
    IBOutlet UIScrollView * scrollView;
    NSMutableArray * arrImages;
    NSMutableArray * arrThumbImages;
    BOOL isFromPhotoLibrary;
    BOOL isFromAlbumLibrary;
    int fromCamera;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIButton * btnShow;
    
    UIImagePickerController * imagePicker;
}
@end

@implementation AnimateMeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton2 *btnAdd = [[NavBarButton2 alloc] init];
	[btnAdd addTarget:self action:@selector(showActionSheet:forEvent:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = addButton;
    
    arrThumbImages = [[NSMutableArray alloc]init];
    arrImages = [[NSMutableArray alloc]init];
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Growth Chart";
    self.navigationItem.titleView = lblName;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        //[imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
        [scrollView setFrame:CGRectMake(0, 0, 320, 420)];
        [btnShow setFrame:CGRectMake(20, 440, 280, 44)];
    }else{
        //[imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
        [scrollView setFrame:CGRectMake(0, 0, 320, 340)];
        [btnShow setFrame:CGRectMake(20, 360, 280, 44)];
        
    }
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    
     imagePicker = [[UIImagePickerController alloc] init];
}

- (IBAction) rightBarButton:(UIBarButtonItem *)sender {
	//[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
    //[self showPhotoLibrary];
}

-(void) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Camera" block:^{
        NSLog(@"Camera");
        [self showCamera];
    }];
    [actionSheet addButtonWithTitle:@"Library" block:^{
        NSLog(@"Library");
        [self showPhotoLibrary];
    }];
    [actionSheet addButtonWithTitle:@"Pet Album" block:^{
        NSLog(@"Pet Album");
        [self showPetAlbum];
    }];
    [actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    
    [actionSheet showWithTouch:event];
}

-(void)showCamera{
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        fromCamera = 1;
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

- (void) showPhotoLibrary{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        fromCamera = 0;
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Fuction is not suported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

- (void) showPetAlbum{
    isFromAlbumLibrary = TRUE;
    isFromPhotoLibrary = FALSE;
    UINavigationController *viewController = [[UIStoryboard storyboardWithName:@"AnimateMe" bundle:nil] instantiateViewControllerWithIdentifier:@"petAlbumList"];
    
      [self.navigationController presentModalViewController:viewController animated:YES];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self deleteAllFilesInDocuments];
    NSLog(@">>>>>>>>>didFinishPickingMediaWithInfo");
    //iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    // Access the uncropped image from info dictionary
    //UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    //UIImage * orginalImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage * thumbImage=[Utility imageByScalingProportionallyToSize:editedImage newsize:CGSizeMake(90, 100) resizeFrame:TRUE];
    
    //UIImage * resizedImageFromLib=[Utility forceImageResize:editedImage newsize:CGSizeMake(90, 90)];
    
    if (fromCamera == 1) {
        // Save image;
        
        /*UIImageWriteToSavedPhotosAlbum(thumbImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dataPath = [Paths objectAtIndex:0];
        NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];
        //delegate.pathSelectedPic = seletedPicPath;
        NSData *imageData = UIImagePNGRepresentation(thumbImage);
        [imageData writeToFile:seletedPicPath atomically:NO];*/
        
        //[self onCrop:resizedImage];
        NSLog(@"Camera selected");
        if (thumbImage != nil) {
            [arrThumbImages addObject:thumbImage];
            [arrImages addObject:editedImage];
            [self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
            
            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil);
        }
        
        if ([arrImages count]<2) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: APP_TITLE
                                  message: @"Please add more than one image to animate!"
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(fromCamera == 0){
        NSLog(@"Library selected");
        /*NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];*/
        //delegate.pathSelectedPic = seletedPicPath;
        
        //NSData *imageData = UIImagePNGRepresentation(resizedImageFromLib);
        //[imageData writeToFile:seletedPicPath atomically:NO];
        
        //[delegate showPhoto];
        //[self onCrop:resizedImageFromLib];
        
        //[delegate showGamePlayDetail:CUSTOM_PHOTO_VIEW];
        //[imgProfileView setImage:resizedImage];
        if (thumbImage != nil) {
            [arrThumbImages addObject:thumbImage];
            [arrImages addObject:editedImage];
            [self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
        }
        
        if ([arrImages count]<2) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: APP_TITLE
                                  message: @"Please add more than one image to animate!"
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
    [self imagePickerControllerDidCancel:picker];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"here is view will appear!!");
    if (isFromAlbumLibrary) {
        NSLog(@"is from album");
        PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        if (delegate.objAPhoto != nil){
            [arrThumbImages addObject:delegate.objAPhoto.imgPetThumbView];
            [arrImages addObject:delegate.objAPhoto.imgPetView];
            [self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
            delegate.objAPhoto = nil;
        }
    }

}

- (void)reloadTheScrollImageViewWithThumbArr:(NSMutableArray *)arr{
    int row = 0;
	int column = 0;
    if ([arr count]>0) {
        for (UIView * v in scrollView.subviews){
            if ([v isKindOfClass:[UIImageView class]]) {
                [v removeFromSuperview];
            }
        }
    }
    
    for(int i = 0; i < arr.count; ++i) {
		
		UIImage *thumb = [arr objectAtIndex:i];
        UIImageView * bIcon = [[UIImageView alloc] initWithFrame: CGRectMake(column*95+24, row*110+10, 90, 100)];
        //bIcon set
		[bIcon setImage:thumb];
        bIcon.contentMode = UIViewContentModeScaleAspectFit;
        [scrollView addSubview:bIcon];
		if (column == 2) {
			column = 0;
			row++;
		} else {
			column++;
		}
	}
    [scrollView setContentSize:CGSizeMake(320, (row+1) * 110 + 10)];
}

- (IBAction)onShowAnimate:(id)sender{
    if ([arrImages count]>1) {
        AnimateDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"AnimateMe" bundle:nil] instantiateViewControllerWithIdentifier:@"detail"];
        viewController.arrImages = arrImages;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    isFromPhotoLibrary = TRUE;
    isFromAlbumLibrary = FALSE;
    [picker dismissModalViewControllerAnimated:YES];
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        // iOS 7
        //self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+20, self.navigationController.navigationBar.frame.size.width, 44);
        //self.navigationController.navigationBar.translucent = NO;
        // for iOS7
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        }
        PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        [delegate windowViewAdjust];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    //isFromPhotoLibrary = FALSE;
    //isFromAlbumLibrary = FALSE;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //[self deleteAllFilesInDocuments];
    // iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    UIAlertView *alert;
    // Unable to save the image
    if (error){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
        // [alert release];
    }
    else{
        /*alert = [[UIAlertView alloc] initWithTitle:@"Success"
         message:@"Image saved to Photo Album."
         delegate:self cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/
        
    }
    
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
