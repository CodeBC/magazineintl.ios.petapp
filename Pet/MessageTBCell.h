//
//  MessageTBCell.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjConversation.h"

@interface MessageTBCell : UITableViewCell
{
    IBOutlet UIImageView * imgProfileView;
    IBOutlet UILabel * lblName;
    IBOutlet UILabel * lblMessage;
    IBOutlet UILabel * lblDTime;
}
- (void) loadTheCellWith:(ObjConversation *)objConv;
@end
