//
//  AlbumListViewController.m
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AlbumListViewController.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "UIImageView+AFNetworking.h"
#import "EditPetViewController.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "NavBarButton2.h"
#import "NavBarButton1.h"
#import "AlbumListCell.h"
#import "petAPIClient.h"
#import "AlbumViewController.h"
@interface AlbumListViewController ()
{
    NSMutableArray * arrAlbum;
    IBOutlet UITableView * tbl;
    IBOutlet UIImageView * imgBgView;
    UITextField * txtParentPassword;
    UITextField * txtEdit;
    ObjAlbum * objTempAlbum;
    BOOL isDeleteDialogShow;
    int current_index;
    NSIndexPath * selectedIndexPath;
}

@end

@implementation AlbumListViewController
@synthesize objP,isFromOther;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Albums";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    //arrAlbum = [[NSMutableArray alloc]init];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    lpgr.delegate = self;
    [tbl addGestureRecognizer:lpgr];
    isDeleteDialogShow = FALSE;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:tbl];
    
    NSIndexPath *indexPath = [tbl indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else
    {
        //UITableViewCell * cell = [bubbleTable cellForRowAtIndexPath:indexPath];
        NSLog(@"long press on table view at row %d and section %d", indexPath.row,indexPath.section);
        current_index = indexPath.row;
        ObjAlbum * objSelectedToEdit = [arrAlbum objectAtIndex:current_index];
    
        selectedIndexPath = indexPath;
        if (!isDeleteDialogShow) {
            isDeleteDialogShow = TRUE;
            [self dialogForEditAlbum:objSelectedToEdit.strName];
        }
    }
}

- (void) manageNavBarButton{
    if (!isFromOther) {
        NavBarButton2 *btnEdit = [[NavBarButton2 alloc] init];
        [btnEdit addTarget:self action:@selector(onAdd:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = editButton;
    }
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
   // [obj]
    [self manageNavBarButton];
    [self loadTheViewWith:objP];
}

- (void)loadTheViewWith:(ObjPet *)objPet{
    arrAlbum = objPet.arrAlbums;
    [self reloadData:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrAlbum count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlbumListCell";
	AlbumListCell *cell = (AlbumListCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AlbumListCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (AlbumListCell *) currentObject;
				cell.accessoryView = nil;
                [cell loadCellView];
				break;
			}
		}
	}
    ObjAlbum * objA = [arrAlbum objectAtIndex:[indexPath row]];
    [cell loatTheCellWith:objA];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ObjAlbum * objAlbum = [arrAlbum objectAtIndex:indexPath.row];
    AlbumViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"album"];
    viewController.objAlbum = objAlbum;
    viewController.isFromOther = isFromOther;
    [self.navigationController pushViewController:viewController animated:YES];
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        ObjAlbum * obj = [arrAlbum objectAtIndex:indexPath.row];
        [self syncDeleteAlbum:obj.idx and:indexPath];
        //[arrMessages removeObjectAtIndex:indexPath.row];
        //[tableView deleteRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)syncDeleteAlbum:(int)con_id and:(NSIndexPath *)indexPath{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    [SVProgressHUD show];
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",ALBUM_NEW_LINK,con_id,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [arrAlbum removeObjectAtIndex:indexPath.row];
            [tbl deleteRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncAlbumEdit:(ObjAlbum *)obj{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    //pet[gender]=%@&pet[pet_name]=%@&pet[profile_image]=%@&pet[dob]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@
    NSDictionary* params = @{@"album[album_name]":obj.strName};
    NSLog(@"params %@ and full link %@",params,[NSString stringWithFormat:@"%@/%d?auth_token=%@",ALBUM_NEW_LINK,obj.idx,strSession]);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",ALBUM_NEW_LINK,obj.idx,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [arrAlbum replaceObjectAtIndex:current_index withObject:obj];
            [tbl reloadData];
            [SVProgressHUD showSuccessWithStatus:strMsg];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)reloadData:(BOOL)animated
{
    //[tbl setFrame:CGRectMake(0 , 0, 320, (80*[arrPet count]))];
    [tbl reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionMoveIn];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.9];
        [[tbl layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
}

- (void) onAdd:(id)sender{
    NSLog(@"album adding!");
    [self dialogForNewAlbum];
}

- (void)dialogForNewAlbum{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Album" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];
    
    txtParentPassword.placeholder = @"Name";
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        //[alert setValue:txtParentPassword forKey:@"textView"];
        txtParentPassword.frame = CGRectMake(0, 45, 215, 30);
        [alert setValue:txtParentPassword forKey:@"accessoryView"];
    }
    else{
        [alert addSubview:txtParentPassword];
    }
    
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    
}

- (void)dialogForEditAlbum:(NSString *)strName{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Album" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtEdit == nil) {
        txtEdit = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtEdit.text = strName;
    txtEdit.textAlignment = UITextAlignmentCenter;
    alert.tag = 2;
    [txtEdit becomeFirstResponder];
    
    txtEdit.placeholder = @"Name";
    txtEdit.backgroundColor = [UIColor whiteColor];
    [txtEdit setBorderStyle:UITextBorderStyleBezel];
    
    //[alert addSubview:txtEdit];
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        //[alert setValue:txtParentPassword forKey:@"textView"];
        txtEdit.frame = CGRectMake(0, 45, 215, 30);
        [alert setValue:txtEdit forKey:@"accessoryView"];
    }
    else{
        [alert addSubview:txtEdit];
    }
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
    
}

- (void)synNewAlbum:(NSString *)strName andPetId:(int)idx{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"album[pet_id]":[NSString stringWithFormat:@"%d", idx],@"album[album_name]":strName};
    NSLog(@"params %@",params);
    if (objTempAlbum == nil) {
        objTempAlbum = [[ObjAlbum alloc]init];
        objTempAlbum.arrAlbumPhoto = [[NSMutableArray alloc]init];
    }
    objTempAlbum.strName = strName;
    
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",ALBUM_NEW_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            objTempAlbum.idx = [[dics objectForKey:@"id"]intValue];
            [arrAlbum addObject:objTempAlbum];
            [tbl reloadData];
            objTempAlbum = nil;
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncPushMessagesWith:(NSString *)strMessage{
   
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            if (![self stringIsEmpty:txtParentPassword.text shouldCleanWhiteSpace:YES]) {
                [self synNewAlbum:txtParentPassword.text andPetId:objP.pet_id];
                
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"Album name is required!   "];
                [self dialogForNewAlbum];
                
            }
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
        }
	}
    
    if( [alertView tag] == 2 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            if (![self stringIsEmpty:txtEdit.text shouldCleanWhiteSpace:YES]) {
                //[self synNewAlbum:txtParentPassword.text andPetId:objP.pet_id];
            //add edit
                ObjAlbum * obj = [arrAlbum objectAtIndex:current_index];
                obj.strName = txtEdit.text;
                [self syncAlbumEdit:obj];
                isDeleteDialogShow = FALSE;
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"Album name is required!  "];
                [self dialogForEditAlbum:txtEdit.text];
                
            }
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
            isDeleteDialogShow = FALSE;
        }
	}
}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL) cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
