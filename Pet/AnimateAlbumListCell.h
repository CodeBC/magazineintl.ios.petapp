//
//  AlbumListCell.h
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAlbum.h"
#import "ObjPet.h"

@interface AnimateAlbumListCell : UITableViewCell
{
    IBOutlet UIImageView * imgThumbView;
    IBOutlet UILabel * lblName;
}
- (void)loatTheCellWith:(ObjAlbum *)objAlbum;
- (void)loatTheCellWithPet:(ObjPet *)objPet;
- (void)loadCellView;
@end
