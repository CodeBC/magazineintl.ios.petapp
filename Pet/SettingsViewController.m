//
//  SettingsViewController.m
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "SettingsViewController.h"
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "PetViewController.h"
#import "TSActionSheet.h"
#import "petAPIClient.h"
#import "ObjReminder.h"
@interface SettingsViewController ()
{
    IBOutlet UIImageView * imgBgView;
    int int_privacy;
    IBOutlet UIButton * btnChangePass;
    IBOutlet UIButton * btnPrivacy;
}

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Settings";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    [btnPrivacy setTitle:@"Public" forState:normal];
    int_privacy = 0;
}

-(IBAction) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    UIButton * btn = (UIButton *)sender;
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Profile is public now" block:^{
        NSLog(@"Public");
        //[self showCamera];
        [btn setTitle:@"Profile is public now" forState:normal];
        int_privacy = 0;
        [self syncLostPetAduse:int_privacy];
    }];
    [actionSheet addButtonWithTitle:@"Profile is private now" block:^{
        NSLog(@"Private");
        [btn setTitle:@"Profile is private now" forState:normal];
        int_privacy = 1;
        [self syncLostPetAduse:int_privacy];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    //actionSheet.
    
    [actionSheet showWithTouch:event];
}

- (void)viewWillAppear:(BOOL)animated{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objuser = [delegate.db getUserObj];
    if (objuser.isFBLogin == 1) {
        btnChangePass.hidden = TRUE;
    }
    else if(objuser.isFBLogin == 0){
        btnChangePass.hidden = FALSE;
    }
    [self syncSettingPrivacy];
}

- (void)syncLostPetAduse:(int)privacy{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"setting[privacy]":[NSString stringWithFormat:@"%d", privacy]};
    
    //[SVProgressHUD show];
    NSLog(@"");
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",SETTING_PRAVICY_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:@"Sent"];
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Info"
                                      message:strMsg
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncSettingPrivacy{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"user[privacy]":@""};
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    ObjUser * objUser = [delegate.db getUserObj];
    [SVProgressHUD show];
    NSLog(@"");
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",SETTING_PRAVICY_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        NSDictionary * dic = [dics objectForKey:@"settings"];
        int privacy = [[dic objectForKey:@"privacy"] intValue];
        
        if (privacy == 1) {
            [btnPrivacy setTitle:@"Private" forState:normal];
            int_privacy = 1;
        }
        else if (privacy == 0) {
            [btnPrivacy setTitle:@"Public" forState:normal];
            int_privacy = 0;
        }
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:@"Sent"];
           /* UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Info"
                                      message:strMsg
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];*/
            
            
            
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (IBAction) onLogout:(id)sender{
    [self syncLogout];
}

- (void) syncLogout{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    NSLog(@"logout link %@ and auth %@",LOGOUT_LINK,strSession);
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@?auth_token=%@",LOGOUT_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD dismiss];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            
            NSMutableArray * arr = [delegate.db getAllReminder];
            for (ObjReminder * obj in arr) {
                [delegate deleteLocalNotification:obj];
                [delegate.db deleteReminder:obj];
            }
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notFirstTime"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"installDate"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [delegate logout:self];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
