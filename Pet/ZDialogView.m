//
//  ZDialogView.m
//  PoS
//
//  Created by Zayar on 7/26/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ZDialogView.h"

@implementation ZDialogView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setCountryButtonTitle:(NSString *)str{
    [btnCountry setTitle:str forState:normal];
}

- (IBAction)onCountry:(id)sender{
    [owner onCountry];
}

- (IBAction)onOk:(id)sender{
    [owner onOk:txtPostal.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
	return YES;
}

- (IBAction)onCancel:(id)sender{
    [owner onCancel];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
