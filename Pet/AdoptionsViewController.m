//
//  AdoptionsViewController.m
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AdoptionsViewController.h"
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "ObjPetType.h"
#import "ObjBreedType.h"
#import "ObjOrganization.h"
#import "AdoptionsListViewController.h"
#import "NavBarButton5.h"
@interface AdoptionsViewController ()
{
    IBOutlet UIImageView * imgBgView;
    NSMutableArray * arrType;
    NSMutableArray * arrBreed;
    NSMutableArray * arrOrgination;
    
    UILabel * lblbtnCap2;
    UILabel * lblbtnCap3;
    UILabel * lblbtnCap4;
    IBOutlet UITableView * tbl;
}
@property (nonatomic,strong) UITableViewCell *btnType;
@property (nonatomic,strong) UITableViewCell *btnBreed;
@property (nonatomic,strong) UITableViewCell *btnOrganisation;
@property (nonatomic,strong) NSArray *cells;

@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;

@property int selectedType;
@property int selectedBreed;
@property int selectedOrganization;
@end

@implementation AdoptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton5 *btnSearch = [[NavBarButton5 alloc] init];
	[btnSearch addTarget:self action:@selector(onSearch:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * searchButton = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = searchButton;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    
    [self loadForUIActionView];
    
    [self loadTheRequiredCell];
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Adoptions";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    
    
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    
    self.selectedType = 0;
    self.selectedBreed = 0;
    self.selectedOrganization = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                                delegate:self
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:nil];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                                delegate:self
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:nil];
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


- (void) loadTheRequiredCell{
    
    self.btnBreed = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap2) {
        //lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 120, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap2  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap2.text = @"Breed";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
//    UILabel * lblBreedResult = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 150, 30)];
//    lblBreedResult.text = @"-";
//    lblBreedResult.textColor = [UIColor blackColor];
//    lblBreedResult.textAlignment = UITextAlignmentRight;
//    lblBreedResult.font = [UIFont boldSystemFontOfSize:13];
//    lblBreedResult.backgroundColor = [UIColor clearColor];
//    lblBreedResult.tag = 2;
    self.btnBreed.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnBreed.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnBreed addSubview:lblbtnCap2];
    //[self.btnBreed addSubview:lblBreedResult];
    
    self.btnType = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap3)
    {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap3  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap3.text = @"Animal Type";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 2;
//    UILabel * lblTypeResult = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 150, 30)];
//    lblTypeResult.text = @"-";
//    lblTypeResult.textColor = [UIColor blackColor];
//    lblTypeResult.textAlignment = UITextAlignmentRight;
//    lblTypeResult.font = [UIFont boldSystemFontOfSize:13];
//    lblTypeResult.backgroundColor = [UIColor clearColor];
//    lblTypeResult.tag = 2;
    self.btnType.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnType.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnType addSubview:lblbtnCap3];
    //[self.btnType addSubview:lblTypeResult];
    
    self.btnOrganisation = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap4)
    {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap4.text = @"Organisation";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 3;
//    UILabel * lblOrganizeResult = [[UILabel alloc]initWithFrame:CGRectMake(130, 7, 150, 30)];
//    lblOrganizeResult.text = @"-";
//    lblOrganizeResult.textColor = [UIColor blackColor];
//    lblOrganizeResult.textAlignment = UITextAlignmentRight;
//    lblOrganizeResult.font = [UIFont boldSystemFontOfSize:13];
//    lblOrganizeResult.backgroundColor = [UIColor clearColor];
//    lblOrganizeResult.tag = 2;
    self.btnOrganisation.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnOrganisation.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnOrganisation addSubview:lblbtnCap4];
    //[self.btnOrganisation addSubview:lblOrganizeResult];
    
    self.cells = @[self.btnType,self.btnBreed,self.btnOrganisation];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncBrowseInfo];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        ObjPetType * obj = [arrType objectAtIndex:row];
        return obj.strName;
    }
    if (pickerView.tag == 1) {
        if ([arrType count]>0) {
            ObjPetType * objType = [arrType objectAtIndex:self.selectedType];
            ObjBreedType * objBT = [objType.arrBreed objectAtIndex:row];
            NSString *strName = objBT.strName;
            return strName;
        }
        return @"";

    }
    if (pickerView.tag == 2) {
        ObjOrganization * objA = [arrOrgination objectAtIndex:row];
        return objA.strName;
    }
    
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrType count];
    }
    else if (pickerView.tag == 1){
        //return [arrBreed count];
        if ([arrType count]>0) {
            ObjPetType * objType = [arrType objectAtIndex:self.selectedType];
            return [objType.arrBreed count];
        }
        return 0;
    }
    else if (pickerView.tag == 2){
        return [arrOrgination count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        self.selectedType = row;
        self.selectedBreed = 0;
    }
    if (pickerView.tag == 1) {
        self.selectedBreed = row;
    }
    if (pickerView.tag == 2) {
        self.selectedOrganization = row;
    }

}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiPickerView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.uiPickerView.tag);
        
        if (self.uiPickerView.tag == 0){
            
            ObjPetType * objPT = [arrType objectAtIndex:self.selectedType];
            
            [self setTypeText:objPT.strName];
            NSLog(@"still selected Type %d",self.selectedType);
            
            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:self.selectedBreed];
            NSString * strBreedName;
            if([objPT.arrBreed count]>0){
                strBreedName = objBT.strName;
            }
            else{
                strBreedName = @"No Breed Type";
            }
            


            [self setBreedText:strBreedName];
        }
        else if (self.uiPickerView.tag == 1){
            ObjPetType * objPT = [arrType objectAtIndex:self.selectedType];
            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:self.selectedBreed];
            NSString * strBreedName = @"";
            if([objPT.arrBreed count]>0){
                strBreedName = objBT.strName;
            }
            else{
                strBreedName = @"No Breed Type";
            }
            lblbtnCap2.text = strBreedName;
            [self setBreedText:strBreedName];
        }
        
        else if (self.uiPickerView.tag == 2){
            ObjOrganization * objO = [arrOrgination objectAtIndex:self.selectedOrganization];
            
            [self setOrganisationText:objO.strName];
        }
        
        [self.uiPickerView removeFromSuperview];
    }
}

-(void)onType{
    [self.menu setTitle:@"Select Type"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(void)onBreed{
    [self.menu setTitle:@"Select Breed"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedBreed inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(void)onOrgaziation{
    [self.menu setTitle:@"Select Organization"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 2;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedOrganization inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (void)onSearch:(id)sender{
    
    AdoptionsListViewController *viewController = [[UIStoryboard storyboardWithName:@"adoptions" bundle:nil] instantiateViewControllerWithIdentifier:@"list"];
     ObjPetType * objPT = [arrType objectAtIndex:self.selectedType];
    ObjOrganization * objO = [arrOrgination objectAtIndex:self.selectedOrganization];
    ObjBreedType * objB = [objPT.arrBreed objectAtIndex:self.selectedBreed];
    viewController.selectedBreedId = objB.idx;
    viewController.selectedOrganastionId = objO.idx;
    viewController.selectedTypeId = objPT.idx;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)syncBrowseInfo{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",ADOPTIONS_INFO_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            
            NSMutableArray *  arr = [dics objectForKey:@"organisations"];
            [arrOrgination removeAllObjects];
            if(arrOrgination == nil){
                arrOrgination = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjOrganization * objO = [[ObjOrganization alloc]init];
                objO.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                objO.strName = [dicNewsFeed objectForKey:@"name"];
                
                [arrOrgination addObject:objO];
            }
            NSLog(@"arrOrgination count %d",[arrOrgination count]);
            
            NSMutableArray *  arrTTemp = [dics objectForKey:@"pet_types"];
            [arrType removeAllObjects];
            if(arrType == nil){
                arrType = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arrTTemp count];i++){
                NSDictionary * dicNewsFeed = [arrTTemp objectAtIndex:i];
                ObjPetType * objO = [[ObjPetType alloc]init];
                objO.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                objO.strName = [dicNewsFeed objectForKey:@"name"];
                NSMutableArray *  arrBreedType = [dicNewsFeed objectForKey:@"pet_breed_types"];
                objO.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];
                
                /*if (self.arrBreed == nil) {
                 self.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];
                 }
                 [self.arrBreed removeAllObjects];*/
                
                for(NSInteger b=0;b<[arrBreedType count];b++){
                    NSDictionary * dicType = [arrBreedType objectAtIndex:b];
                    ObjBreedType * objBT = [[ObjBreedType alloc]init];
                    objBT.idx = [[dicType objectForKey:@"id"]intValue];
                    objBT.strName = [dicType objectForKey:@"name"];
                    [objO.arrBreed addObject:objBT];
                }
                [arrType addObject:objO];
            }
            NSLog(@"arrType count %d",[arrType count]);
            
            /*NSMutableArray *  arrBTemp = [dics objectForKey:@"pet_breed_types"];
            [arrBreed removeAllObjects];
            if(arrBreed == nil){
                arrBreed = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arrBTemp count];i++){
                NSDictionary * dicNewsFeed = [arrBTemp objectAtIndex:i];
                ObjBreedType * objO = [[ObjBreedType alloc]init];
                objO.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                objO.strName = [dicNewsFeed objectForKey:@"name"];
                
                [arrBreed addObject:objO];
            }*/
            //NSLog(@"arrBreed count %d",[arrBreed count]);
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        [self onType];
     }
     else if (indexPath.row == 1) {
         [self onBreed];
     }
     else if (indexPath.row == 2) {
         [self onOrgaziation];
     }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    NSString * strTitle;
    strTitle=@"Browse";
    
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)];
    [v setBackgroundColor:[UIColor clearColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0, tableView.bounds.size.width - 10,20)];
    label.text = strTitle;
    label.textColor = [UIColor darkGrayColor];
    label.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    label.backgroundColor = [UIColor clearColor];
    [v addSubview:label];
    
    return v;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setBreedText:(NSString *)strValue{
    lblbtnCap2.text = [NSString stringWithFormat:@"Breed: %@",strValue];
}

- (void)setTypeText:(NSString *)strValue{
    lblbtnCap3.text = [NSString stringWithFormat:@"Type: %@",strValue];
}

- (void)setOrganisationText:(NSString *)strValue{
    lblbtnCap4.text = [NSString stringWithFormat:@"Organisation: %@",strValue];
}


@end
