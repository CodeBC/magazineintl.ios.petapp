//
//  AddReminderViewController.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddReminderViewController : PetBasedViewController
-(IBAction)onDate:(id)sender;
-(IBAction)onReminderDate:(id)sender;
-(IBAction)onAdd:(id)sender;
- (IBAction) leftBarButton:(UIBarButtonItem *)sender;
@end
