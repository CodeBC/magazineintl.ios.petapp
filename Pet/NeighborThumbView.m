//
//  PetView.m
//  Pet
//
//  Created by Zayar on 6/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "NeighborThumbView.h"
#import "UIImageView+AFNetworking.h"
#import "StringTable.h"

@implementation NeighborThumbView
@synthesize owner,intThumbType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadThumbView:(ObjPet *)objPet{
    objPetSelected = objPet;
    intThumbType = THUMB_PET;
    [thumbImgView setImageWithURL:[NSURL URLWithString:objPetSelected.strImgLink] placeholderImage:nil];
    lblDescription.text = objPet.strName;
    idx = objPet.pet_id;
    [self attachEventHandlers];
    
    thumbImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    thumbImgView.layer.borderWidth = 3.0;
}

- (void)loadThumbViewWithUser:(ObjUser *)obj{
    objUserSelected = obj;
    intThumbType = THUMB_USER;
    [thumbImgView setImageWithURL:[NSURL URLWithString:obj.strProfileImgLink] placeholderImage:nil];
    lblDescription.text = obj.strName;
    //lblDistance.text = obj.
    idx = obj.idx;
    [self attachEventHandlers];
    
    thumbImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    thumbImgView.layer.borderWidth = 3.0;
}


- (void) attachEventHandlers{
	/*UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
     [doubleTap setNumberOfTapsRequired:2];
     [self addGestureRecognizer:doubleTap];*/
    
	UITapGestureRecognizer *tapping = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
	tapping.delegate = self;
	tapping.numberOfTapsRequired = 1;
	tapping.numberOfTouchesRequired = 1;
	//[tapping requireGestureRecognizerToFail:doubleTap];
	[self addGestureRecognizer:tapping];
}

- (void) onTap:(UIGestureRecognizer *)gestureRecognizer{
	NSLog(@"onTap");
    if (intThumbType == THUMB_PET) {
        [owner onNeighborThumbViewOnClicked:objPetSelected];
    }
    else if(intThumbType == THUMB_USER){
        [owner onNeighborThumbUserViewSelected:objUserSelected];
    }
}

- (void) cleanImageView{
    if (thumbImgView.image != nil) {
        thumbImgView.image = nil;
    }
}

@end
