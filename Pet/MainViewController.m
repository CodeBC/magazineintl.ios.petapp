/*
 * Copyright (c) 2012-2013 Adam Debono. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#import "MainViewController.h"
#import "PetViewController.h"
#import "NavBarButton.h"
#import "UIBarButtonItem+appearance.h"
#import "StringTable.h"
#import "petAPIClient.h"
#import "ObjNewsFeed.h"
#import "CustomNewsFeedView.h"
#import "ProfileViewController.h"
#import "PetViewController.h"
#import "DetailPetViewController.h"
#import "PetAppDelegate.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "Utility.h"
@interface MainViewController ()
{
    IBOutlet UIImageView * imgBgView;
    NSMutableArray * arrNewsFeed;
    IBOutlet UIScrollView * scroll;
    ObjPet * selectedReportPet;
}

@end

@implementation MainViewController
@synthesize leftSecondaryLayoutType;
@synthesize rightSecondaryLayoutType;
@synthesize btnMenu;

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self updatePressed:nil];
    ADSlidingViewController *slidingViewController = [self slidingViewController];
    [slidingViewController setLeftViewAnchorWidth:280];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"d3d3d3"]];
}

- (void)viewDidLoad{
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Home";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
            [scroll setFrame:CGRectMake(0, 64, 320, self.view.frame.size.height-40)];
        }
        else{
            [scroll setFrame:CGRectMake(0, 44, 320, self.view.frame.size.height-20)];
        }
    }else{
        if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
            [scroll setFrame:CGRectMake(0, 64, 320, self.view.frame.size.height-40)];
        }
        else{
            [scroll setFrame:CGRectMake(0, 44, 320, 460-20)];
        }
    }
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    
    
    [self hereloadBgView];
}

- (void)syncNewsFeed{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",NEWS_FEED_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        NSString * strError = [dics objectForKey:@"error"];
        if (![Utility stringIsEmpty:strError shouldCleanWhiteSpace:YES]) {
            [SVProgressHUD showErrorWithStatus:strError];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        else{
            if (status == STATUS_ACTION_SUCCESS) {
                NSMutableArray *  arr = [dics objectForKey:@"pets"];
                arrNewsFeed = [[NSMutableArray alloc]initWithCapacity:[arr count]];
                for(NSInteger i=0;i<[arr count];i++){
                    NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                    ObjNewsFeed * objNewsFeed = [[ObjNewsFeed alloc]init];
                    objNewsFeed.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                    objNewsFeed.objPet.strName = [dicNewsFeed objectForKey:@"pet_name"];
                    objNewsFeed.objPet.strImgLink = [dicNewsFeed objectForKey:@"pet_image"];
                    objNewsFeed.objPet.strType = [dicNewsFeed objectForKey:@"pet_type_name"];
                    objNewsFeed.objPet.type_id = [[dicNewsFeed objectForKey:@"pet_type_id"] intValue];
                    objNewsFeed.objPet.strBreed =[dicNewsFeed objectForKey:@"pet_breed_type_name"] ;
                    objNewsFeed.objPet.breed_type_id  =[[dicNewsFeed objectForKey:@"pet_breed_type_id"] intValue];
                    objNewsFeed.objPet.strDob =[dicNewsFeed objectForKey:@"dob"];
                    objNewsFeed.objPet.strGender =[dicNewsFeed objectForKey:@"gender"];
                    objNewsFeed.objPet.strDescription =[dicNewsFeed objectForKey:@"description"];
                    objNewsFeed.objPet.pet_id =[[dicNewsFeed objectForKey:@"id"] intValue];
                    
                    if ([dicNewsFeed objectForKey:@"albums"] != [NSNull null]) {
                        NSMutableArray * arrTempAlbums = [dicNewsFeed objectForKey:@"albums"];
                        objNewsFeed.objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrTempAlbums count]];
                        for(NSInteger y=0;y<[arrTempAlbums count];y++){
                            ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                            NSDictionary * dicAlbum = [arrTempAlbums objectAtIndex:y];
                            objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                            objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                            NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                            objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                            for(NSInteger m=0;m<[arrTempImages count];m++){
                                NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                                ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                                objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];
                                
                                objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];
                                
                                objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];
                                
                                objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];
                                
                                [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                            }
                            
                            [objNewsFeed.objPet.arrAlbums addObject:objAlbum];
                        }
                    }
                    
                    NSDictionary * dicUser = [dicNewsFeed objectForKey:@"user"];
                    objNewsFeed.objUser.strFName = [dicUser objectForKey:@"first_name"];
                    objNewsFeed.objUser.strLName = [dicUser objectForKey:@"last_name"];
                    objNewsFeed.objUser.strProfileImgLink = [dicUser objectForKey:@"profile_pic"];
                    objNewsFeed.objUser.idx = [[dicUser objectForKey:@"id"]intValue];
                    objNewsFeed.objUser.strName = [dicUser objectForKey:@"username"];
                    [arrNewsFeed addObject:objNewsFeed];
                }
                NSLog(@"news feed count %d",[arrNewsFeed count]);
                [self loadTheScrollWith:arrNewsFeed];
            }
            else if(status == STATUS_ACTION_FAILED){
                
                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strMsg];
            }
            else if(status == STATUS_INVALID_AUTH){
                //[self textValidateAlertShow:strErrorMsg];
                NSString * strMsg = [dics objectForKey:@"message"];
                [SVProgressHUD showErrorWithStatus:strMsg];
                PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
                [delegate logout:self];
            }
            [SVProgressHUD dismiss];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncNewsFeed];
}

- (IBAction)leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (IBAction)rightBarButton:(UIBarButtonItem *)sender {
	//[[self slidingViewController] anchorTopViewTo:ADAnchorSideLeft];
}

- (IBAction)updatePressed:(UIButton *)sender {
	/*ADSlidingViewController *slidingViewController = [self slidingViewController];
	
	[slidingViewController setLeftViewAnchorWidth:[[self leftAnchorAmountStepper] value]];
	[slidingViewController setRightViewAnchorWidth:[[self rightAnchorAmountStepper] value]];
	
	[slidingViewController setLeftViewAnchorWidthType:[[self leftAnchorWidthType] selectedSegmentIndex]];
	[slidingViewController setRightViewAnchorWidthType:[[self rightAnchorWidthType] selectedSegmentIndex]];
	
	[slidingViewController setLeftMainAnchorType:[[self leftAnchorLayoutType] selectedSegmentIndex]];
	[slidingViewController setRightMainAnchorType:[[self rightAnchorLayoutType] selectedSegmentIndex]];
	
	[slidingViewController setLeftUnderAnchorType:[[self leftSecondaryLayoutType] selectedSegmentIndex]];
	[slidingViewController setRightUnderAnchorType:[[self rightSecondaryLayoutType] selectedSegmentIndex]];
	
	[slidingViewController setUndersidePersistencyType:[[self undersidePersistencyControl] selectedSegmentIndex]];*/
}

- (IBAction)leftAnchorValueChanged:(UIStepper *)sender {
	[[self leftAnchorAmountLabel] setText:[NSString stringWithFormat:@"%@", @( [sender value])]];
}

- (IBAction)rightAnchorValueChanged:(UIStepper *)sender {
	[[self rightAnchorAmountLabel] setText:[NSString stringWithFormat:@"%@", @( [sender value] )]];
}

- (void) loadTheScrollWith:(NSMutableArray *)arrNF{
    for(UIView * v in scroll.subviews){
        if( [v isKindOfClass: [CustomNewsFeedView class]]){
            v.hidden = TRUE;
            CustomNewsFeedView * customView = (CustomNewsFeedView *)v;
            customView = nil;
        }
    }
    if ( [arrNF count] > 0) {
        int x=0;
        int y=0;
        int articleCount = 0;
        int yOffset = 10;
        int height = 210;
        int total = 0;
        
        for (ObjNewsFeed * objNF in arrNF) {
            y = (height + yOffset) * articleCount;
            //NSLog(@"custom thumb view x :%d and y:%d",x,0);
            CustomNewsFeedView * customView;
            NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomNewsFeedView" owner:nil options:nil];
            for(id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[CustomNewsFeedView class]]){
                    customView = (CustomNewsFeedView *) currentObject;
                    customView.frame = CGRectMake(x,y,320, height);
                    [customView loadTheView];
                    [customView loadTheViewWith:objNF];
                    break;
                }
            }
            customView.owner = self;
            //[customView setBackgroundColor:[UIColor whiteColor]];
            [scroll addSubview:customView];
            if (articleCount%2==0) {
                [customView setThemeGrayAndWhite:YES];
            }
            else{
                [customView setThemeGrayAndWhite:NO];
            }
            articleCount ++;
            total = y;
        }
        
        total += height + yOffset;
        scroll.pagingEnabled = FALSE;
        [scroll setContentSize:CGSizeMake(scroll.frame.size.width,total)];
        [self.view bringSubviewToFront:scroll];
    }
}

- (void) onPetSelected:(ObjPet *)objSPet andNewsFeed:(ObjNewsFeed *)objNewsFeed{
    DetailPetViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"petDetail"];
    
    //viewController.pe = objAdo;
    viewController.objPet = objSPet;
    viewController.objNewsFeed = objNewsFeed;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) onUserSelected:(ObjUser *)objSPet{
    //uprofile
    ProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"uprofile"];

    //viewController.pe = objAdo;
    viewController.user = objSPet;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) onMenuSelectedPet:(ObjPet *)objSPet andButton:(UIButton *)sender{
    NSLog(@"on menu!!!");
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Report Inappropriate"
                                                    otherButtonTitles:nil];
    [actionSheet setTag:1];
    [actionSheet setBackgroundColor:[UIColor grayColor]];
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [delegate windowViewReAdjust];
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    [actionSheet showInView:window];
    selectedReportPet = objSPet;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Report Inappropriate");
        if (selectedReportPet) {
            [self syncReportPet:selectedReportPet];
        }
    }
    else if(buttonIndex == 1){
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    [delegate windowViewAdjust];
}
- (void)syncReportPet:(ObjPet *)obj{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary * param = @{@"pet_id":[NSString stringWithFormat:@"%d",obj.pet_id]};
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",PET_REPORT_LINK,strSession] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[self syncNewsFeed];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)viewDidUnload {
	[self setLeftSecondaryLayoutType:nil];
	[self setRightSecondaryLayoutType:nil];
	[super viewDidUnload];
}
@end
