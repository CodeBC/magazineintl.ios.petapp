//
//  PetView.h
//  Pet
//
//  Created by Zayar on 6/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import "ObjUser.h"
@protocol ThumbImgViewDelegate
- (void) onThumbImgViewSelected:(ObjPet *)objSPet;
- (void) onThumbImgUserViewSelected:(ObjUser *)obj;
- (void) onThumbImgViewOnClicked:(ObjPet *)objSPet;
@end
@interface PetThumbView : UIView
{
    IBOutlet UIImageView * thumbImgView;
    IBOutlet UILabel * lblDescription;
    
    int idx;
    id<ThumbImgViewDelegate> owner;
    ObjPet * objPetSelected;
    ObjUser * objUserSelected;
    int intThumbType;
}
@property id<ThumbImgViewDelegate> owner;
@property int intThumbType;
- (void)loadThumbView:(ObjPet *)objPet;
- (void)loadThumbViewWithUser:(ObjUser *)obj;
- (void) cleanImageView;
@end
