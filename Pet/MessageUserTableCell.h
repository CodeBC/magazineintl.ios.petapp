//
//  MessageUserTableCell.h
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageUserTableCell : UITableViewCell
{
    IBOutlet UILabel * lblName;
    IBOutlet UIImageView * imgThumbView;
}
- (void)loadTheViewWith:(ObjUser *)objUser;
@end
