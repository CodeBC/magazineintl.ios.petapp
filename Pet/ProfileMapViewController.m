//
//  ProfileMapViewController.m
//  Pet
//
//  Created by Zayar on 6/15/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ProfileMapViewController.h"
#import "DisplayMap.h"
#import "StringTable.h"
@interface ProfileMapViewController ()
{
    //map
    //MKMapView * mapView;
}
@property (nonatomic, strong) IBOutlet MKMapView * mapView;
@end

@implementation ProfileMapViewController
@synthesize owner,seletedLat,seletedLon,isEditable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)manageEditeableView{
    if (isEditable) {
        UIBarButtonItem * btnCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onCancel:)];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = btnCancel;
        
        UIBarButtonItem * btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = btnDone;
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0; //user needs to press for 2 seconds
        [self.mapView addGestureRecognizer:lpgr];
    }
    else{
        UIBarButtonItem * btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = btnDone;
    }
}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    [self removeAllAnnotations];
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    
    DisplayMap *ann = [[DisplayMap alloc] init];
    //ann.canShowCallout=YES;
    //ann.animatesDrop=YES;
    
    ann.coordinate = touchMapCoordinate;
    [self.mapView addAnnotation:ann];
}

- (void)setUserSelectedLocation:(float)lat andLong:(float)lon{
    [self removeAllAnnotations];
    
    if (lat != 0.0 && lon != 0.0) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        CLLocationCoordinate2D touchMapCoordinate = location.coordinate;
        
        DisplayMap *ann = [[DisplayMap alloc] init];
        //ann.canShowCallout=YES;
        //ann.animatesDrop=YES;
        ann.coordinate = touchMapCoordinate;
        [self.mapView addAnnotation:ann];
        MKMapRect zoomRect = MKMapRectNull;
        for (id <MKAnnotation> annotation in self.mapView.annotations)
        {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
        [self.mapView setVisibleMapRect:zoomRect animated:YES];
    }
    else{
        if (isEditable) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_TITLE
                                                             message:@"For better accuracy, press and hold on the area of the map that you are at until a red pin appears."
                                                            delegate:self cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)done:(id)sender{
    NSLog(@"self mapview count %d",[self.mapView.annotations count]);
    if ([self.mapView.annotations count] >0) {
        DisplayMap *ann = [self.mapView.annotations objectAtIndex:0];
        CLLocationCoordinate2D selectedLocation = ann.coordinate;
        float currentLat = selectedLocation.latitude;
        float currentLong = selectedLocation.longitude;
        [owner finishWithCoordinate:currentLat andlong:currentLong];
        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
}

- (void)removeAllAnnotations {
    
    NSMutableArray *annotationsToRemove = [NSMutableArray arrayWithCapacity:[self.mapView.annotations count]];
    NSLog(@"self mapview count %d",[self.mapView.annotations count]);
    
    for (int i = 0; i < [self.mapView.annotations count]; i++) {
        NSLog(@"removepin......");
        //if ([[self.mapView.annotations objectAtIndex:i] isKindOfClass:[DisplayMap class]]) {
            NSLog(@"removepin2......");
            [annotationsToRemove addObject:[self.mapView.annotations objectAtIndex:i]];
        //}
    }
    
    [self.mapView removeAnnotations:annotationsToRemove];
}

- (void)onCancel:(id)sender{
    [owner closeMap];
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    //[self loadCurrentLocation:0.0 andLong:0.0];
    [self manageEditeableView];
    NSLog(@"selected location %f and long %f",seletedLat,seletedLon);
    [self setUserSelectedLocation:seletedLat andLong:seletedLon];
    self.mapView.showsUserLocation = YES;
}

- (void) loadCurrentLocation:(float)late andLong:(float)lng{
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude =19.8761;
    region.center.longitude = 96.0452;
    region.span.longitudeDelta = 19.8761f;
    region.span.latitudeDelta = 96.0452f;
    [self.mapView setRegion:region animated:YES];
    
    [self.mapView setDelegate:self];
    
    DisplayMap *ann = [[DisplayMap alloc] init];
    
    ann.coordinate = region.center;
    [self.mapView addAnnotation:ann];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
