//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable

NSString * const DBNAME	= @"smath.sqlite";
NSString * const APP_TITLE = @"Pet";
NSString * const APP_ID = @"---";
//NSString * const BASE_LINK= @"http://pets.iwh.com.sg/petmobile/";
//NSString * const BASE_LINK= @"http://192.168.2.3:3000/";
NSString * const BASE_LINK= @"http://pets-app.herokuapp.com/";
NSString * const GENR_KEY_LINK = @"modules/client/_ext_generate_key.php";
NSString * const LOGIN_LINK=@"api/users/sign_in";
NSString * const REGISTER_LINK= @"api/v1/users";
NSString * const CHECK_LOGIN_SESSION_LINK = @"modules/client/_ext_check_user_session.php";
NSString * const PROFILE_LINK = @"api/v1/users";
NSString * const PROFILE_EDIT_LINK= @"api/v1/users";
NSString * const MYPET_LINK=@"api/v1/pets";
NSString * const ADDPET_LINK=@"api/v1/pets";
NSString * const EDITPET_LINK =@"api/v1/pets";
NSString * const REMINDER_LINK = @"api/v1/reminders";
NSString * const LOST_FOUND_BROWSE_LINK= @"api/v1/lost_founds";
NSString * const LOST_REPORT_LINK= @"api/v1/lost";
NSString * const FOUND_REPORT_LINK= @"api/v1/found";
NSString * const VIDEO_LINK= @"api/v1/videos";
NSString * const DEAL_LINK= @"api/v1/deals";
NSString * const PET_TYPE_BREED_LINK=@"api/v1/pets/types";
NSString * const NEWS_FEED_LINK=@"api/v1/home";
NSString * const MESSAGE_LINK=@"/api/v1/messages";
NSString * const MESSAGE_DETAIL_LINK=@"api/v1/messages/";
NSString * const MESSAGE_PUSH_LINK=@"api/v1/messages/sendto";
NSString * const MESSAGE_CONVERSATION_DELETE_LINK = @"api/v1/messages/delete/conversation";
NSString * const MESSAGE_DELETE_LINK = @"api/v1/messages/delete/message";
NSString * const ALL_USER_LINK=@"api/v1/users/all";
NSString * const LOGOUT_LINK=@"api/users/sign_out";
NSString * const USER_CHANGE_PASS_LINK=@"api/v1/users/pwchange";
NSString * const MAGAZINE_LINK=@"api/v1/magazines";
NSString * const REPORT_LOST_INFO_LINK=@"api/v1/lost/info";
NSString * const REPORT_FOUND_INFO_LINK=@"api/v1/found/info";
NSString * const ADOPTIONS_INFO_LINK=@"api/v1/adoptions/browse";
NSString * const ADOPTIONS_LIST_LINK=@"api/v1/adoptions/search";
NSString * const ADOPTIONS_INTERESTED=@"api/v1/adoptions/interest";
NSString * const ALBUM_NEW_LINK=@"api/v1/albums";
NSString * const FB_REGISTER_LINK=@"api/v1/users";
NSString * const ALBUM_PHOTO_NEW_LINK=@"api/v1/album_images";
NSString * const REPORT_ABUSE_LINK=@"api/v1/lost/report_abuse";
NSString * const SETTING_PRAVICY_LINK=@"api/v1/settings";
NSString * const SETTING_PRAVICY_GET_LINK=@"api/v1/settings";
NSString * const PET_REPORT_LINK = @"api/v1/report";

NSString * const COUNTRY_STRING_ARRAY = @"Singapore,Afghanistan,Albania,Algeria,Andorra,Angola,Antigua &amp; Barbuda,Argentina,Armenia,Australia,Austria,Azerbaijan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bhutan,Bolivia,Bosnia and Herzegovina,Botswana,Brazil,Brunei,Bulgaria,Burkina Faso,Burma,Burundi,Cambodia,Cameroon,Canada,Cape Verde,Central African Republic,Chad,Chile,China,Colombia,Comoros,Congo,Democratic Republic of the,Congo,Republic of the,Costa Rica,Cote d\'voire,Croatia,Cuba,Cyprus,Czech Republic,Denmark,Djibouti,Dominica,Dominican Republic,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Ethiopia,Fiji,Finland,France,Gabon,Gambia,The,Georgia,Ghana,Greece,Grenada,Guatemala,Guinea,Guinea-Bissau,Guyana,Haiti,Holy See,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran,Iraq,Ireland,Israel,Italy,Jamaica,Japan,Jordan,Kazakhstan,Kenya,Kiribati,Korea,North,Korea,South,Kosovo,Kuwait,Kyrgyzstan,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macau,Macedonia,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Mauritania,Mauritius,Mexico,Micronesia,Moldova,Monaco,Mongolia,Montenegro,Morocco,Mozambique,Namibia,Nauru,Nepal,Netherlands,Netherlands Antilles,New Zealand,Nicaragua,Niger,Nigeria,North Korea,Norway,Oman,Pakistan,Palau,Palestinian Territories,Panama,Papua New Guinea,Paraguay,Peru,Philippines,Poland,Portugal,Qatar,Romania,Russia,Rwanda,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Samoa,San Marino,Sao Tome and Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Slovakia,Slovenia,Solomon Islands,Somalia,South Africa,South Korea,South Sudan,Spain,Sri Lanka,Sudan,Suriname,Swaziland,Sweden,Switzerland,Syria,Taiwan,Tajikistan,Tanzania,Thailand ,Timor-Leste,Togo,Tonga,Trinidad and Tobago,Tunisia,Turkey,Turkmenistan,Tuvalu,Uganda,Ukraine,United Arab Emirates,United Kingdom,Uruguay,Uzbekistan,Vanuatu,Venezuela,Vietnam,Yemen,Zambia,Zimbabwe,";
//api/v1/adoptions/browse

NSString * const ANSWER_TYPE_IMAGE = @"I";
NSString * const ANSWER_TYPE_VIDEO = @"V";
NSString * const ANSWER_TYPE_TEXT = @"T";
NSString * const ANSWER_TYPE_NULL = @"null";

NSString * const REPEAT_TYPE_NEVER=@"Never";
NSString * const REPEAT_TYPE_EVER=@"Always";
NSString * const REPEAT_TYPE_ONCE=@"Once";

NSString * const ERRMSG1 = @"Please enter First Name is required!";
NSString * const ERRMSG2 = @"Please enter Last Name is required!";
NSString * const ERRMSG3 = @"Please enter User Name is required!";
NSString * const ERRMSG4 = @"Please enter valid email address is required!";
NSString * const ERRMSG5 = @"Please enter Password is required!";
NSString * const ERRMSG6 = @"User name and password is incorrect!";
NSString * const ERRMSG7 = @"The email is already exit, Please use another email!";
NSString * const ERRMSG8 = @"Passwords must match!";
NSString * const ERRMSG9 = @"Passwords must have minimum 8 characters!";
NSString * const ERRMSG10= @"Please select a country!";
NSString * const ERRMSG11= @"Please enter Postal Code!";

NSString * const APN_SERVER_PATH  = @"test.balancedconsultancy.com.sg/tsmaths/apn";

int const STATUS_ACTION_SUCCESS = 1;
int const STATUS_RETURN_RECORD = 3;
int const STATUS_ACTION_FAILED = 2;
int const STATUS_SESSION_EXPIRED = 5;
int const STATUS_NO_RECORD_FOUND = 6;
int const STATUS_INVALID_AUTH = 3;

int const REMINDER_DONE = 1;
int const REMINDER_NOT_DONE = 0;
int const REMINDER_PRESET = 99;

int const THUMB_PET=1;
int const THUMB_USER=2;

float const ACTIONSHEET_HEIGHT = 462;

NSString * const ANSWER_STATUS_PENDING = @"P";
NSString * const ANSWER_STATUS_ANSWERED = @"A";
NSString * const ANSWER_STATUS_REJECTED = @"R";

//double const CACHE_DURATION		= 86400 * 5.0;
double const CACHE_DURATION		= 0;

int const MINUTE_INTERVAL = 60;
@end
