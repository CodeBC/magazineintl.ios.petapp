//
//  DetailPetViewController.m
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DetailPetViewController.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "ObjUser.h"
#import "UIImageView+AFNetworking.h"
#import "EditPetViewController.h"
#import "NavBarButton1.h"
#import "NavBarButton3.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "AlbumListViewController.h"
#import "petAPIClient.h"
#import "ProfileViewController.h"

@interface DetailPetViewController ()
{
    IBOutlet UILabel * lblPetName;
    IBOutlet UILabel * lblTypeBreed;
    IBOutlet UILabel * lblAddress;
    IBOutlet UILabel * lblDob;
    IBOutlet UIImageView * imgPetProfile;
    IBOutlet UIImageView * imgOwnerProfile;
    IBOutlet UIImageView * imgAlbum;
    IBOutlet UILabel * lblOwnerName;
    IBOutlet UILabel * lblOwnerGender;
    IBOutlet UIImageView * imgBgView;
    IBOutlet UILabel * lblCapOwer;
    IBOutlet UILabel * lblCapAlbums;
    UILabel * lblName;

}
@end

@implementation DetailPetViewController
@synthesize objPet,isFromOther,objNewsFeed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
    }

    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/

    NSString * strNormalFont=@"Agfa Rotis Sans Serif";
    NSString * strValueFontName=@"SerifaStd-Black";
    lblPetName.font = [UIFont fontWithName:strValueFontName size:14];
    lblOwnerName.font = [UIFont fontWithName:strValueFontName size:14];
    lblAddress.font = [UIFont fontWithName:strNormalFont size:14];
    lblDob.font = [UIFont fontWithName:strNormalFont size:14];
    lblTypeBreed.font = [UIFont fontWithName:strNormalFont size:14];
    lblCapOwer.font = [UIFont fontWithName:strValueFontName size:14];
    lblCapAlbums.font = [UIFont fontWithName:strValueFontName size:14];

}

- (IBAction)onOwnerClicked:(id)sender{
    ProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"uprofile"];

    //viewController.pe = objAdo;
    viewController.user = objNewsFeed.objUser;
    viewController.isFromOther = TRUE;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addTheNavBtn{
    /*UIBarButtonItem * btnAdd = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(onEdit:)];
    self.navigationItem.rightBarButtonItem = btnAdd;*/

    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];

	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;

    if (!isFromOther) {
        NavBarButton3 *btnEdit = [[NavBarButton3 alloc] init];
        [btnEdit addTarget:self action:@selector(onEdit:) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithCustomView:btnEdit];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = editButton;
    }
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self addTheNavBtn];
    //self.navigationItem.title = objPet.strName;
    if (lblName == nil) {
        lblName  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    }
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= objPet.strName;
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    if (!isFromOther) {
        //[self loadPetDetailView];
        [self syncDetailPetWithId:objPet.pet_id];
    }
    else{
        //[self loadTheViewWithNewsFeed:objNewsFeed];
        [self syncDetailPetWithId:objPet.pet_id];
    }
}

- (void) loadTheViewWithNewsFeed:(ObjNewsFeed *)objnewsFeed{
    lblTypeBreed.text = [NSString stringWithFormat:@"%@-%@",objnewsFeed.objPet.strType,objnewsFeed.objPet.strBreed];
    if ([self stringIsEmpty:objPet.strDescription shouldCleanWhiteSpace:YES]) {
        lblAddress.text = @"Not specified";
    }
    else
        lblAddress.text = [NSString stringWithFormat:@"%@",objnewsFeed.objPet.strDescription];

    if ([self stringIsEmpty:objPet.strDob shouldCleanWhiteSpace:YES]) {
        lblDob.text = @"Not specified";
    }else
        lblDob.text = [NSString stringWithFormat:@"Born on %@",objnewsFeed.objPet.strDob];

    [imgOwnerProfile setImageWithURL:[NSURL URLWithString:objnewsFeed.objUser.strProfileImgLink] placeholderImage:[UIImage imageNamed:@"img_profile_default"]];
    [imgPetProfile setImageWithURL:[NSURL URLWithString:objnewsFeed.objPet.strImgLink] placeholderImage:[UIImage imageNamed:@"img_profile_default"]];

    NSLog(@"Album count %d",[objnewsFeed.objPet.arrAlbums count]);
    if ([objnewsFeed.objPet.arrAlbums count]>0) {
        ObjAlbum * objA = [objnewsFeed.objPet.arrAlbums objectAtIndex:0];
        if ([objA.arrAlbumPhoto count]>0) {

            ObjAlbumPhoto * objAP = [objA.arrAlbumPhoto objectAtIndex:0];
            // [imgAlbum setImageWithURL:[NSURL URLWithString:objAP.strImgURL] placeholderImage:nil];
            [imgAlbum setImageWithURL:[NSURL URLWithString:objAP.strImgURL] placeholderImage:[UIImage imageNamed:@"img_default_add.png"]];
            NSLog(@"Here is in album str link %@",objAP.strImgURL);
        }
        else{
            if (isFromOther)
                [imgAlbum setImage:[UIImage imageNamed:@"img_default_noImage"]];

            else [imgAlbum setImage:[UIImage imageNamed:@"img_default_add.png"]];
        }
    }
    else{
        if (isFromOther)
            [imgAlbum setImage:[UIImage imageNamed:@"img_default_noImage"]];

        else [imgAlbum setImage:[UIImage imageNamed:@"img_default_add.png"]];
    }
    lblOwnerName.text = objnewsFeed.objUser.strName;
    lblOwnerGender.text = objnewsFeed.objUser.strGender;
}

- (void)onEdit:(UIButton *)sender{
    EditPetViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"petEdit"];
    viewController.objPet = objNewsFeed.objPet;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)loadPetDetailView{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];

    ObjUser * user = [delegate.db getUserObj];
    lblPetName.text = objPet.strName;
    lblTypeBreed.text = [NSString stringWithFormat:@"%@-%@",objPet.strType,objPet.strBreed];
    if ([self stringIsEmpty:objPet.strDescription shouldCleanWhiteSpace:YES]) {
        lblAddress.text = @"Not specified";
    }
    else
    lblAddress.text = [NSString stringWithFormat:@"%@",objPet.strDescription];

    if ([self stringIsEmpty:objPet.strDob shouldCleanWhiteSpace:YES]) {
        lblDob.text = @"Not specified";
    }else
    lblDob.text = [NSString stringWithFormat:@"Born on %@",objPet.strDob];
    NSLog(@"img profile link %@",user.strProfileImgLink);
    [imgOwnerProfile setImageWithURL:[NSURL URLWithString:user.strProfileImgLink] placeholderImage:[UIImage imageNamed:@"img_profile_default"]];
    [imgPetProfile setImageWithURL:[NSURL URLWithString:objPet.strImgLink] placeholderImage:[UIImage imageNamed:@"img_profile_default"]];
    if ([objPet.arrAlbums count]>0) {
        ObjAlbum * objA = [objPet.arrAlbums objectAtIndex:0];
        if ([objA.arrAlbumPhoto count]>0) {
            ObjAlbumPhoto * objAP = [objA.arrAlbumPhoto objectAtIndex:0];
           // [imgAlbum setImageWithURL:[NSURL URLWithString:objAP.strImgURL] placeholderImage:nil];
            [imgAlbum setImageWithURL:[NSURL URLWithString:objAP.strImgURL] placeholderImage:nil];
        }
    }
    lblOwnerName.text = user.strName;
    lblOwnerGender.text = user.strGender;
}

- (IBAction)onAlbumSelected:(id)sender{
    AlbumListViewController  *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"albums"];
    viewController.objP = objNewsFeed.objPet;
    viewController.isFromOther = isFromOther;
    [self.navigationController pushViewController:viewController animated:YES];

}

- (BOOL) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {

    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }

    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }

    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }

    return NO;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) syncDetailPetWithId:(int)idx{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",ADDPET_LINK,idx,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        NSDictionary * dicNewsFeed = [dics objectForKey:@"pet"];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjNewsFeed * obj = [[ObjNewsFeed alloc]init];
            obj.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
            obj.objPet.strName = [dicNewsFeed objectForKey:@"pet_name"];
            obj.objPet.strImgLink = [dicNewsFeed objectForKey:@"pet_image"];
            obj.objPet.strType = [dicNewsFeed objectForKey:@"pet_type_name"];
            obj.objPet.type_id = [[dicNewsFeed objectForKey:@"pet_type_id"] intValue];
            obj.objPet.strBreed =[dicNewsFeed objectForKey:@"pet_breed_type_name"] ;
            obj.objPet.breed_type_id  =[[dicNewsFeed objectForKey:@"pet_breed_type_id"] intValue];
            obj.objPet.strDob =[dicNewsFeed objectForKey:@"dob"];
            obj.objPet.strGender =[dicNewsFeed objectForKey:@"gender"];
            obj.objPet.strDescription =[dicNewsFeed objectForKey:@"description"];
            obj.objPet.pet_id =[[dicNewsFeed objectForKey:@"pet_id"] intValue];

            if ([dicNewsFeed objectForKey:@"albums"] != [NSNull null]) {
                NSMutableArray * arrTempAlbums = [dicNewsFeed objectForKey:@"albums"];
                obj.objPet.arrAlbums = [[NSMutableArray alloc]initWithCapacity:[arrTempAlbums count]];
                for(NSInteger y=0;y<[arrTempAlbums count];y++){
                    ObjAlbum * objAlbum = [[ObjAlbum alloc]init];
                    NSDictionary * dicAlbum = [arrTempAlbums objectAtIndex:y];
                    objAlbum.idx = [[dicAlbum objectForKey:@"album_id"]intValue];
                    objAlbum.strName = [dicAlbum objectForKey:@"album_name"];
                    NSMutableArray * arrTempImages = [dicAlbum objectForKey:@"album_images"];
                    objAlbum.arrAlbumPhoto = [[NSMutableArray alloc]initWithCapacity:[arrTempImages count]];
                    for(NSInteger m=0;m<[arrTempImages count];m++){
                        NSDictionary * dicAP = [arrTempImages objectAtIndex:m];
                        ObjAlbumPhoto * objAPhoto = [[ObjAlbumPhoto alloc]init];
                        objAPhoto.strName = [dicAP objectForKey:@"album_image_name"];

                        objAPhoto.strImgURL = [dicAP objectForKey:@"album_image_url"];

                        objAPhoto.strThumbImgURL = [dicAP objectForKey:@"thumbnail"];

                        objAPhoto.idx = [[dicAP objectForKey:@"id"] intValue];

                        [objAlbum.arrAlbumPhoto addObject:objAPhoto];
                    }

                    [obj.objPet.arrAlbums addObject:objAlbum];
                }
            }

            obj.objUser.strGender = [dicNewsFeed objectForKey:@"owner_gender"];
            obj.objUser.strName = [dicNewsFeed objectForKey:@"owner_name"];
            obj.objUser.strProfileImgLink = [dicNewsFeed objectForKey:@"owner_image"];
            obj.objUser.idx = [[dicNewsFeed objectForKey:@"owner_id"]intValue];

            [self loadTheViewWithNewsFeed:obj];
            objNewsFeed = obj;
        }
        else if(status == STATUS_ACTION_FAILED){

            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
