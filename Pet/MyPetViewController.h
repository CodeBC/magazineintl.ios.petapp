//
//  MyPetViewController.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPetViewController : PetBasedViewController
- (IBAction) leftBarButton:(UIBarButtonItem *)sender;
- (IBAction) rightBarButton:(id)sender;
@end
