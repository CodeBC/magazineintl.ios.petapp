//
//  MessageDetailViewController.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"
#import "ObjConversation.h"
#import "CustomPullToRefresh.h"
@interface MessageDetailViewController : PetBasedViewController<UIBubbleTableViewDataSource,CustomPullToRefreshDelegate>
- (IBAction)sayPressed:(id)sender;
@property (nonatomic,strong) ObjConversation * objConv;
@end
