//
//  ProfileMapViewController.h
//  Pet
//
//  Created by Zayar on 6/15/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@protocol ProfileMapViewDelegate
- (void) finishWithCoordinate:(float)late andlong:(float)lng;
- (void) closeMap;
@end
@interface ProfileMapViewController : UIViewController
{
    id<ProfileMapViewDelegate> owner;
    float seletedLat;
    float seletedLon;
}
@property id<ProfileMapViewDelegate> owner;
@property float seletedLat;
@property float seletedLon;
@property BOOL isEditable;
@end
