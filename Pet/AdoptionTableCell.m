//
//  AdoptionTableCell.m
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AdoptionTableCell.h"
#import "ObjAdoption.h"
#import "UIImageView+AFNetworking.h"

@implementation AdoptionTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadTheCellWith:(ObjAdoption *)objAdo{
    lblTypeAndBreed.text = [NSString stringWithFormat:@"%@, %@",objAdo.strPetType,objAdo.strPetBreed];
    lblOrgaization.text = objAdo.strOrganization;
    [imgView setImageWithURL:[NSURL URLWithString:objAdo.strPetImgLink]placeholderImage:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
