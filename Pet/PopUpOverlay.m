//
//  PopUpOverlay.m
//  zBox
//
//  Created by Zayar Cn on 6/2/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "PopUpOverlay.h"

@implementation PopUpOverlay

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)popupOverlayViewDidLoad{
    NSString * strPath = [NSString stringWithFormat:@"%@/popupbg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
@end
