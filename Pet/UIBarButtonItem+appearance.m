#import "UIBarButtonItem+appearance.h"
#import <objc/runtime.h>

@implementation UIBarButtonItem (appearance)

+ (void) setupAppearance {
   [[UIBarButtonItem appearance] setBackgroundImage:[UIImage imageNamed:@"bar_button"]
                                           forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
   [[UIBarButtonItem appearance] setBackgroundImage:[UIImage imageNamed:@"bar_button_hl"]
                                           forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

   Class klass = objc_getClass("UIBarButtonItem");
   Method targetMethod = class_getInstanceMethod(klass, @selector(setStyle:));
   Method newMethod = class_getInstanceMethod(klass, @selector(__setStyle:));
   method_exchangeImplementations(targetMethod, newMethod);
}

- (void) __setStyle:(UIBarButtonItemStyle)style {
   [self __setStyle:style];

   if(style == UIBarButtonItemStyleDone) {
      [self setBackgroundImage:[UIImage imageNamed:@"bar_button_save"]
                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
      [self setBackgroundImage:[UIImage imageNamed:@"bar_button_save_hl"]
                      forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
   } else {
      [self setBackgroundImage:[UIImage imageNamed:@"btn_slide"]
                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
      [self setBackgroundImage:[UIImage imageNamed:@"btn_slide"]
                      forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
   }
}

@end
