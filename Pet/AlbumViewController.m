//
//  AlbumViewController.m
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AlbumViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjDeal.h"
#import <QuartzCore/QuartzCore.h>
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "NavBarButton1.h"
#import "NavBarButton2.h"
#import "Utility.h"
#import "ObjAlbum.h"
#import "ObjAlbumPhoto.h"
#import "UIImageView+AFNetworking.h"
#import "FGalleryViewController.h"
#import "TSActionSheet.h"
#import "ObjAlbumPhoto.h"
#import "petAPIClient.h"

@interface AlbumViewController ()
{
    IBOutlet UIScrollView * scrollView;
    NSMutableArray * arrImages;
    NSMutableArray * arrThumbImages;
    BOOL isFromPhotoLibrary;
    int fromCamera;
    IBOutlet UIImageView * imgBgView;
    UIImagePickerController *imagePicker;
    
    NSMutableArray *networkCaptions;
    NSMutableArray *networkImages;
    FGalleryViewController *networkGallery;
    UITextField * txtParentPassword;
    ObjAlbumPhoto * objAlbumPhoto;
    UILabel * lblName;
    UIImage * selectedImage;
    UITextField * txtEdit;
    ObjAlbumPhoto * objSelectedPhoto;
}
@end
/*
 if ([Utility isGreaterOSVersion:@"7.0"]) {
 // iOS 7
 self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y, self.navigationController.navigationBar.frame.size.width, 64);
 }
 */
@implementation AlbumViewController
@synthesize objAlbum,isFromOther;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    arrThumbImages = [[NSMutableArray alloc]init];
    arrImages = [[NSMutableArray alloc]init];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        //[imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        [scrollView setFrame:CGRectMake(-4, 44, 320, self.view.frame.size.height)];
    }else{
        //[imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        [scrollView setFrame:CGRectMake(-4, 44, 320, self.view.frame.size.height)];
    }*/
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        [scrollView setFrame:CGRectMake(-4, 64, 320, self.view.frame.size.height)];
    }
    else [scrollView setFrame:CGRectMake(-4, 44, 320, self.view.frame.size.height)];
    //[imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    
    networkImages = [[NSMutableArray alloc] init];
    networkCaptions = [[NSMutableArray alloc] init];
    
     imagePicker = [[UIImagePickerController alloc] init];
}

- (void) manageNavBarButton{
    if (!isFromOther) {
        NavBarButton2 *btnAdd = [[NavBarButton2 alloc] init];
        [btnAdd addTarget:self action:@selector(showActionSheet:forEvent:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = addButton;
    }
}

-(void) showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Camera" block:^{
        NSLog(@"Camera");
        [self showCamera];
    }];
    [actionSheet addButtonWithTitle:@"Library" block:^{
        NSLog(@"Library");
        [self showPhotoLibrary];
    }];
    [actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    
    [actionSheet showWithTouch:event];
}

- (void) viewWillAppear:(BOOL)animated{
    [self manageNavBarButton];
    [self reloadTheScrollImageViewWithThumbArr:objAlbum.arrAlbumPhoto];
    arrImages = objAlbum.arrAlbumPhoto;
    if (lblName == nil) {
        lblName  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    }
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= objAlbum.strName;
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
}

- (void) showPhotoLibrary{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    // Set source to the camera
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        fromCamera = 0;
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Fuction is not suported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

-(void) showCamera{
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        fromCamera = 1;
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self deleteAllFilesInDocuments];
    NSLog(@">>>>>>>>>didFinishPickingMediaWithInfo");
    //iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    // Access the uncropped image from info dictionary
    //UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    //UIImage * orginalImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage * thumbImage=[Utility imageByScalingProportionallyToSize:editedImage newsize:CGSizeMake(90, 100) resizeFrame:TRUE];
    
    //UIImage * resizedImageFromLib=[Utility forceImageResize:editedImage newsize:CGSizeMake(90, 90)];
    
    if (fromCamera == 1) {
        // Save image;
        
        /*UIImageWriteToSavedPhotosAlbum(thumbImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
         NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];
         //delegate.pathSelectedPic = seletedPicPath;
         NSData *imageData = UIImagePNGRepresentation(thumbImage);
         [imageData writeToFile:seletedPicPath atomically:NO];*/
        NSLog(@"here is photo selected!!");
        //[self onCrop:resizedImage];
        if (thumbImage != nil) {
            //[arrThumbImages addObject:thumbImage];
            //[arrImages addObject:editedImage];
            //[self reloadTheScrollImageViewWithThumbArr:arrThumbImages];

            
            [self showDialogWithPhoto:thumbImage];
            NSData *imageData = UIImageJPEGRepresentation(editedImage,0.4);
            NSString *base64image = [self base64forData:imageData];
            if (objAlbumPhoto == nil) {
                objAlbumPhoto = [[ObjAlbumPhoto alloc]init];
            }
            
            objAlbumPhoto.strImgURL = base64image;
            selectedImage = thumbImage;
        }
    }
    else if(fromCamera == 0){
        
        /*NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
         NSString *dataPath = [Paths objectAtIndex:0];
         NSString *seletedPicPath = [dataPath stringByAppendingPathComponent: @"seletedPic.png"];*/
        //delegate.pathSelectedPic = seletedPicPath;
        
        //NSData *imageData = UIImagePNGRepresentation(resizedImageFromLib);
        //[imageData writeToFile:seletedPicPath atomically:NO];
        
        //[delegate showPhoto];
        //[self onCrop:resizedImageFromLib];
        
        //[delegate showGamePlayDetail:CUSTOM_PHOTO_VIEW];
        //[imgProfileView setImage:resizedImage];
        if (thumbImage != nil) {
            //[arrThumbImages addObject:thumbImage];
            //[arrImages addObject:editedImage];
            //[self reloadTheScrollImageViewWithThumbArr:arrThumbImages];
            NSLog(@"here is library selected!!");
            [self showDialogWithPhoto:thumbImage];
            NSData *imageData = UIImageJPEGRepresentation(editedImage,0.4);
            NSString *base64image = [self base64forData:imageData];
            if (objAlbumPhoto == nil) {
                objAlbumPhoto = [[ObjAlbumPhoto alloc]init];
            }
            
            objAlbumPhoto.strImgURL = base64image;
            selectedImage = thumbImage;
        }
        
    }
    [self imagePickerControllerDidCancel:picker];
}

- (void)showDialogWithPhoto:(UIImage *)seletedImage{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Photo Name" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextField alloc]initWithFrame:CGRectMake(50, 45, 215, 30)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentCenter;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];
    
    txtParentPassword.placeholder = @"Name";
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    [txtParentPassword setBorderStyle:UITextBorderStyleBezel];
    
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 45, 30, 30)];
    imgView.image = seletedImage;
    
    
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        //[alert setValue:txtParentPassword forKey:@"textView"];
        txtParentPassword.frame = CGRectMake(0, 45, 215, 30);
        [alert setValue:txtParentPassword forKey:@"accessoryView"];
        //[alert setValue:imgView forKey:@"accessoryView"];
    }
    else{
        [alert addSubview:txtParentPassword];
        [alert addSubview:imgView];
    }
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //[self synNewAlbum:txtParentPassword.text andPetId:objP.pet_id];
            /*if (![self stringIsEmpty:txtParentPassword.text shouldCleanWhiteSpace:YES]) {
                objAlbumPhoto.strName = txtParentPassword.text;
                [self synNewPhoto:objAlbumPhoto];
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"Photo name is required!   "];
                [self showDialogWithPhoto:selectedImage];
            }*/
            objAlbumPhoto.strName = txtParentPassword.text;
            [self synNewPhoto:objAlbumPhoto];
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
        }
	}
    
    if( [alertView tag] == 2 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            if (![self stringIsEmpty:txtEdit.text shouldCleanWhiteSpace:YES]) {
                //[self synNewAlbum:txtParentPassword.text andPetId:objP.pet_id];
                //add edit
                objSelectedPhoto.strName = txtEdit.text;
                [self syncPhotoEdit:objSelectedPhoto];
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"Album name is required!  "];
                [self dialogForEditAlbum:txtEdit.text];
            }
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
        }
	}
}

- (void)synNewPhoto:(ObjAlbumPhoto *)objAP{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"album_image[album_id]":[NSString stringWithFormat:@"%d", objAlbum.idx],@"album_image[album_image_name]":objAP.strName,@"album_image[album_image_url]":objAP.strImgURL};
    NSLog(@"params %@",params);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",ALBUM_PHOTO_NEW_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjAlbumPhoto * objReturnPhoto = [[ObjAlbumPhoto alloc]init];
            objReturnPhoto.strName = [dics objectForKey:@"image_name"];
            objReturnPhoto.strImgURL = [dics objectForKey:@"album_image_url"];
            objReturnPhoto.strThumbImgURL = [dics objectForKey:@"album_image_thumb_url"];
            objReturnPhoto.idx = [[dics objectForKey:@"id"] intValue];
            
            [objAlbum.arrAlbumPhoto addObject:objReturnPhoto];
            
            [SVProgressHUD showSuccessWithStatus:strMsg];
            
            [self reloadTheScrollImageViewWithThumbArr:objAlbum.arrAlbumPhoto];
            //objAlbum.arrAlbumPhoto
            NSLog(@"success in Album!! and objAlbum count %d",[objAlbum.arrAlbumPhoto count]);
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

- (void)reloadTheScrollImageViewWithThumbArr:(NSMutableArray *)arr{
    NSLog(@"here is reloaded!!");
    int row = 0;
	int column = 0;
    if ([arr count]>0) {
        for (UIView * v in scrollView.subviews){
            if ([v isKindOfClass:[UIImageView class]]) {
                [v removeFromSuperview];
            }
            if ([v isKindOfClass:[UIButton class]]) {
                [v removeFromSuperview];
            }
        }
    }
    if ([networkImages count]>0) {
        [networkImages removeAllObjects];
    }
    if ([networkCaptions count]>0) {
        [networkCaptions removeAllObjects];
    }
    
    for(int i = 0; i < arr.count; ++i) {
		
		//UIImage *thumb = [arr objectAtIndex:i];
        ObjAlbumPhoto * objPhoto = [arr objectAtIndex:i];
        
        [networkImages addObject:objPhoto.strImgURL];
        [networkCaptions addObject:objPhoto.strName];
        
        UIImageView * bIcon = [[UIImageView alloc] initWithFrame: CGRectMake(column*95+24, row*110+10, 90, 100)];
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(column*95+24, row*110+10, 90, 100);
        btn.backgroundColor = [UIColor clearColor];
        [btn setTag:i];
        [btn addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    
        NSLog(@"obj thumb link %@",objPhoto.strThumbImgURL);
		[bIcon setImageWithURL:[NSURL URLWithString:objPhoto.strThumbImgURL] placeholderImage:nil];
        [self setBorderTheViewWith:bIcon];
        bIcon.tag = i;
        [scrollView addSubview:bIcon];
        [scrollView addSubview:btn];
		if (column == 2) {
			column = 0;
			row++;
		} else {
			column++;
		}
	}
    [scrollView setContentSize:CGSizeMake(320, (row+1) * 110 + 10)];
}

- (void) cleanTheGalleryGridView{
    for (UIView * v in scrollView.subviews){
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
        if ([v isKindOfClass:[UIButton class]]) {
            [v removeFromSuperview];
        }
    }
    
    [networkImages removeAllObjects];
    [networkCaptions removeAllObjects];
}

- (IBAction)onClick:(UIButton *)sender{
    
    //ObjAlbumPhoto * objPhoto = [objAlbum.arrAlbumPhoto objectAtIndex:sender.tag];
    if(networkGallery == nil)
    networkGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    
    [networkGallery setCurrentIndex:sender.tag];
    if (isFromOther) {
        networkGallery.hideEdit = YES;
    }
    else networkGallery.hideEdit = NO;
    
    [self.navigationController pushViewController:networkGallery animated:YES];
    
}

- (void)attachEventHandlers:(UIView *)imgIcon{
	UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	[doubleTap setNumberOfTapsRequired:2];
	[imgIcon addGestureRecognizer:doubleTap];
    
	UITapGestureRecognizer *tapping = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    
	tapping.delegate = self;
	tapping.numberOfTapsRequired = 1;
	tapping.numberOfTouchesRequired = 1;
	[tapping requireGestureRecognizerToFail:doubleTap];
	[imgIcon addGestureRecognizer:tapping];
}

- (void)handleDoubleTap:(UIGestureRecognizer*) gestureRecognizer{
	//
}

- (void)onTap:(UIGestureRecognizer*) gestureRecognizer{
    
}

- (void)setBorderTheViewWith:(UIImageView *)imgView{
    imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    imgView.layer.borderWidth = 3.0;
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    //[self deleteAllFilesInDocuments];
    // iConfusionAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    
    UIAlertView *alert;
    // Unable to save the image
    if (error){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
        // [alert release];
    }
    else{
        /*alert = [[UIAlertView alloc] initWithTitle:@"Success"
         message:@"Image saved to Photo Album."
         delegate:self cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/
        
    }
    
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        // iOS 7
        //self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+20, self.navigationController.navigationBar.frame.size.width, 44);
        //self.navigationController.navigationBar.translucent = NO;
        // for iOS7
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        }
        PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        [delegate windowViewAdjust];
    }
    NSLog(@"here imagePickerControllerDidCancel is cancel! ...,,,,,,<<>>>>");
    isFromPhotoLibrary = TRUE;
    [picker dismissModalViewControllerAnimated:YES];
} 

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FGalleryViewControllerDelegate Methods
- (int)numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
    int num;
    num = [networkImages count];
	return num;
}

- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
	return FGalleryPhotoSourceTypeNetwork;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery captionForPhotoAtIndex:(NSUInteger)index
{
    NSString *caption;
    if( gallery == networkGallery ) {
        caption = [networkCaptions objectAtIndex:index];
    }
	return caption;
}

- (NSString*)photoGallery:(FGalleryViewController *)gallery urlForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index {
    return [networkImages objectAtIndex:index];
}

- (void)handleTrashButtonTouch:(id)sender {
    // here we could remove images from our local array storage and tell the gallery to remove that image
    // ex:
    //[localGallery removeImageAtIndex:[localGallery currentIndex]];
}

- (void)handleEditCaptionButtonTouch:(id)sender {
    // here we could implement some code to change the caption for a stored image
}

- (void) removeIndex:(int)removeIdx{
    ObjAlbumPhoto * obj = [objAlbum.arrAlbumPhoto objectAtIndex:removeIdx];
    NSLog(@"remove index %d",removeIdx);
    [self syncDeleteAlbum:obj.idx andIndex:removeIdx];
}

- (void)syncDeleteAlbum:(int)con_id andIndex:(int)index{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",ALBUM_PHOTO_NEW_LINK,con_id,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [objAlbum.arrAlbumPhoto removeObjectAtIndex:index];
            [networkImages removeObjectAtIndex:index];
            [networkCaptions removeObjectAtIndex:index];
            [self reloadTheScrollImageViewWithThumbArr:objAlbum.arrAlbumPhoto];
            [networkGallery reloadGallery];
            if ([objAlbum.arrAlbumPhoto count] == 0) {
                [networkGallery goBack:nil];
                [self cleanTheGalleryGridView];
            }
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) updateIndex:(int)updateIdx{
    ObjAlbumPhoto * obj = [objAlbum.arrAlbumPhoto objectAtIndex:updateIdx];
    objSelectedPhoto = obj;
    [self dialogForEditAlbum:obj.strName];
}

- (void)dialogForEditAlbum:(NSString *)strName{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Photo" message:@"   " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtEdit == nil) {
        txtEdit = [[UITextField alloc]initWithFrame:CGRectMake(40, 45, 215, 30)];
    }
    txtEdit.text = strName;
    txtEdit.textAlignment = UITextAlignmentCenter;
    alert.tag = 2;
    [txtEdit becomeFirstResponder];
    
    txtEdit.placeholder = @"Name";
    txtEdit.backgroundColor = [UIColor whiteColor];
    [txtEdit setBorderStyle:UITextBorderStyleBezel];
    
    //[alert addSubview:txtEdit];
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        //[alert setValue:txtParentPassword forKey:@"textView"];
        txtEdit.frame = CGRectMake(0, 45, 215, 30);
        [alert setValue:txtEdit forKey:@"accessoryView"];
    }
    else{
        [alert addSubview:txtEdit];
    }
    alert.frame =  CGRectMake(10, 100, 310, 320);
    alert.delegate = self;
    [alert show];
}

- (void)syncPhotoEdit:(ObjAlbumPhoto *)obj{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    //pet[gender]=%@&pet[pet_name]=%@&pet[profile_image]=%@&pet[dob]=%@&pet[pet_breed_type_id]=%@&pet[pet_type_id]=%@&pet[description]=%@
    NSDictionary* params = @{@"album_image[album_image_name]":obj.strName};
    NSLog(@"params %@ and full link %@",params,[NSString stringWithFormat:@"%@/%d?auth_token=%@",ALBUM_PHOTO_NEW_LINK,obj.idx,strSession]);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",EDITPET_LINK,obj.idx,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
