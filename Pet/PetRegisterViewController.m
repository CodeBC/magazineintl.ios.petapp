//
//  PetRegisterViewController.m
//  Pet
//
//  Created by Zayar on 4/27/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "PetRegisterViewController.h"
#import "StringTable.h"
#import "ObjUser.h"
#import "PetAppDelegate.h"
#import "Utility.h"
#import "ZYActionSheet.h"
@interface PetRegisterViewController ()
{
    IBOutlet UIButton * btnUDontHvAccount;
}
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) ZYActionSheet *menu;
@property int selectedLevel;
@property int selectedCountry;
@property (nonatomic, strong) IBOutlet UIButton * btnDropDown;
@property (nonatomic, strong) IBOutlet UIButton * btnCountry;
@property (nonatomic, strong) NSMutableArray * arrGender;
@property (nonatomic, strong) NSMutableArray * arrCountry;
@property BOOL isSelected;
@property BOOL isSelectedCountry;
@end

@implementation PetRegisterViewController
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        if ([Utility isGreaterOSVersion:@"7.0"]) {
            scrollView.frame  = CGRectMake(0, 0, 320, 568);
            scrollView.contentSize  = CGSizeMake(320, btnUDontHvAccount.frame.origin.y + btnUDontHvAccount.frame.size.height + 10);
        }
        else{
            scrollView.frame  = CGRectMake(0, 0, 320, 548);
            scrollView.contentSize  = CGSizeMake(320, btnUDontHvAccount.frame.origin.y + btnUDontHvAccount.frame.size.height + 10);
        }
    }
    else{
        if ([Utility isGreaterOSVersion:@"7.0"]) {
            scrollView.frame  = CGRectMake(0, 0, 320, 500);
            scrollView.contentSize = CGSizeMake(320, 500);
        }
        else{
            scrollView.frame  = CGRectMake(0, -35, 320, 500);
            scrollView.contentSize = CGSizeMake(320, 500);
        }
        
    }
	
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    self.menu = [[ZYActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
    }
    
    
    [self.menu addSubview:controlToolbar];
    
    self.selectedLevel = 0;
    [self hardCodeGenderList];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, btnUDontHvAccount.frame.origin.y + btnUDontHvAccount.frame.size.height + 10)];
    
    
    
    
    NSLog(@"view did load!!");
    
    NSLog(@"scroll width %f and scroll hight content %f and conbtnX: %f",scrollView.contentSize.width,scrollView.contentSize.height,btnUDontHvAccount.frame.origin.y);
    self.navigationItem.title = @"Sign Up";
    
    [Utility makeCornerRadius:self.btnDropDown andRadius:13];
    [Utility makeCornerRadius:self.btnCountry andRadius:13];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    scrollViewOriginalSize2 = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    NSLog(@"scrollViewOriginalSize height %f",scrollViewOriginalSize2.height);
    
    UITapGestureRecognizer *actionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUIActionOut:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:actionTap];
    
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtLoginName];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtPassword];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtFName];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtLName];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtEmail];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtPostal];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtRePassword];
}

-(void)tapUIActionOut:(UIGestureRecognizer *)gestureRecognizer {
    [self dismissAndSelectActivityActionSheet];
    CGPoint p = [gestureRecognizer locationInView:self.menu];
    if (p.y < 0) {
        
    }
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    self.isSelected = FALSE;
    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    txtPassword.text = @"";
    txtLoginName.text = @"";
    txtRePassword.text = @"";
    txtFName.text = @"";
    txtLName.text = @"";
    txtEmail.text = @"";
    txtPostal.text = @"";
    strGender = @"";
    strCountry = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    [self scrollOriginalView];
}

- (void) scrollOriginalView{
    NSLog(@"scrollOriginalView height %f",scrollViewOriginalSize2.height);
    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize2;
    [UIView commitAnimations];
    [scrollView scrollToTop];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        UIView * nextView = [self.view viewWithTag:2];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 2) {
        UIView * nextView = [self.view viewWithTag:3];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 3) {
        UIView * nextView = [self.view viewWithTag:4];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 4) {
        UIView * nextView = [self.view viewWithTag:5];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 5) {
        UIView * nextView = [self.view viewWithTag:6];
        [nextView becomeFirstResponder];
        return NO;
    }
    else if(textField.tag == 6) {
        UIView * nextView = [self.view viewWithTag:7];
        [nextView becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
	return YES;
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
}

- (void) hardCodeGenderList{
    self.arrGender = [[NSMutableArray alloc]init];
    [self.arrGender addObject:@"Male"];
    [self.arrGender addObject:@"Female"];
    
    self.arrCountry = [[NSMutableArray alloc]init];
    NSArray * commands = nil;
    if( [COUNTRY_STRING_ARRAY rangeOfString:@","].location != NSNotFound ){
        
        commands = [COUNTRY_STRING_ARRAY componentsSeparatedByString:@","];
        
        for(NSString * strWifiName in commands){
            [self.arrCountry addObject:strWifiName];
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        NSString *strName = [self.arrGender objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return strName;
        
    }
    if (pickerView.tag == 1) {
        NSString *strName = [self.arrCountry objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return strName;
        
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrGender count];
    }
    if (pickerView.tag == 1) {
        return [self.arrCountry count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedLevel = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
        
        NSString * strName = [self.arrGender objectAtIndex:self.selectedLevel];
        [self.btnDropDown setTitle:strName forState:normal];
        self.isSelected = TRUE;
        strGender = strName;
        [self.menu dismissWithClickedButtonIndex:1 animated:YES];
    }
    if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedCountry = row;
        NSString * strName = [self.arrCountry objectAtIndex:self.selectedCountry];
        [self.btnCountry setTitle:strName forState:normal];
        self.isSelectedCountry = TRUE;
        strCountry = strName;
        [self.menu dismissWithClickedButtonIndex:1 animated:YES];
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    
    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrGender objectAtIndex:self.selectedLevel];
            [self.btnDropDown setTitle:strName forState:normal];
            self.isSelected = TRUE;
            strGender = strName;
        }
        
        if (self.uiPickerView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrCountry objectAtIndex:self.selectedCountry];
            [self.btnCountry setTitle:strName forState:normal];
            self.isSelectedCountry = TRUE;
            strCountry = strName;
        }
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction) onBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction) onRegister:(id)sender{
    strFName = [txtFName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strLName = [txtLName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strLoginName = [txtLoginName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strPassword = [txtPassword.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strEmail = [txtEmail.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    strPostalCode = [txtPostal.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL isValidEmail = [self validateEmailWithString:strEmail];
    
    if ([strFName isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG1
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strLName isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG2
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strEmail isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG4
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strLoginName isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG3
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if([strPassword isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG5
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(strPassword.length < 8){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG9
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(!isValidEmail){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG4
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if (![strPassword isEqualToString:txtRePassword.text]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG8
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([strCountry isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG10
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([strCountry isEqualToString:@"Singapore"] && [strPostalCode isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG11
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        ObjUser * user = [[ObjUser alloc]init];
        user.strName = strLoginName;
        user.strFName = strFName;
        user.strLName = strLName;
        user.strPassword = strPassword;
        user.strEmail = strEmail;
        user.strGender = strGender;
        user.strUDID = delegate.strUdid;
        user.strPostalCode = strPostalCode;
        user.strCountry = strCountry;
        [self syncRegister:user];
    }
}

- (IBAction) onFBRegister:(id)sender{
    PetAppDelegate *delegate = (PetAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate fbRegister:self];
}

- (void) syncRegister:(ObjUser *)user{
    if (registerRequest == nil) {
        registerRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    registerRequest.processId = 4;
    //NSLog(@"user :%@ and pass : %@",objUser.strName,objUser.strPassword);
    [registerRequest syncRegisterWithUser:user];
    [SVProgressHUD show];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 4) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strErrorMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strSession = [dics objectForKey:@"auth_token"];
            objUser.userId = [[dics objectForKey:@"user_id"] intValue];
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objUser.strSession forKey:LOGIN_LINK];
            objUser.strName = txtLoginName.text;
            objUser.strPassword = txtPassword.text;
            
            objUser.isFBLogin = 0;
            [delegate.db updateUser:objUser];
            [SVProgressHUD showSuccessWithStatus:strErrorMsg];
            [self hideRegister];
            
            NSDate *installDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"installDate"];
            if (!installDate) {
                //no date is present
                //this app has not run before
                NSDate *todaysDate = [NSDate date];
                [[NSUserDefaults standardUserDefaults]setObject:todaysDate forKey:@"installDate"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //nothing more to do?
            }
            [delegate preSetReminders];
        }
        else if(status == STATUS_ACTION_FAILED){
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
        else if(status == 4){
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
    }
}

- (void)hideRegister{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)onLevel:(id)sender{
    [self.menu setTitle:@"Select Gender"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    NSLog(@"UIPicker width %f",self.uiPickerView.frame.size.width);
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onCountry:(id)sender{
    [self.menu setTitle:@"Select Country"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedCountry inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

@end
