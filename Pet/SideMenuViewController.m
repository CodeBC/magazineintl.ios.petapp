//
//  SideMenuViewController.m
//  Feel
//
//  Created by Zayar on 12/11/12.
//
//

#import "SideMenuViewController.h"
#import "PetAppDelegate.h"
#import "SideMenuObject.h"
#import "MenuSideTableCell.h"
#import "ProfileViewController.h"
#import "ADSlidingViewController.h"
#import "ReminderViewController.h"
#import "LostFoundViewController.h"
#import "StringTable.h"
#import "petAPIClient.h"
#import "Utility.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //NSString * bgImageName=@"";
    self.view.backgroundColor = [UIColor darkGrayColor];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    /*UIImageView * imgSideNavView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 290, 48)];
    //PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    //[delegate windowViewAdjust];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        imgSideNavView.frame = CGRectMake(0, 20, 290, 48);
    }
    imgSideNavView.image = [UIImage imageNamed:@"side_nav_bar.png"];
    [self.view addSubview:imgSideNavView];*/
    
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        //bgImageName=@"sidemenubg_5.png";
        [self.view setFrame:CGRectMake(0, 0, 290, 504)];
        [imgBgView setFrame:CGRectMake(0, 0, 320, 544)];
        
    }
    else {
        // code for 3.5-inch screen
        //bgImageName=@"sidemenubg.png";
        [self.view setFrame:CGRectMake(0, 0, 290, 416)];
        [imgBgView setFrame:CGRectMake(0, 0, 320, 358)];
        //tbl.frame = CGRectMake(0, 20, 290, self.view.frame.size.height - 20);
    }
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        tbl.frame = CGRectMake(0, 20, 290, self.view.frame.size.height - 20);
    }
    else{
        tbl.frame = CGRectMake(0, 0, 290, self.view.frame.size.height);
    }
    /*NSString * strPath = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],bgImageName];
     UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
     [imgBgView setImage:imgBg];*/
    
    tbl.backgroundColor = [UIColor darkGrayColor];
    imgBgView.backgroundColor = [UIColor darkGrayColor];
    
    //NSLog(@"view did load!!")
    
    [self hardCodeSideMenuList];
    //tbl.contentSize = CGSizeMake(290, ([arrList count] *45)+80+68);
}

- (void) hardCodeSideMenuList{
    arrList = [[NSMutableArray alloc]init];
    SideMenuObject * sideObject = [[SideMenuObject alloc]init];
    sideObject.idx = 1;
    sideObject.strName = @"Owner";
    sideObject.strImageName = @"img_profile_icon.png";
    [arrList addObject:sideObject];
    
    SideMenuObject * sideObject3 = [[SideMenuObject alloc]init];
    sideObject3.idx = 2;
    sideObject3.strName = @"Pets";
    sideObject3.strImageName = @"img_profile_icon.png";
    [arrList addObject:sideObject3];
    
    
    arrSecList = [[NSMutableArray alloc]init];
    
    SideMenuObject * sideObject13 = [[SideMenuObject alloc]init];
    sideObject13.idx = 1;
    sideObject13.strName = @"Newsfeed";
    sideObject13.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject13];
    
    SideMenuObject * sideObject4 = [[SideMenuObject alloc]init];
    sideObject4.idx = 3;
    sideObject4.strName = @"Messages";
    sideObject4.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject4];
    
    SideMenuObject * sideObject5 = [[SideMenuObject alloc]init];
    sideObject5.idx = 4;
    sideObject5.strName = @"Reminders";
    sideObject5.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject5];
    
    SideMenuObject * sideObject6 = [[SideMenuObject alloc]init];
    sideObject6.idx = 5;
    sideObject6.strName = @"Lost & Found";
    sideObject6.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject6];
    
    SideMenuObject * sideObject7 = [[SideMenuObject alloc]init];
    sideObject7.idx = 6;
    sideObject7.strName = @"Pet Deals";
    sideObject7.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject7];
    
    SideMenuObject * sideObject8 = [[SideMenuObject alloc]init];
    sideObject8.idx = 7;
    sideObject8.strName = @"Adoptions";
    sideObject8.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject8];
    
    /*SideMenuObject * sideObject9 = [[SideMenuObject alloc]init];
    sideObject9.idx = 8;
    sideObject9.strName = @"Growth Chart";
    sideObject9.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject9];*/
    
    SideMenuObject * sideObject10 = [[SideMenuObject alloc]init];
    sideObject10.idx = 8;
    sideObject10.strName = @"Pets Magazine";
    sideObject10.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject10];
    
    SideMenuObject * sideObject11 = [[SideMenuObject alloc]init];
    sideObject11.idx = 9;
    sideObject11.strName = @"Videos";
    sideObject11.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject11];
    
    SideMenuObject * sideObject9 = [[SideMenuObject alloc]init];
    sideObject9.idx = 10;
    sideObject9.strName = @"Help";
    sideObject9.strImageName = @"img_profile_icon.png";
    [arrSecList addObject:sideObject9];
    
    
    SideMenuObject * sideObject2 = [[SideMenuObject alloc]init];
    sideObject2.idx = 2;
    sideObject2.strName = @"Logout";
    sideObject2.strImageName = @"img_setting_icon.png";
    [arrSecList addObject:sideObject2];
    
    
    NSLog(@"array count %d",[arrList count]);
}

- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"viewDidAppear");
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSLog(@"sidemenu viewWillAppear");
    tbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 305.0f, 10.0f)] ;
    [tbl reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 return [NSString stringWithFormat:@"Section %d", section];
 }*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [arrList count];
    }
    else if(section == 1){
        return [arrSecList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuSideTableCell";
	MenuSideTableCell *cell = (MenuSideTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MenuSideTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MenuSideTableCell *) currentObject;
				cell.accessoryView = nil;
				
				break;
			}
		}
	}
    if (indexPath.section == 0) {
        SideMenuObject * sideMenuObject = [arrList objectAtIndex:[indexPath row]];
        NSLog(@"cell img name %@",sideMenuObject.strImageName);
        //[cell.imgView setImage:[UIImage imageNamed:sideMenuObject.strImageName]];
        cell.lblName.text = sideMenuObject.strName;
    }
    else if(indexPath.section == 1){
        SideMenuObject * sideMenuObject = [arrSecList objectAtIndex:[indexPath row]];
       // NSLog(@"cell img name %@",sideMenuObject.strImageName);
        //[cell.imgView setImage:[UIImage imageNamed:sideMenuObject.strImageName]];
        cell.lblName.text = sideMenuObject.strName;
    }
    
    
    //cell.accessoryType=UITableViewCellAccessoryNone;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblName.textColor = [UIColor whiteColor];
    cell.lblName.shadowColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor darkGrayColor];
    cell.lblName.backgroundColor = [UIColor darkGrayColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
     if ([indexPath row]==0) {
     ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] ;
     //demoController.title = [NSString stringWithFormat:@"CouponViewController #%d-%d", 0, 0];
     
     NSArray *controllers = [NSArray arrayWithObject:viewController];
     //FeelDelegate *delegate = [[UIApplication sharedApplication] delegate];
     //delegate.header_image = @"header.png";
     [MFSideMenuManager sharedManager].navigationController.viewControllers = controllers;
     [MFSideMenuManager sharedManager].navigationController.menuState = MFSideMenuStateHidden;
     }
     if ([indexPath row]==1) {
     
     NSLog(@"load coupon...");
     CouponViewController *couponController = [[CouponViewController alloc] initWithNibName:@"CouponViewController" bundle:nil] ;
     NSArray *controllers = [NSArray arrayWithObject:couponController];
     [MFSideMenuManager sharedManager].navigationController.viewControllers = controllers;
     [MFSideMenuManager sharedManager].navigationController.menuState = MFSideMenuStateHidden;
     
     
     }*/
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            UINavigationController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"profile"];
            //NSArray *controllers = [NSArray arrayWithObject:viewController];
            //UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:viewController];
            ProfileViewController * profileViewController = (ProfileViewController *)viewController.topViewController;
            profileViewController.isFromOther = FALSE;
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            
            [slidingViewController setMainViewController:viewController];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
            //[slidingViewController setR];
        }
        if (indexPath.row == 1) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"mypet"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }    }
    else if(indexPath.section == 1){
        if (indexPath.row == 0) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PetViewController"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        if (indexPath.row == 1) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"message" bundle:nil] instantiateViewControllerWithIdentifier:@"message"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        if (indexPath.row == 2) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"reminder"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        if (indexPath.row == 3) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"LostAndFound" bundle:nil] instantiateViewControllerWithIdentifier:@"mainLF"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        
        if (indexPath.row == 4) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Deal" bundle:nil] instantiateViewControllerWithIdentifier:@"deal"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        
        if (indexPath.row == 5) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"adoptions" bundle:nil] instantiateViewControllerWithIdentifier:@"adoptions"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        
        /*if (indexPath.row == 6) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"AnimateMe" bundle:nil] instantiateViewControllerWithIdentifier:@"animate"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            //[slidingViewController anchorTopViewTo:ADAnchorSideCenter];
        }*/
        
        if (indexPath.row == 6) {
            /*UINavigationController * nav = [[UIStoryboard storyboardWithName:@"magazine" bundle:nil] instantiateViewControllerWithIdentifier:@"magazine"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];*/
            NSString * strMagzineFileLink = @"https://itunes.apple.com/sg/app/pets-magazine/id496227308?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: strMagzineFileLink]];
            
        }
        
        //AnimateMe
        if (indexPath.row == 7) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"video"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        
        if (indexPath.row == 8) {
            UINavigationController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"Help"];
            ADSlidingViewController *slidingViewController = [self slidingViewController];
            [slidingViewController setMainViewController:nav];
            [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
            //[slidingViewController setLeftViewAnchorWidth:200];
        }
        if (indexPath.row == 9) {
            [self syncLogout];
        }
    }
    NSLog(@"here select!!");
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView * v = [[UIView alloc]initWithFrame:CGRectMake(0,0,tableView.frame.size.width,25)];
        v.backgroundColor = [UIColor blackColor];
//        UIImageView * img = [[UIImageView alloc]initWithFrame:v.frame];
//        [img setImage:[UIImage imageNamed:@"img_cell_header_account.png"]];
//        [v addSubview:img];
        UILabel * lbl = [[UILabel alloc] initWithFrame:v.frame];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [UIFont systemFontOfSize:15];
        lbl.text = @"ACCOUNT";
        lbl.textColor = [UIColor whiteColor];
        [v addSubview:lbl];
        return v;
    }
    if (section == 1) {
        UIView * v = [[UIView alloc]initWithFrame:CGRectMake(0,0,tableView.frame.size.width,25)];
        v.backgroundColor = [UIColor blackColor];
        //        UIImageView * img = [[UIImageView alloc]initWithFrame:v.frame];
        //        [img setImage:[UIImage imageNamed:@"img_cell_header_account.png"]];
        //        [v addSubview:img];
        UILabel * lbl = [[UILabel alloc] initWithFrame:v.frame];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [UIFont systemFontOfSize:16];
        lbl.text = @"PETS APP";
        lbl.textColor = [UIColor whiteColor];
        [v addSubview:lbl];
        return v;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void) syncLogout{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    NSLog(@"logout link %@ and auth %@",LOGOUT_LINK,strSession);
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@?auth_token=%@",LOGOUT_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD dismiss];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
            
            NSMutableArray * arr = [delegate.db getAllReminder];
            for (ObjReminder * obj in arr) {
                [delegate deleteLocalNotification:obj];
                [delegate.db deleteReminder:obj];
            }
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notFirstTime"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"installDate"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

@end
