//
//  AddPetViewController.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import <TapkuLibrary/TapkuLibrary.h>
@interface EditPetViewController : PetBasedViewController
@property (nonatomic, strong) ObjPet * objPet;
-(IBAction)onGender:(id)sender;
-(IBAction)onType:(id)sender;
-(IBAction)onBreed:(id)sender;
- (IBAction) showPhotoLibrary:(id) sender;

@property (nonatomic,strong) NSArray *cells;
@property (nonatomic,strong) UITableViewCell *typeCell;
@property (nonatomic,strong) UITableViewCell *btnDateCell;
@property (nonatomic,strong) UITableViewCell *btnBreedCell;
@property (nonatomic,strong) UITableViewCell *btnGenderCell;

@end
