//
//  ObjNewsFeed.h
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjUser.h"
#import "ObjPet.h"

@interface ObjNewsFeed : NSObject
@property int idx;
@property (nonatomic, strong) ObjUser * objUser;
@property (nonatomic, strong) ObjPet * objPet;
@property (nonatomic, strong) NSMutableArray * arrPets;
@end
