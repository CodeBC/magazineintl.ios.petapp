//
//  PetLoginViewController.m
//  Pet
//
//  Created by Zayar on 4/24/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "PetLoginViewController.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjUser.h"
#import "FBConnect.h"
#import "NSDate-Utilities.h"
#import "Utility.h"
@interface PetLoginViewController ()

@end

@implementation PetLoginViewController
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    NSString * strPath = [NSString stringWithFormat:@"%@/login_bg.png", [[NSBundle mainBundle] resourcePath]];
    UIImage * imgBg = [[UIImage alloc] initWithContentsOfFile:strPath];
    [imgBgView setImage:imgBg];

    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        scrollView.frame  = CGRectMake(0, 0, 320, 548);
    }
    else{
        scrollView.frame  = CGRectMake(0, 0, 320, 460);
    }

    /*UIBarButtonItem* close = [[UIBarButtonItem alloc] initWithTitle:@"close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem  = close;*/


    scrollViewOriginalSize = scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];

    NSLog(@"view did load!!");

    NSLog(@"scroll width %f and scroll hight %f",scrollView.contentSize.width,scrollView.contentSize.height);

    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Login";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtLoginName];
    [Utility textPlaceholderColor:[UIColor colorWithHexString:@"e5e5e5"] andText:txtPassword];
}


-(IBAction)close:(id)sender
{
    /*if([self.delegate respondsToSelector:@selector(dismissWithViewController:)])
    {
        [self.delegate dismissWithViewController:self];
    }*/
}

-(void)dismissKeyboard {
    [txtLoginName resignFirstResponder];
    [txtPassword resignFirstResponder];
}

- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"view did appear! Login:");
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * user = [delegate.db getUserObj];
    if(![user.strSession isEqualToString:@""]){
        ///PetLoginViewController *loginvc =
        [self dismissModalViewControllerAnimated:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    for(UIView * v in self.navigationController.view.subviews){
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 10) {
                [v removeFromSuperview];
            }

        }
    }


    //self.navigationItem.title = @"Rinnai";
    UILabel * lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 320, 44)];
    //lblTitle.text = @"Pet Login";
    lblTitle.textAlignment = UITextAlignmentCenter;
    lblTitle.tag = 10;
    lblTitle.font = [UIFont fontWithName:@"Georgia" size:25];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor whiteColor];
    [self.navigationController.view addSubview:lblTitle];

    //---registers the notifications for keyboard---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    txtPassword.text = @"";
    txtLoginName.text = @"";

    //[super viewWillAppear:NO];

    /*PetLoginViewController* newpost = [[PetLoginViewController alloc] initWithNibName:@"PetLoginViewController" bundle:nil];
     //newpost.delegate = self;
     UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:newpost];

     [self.navigationController presentModalViewController:nav animated:YES];*/
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;

    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;

    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;

    //---set the new scrollView contentSize---
    scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);

    //---scroll the ScrollView---
    [scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    [self scrollToOriginal];
}

- (void) scrollToOriginal{
    NSLog(@"scrollView.contentSize height %f",scrollView.contentSize.height);
    [UIView beginAnimations:@"back to original size" context:nil];
    scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        UIView * nextView = [self.view viewWithTag:2];
        [nextView becomeFirstResponder];
        return NO;
    }

    NSLog(@"textFieldShouldReturn!!");
    [textField resignFirstResponder];
	return YES;
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];

}

-(void) keyboardWillHide:(NSNotification *) notification {
}

//////Sync Login//////
-(IBAction)onLogin:(id)sender{
        PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        //if (delegate.getConnection) {
            if ([[txtLoginName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
                [self textValidateAlertShow:@"Please enter your email"];
                //[self textValidateAlertShow:@"Retry your name and password!"];
                [self textFieldDidEndEditing:txtLoginName];
                return;
            }
            if ([[txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
                [self textValidateAlertShow:@"Please enter your password"];
                [self textFieldDidEndEditing:txtPassword];
                return;
            }

            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strName = txtLoginName.text;
            objUser.strPassword = txtPassword.text;
            [self syncLgoinWith:objUser];
            //NSLog(@"user :%@ and pass : %@",objUser.strName,objUser.strPassword);
        //}
        [txtLoginName resignFirstResponder];
        [txtPassword resignFirstResponder];
}

- (void)syncLgoinWith:(ObjUser *)objUser{
    if (loginRequest == nil) {
         loginRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    loginRequest.processId = 3;
    NSLog(@"user :%@ and pass : %@",objUser.strName,objUser.strPassword);
    [loginRequest syncLoginWithUser:objUser];
    [SVProgressHUD show];
}

- (void) onErrorLoad: (int) processId{
   // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 3) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjUser * objUser = [[ObjUser alloc]init];
            objUser.strSession = [dics objectForKey:@"auth_token"];

            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objUser.strSession forKey:LOGIN_LINK];
            objUser.strEmail = [dics objectForKey:@"email"];
            objUser.strPassword = txtPassword.text;
            objUser.userId = [[dics objectForKey:@"user_id"] intValue];
            
            if ([dics objectForKey:@"profile_image"] != [NSNull null]) {
                objUser.strProfileImgLink = [dics objectForKey:@"profile_image"];
            }
            else objUser.strProfileImgLink = @"";
            
            objUser.strName = [dics objectForKey:@"user_name"];
            objUser.isFBLogin = 0;
            NSLog(@"user session id %@",objUser.strSession);
            delegate.isNowInLogin = FALSE;

            [delegate.db updateUser:objUser];
            [SVProgressHUD dismiss];
            [self hideLogin];
            NSLog(@"complete login!!");
            NSDate *installDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"installDate"];
            if (!installDate) {
                //no date is present
                //this app has not run before
                NSDate *todaysDate = [NSDate date];
                [[NSUserDefaults standardUserDefaults]setObject:todaysDate forKey:@"installDate"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //nothing more to do?
            }
            [delegate preSetReminders];
        }
        else if(status == STATUS_ACTION_FAILED){
            NSString * strErrorMsg = [dics objectForKey:@"message"];
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strErrorMsg];
        }
    }
}


//////END Sync Login//////

- (void) hideLogin{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) textValidateAlertShow:(NSString *) strError{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: APP_TITLE
                          message: strError
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void) viewWillDisappear:(BOOL)animated{
    NSLog(@"login viewWillDisappear");
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    delegate.isFromLogin = TRUE;
    
    scrollViewOriginalSize = applicationFrame.size;
    [self scrollToOriginal];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

//FB Login
/**
 * Show the authorization dialog.
 */
- (IBAction)fblogin:(id)sender {
    PetAppDelegate *delegate = (PetAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate fbLogin:self];
}

@end
