#import <Foundation/Foundation.h>

@interface UIBarButtonItem (appearance)

+ (void) setupAppearance;

@end
