//
//  ZDialogView.h
//  PoS
//
//  Created by Zayar on 7/26/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ZDialogViewDelegate
- (void) onOk:(NSString *)strPostalCode;
- (void) onCancel;
- (void) onCountry;
@end
@interface ZDialogView : UIView
{
    id<ZDialogViewDelegate> owner;
    IBOutlet UITextField * txtPostal;
    IBOutlet UIButton * btnCountry;
}
@property id<ZDialogViewDelegate> owner;
- (void)setCountryButtonTitle:(NSString *)str;
@end
