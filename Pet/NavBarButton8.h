//
//  NavBarButton4.h
//  Pet
//
//  Created by Ye Myat Min on 21/5/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavBarButton8 : UIButton {
    BOOL landscape;
}

-(id)init;

@property BOOL landscape;
- (void)setImageWithName:(NSString *)strName;
@end
