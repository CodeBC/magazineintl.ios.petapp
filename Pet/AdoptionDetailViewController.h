//
//  AdoptionDetailViewController.h
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAdoption.h"

@interface AdoptionDetailViewController : PetBasedViewController {
    IBOutlet UIButton *interestedButton;
}
@property (nonatomic, retain) ObjAdoption * objAdo;
@property (nonatomic, retain) IBOutlet UIButton *interestedButton;

- (void) loadTheViewWith:(ObjAdoption *)obj;
- (IBAction) notifyInterested:(id) sender;

@end
