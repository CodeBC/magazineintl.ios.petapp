//
//  ObjAlbum.h
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjAlbum : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSMutableArray * arrAlbumPhoto;
@end
