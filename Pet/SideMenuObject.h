//
//  SideMenuObject.h
//  Feel
//
//  Created by Zayar on 12/12/12.
//
//

#import <Foundation/Foundation.h>

@interface SideMenuObject : NSObject
{
    int idx;
    NSString * strName;
    NSString * strImageName;
}
@property int idx;
@property (nonatomic, retain) NSString * strName;
@property (nonatomic, retain) NSString * strImageName;
@end
