//
//  ObjLostFound.h
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjLostFound : NSObject
@property int idx;
@property int intBreedType;
@property int intPetType;
@property int intOwnerId;
@property float reward;
@property int intLostFoundType;
@property int intLastSeenTimestamp;
@property float lat;
@property float lon;
@property (nonatomic, strong) NSString * strPetName;
@property (nonatomic, strong) NSString * strPetGender;
@property (nonatomic, strong) NSString * strPetBreed;
@property (nonatomic, strong) NSString * strPetType;
@property (nonatomic, strong) NSString * strLastSeenDate;
@property (nonatomic, strong) NSString * strDescription;
@property (nonatomic, strong) NSString * strLostFoundType;
@property (nonatomic, strong) NSString * strPetImgLink;
@end
