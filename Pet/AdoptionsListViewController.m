//
//  AdoptionsListViewController.m
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "AdoptionsListViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "petAPIClient.h"
#import "ObjAdoption.h"
#import "AdoptionTableCell.h"
#import "AdoptionDetailViewController.h"
@interface AdoptionsListViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    NSMutableArray * arrAdoptions;
    
}
@end

@implementation AdoptionsListViewController
@synthesize selectedTypeId,selectedBreedId,selectedOrganastionId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Adoptions";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncAdoptionsList];
}

- (void)syncAdoptionsList{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"search[pet_type_id]":[NSString stringWithFormat:@"%d",selectedTypeId],@"search[pet_breed_type_id]":[NSString stringWithFormat:@"%d",selectedBreedId],@"search[organization_id]":[NSString stringWithFormat:@"%d",selectedOrganastionId]};
    NSLog(@"type id %d, breed id %d, organization id %d",selectedTypeId,selectedBreedId,selectedOrganastionId);
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",ADOPTIONS_LIST_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            
            NSMutableArray *  arr = [dics objectForKey:@"adoptions"];
            [arrAdoptions removeAllObjects];
            if(arrAdoptions == nil){
                arrAdoptions = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjAdoption * objAdo = [[ObjAdoption alloc]init];
                objAdo.idx = [[dicNewsFeed objectForKey:@"adoption_id"] intValue];
                /* 
                 "adoption_id" = 5;
                 dob = "1992-11-20";
                 gender = male;
                 information = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodrure dolor in reprehenderit in voluptate velit esse";
                 name = "Torrey Sauer";
                 organization = SPCA;
                 "organization_id" = 3;
                 "pet_breed_type" = Dachschund;
                 "pet_breed_type_id" = 2;
                 "pet_image" = "http://swipetelecom.com/blog/wp-content/uploads/pets.jpg";
                 "pet_type" = Dog;
                 "pet_type_id" = 3;
                 */
                objAdo.strPetDob = [dicNewsFeed objectForKey:@"dob"];
                objAdo.strPetGender = [dicNewsFeed objectForKey:@"gender"];
                objAdo.strInfo = [dicNewsFeed objectForKey:@"information"];
                objAdo.strName = [dicNewsFeed objectForKey:@"name"];
                objAdo.strOrganization = [dicNewsFeed objectForKey:@"organization"];
                objAdo.intOrganization = [[dicNewsFeed objectForKey:@"organization_id"]intValue];
                objAdo.strPetBreed = [dicNewsFeed objectForKey:@"pet_breed_type"];
                objAdo.intBreedType = [[dicNewsFeed objectForKey:@"pet_breed_type_id"]intValue];
                objAdo.strPetImgLink = [dicNewsFeed objectForKey:@"pet_image"];
                objAdo.strPetType = [dicNewsFeed objectForKey:@"pet_type"];
                objAdo.intPetType = [[dicNewsFeed objectForKey:@"pet_type_id"]intValue];
                objAdo.isInterested = [[dicNewsFeed objectForKey:@"is_interested"] boolValue];
               
                [arrAdoptions addObject:objAdo];
            }
            NSLog(@"arrMessages count %d",[arrAdoptions count]);
            
            [tbl reloadData];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            //[self goBack:nil];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: APP_TITLE
                                  message: @"No record found!"
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrAdoptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"AdoptionTableCell";
	AdoptionTableCell *cell = (AdoptionTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AdoptionTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (AdoptionTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    
    ObjAdoption * obj = [arrAdoptions objectAtIndex:[indexPath row]];
    [cell loadTheCellWith:obj];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AdoptionDetailViewController *viewController = [[UIStoryboard storyboardWithName:@"adoptions" bundle:nil] instantiateViewControllerWithIdentifier:@"detail"];
    ObjAdoption * objAdo = [arrAdoptions objectAtIndex:indexPath.row];
    viewController.objAdo = objAdo;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
