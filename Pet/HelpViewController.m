//
//  HelpViewController.m
//  Pet
//
//  Created by Zayar on 3/11/14.
//  Copyright (c) 2014 bc. All rights reserved.
//

#import "HelpViewController.h"
#import "ADSlidingViewController.h"
#import "NavBarButton.h"
@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Help";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
