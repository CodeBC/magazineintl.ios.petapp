//
//  SettingsViewController.h
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : PetBasedViewController
-(IBAction) showActionSheet:(id)sender forEvent:(UIEvent*)event;
@end
