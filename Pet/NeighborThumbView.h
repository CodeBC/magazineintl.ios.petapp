//
//  PetView.h
//  Pet
//
//  Created by Zayar on 6/6/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjPet.h"
#import "ObjUser.h"
@protocol NeighborThumbViewDelegate
- (void) onNeighborThumbViewSelected:(ObjPet *)objSPet;
- (void) onNeighborThumbUserViewSelected:(ObjUser *)obj;
- (void) onNeighborThumbViewOnClicked:(ObjPet *)objSPet;
@end
@interface NeighborThumbView : UIView
{
    IBOutlet UIImageView * thumbImgView;
    IBOutlet UILabel * lblDescription;
    IBOutlet UILabel * lblDistance;
    
    int idx;
    id<NeighborThumbViewDelegate> owner;
    ObjPet * objPetSelected;
    ObjUser * objUserSelected;
    int intThumbType;
}
@property id<NeighborThumbViewDelegate> owner;
@property int intThumbType;
- (void)loadThumbView:(ObjPet *)objPet;
- (void)loadThumbViewWithUser:(ObjUser *)obj;
- (void) cleanImageView;
@end
