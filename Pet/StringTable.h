//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}

extern NSString * const DBNAME;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;

extern NSString * const CONFIG_LINK;
extern NSString * const BASE_LINK;
extern NSString * const GENR_KEY_LINK;
extern NSString * const LOGIN_LINK;
extern NSString * const REGISTER_LINK;
extern NSString * const CHECK_LOGIN_SESSION_LINK;
extern NSString * const PROFILE_LINK;
extern NSString * const PROFILE_EDIT_LINK;
extern NSString * const MYPET_LINK;
extern NSString * const ADDPET_LINK;
extern NSString * const EDITPET_LINK;
extern NSString * const REMINDER_LINK;
extern NSString * const LOST_FOUND_BROWSE_LINK;
extern NSString * const LOST_REPORT_LINK;
extern NSString * const VIDEO_LINK;
extern NSString * const DEAL_LINK;
extern NSString * const PET_TYPE_BREED_LINK;
extern NSString * const NEWS_FEED_LINK;
extern NSString * const MESSAGE_LINK;
extern NSString * const MESSAGE_DETAIL_LINK;
extern NSString * const MESSAGE_PUSH_LINK;
extern NSString * const ALL_USER_LINK;
extern NSString * const LOGOUT_LINK;
extern NSString * const USER_CHANGE_PASS_LINK;
extern NSString * const MAGAZINE_LINK;
extern NSString * const REPORT_LOST_INFO_LINK;
extern NSString * const REPORT_FOUND_INFO_LINK;
extern NSString * const FOUND_REPORT_LINK;
extern NSString * const ADOPTIONS_INFO_LINK;
extern NSString * const ADOPTIONS_LIST_LINK;
extern NSString * const ADOPTIONS_INTERESTED;
extern NSString * const ALBUM_NEW_LINK;
extern NSString * const FB_REGISTER_LINK;
extern NSString * const ALBUM_PHOTO_NEW_LINK;
extern NSString * const REPORT_ABUSE_LINK;
extern NSString * const SETTING_PRAVICY_LINK;
extern NSString * const MESSAGE_CONVERSATION_DELETE_LINK;
extern NSString * const MESSAGE_DELETE_LINK;
extern NSString * const PET_REPORT_LINK;

extern NSString * const REPEAT_TYPE_NEVER;
extern NSString * const REPEAT_TYPE_EVER;
extern NSString * const REPEAT_TYPE_ONCE;

extern NSString * const ANSWER_TYPE_IMAGE;
extern NSString * const ANSWER_TYPE_VIDEO;
extern NSString * const ANSWER_TYPE_TEXT;
extern NSString * const ANSWER_TYPE_NULL;

extern NSString * const ANSWER_STATUS_PENDING;
extern NSString * const ANSWER_STATUS_ANSWERED;
extern NSString * const ANSWER_STATUS_REJECTED;

extern NSString * const APN_SERVER_PATH;

extern float const ACTIONSHEET_HEIGHT;

extern int const STATUS_ACTION_SUCCESS;
extern int const STATUS_RETURN_RECORD;
extern int const STATUS_ACTION_FAILED;
extern int const STATUS_SESSION_EXPIRED;
extern int const STATUS_NO_RECORD_FOUND;
extern int const STATUS_INVALID_AUTH;

extern int const THUMB_PET;
extern int const THUMB_USER;

extern int const MINUTE_INTERVAL;

extern NSString * const ERRMSG1;
extern NSString * const ERRMSG2;
extern NSString * const ERRMSG3;
extern NSString * const ERRMSG4;
extern NSString * const ERRMSG5;
extern NSString * const ERRMSG6;
extern NSString * const ERRMSG7;
extern NSString * const ERRMSG8;
extern NSString * const ERRMSG9;
extern NSString * const ERRMSG10;
extern NSString * const ERRMSG11;
extern NSString * const COUNTRY_STRING_ARRAY;

extern double const CACHE_DURATION;

@end
