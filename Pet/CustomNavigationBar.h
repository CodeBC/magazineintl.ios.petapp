//
//  CustomNavigationBar.h
//  GroovyMap
//
//  Created by Tonytoons on 1/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomNavigationBar : UINavigationBar<UINavigationControllerDelegate> {
    int selectedLanguage;
}

- (void) updateTitle:(UIViewController *)viewController;
- (void)drawRect:(CGRect)rect;

@end
