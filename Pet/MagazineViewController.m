//
//  MagazineViewController.m
//  Pet
//
//  Created by Zayar on 6/10/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MagazineViewController.h"
#import "NavBarButton.h"
#import "ADSlidingViewController.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "ObjMagazine.h"
#import "MagazineTableCell.h"
#import "SVModalWebViewController.h"
@interface MagazineViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UITableView * tbl;
    NSMutableArray * arrMagazine;
}
@end

@implementation MagazineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavBarButton *btnBack = [[NavBarButton alloc] init];
	[btnBack addTarget:self action:@selector(leftBarButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Magazines";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    tbl.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [self syncMagazine];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [arrMagazine count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MagazineTableCell";
	MagazineTableCell *cell = (MagazineTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MagazineTableCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[UITableViewCell class]]){
				cell = (MagazineTableCell *) currentObject;
				cell.accessoryView = nil;
				break;
			}
		}
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ObjMagazine * objMaga = [arrMagazine objectAtIndex:[indexPath row]];
    [cell loadTheCellWith:objMaga];
    cell.owner = self;
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)syncMagazine{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",MAGAZINE_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            
            NSMutableArray *  arr = [dics objectForKey:@"magazines"];
            [arrMagazine removeAllObjects];
            if(arrMagazine == nil){
                arrMagazine = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjMagazine * objMaga = [[ObjMagazine alloc]init];
                objMaga.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                objMaga.strName = [dicNewsFeed objectForKey:@"name"];
                objMaga.strMagzineFileLink = [dicNewsFeed objectForKey:@"magazine_file"];
                objMaga.strDate = [dicNewsFeed objectForKey:@"date"];
                
                [arrMagazine addObject:objMaga];
            }
            NSLog(@"arrMessages count %d",[arrMagazine count]);
            [tbl reloadData];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) onScribeWith:(ObjMagazine *)objMaga{
    NSLog(@"on Subscribe");
    NSURL *URL = [NSURL URLWithString:@"http://www.petsmagazine.com.sg/"];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    [self presentModalViewController:webViewController animated:YES];
}

- (void) onDownloadWith:(ObjMagazine *)objMaga{
    NSLog(@"on Download");
    objMaga.strMagzineFileLink = @"https://itunes.apple.com/sg/app/pets-magazine/id496227308?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: objMaga.strMagzineFileLink]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
