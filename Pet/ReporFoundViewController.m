//
//  ReporLostViewController.m
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "ReporFoundViewController.h"
#import "SOAPRequest.h"
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "ObjPet.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "petAPIClient.h"
#import "ObjArea.h"
#import "ObjBreedType.h"
#import "Utility.h"
#import "ObjPetType.h"
#import "TSActionSheet.h"
#import "ProfileMapViewController.h"
#import "Utility.h"
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollView---
CGSize scrollViewOriginalSize;
@interface ReporFoundViewController ()
{
    SOAPRequest * myLostPetRequest;
    NSMutableArray * arrPet;
    NSMutableArray * arrBreed;
    NSMutableArray * arrGender;
    NSMutableArray * arrArea;
    NSMutableArray * arrType;
    IBOutlet UIButton * btnPet;
    IBOutlet UITextField * txtArea;
    IBOutlet UITextView * txtDescription;
    IBOutlet UITextField * txtLastSeen;
    IBOutlet UITextField * txtReward;
    ObjPet * objPet;
    
    IBOutlet UIImageView * imgBgView;
    TKLabelTextFieldCell *cell3;
    TKLabelTextViewCell *cell2;
    TKLabelTextFieldCell * areaTextCell;
    TKLabelTextFieldCell * rewardTextCell;
    TKLabelTextFieldCell * lastSeenTextCell;
    TKSwitchCell * currentLocationCell;
    IBOutlet UITableView *tbl;
    IBOutlet UIToolbar *keyboardToolbar;
    UIImageView * imgProfile;
    
    NSDate * date;
    NSString * strDate;
    int selectedBreed;
    int selectedGender;
    int selectedArea;
    int selectedType;
    
    UIImagePickerController *imagePicker;
    BOOL isImageCamaraSelected;
    BOOL isTypeSelected;
    BOOL isBreedSelected;
    BOOL isGenderSelected;
    BOOL isAreaSelected;
    int fromCamera;
    
    float lat;
    float lon;
    BOOL isFromMap;
    UILabel * lblbtnCap1;
    UILabel * lblbtnCap2;
    UILabel * lblbtnCap3;
    UILabel * lblbtnCap4;
    UILabel * lblbtnCap5;
    UILabel * lblbtnCap6;
}
@property int selectedLevel;
@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIActionSheet *menu;
@property (nonatomic,strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic,strong) UITableViewCell *btnPet;
@property (nonatomic,strong) UITableViewCell *btnBreed;
@property (nonatomic,strong) UITableViewCell *btnPetType;
@property (nonatomic,strong) UITableViewCell *btnGender;
@property (nonatomic,strong) UITableViewCell *btnArea;
@property (nonatomic,strong) UITableViewCell *btnDate;
@property (nonatomic,strong) NSArray *cells;
@property (nonatomic, strong) UIDatePicker *uiDateView;
@property (nonatomic,strong) UITableViewCell *btnLocation;

@end

@implementation ReporFoundViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (!isFromMap) {
        [self syncLostInfo];
        lat = 0.0;
        lon = 0.0;
    }
}

- (void) loadTheRequiredCell{
    /*self.btnPet = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap2.text = @"Select Pet";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
    self.btnPet.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnPet.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnPet addSubview:lblbtnCap2];
    
    lastSeenTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lastSeenTextCell.label.text = @"Last Seen:";
    lastSeenTextCell.label.textAlignment = UITextAlignmentLeft;
    lastSeenTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    lastSeenTextCell.label.textColor = [UIColor blackColor];
    
    lastSeenTextCell.label.frame = CGRectMake(30 + lastSeenTextCell.label.frame.origin.x, lastSeenTextCell.label.frame.origin.y, cell3.frame.size.width+10, lastSeenTextCell.frame.size.height);
	lastSeenTextCell.field.text = @"";
    lastSeenTextCell.field.tag = 0;
    lastSeenTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    areaTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	areaTextCell.label.text = @"Area:";
    areaTextCell.label.textAlignment = UITextAlignmentLeft;
    areaTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    areaTextCell.label.textColor = [UIColor blackColor];
    
    areaTextCell.label.frame = CGRectMake(30 + areaTextCell.label.frame.origin.x, areaTextCell.label.frame.origin.y, areaTextCell.frame.size.width+10, areaTextCell.frame.size.height);
	areaTextCell.field.text = @"";
    areaTextCell.field.tag = 0;
    areaTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    rewardTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	rewardTextCell.label.text = @"Reward:";
    rewardTextCell.label.textAlignment = UITextAlignmentLeft;
    rewardTextCell.label.font = [UIFont boldSystemFontOfSize:13];
    rewardTextCell.label.textColor = [UIColor blackColor];
    
    rewardTextCell.label.frame = CGRectMake(30 + rewardTextCell.label.frame.origin.x, rewardTextCell.label.frame.origin.y, rewardTextCell.frame.size.width+10, rewardTextCell.frame.size.height);
	rewardTextCell.field.text = @"";
    rewardTextCell.field.tag = 0;
    rewardTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Description";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:13];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cells = @[self.btnPet,areaTextCell,lastSeenTextCell,rewardTextCell,cell2];*/
    self.btnPetType = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap1) {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap1  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap1 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
        //lblbtnCap1  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    }
    lblbtnCap1.text = @"Select Type";
    lblbtnCap1.textColor = [UIColor blackColor];
    lblbtnCap1.textAlignment = UITextAlignmentLeft;
    lblbtnCap1.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap1.backgroundColor = [UIColor clearColor];
    lblbtnCap1.tag = 1;
    self.btnPetType.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnPetType.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnPetType addSubview:lblbtnCap1];
    
    self.btnBreed = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap2) {
        //lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap2  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap2.text = @"Select Breed";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
    self.btnBreed.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnBreed.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnBreed addSubview:lblbtnCap2];
    
    self.btnGender = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap3) {
        //lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap3  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap3.text = @"Select Gender";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 1;
    self.btnGender.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnGender.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnGender addSubview:lblbtnCap3];
    
    self.btnArea = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap4) {
        //lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap4.text = @"Select Area";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 1;
    self.btnArea.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnArea.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnArea addSubview:lblbtnCap4];
    
    self.btnDate = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap5) {
        //lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap5  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap5.text = @"Last seen Date";
    lblbtnCap5.textColor = [UIColor blackColor];
    lblbtnCap5.textAlignment = UITextAlignmentLeft;
    lblbtnCap5.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap5.backgroundColor = [UIColor clearColor];
    lblbtnCap5.tag = 1;
    self.btnDate.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnDate.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnDate addSubview:lblbtnCap5];
    
    /*self.btnLocation = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap6) {
        //lblbtnCap6 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap6  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap6 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
        }
    }
    lblbtnCap6.text = @"Location";
    lblbtnCap6.textColor = [UIColor blackColor];
    lblbtnCap6.textAlignment = UITextAlignmentLeft;
    lblbtnCap6.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap6.backgroundColor = [UIColor clearColor];
    lblbtnCap6.tag = 1;
    self.btnLocation.selectionStyle = UITableViewCellSelectionStyleNone;
    self.btnLocation.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnLocation addSubview:lblbtnCap6];*/
    currentLocationCell = [[TKSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    currentLocationCell.textLabel.text = @"Are you at the location?";
    currentLocationCell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    
    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Description:";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:13];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITableViewCell *cell4 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	UILabel * lblCap;
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        lblCap  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 200, 30)];
    }
    else if([Utility isGreaterOREqualOSVersion:@"6"])
    {
        lblCap = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    }
    lblCap.textColor = [UIColor blackColor];
    lblCap.backgroundColor = [UIColor clearColor];
    lblCap.text=@"Take Photo:";
    lblCap.font = [UIFont boldSystemFontOfSize:14];
    lblCap.numberOfLines = 0;
    imgProfile = [[UIImageView alloc]initWithFrame:CGRectMake(210, 7, 90, 90)];
    imgProfile.backgroundColor = [UIColor darkGrayColor];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell4 addSubview:lblCap];
    [cell4 addSubview:imgProfile];
    
    self.cells = @[self.btnArea,self.btnPetType,self.btnBreed,self.btnGender,cell2,self.btnDate,cell4,currentLocationCell];
}

- (void)setBreedText:(NSString *)strValue{
    lblbtnCap2.text = [NSString stringWithFormat:@"Breed: %@",strValue];
}

- (void)setTypeText:(NSString *)strValue{
    lblbtnCap1.text = [NSString stringWithFormat:@"Type: %@",strValue];
}

- (void)setLastSDText:(NSString *)strValue{
    lblbtnCap5.text = [NSString stringWithFormat:@"Last Seen Date: %@",strValue];
}

- (void)setAreaText:(NSString *)strValue{
    lblbtnCap4.text = [NSString stringWithFormat:@"Area: %@",strValue];
}

- (void)setGenderText:(NSString *)strValue{
    lblbtnCap3.text = [NSString stringWithFormat:@"Gender: %@",strValue];
}


- (void)hardCodeList{
    arrGender = [[NSMutableArray alloc]initWithCapacity:3];
    [arrGender addObject:@"I don't know"];
    [arrGender addObject:@"Male"];
    [arrGender addObject:@"Female"];
}

- (void) syncPets{
    [SVProgressHUD show];
    if (myLostPetRequest == nil) {
        myLostPetRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    myLostPetRequest.processId = 7;
    [myLostPetRequest syncMyPet];
}

- (void) syncLostInfo{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    [self hideKeyboard:nil];
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",REPORT_FOUND_INFO_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            
            NSMutableArray *  arrTemp = [dics objectForKey:@"areas"];
            [arrArea removeAllObjects];
            if(arrArea == nil){
                arrArea = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arrTemp count];i++){
                NSDictionary * dicNewsFeed = [arrTemp objectAtIndex:i];
                ObjArea * objA = [[ObjArea alloc]init];
                if ([dicNewsFeed objectForKey:@"id"] != [NSNull null]) {
                    objA.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                }
                if ([dicNewsFeed objectForKey:@"name"] != [NSNull null]) {
                    objA.strName = [dicNewsFeed objectForKey:@"name"];
                }
                [arrArea addObject:objA];
            }
            
            NSLog(@"arrArea count %d",[arrArea count]);
            
            /*NSMutableArray *  arrTemp2 = [dics objectForKey:@"pet_breed_types"];
            [arrBreed removeAllObjects];
            if(arrBreed == nil){
                arrBreed = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arrTemp2 count];i++){
                NSDictionary * dicNewsFeed = [arrTemp2 objectAtIndex:i];
                ObjBreedType * objB = [[ObjBreedType alloc]init];
                if ([dicNewsFeed objectForKey:@"id"] != [NSNull null]) {
                    objB.idx = [[dicNewsFeed objectForKey:@"id"] intValue];
                }
                if ([dicNewsFeed objectForKey:@"name"] != [NSNull null]) {
                    objB.strName = [dicNewsFeed objectForKey:@"name"];
                }
                [arrBreed addObject:objB];
            }
            
            NSLog(@"arrBreed count %d",[arrBreed count]);*/
           /* NSMutableArray *  arr = [dics objectForKey:@"pets"];
            [arrPet removeAllObjects];
            if(arrPet == nil){
                arrPet = [[NSMutableArray alloc]init];
            }
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjPet * obj = [[ObjPet alloc]init];
                if ([dicNewsFeed objectForKey:@"id"] != [NSNull null]) {
                    obj.pet_id = [[dicNewsFeed objectForKey:@"id"] intValue];
                }
                if ([dicNewsFeed objectForKey:@"name"] != [NSNull null]) {
                    obj.strName = [dicNewsFeed objectForKey:@"name"];
                }
                [arrPet addObject:obj];
            }
            
            NSLog(@"arrPet count %d",[arrPet count]);
            
            */
            
            NSMutableArray *  arr = [dics objectForKey:@"pet_types"];
            if (arrType == nil) {
                arrType = [[NSMutableArray alloc]initWithCapacity:[arr count]];
            }
            [arrType removeAllObjects];
            
            
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicPType = [arr objectAtIndex:i];
                ObjPetType * objPT = [[ObjPetType alloc]init];
                objPT.idx = [[dicPType objectForKey:@"id"]intValue];
                objPT.strName = [dicPType objectForKey:@"name"];
                
                NSMutableArray *  arrBreedType = [dicPType objectForKey:@"pet_breed_types"];
                objPT.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]+1];
                
                /*if (self.arrBreed == nil) {
                 self.arrBreed = [[NSMutableArray alloc]initWithCapacity:[arrBreedType count]];
                 }
                 [self.arrBreed removeAllObjects];*/
                ObjBreedType * objBT = [[ObjBreedType alloc]init];
                objBT.idx = 0;
                objBT.strName = @"None";
                [objPT.arrBreed addObject:objBT];
                for(NSInteger b=0;b<[arrBreedType count];b++){
                    NSDictionary * dicType = [arrBreedType objectAtIndex:b];
                    ObjBreedType * objBT = [[ObjBreedType alloc]init];
                    objBT.idx = [[dicType objectForKey:@"id"]intValue];
                    objBT.strName = [dicType objectForKey:@"name"];
                    [objPT.arrBreed addObject:objBT];
                }
                [arrType addObject:objPT];
            }
            
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 7) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        if ([dics objectForKey:@"pets"] != [NSNull null]) {
            NSMutableArray * arrRawPets = [dics objectForKey:@"pets"];
            if ([arrRawPets count] != 0) {
                arrPet = [[NSMutableArray alloc]initWithCapacity:[arrRawPets count]];
                for(NSInteger i=0;i<[arrRawPets count];i++){
                    NSDictionary * dicPet = [arrRawPets objectAtIndex:i];
                    ObjPet * objPet = [[ObjPet alloc]init];
                    if ([dicPet objectForKey:@"pet_id"] != [NSNull null]) {
                        objPet.pet_id = [[dicPet objectForKey:@"pet_id"] intValue];
                    }
                    if ([dicPet objectForKey:@"pet_name"] != [NSNull null]) {
                        objPet.strName = [dicPet objectForKey:@"pet_name"];
                    }
                    if ([dicPet objectForKey:@"pet_image"] != [NSNull null]) {
                        objPet.strImgLink = [dicPet objectForKey:@"pet_image"];
                    }
                    if ([dicPet objectForKey:@"pet_type"] != [NSNull null]) {
                        objPet.strType = [dicPet objectForKey:@"pet_type"];
                    }
                    [arrPet addObject:objPet];
                    NSLog(@"arr pet count %d and objPet name %@ and id %d",[arrPet count],objPet.strName,objPet.pet_id);
                }
                ///[self reloadData:YES];
                [SVProgressHUD dismiss];
            }
            else
            {
                int status = [[dics objectForKey:@"status"] intValue];
                NSString * strMsg = [dics objectForKey:@"message"];
                if (status == STATUS_ACTION_SUCCESS) {
                    [SVProgressHUD showSuccessWithStatus:strMsg];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
                else if(status == STATUS_ACTION_FAILED){
                    
                    //[self textValidateAlertShow:strErrorMsg];
                    [SVProgressHUD showErrorWithStatus:strMsg];
                }
            }
        }
        
        
        
    }
    if (processId == 12) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.scrollView.frame = CGRectMake(0, 44, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    
    scrollViewOriginalSize = self.scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    [self loadForUIActionView];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton4 *btnSave = [[NavBarButton4 alloc] init];
	[btnSave addTarget:self action:@selector(onAdd:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithCustomView:btnSave];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = saveButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Report Found";
    self.navigationItem.titleView = lblName;
    
    [self loadTheRequiredCell];
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
    
    //[self syncPets];
    //[self syncLostInfo];
    isImageCamaraSelected = FALSE;
    strDate = @"";
    isTypeSelected = FALSE;
    isBreedSelected = FALSE;
    isGenderSelected = FALSE;
    isAreaSelected = FALSE;
    
    
     imagePicker = [[UIImagePickerController alloc] init];
    [self hardCodeList];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    
    CGRect newframe = self.view.frame;
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        
    }
    else {
        newframe.origin.y -= 20;
    }
    tbl.frame = newframe;
    
    UIView * v = [[UIView alloc] init];
    tbl.tableFooterView = v;
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    self.uiDateView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    self.selectedLevel = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}


- (void) showActionSheetCam:(id)sender forEvent:(UIEvent*)event
{
    UITableViewCell * cell = (UITableViewCell *)sender;
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Camera" block:^{
        NSLog(@"Camera");
        [self showCamera];
    }];
    [actionSheet addButtonWithTitle:@"Library" block:^{
        NSLog(@"Library");
        // [self showPhotoLibrary];
        [self showPhotoLibrary:nil];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    //actionSheet.
    
    [actionSheet showWithCell:cell];
}

- (IBAction) showPhotoLibrary:(id) sender{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    
    // Set source to the camera
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        fromCamera = 0;
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Fuction is not suported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

-(void)showCamera{
    if (imagePicker ==  nil) {
        imagePicker = [[UIImagePickerController alloc] init];
    }
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = YES;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        fromCamera = 1;
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
        fromCamera = -1;
    }
    @finally {
        
    }
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onAdd:(id)sender{
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    ObjArea * objA = [arrArea objectAtIndex:selectedArea];
    ObjPetType * objT = [arrType objectAtIndex:selectedType];
    ObjArea * objB = [objT.arrBreed objectAtIndex:selectedBreed];
    //NSString * strArea = objA.strName;
    NSString * strLastSeen = [strDate stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * strArea =[objA.strName stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * strType =[[NSString stringWithFormat:@"%@",objT.strName] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * strBreed =[[NSString stringWithFormat:@"%@",objB.strName] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString * strGender = [arrGender objectAtIndex:selectedGender];
    NSString * strDescription =[cell2.textView.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([strLastSeen isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Last Seen is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(!isAreaSelected){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Area is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(!isTypeSelected){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Type is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if(!isBreedSelected){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Breed is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    /*else if(!isGenderSelected){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Gender is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }*/
    else if([strDescription isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Description is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    /*else if (lat == 0.0 && lon == 0.0){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"Pet lost location is required!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }*/
    else{
        if ([currentLocationCell.switcher isOn]) {
            lat = delegate.currentLat;
            lon = delegate.currentLng;
        }
        else{
            lat = 0;
            lon = 0;
        }
        ObjPet * pet = [[ObjPet alloc]init];
        pet.strArea = strArea;
        pet.strDescription = strDescription;
        pet.strLastSeen = strLastSeen;
        pet.strGender = strGender;
        pet.strBreed = strBreed;
        pet.strType = strType;
        pet.lat  = lat;
        pet.lon  = lon;
        NSData *imageData = UIImageJPEGRepresentation(imgProfile.image,0.4);
        NSString *base64image = [self base64forData:imageData];
        pet.strImgLink = base64image;
        //pet.strImgLink =
        //NSLog(@"pet strImgLink %@",pet.strImgLink);
        [self syncFoundPet:pet];
    }
}

/*- (void)syncLostPet:(ObjPet *)pet{
   
        [SVProgressHUD show];
        if (myLostPetRequest == nil) {
            myLostPetRequest = [[SOAPRequest alloc] initWithOwner:self];
        }
        myLostPetRequest.processId = 12;
        [myLostPetRequest syncReportLostPet:pet];
}*/

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        /*ObjBreedType * objB = [arrBreed objectAtIndex:row];
        return objB.strName;*/
        if ([arrType count]>0) {
            ObjPetType * objType = [arrType objectAtIndex:selectedType];
            ObjBreedType * objBT = [objType.arrBreed objectAtIndex:row];
            NSString *strName = objBT.strName;
            return strName;
        }
        return @"";
    }
    if (pickerView.tag == 1) {
        NSString * strGender = [arrGender objectAtIndex:row];
        return strGender;
    }
    if (pickerView.tag == 2) {
        ObjArea * objA = [arrArea objectAtIndex:row];
        return objA.strName;
    }
    if (pickerView.tag == 3) {
        ObjPetType * obj = [arrType objectAtIndex:row];
        return obj.strName;
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        if ([arrType count]>0) {
            ObjPetType * objType = [arrType objectAtIndex:selectedType];
            return [objType.arrBreed count];
        }
        return 0;
    }
    if (pickerView.tag == 1) {
        return [arrGender count];
    }
    if (pickerView.tag == 2) {
        return [arrArea count];
    }
    if (pickerView.tag == 3) {
        return [arrType count];
    }

    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if(pickerView.tag == 0) {
        selectedBreed = row;
    }
    else if(pickerView.tag == 1) {
        selectedGender = row;
    }
    else if(pickerView.tag == 2) {
        selectedArea = row;
    }
    else if(pickerView.tag == 3) {
        selectedType = row;
        selectedBreed = 0;
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);
        [self.uiDateView removeFromSuperview];
        [self.uiPickerView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            ObjPetType * objPT = [arrType objectAtIndex:selectedType];
            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:selectedBreed];
            NSString * strBreedName;
            if([objPT.arrBreed count]>0){
                strBreedName = objBT.strName;
            }
            else{
                strBreedName = @"No Breed Type";
            }
            
            
            //lblbtnCap2.text = strBreedName;
            [self setBreedText:strBreedName];
            isBreedSelected  = TRUE;
        }
        if (self.uiPickerView.tag == 1){
            NSString * strGender = [arrGender objectAtIndex:selectedGender];
            
            [self setGenderText:strGender];
            isGenderSelected = TRUE;
        }
        if (self.uiPickerView.tag == 3){
            ObjPetType * objPT = [arrType objectAtIndex:selectedType];
            NSString * strName = objPT.strName;
            
            [self setTypeText:strName];
            ObjBreedType * objBT = [objPT.arrBreed objectAtIndex:selectedBreed];
            NSString * strBreedName;
            if([objPT.arrBreed count]>0){
                strBreedName = objBT.strName;
            }
            else{
                strBreedName = @"No Breed Type";
            }
        
            [self setBreedText:strBreedName];
            isTypeSelected = TRUE;
            isBreedSelected  = TRUE;
        }
        if (self.uiPickerView.tag == 2){
            ObjArea * obj = [arrArea objectAtIndex:selectedArea];
            
            [self setAreaText:obj.strName];
        }
        
        if (self.uiDateView.tag == 1){
            date = [self.uiDateView date];
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            strDate = [dateFormatter stringFromDate:date];
            [self setLastSDText:strDate];
            isAreaSelected = TRUE;
        }
    }
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

-(IBAction)onPet:(id)sender{
    [self.menu setTitle:@"Select Pet"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,600)];
}

-(IBAction)onType{
    [self.menu setTitle:@"Select Type"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 3;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onBreed{
    [self.menu setTitle:@"Select Breed"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onGender{
    [self.menu setTitle:@"Select Gender"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onArea{
    [self.menu setTitle:@"Select Area"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 2;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedLevel inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onDate:(id)sender{
    [self.menu setTitle:@"Select Date"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiDateView.tag = 1;
    self.uiDateView.datePickerMode = UIDatePickerModeDate;
    self.uiDateView.maximumDate = [NSDate date];
    //self.uiDateView.delegate = self;
    //[self.uiDateView reloadAllComponents];
    //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiDateView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (void)syncFoundPet:(ObjPet *)pet{
    
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    ObjArea * objArea = [arrArea objectAtIndex:selectedArea];
    ObjPetType * objType = [arrType objectAtIndex:selectedType];
    ObjBreedType * objBType = [objType.arrBreed objectAtIndex:selectedBreed];
    NSString * strGender = [arrGender objectAtIndex:selectedGender];
    
    NSDictionary* params = @{@"found[pet_type_id]":[NSString stringWithFormat:@"%d",objType.idx],@"found[pet_breed_type_id]":[NSString stringWithFormat:@"%d",objBType.idx],@"found[gender]":strGender,@"found[description]":pet.strDescription,@"found[area_id]":[NSString stringWithFormat:@"%d",objArea.idx],@"found[lastseen_date]":pet.strLastSeen,@"found[pet_image]":pet.strImgLink,@"found[lat]":[NSString stringWithFormat:@"%f",pet.lat],@"found[long]":[NSString stringWithFormat:@"%f",pet.lon]};
    
    //found[lat]
    NSLog(@"params variable %@",params);
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",FOUND_REPORT_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:strMsg];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: APP_TITLE
                                  message: strMsg
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 4) {
        return 120;
    }
    else if(indexPath.row == 6){
        return 120;
    }
    return 44;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    /*if (indexPath.row == 1) {
        [self onType:nil];
    }
    else if (indexPath.row == 2) {
        [self onDate:nil];
    }
    else if (indexPath.row == 3) {
        [self onReminderDate:nil];
    }*/
    if (indexPath.row == 0) {
        [self onArea];
    }
    if (indexPath.row == 1) {
        [self onType];
    }
    if (indexPath.row == 2) {
        [self onBreed];
    }
    if (indexPath.row == 4) {
        [self onGender];
    }
    if (indexPath.row == 5) {
        [self onDate:nil];
    }
    if (indexPath.row == 6) {
        [self showActionSheetCam:cell forEvent:nil];
    }
    if (indexPath.row == 7) {
        [self onLocation];
    }
}

- (void)onLocation{
    ProfileMapViewController * mapViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"profileMap"];
    mapViewController.owner = self;
    mapViewController.seletedLat = lat;
    mapViewController.seletedLon = lon;
    mapViewController.isEditable = TRUE;
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:mapViewController];
    nav.navigationBar.tintColor = [UIColor blackColor];
    
    [self.navigationController presentModalViewController:nav animated:YES];
    NSLog(@"here is on map click");
    isFromMap = TRUE;
}

- (void) finishWithCoordinate:(float)late andlong:(float)lng{
    NSLog(@"home town location %f and long %f",late,lng);
    lat = late;
    lon = lng;
    for (UIView * v in self.btnLocation.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 1) {
                UILabel * lbl = (UILabel *)v;
                lbl.text = [NSString stringWithFormat:@"%0.6f, %0.6f",late,lng];
            }
        }
    }
    isFromMap = TRUE;
}

- (void) closeMap{
    isFromMap = TRUE;
}

-(void) moveScrollView:(UIView *) theView {
    //---get the y-coordinate of the view---
    CGFloat viewCenterY = theView.center.y + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    //scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    //[scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

-(void)onPhoto {
    @try {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Allow editing of image ?
        imagePicker.allowsImageEditing = NO;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
        //}
        
    }
    @catch (NSException *exception) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Camera Function is not supported for simulation and iPad1."
                                                        delegate:self cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        NSLog(@"exception %@",exception);
    }
    @finally {
        
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //UIImage * editedImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImage * orginalImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage * resizedImage=[Utility imageByScalingProportionallyToSize:orginalImage newsize:CGSizeMake(180, 180) resizeFrame:FALSE];
    [self imagePickerControllerDidCancel:picker];
    [imgProfile setImage:resizedImage];
    //[self showPreviewWithImage:editedImage andIsCamera:YES];
    isImageCamaraSelected = TRUE;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    // Unable to save the image
    if (error){
        
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissModalViewControllerAnimated:YES];
    
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        // iOS 7
        //self.navigationController.navigationBar.frame = CGRectMake(self.navigationController.navigationBar.frame.origin.x, self.navigationController.navigationBar.frame.origin.y+20, self.navigationController.navigationBar.frame.size.width, 44);
        //self.navigationController.navigationBar.translucent = NO;
        // for iOS7
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
            
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        }
        PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        [delegate windowViewAdjust];
    }
}

-(void) textFieldDidBeginEditing:(UITextField *)textFieldView {
    [self moveScrollView:textFieldView];
}

-(void) textFieldDidEndEditing:(UITextField *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.tag == 1) {
        //[txtVDescription becomeFirstResponder];
        //return NO;
    }
    //[textField resignFirstResponder];
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *) textView{
    //[self moveScrollView:textView];
    UITableViewCell *cell = (UITableViewCell *)[textView superview];
    NSIndexPath *indexPath = [tbl indexPathForCell:cell];
    [tbl scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void)textViewDidEndEditing:(UITextView *) textFieldView {
    
    [UIView beginAnimations:@"back to original size" context:nil];
    //scrollView.contentSize = scrollViewOriginalSize;
    [UIView commitAnimations];
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (IBAction)hideKeyboard:(id)sender {
	//[self ];
    [self.view endEditing:YES];
}

//keyboard appear
-(void) keyboardWillShow:(NSNotification *) notification {
    //---gets the size of the keyboard---
    NSDictionary *userInfo = [notification userInfo];
    NSValue *keyboardValue = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
    [keyboardValue getValue:&keyboardBounds];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height - 260.0;
	keyboardToolbar.frame = frame;
	
	[UIView commitAnimations];
    
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
    
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	
	CGRect frame = keyboardToolbar.frame;
	frame.origin.y = self.view.frame.size.height;
	keyboardToolbar.frame = frame;
    
    NSTimeInterval duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [tbl setContentInset:edgeInsets];
        [tbl setScrollIndicatorInsets:edgeInsets];
    }];
	
	[UIView commitAnimations];
}


@end
