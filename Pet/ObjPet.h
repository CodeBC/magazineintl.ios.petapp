//
//  ObjPet.h
//  Pet
//
//  Created by Zayar on 4/29/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ObjPet : NSObject
{
    int pet_id;
    NSString * strName;
    NSString * strImgLink;
    NSString * strType;
    int type_id;
    NSString * strDescription;
    NSString * strGender;
    NSString * strBreed;
    int breed_type_id;
    NSString * strDob;
    NSString * strArea;
    NSString * strLastSeen;
    NSString * strReward;
    NSString * strDistance;
    NSMutableArray * arrAlbums;
    float lat;
    float lon;
    int neighbor_type;
}
@property int pet_id;
@property int type_id;
@property int breed_type_id;
@property int neighbor_type;
@property float lat;
@property float lon;
@property (nonatomic,strong)NSString * strName;
@property (nonatomic,strong)NSString * strImgLink;
@property (nonatomic,strong)NSString * strType;
@property (nonatomic,strong)NSString * strDescription;
@property (nonatomic,strong)NSString * strGender;
@property (nonatomic,strong)NSString * strBreed;
@property (nonatomic,strong)NSString * strDob;
@property (nonatomic,strong)NSString * strArea;
@property (nonatomic,strong)NSString * strLastSeen;
@property (nonatomic,strong)NSString * strReward;
@property (nonatomic,strong)NSString * strDistance;
@property (nonatomic, strong)NSMutableArray * arrAlbums;
@end
