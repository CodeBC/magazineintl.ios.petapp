//
//  AddReminderViewController.m
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DetailReminderViewController.h"
#import "ObjReminder.h"
#import "StringTable.h"
#import "PetAppDelegate.h"
#import "ADSlidingViewController.h"
#import "NavBarButton1.h"
#import "NavBarButton4.h"
#import "ObjAlertType.h"

@interface DetailReminderViewController (){
    NSString * strType;
    IBOutlet UIButton * btnType;
    NSDate * date;
    NSDate * remindDate;
    NSString * strDate;
    NSString * strRemindDate;
    IBOutlet UIButton * btnDate;
    IBOutlet UIButton * btnRemindDate;
    IBOutlet UITextField * txtName;
    SOAPRequest * reminderRequest;
    
    IBOutlet UIImageView * imgBgView;
    TKLabelTextFieldCell *cell3;
    NSString * strDefaultName;
    ObjAlertType * objSelectedAlertType;
    IBOutlet UITableView * tbl;
    
    UILabel * lblbtnCap2;
    UILabel * lblbtnCap3;
    UILabel * lblbtnCap4;
}

@property (nonatomic, strong) UIPickerView *uiPickerView;
@property (nonatomic, strong) UIDatePicker *uiDateView;
@property (nonatomic, strong) UIActionSheet *menu;
@property (nonatomic, strong) NSMutableArray * arrType;
@property (nonatomic, strong) NSMutableArray * arrDefaultName;
@property (nonatomic, strong) NSMutableArray * arrAlertType;
@property (nonatomic,strong) UITableViewCell *btnRepeatType;
@property (nonatomic,strong) UITableViewCell *btnDueDate;
@property (nonatomic,strong) UITableViewCell *btnReminderDate;
@property (nonatomic,strong) NSArray *cells;

@property int selectedType;
@property int selectedDefaultName;
@property int selectedAlertType;
@end

@implementation DetailReminderViewController
@synthesize objReminder;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)hardCodeList{
    
    self.arrType = [[NSMutableArray alloc]initWithCapacity:3];
    //[self.arrType addObject:REPEAT_TYPE_NEVER];
    //[self.arrType addObject:REPEAT_TYPE_EVER];
    //[self.arrType addObject:REPEAT_TYPE_ONCE];
    
    [self.arrType addObject:@"None"];
    [self.arrType addObject:@"Every Day"];
    [self.arrType addObject:@"Every Week"];
    [self.arrType addObject:@"Every Month"];
    [self.arrType addObject:@"Every Year"];
    
    self.arrDefaultName = [[NSMutableArray alloc]initWithCapacity:3];
    [self.arrDefaultName addObject:@"Annual Jab"];
    [self.arrDefaultName addObject:@"Annual Vet Visit"];
    [self.arrDefaultName addObject:@"Flea & Tick"];
    
    self.arrAlertType = [[NSMutableArray alloc]init];
    ObjAlertType * objAlert = [[ObjAlertType alloc]init];
    objAlert.idx = 1;
    objAlert.strName = @"None";
    objAlert.timeInterval = -1;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 2;
    objAlert.strName = @"At time of event";
    objAlert.timeInterval = MINUTE_INTERVAL * 0;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 3;
    objAlert.strName = @"5 minutes before";
    objAlert.timeInterval = MINUTE_INTERVAL * 5;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 4;
    objAlert.strName = @"15 minutes before";
    objAlert.timeInterval = MINUTE_INTERVAL * 15;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 5;
    objAlert.strName = @"30 minutes before";
    objAlert.timeInterval = MINUTE_INTERVAL * 30;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 6;
    objAlert.strName = @"1 hour before";
    objAlert.timeInterval = MINUTE_INTERVAL * 60;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 7;
    objAlert.strName = @"2 hours before";
    objAlert.timeInterval = (MINUTE_INTERVAL * 60) * 2;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 8;
    objAlert.strName = @"1 day before";
    objAlert.timeInterval = (MINUTE_INTERVAL * 60) * 24;
    [self.arrAlertType addObject:objAlert];
    
    objAlert = [ObjAlertType new];
    objAlert.idx = 9;
    objAlert.strName = @"2 days before";
    objAlert.timeInterval = (MINUTE_INTERVAL * 60) * 48;
    [self.arrAlertType addObject:objAlert];
}

- (void) loadTheRequiredCell{
    cell3 = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell3.label.text = @"Title:";
    cell3.label.textAlignment = UITextAlignmentLeft;
    cell3.label.font = [UIFont boldSystemFontOfSize:13];
    cell3.label.textColor = [UIColor blackColor];
    
    cell3.label.frame = CGRectMake(30 + cell3.label.frame.origin.x, cell3.label.frame.origin.y, cell3.frame.size.width+10, cell3.frame.size.height);
	cell3.field.text = @"";
    cell3.field.tag = 0;
    cell3.field.textAlignment = NSTextAlignmentRight;
    UIButton * btn= [UIButton buttonWithType:UIButtonTypeContactAdd];
    //cell3.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    [btn addTarget:self action:@selector(onShowDefault:) forControlEvents:UIControlEventTouchUpInside];
    cell3.accessoryView = btn;
    
    self.btnRepeatType = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if(!lblbtnCap2)
    {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap2  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 220, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 220, 30)];
        }
    }
       // lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap2.text = @"Repeat";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
    self.btnRepeatType.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnRepeatType addSubview:lblbtnCap2];
    
    self.btnDueDate = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap3)
    {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap3  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 240, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 240, 30)];
        }
    }
        //lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap3.text = @"Due Date";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 1;
    self.btnDueDate.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnDueDate addSubview:lblbtnCap3];
    
    self.btnReminderDate = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    if (!lblbtnCap4)
    {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            lblbtnCap4  = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, 240, 30)];
        }
        else if([Utility isGreaterOREqualOSVersion:@"6"])
        {
            lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 240, 30)];
        }
    }
    //lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap4.text = @"Remind Date";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 1;
    self.btnReminderDate.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self.btnReminderDate addSubview:lblbtnCap4];
    
    self.cells = @[cell3,self.btnRepeatType,self.btnDueDate,self.btnReminderDate];
}

- (IBAction) leftBarButton:(UIBarButtonItem *)sender {
	[[self slidingViewController] anchorTopViewTo:ADAnchorSideRight];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadForUIActionView];
    [self hardCodeList];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    UILabel * lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text= @"Detail Reminder";
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
    
    NavBarButton4 *btnAdd = [[NavBarButton4 alloc] init];
	[btnAdd addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithCustomView:btnAdd];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = addButton;
    
    [self loadTheRequiredCell];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    self.uiDateView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    self.selectedType = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
	return YES;
}

- (void)viewWillAppear:(BOOL)animated{
    //[self viewLoadWith:objReminder];
    [self viewTableWith:objReminder];
}

- (void)viewLoadWith:(ObjReminder *)obj{
    self.navigationItem.title = obj.strName;
    txtName.text = obj.strName;
    strDate = [NSString stringWithFormat:@"%@",obj.strDate];
    strRemindDate = [NSString stringWithFormat:@"%@",obj.strReminderDate];
    strType = obj.strRepeatType;
    [btnDate setTitle:[NSString stringWithFormat:@"%@",obj.strDate] forState:normal];
    [btnRemindDate setTitle:[NSString stringWithFormat:@"%@",obj.strReminderDate] forState:normal];
    [btnType setTitle:[NSString stringWithFormat:@"%@",obj.strRepeatType] forState:normal];
    NSDateFormatter * fomartter = [[NSDateFormatter alloc]init];
    [fomartter setDateStyle:NSDateFormatterMediumStyle];
    [fomartter setTimeStyle:NSDateFormatterShortStyle];
    date = [fomartter dateFromString:obj.strDate];
    remindDate = [fomartter dateFromString:obj.strReminderDate];
}

- (void)viewTableWith:(ObjReminder *)obj{
    self.navigationItem.title = obj.strName;
    cell3.field.text = obj.strName;
    strDate = [NSString stringWithFormat:@"%@",obj.strDate];
    strRemindDate = [NSString stringWithFormat:@"%@",obj.strReminderDate];
    strType = obj.strRepeatType;
    NSDateFormatter * fomartter = [[NSDateFormatter alloc]init];
    [fomartter setDateStyle:NSDateFormatterMediumStyle];
    [fomartter setTimeStyle:NSDateFormatterShortStyle];
    date = [fomartter dateFromString:obj.strDate];
    NSLog(@"date due date %@ and strDate %@",date,obj.strDate);
    remindDate = date;
    if (objSelectedAlertType == nil) {
        objSelectedAlertType = [[ObjAlertType alloc]init];
    }
    NSLog(@"alart time %@",strRemindDate);
    objSelectedAlertType.strName = strRemindDate;
    objSelectedAlertType.timeInterval = obj.reminder_timetick;
    /*for (UIView * v in self.btnRepeatType.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 1) {
                UILabel * lbl = (UILabel *)v;
                lbl.text = [NSString stringWithFormat:@"Repeat (%@)", strType];
            }
        }
    }
    for (UIView * v in self.btnDueDate.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 1) {
                UILabel * lbl = (UILabel *)v;
                lbl.text = [NSString stringWithFormat:@"%@", strDate];
            }
        }
    }
    for (UIView * v in self.btnReminderDate.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            if (v.tag == 1) {
                UILabel * lbl = (UILabel *)v;
                lbl.text = [NSString stringWithFormat:@"Alert (%@)", strRemindDate];
            }
        }
    }*/
    lblbtnCap2.text = [NSString stringWithFormat:@"Repeat: %@", strType];
    //lblbtnCap3.text = [NSString stringWithFormat:@"%@", strDate];
    [self setRemindDateText:strDate];
    lblbtnCap4.text = [NSString stringWithFormat:@"Alert: %@", strRemindDate];
}

- (void)setRemindDateText:(NSString *)strValue{
    lblbtnCap3.text = [NSString stringWithFormat:@"Due Date: %@", strValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView.tag == 0) {
        NSString *strName = [self.arrType objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return strName;
        
    }
    if (pickerView.tag == 1) {
        NSString *strName = [self.arrDefaultName objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return strName;
        
    }
    if (pickerView.tag == 2) {
        ObjAlertType * obj = [self.arrAlertType objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);
        
        return obj.strName;
    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [self.arrType count];
    }
    else if (pickerView.tag == 1) {
        return [self.arrDefaultName count];
    }
    else if (pickerView.tag == 2) {
        return [self.arrAlertType count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    
    if (pickerView.tag == 0) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedType = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }
    if (pickerView.tag == 1) {
        
        self.selectedDefaultName = row;
        
    }
    
    if (pickerView.tag == 2) {
        self.selectedAlertType = row;
        
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
        [self.uiPickerView removeFromSuperview];
       
        [self.uiDateView removeFromSuperview];
    }
    
    else if (buttonIndex == 1) {
        [self.uiPickerView removeFromSuperview];
        [self.uiDateView removeFromSuperview];
        if (self.uiPickerView.tag == 0){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrType objectAtIndex:self.selectedType];
            //[btnType setTitle:strName forState:normal];
            /*for (UIView * v in self.btnRepeatType.subviews) {
                if ([v isKindOfClass:[UILabel class]]) {
                    if (v.tag == 1) {
                        UILabel * lbl = (UILabel *)v;
                        lbl.text = [NSString stringWithFormat:@"Repeat (%@)", strName];
                    }
                }
            }*/
            lblbtnCap2.text = [NSString stringWithFormat:@"Repeat: %@", strName];
            strType = strName;
            //[self.uiPickerView removeFromSuperview];
        }
        
        else if (self.uiPickerView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrDefaultName objectAtIndex:self.selectedDefaultName];
            //[btnType setTitle:strName forState:normal];
            cell3.field.text = strName;
            strDefaultName = strName;
            //[self.uiPickerView removeFromSuperview];
        }
        
        else if (self.uiPickerView.tag == 2){
            //Date picker click
            
            ObjAlertType * obj = [self.arrAlertType objectAtIndex:self.selectedAlertType];
            //Date picker click
            if (obj.idx == 1) {
                objSelectedAlertType = nil;
            }
            else{
                objSelectedAlertType = obj;
            }
            
            int dueDateTimetick = [date timeIntervalSince1970];
            
            int remindTimetick = dueDateTimetick - obj.timeInterval;
            
            remindDate = [[NSDate alloc]initWithTimeIntervalSince1970:remindTimetick];
            NSLog(@"Remind Date %@ and reminTimetick %d",remindDate,remindTimetick);
            
            /*for (UIView * v in self.btnReminderDate.subviews) {
                if ([v isKindOfClass:[UILabel class]]) {
                    if (v.tag == 1) {
                        UILabel * lbl = (UILabel *)v;
                        lbl.text = [NSString stringWithFormat:@"Alert (%@)", obj.strName];
                    }
                }
            }*/
            lblbtnCap4.text = [NSString stringWithFormat:@"Alert: %@", obj.strName];
            self.uiDateView.tag = 0;
            strRemindDate = obj.strName;
            NSLog(@"Actual Remind date 2%@",remindDate);
            //[self.uiDateView removeFromSuperview];
        }
        
        if (self.uiDateView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            date = [self.uiDateView date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            NSString * strDate2 = [dateFormatter stringFromDate:date];
            /*for (UIView * v in self.btnDueDate.subviews) {
                if ([v isKindOfClass:[UILabel class]]) {
                    if (v.tag == 1) {
                        UILabel * lbl = (UILabel *)v;
                        lbl.text = [NSString stringWithFormat:@"%@", strDate2];
                    }
                }
            }*/
            [self setRemindDateText:strDate2];

            remindDate = [self.uiDateView date];
            strDate = [NSString stringWithFormat:@"%@",strDate2];
            NSLog(@"Actual Remind date date 1%@",remindDate);
        }
        
        /*if (self.uiDateView.tag == 2){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            remindDate = [self.uiDateView date];
            //[btnRemindDate setTitle:[NSString stringWithFormat:@"%@",remindDate] forState:normal];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            NSString * strDate2 = [dateFormatter stringFromDate:remindDate];
            for (UIView * v in self.btnReminderDate.subviews) {
                if ([v isKindOfClass:[UILabel class]]) {
                    if (v.tag == 1) {
                        UILabel * lbl = (UILabel *)v;
                        lbl.text = [NSString stringWithFormat:@"%@", strDate2];
                    }
                }
            }
            strRemindDate = [NSString stringWithFormat:@"%@",strDate2];
        }*/
    }
}

-(IBAction)onType:(id)sender{
    [self.menu setTitle:@"Select Repeat Type"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 0;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onDate:(id)sender{
    [self.menu setTitle:@"Select Date"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiDateView.tag = 1;
    self.uiDateView.datePickerMode = UIDatePickerModeDateAndTime;
    self.uiDateView.minimumDate = [NSDate date];
    //self.uiDateView.delegate = self;
    //[self.uiDateView reloadAllComponents];
    //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
    [self.menu addSubview:self.uiDateView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onReminderDate:(id)sender{
    if (date != nil) {
        [self.menu setTitle:@"Select Date"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiDateView.tag = 2;
        self.uiDateView.datePickerMode = UIDatePickerModeDateAndTime;
        self.uiDateView.minimumDate = [NSDate date];
        self.uiDateView.maximumDate = date;
        //self.uiDateView.delegate = self;
        //[self.uiDateView reloadAllComponents];
        //[self.uiDateView selectRow:self.selectedType inComponent:0 animated:YES];
        [self.menu addSubview:self.uiDateView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"You must select the due date first!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    
}

-(IBAction)onDelete:(id)sender{
    NSLog(@"on delete!!!");
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSString * strName = cell3.field.text;
    NSLog(@"str event name %@",strName);
        ObjReminder * objRemind = [[ObjReminder alloc]init];
        objRemind.strName = strName;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        NSString * strDate2 = [dateFormatter stringFromDate:date];
        NSString * strRemindDate2 = [dateFormatter stringFromDate:remindDate];
        objRemind.strDate = [NSString stringWithFormat:@"%@",strDate2];
        objRemind.strReminderDate = [NSString stringWithFormat:@"%@",strRemindDate2];
        objRemind.strRepeatType = strType;
        objRemind.isDone = 0;
        objRemind.idx = objReminder.idx;
        objRemind.date_timetick = [date timeIntervalSince1970];
    objRemind.reminder_timetick = [remindDate timeIntervalSince1970];
        //objReminder = objRemind;
        //[self syncAddRemind:objRemind];
    [delegate.db deleteReminder:objRemind];
    
    [self deleteLocalNotification:objReminder];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)onSave:(id)sender{
    NSLog(@"remind date %@",remindDate);
    if ([remindDate compare:[NSDate date]] == NSOrderedDescending){
        PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
        NSString * strName = [cell3.field.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        ObjReminder * objRemind = [[ObjReminder alloc]init];
        objRemind.strName = strName;
        objRemind.strDate = strDate;
        objRemind.strReminderDate = strRemindDate;
        objRemind.strRepeatType = strType;
        objRemind.isDone = 0;
        objRemind.idx = objReminder.idx;
        objRemind.date_timetick = [date timeIntervalSince1970];
        objRemind.reminder_timetick = [remindDate timeIntervalSince1970];
        objReminder = objRemind;
        objReminder.isDone = 0;
        objReminder.isSwitch = 1;
        //[self syncAddRemind:objRemind];
        [delegate.db updateReminder:objReminder];
        [self manageLocalnotification:objRemind];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [Utility showAlert:APP_TITLE message:@"Reminder date should not be past date!"];
    }
}

- (void) deleteLocalNotification:(ObjReminder *)obj{
    NSString *myIDToCancel = [NSString stringWithFormat:@"%d",obj.idx];
    UILocalNotification *notificationToCancel=nil;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        NSArray * commands = nil;
        NSString * notiId = @"";
        NSString * notiName = @"";
        if( [[aNotif.userInfo objectForKey:@"ID"] rangeOfString:@","].location != NSNotFound ){
            
            commands = [[aNotif.userInfo objectForKey:@"ID"] componentsSeparatedByString:@","];
            notiId = [commands objectAtIndex:0];
            notiName = [commands objectAtIndex:1];
        }
        NSLog(@"notid id %@ and obj id %@",notiId,myIDToCancel);
        if([notiId isEqualToString:myIDToCancel]) {
            notificationToCancel=aNotif;
            break;
        }
        
    }
    if (notificationToCancel != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
    }
}

- (void) manageLocalnotification:(ObjReminder *)obj{
    NSString *myIDToCancel = [NSString stringWithFormat:@"%d",obj.idx];
    UILocalNotification *notificationToCancel=nil;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        NSArray * commands = nil;
        NSString * notiId = @"";
        NSString * notiName = @"";
        if( [[aNotif.userInfo objectForKey:@"ID"] rangeOfString:@","].location != NSNotFound ){
            
            commands = [[aNotif.userInfo objectForKey:@"ID"] componentsSeparatedByString:@","];
            notiId = [commands objectAtIndex:0];
            notiName = [commands objectAtIndex:1];
        }
         NSLog(@"notid id %@ and obj id %@",notiId,myIDToCancel);
        if([notiId isEqualToString:myIDToCancel]) {
            notificationToCancel=aNotif;
            break;
        }
    }
    if (notificationToCancel != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
    }
    NSLog(@"remind date %@",strRemindDate);
    if (![strRemindDate isEqualToString:@"None"]) {
        [self addLocalReminder:obj];
    }
}

- (void) addLocalReminder:(ObjReminder *)obj{
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    // Break the date up into components
    NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                                   fromDate:remindDate];
    NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
                                                   fromDate:remindDate];
    
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    // Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    switch (self.selectedType) {
        case 1:
            localNotif.repeatInterval = NSDayCalendarUnit;
            break;
        case 2:
            localNotif.repeatInterval = NSWeekCalendarUnit;
            break;
        case 3:
            localNotif.repeatInterval = NSMonthCalendarUnit;
            break;
        case 4:
            localNotif.repeatInterval = NSYearCalendarUnit;
            break;
        default:
            localNotif.repeatInterval = 0;
            break;
    }
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    localNotif.alertBody = obj.strName;
    NSLog(@"reminder name %@",obj.strName);
    // Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    // Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d,%@,",obj.idx,obj.strName] forKey:@"ID"];
    localNotif.userInfo = infoDict;
    
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

- (void) syncAddRemind:(ObjReminder *)pet{
    [SVProgressHUD show];
    if (reminderRequest == nil) {
        reminderRequest = [[SOAPRequest alloc] initWithOwner:self];
    }
    reminderRequest.processId = 10;
    [reminderRequest syncAddRemind:pet];
}

- (void) onErrorLoad: (int) processId{
    // PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{
    
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (processId == 10) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [delegate.db insertReminder:objReminder];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
    }
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        [self onType:nil];
    }
    else if (indexPath.row == 2) {
        [self onDate:nil];
    }
    else if (indexPath.row == 3) {
        //[self onReminderDate:nil];
        [self onAlertType:nil];
    }
}

- (void)onShowDefault:(UIButton *)sender{
    NSLog(@"btn default!!");
    [self onDefault:nil];
}

-(IBAction)onDefault:(id)sender{
    [self.menu setTitle:@"Select Name"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedDefaultName inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

-(IBAction)onAlertType:(id)sender{
    if (date != nil) {
        [self.menu setTitle:@"Select Alert Type"];
        //UIButton * btn = (UIButton *)sender;
        // Add the picker
        self.uiPickerView.tag = 2;
        self.uiPickerView.delegate = self;
        [self.uiPickerView reloadAllComponents];
        [self.uiPickerView selectRow:self.selectedAlertType inComponent:0 animated:YES];
        [self.menu addSubview:self.uiPickerView];
        [self.menu showInView:self.view];
        [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: @"You must select the due date first!"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    
}
@end

