//
//  DealDetailViewController.h
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjDeal.h"
@interface DealDetailViewController : PetBasedViewController

@property (nonatomic, strong) ObjDeal * objDeal;
- (IBAction)onBuy:(id)sender;
@end
