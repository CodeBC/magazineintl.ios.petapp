//
//  VideoTableCell.m
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "BrowseLFTableCell.h"
#import "ObjLostFound.h"
#import "UIImageView+AFNetworking.h"

@implementation BrowseLFTableCell
@synthesize imgView,lblName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadTheCellWith:(ObjLostFound *)obj{
    NSURL * url = [NSURL URLWithString:obj.strPetImgLink];
    NSLog(@"Image - %@", obj.strPetImgLink);
    [self.imgView setImageWithURL:url placeholderImage:nil];
    self.lblName.text = obj.strPetName;
    self.lblType.text = obj.strPetGender;
    if (obj.intLostFoundType == 1) {
        self.lblReportType.text = @"(Found)";
    }
    else if (obj.intLostFoundType == 0) {
        self.lblReportType.text = @"(Lost)";
    }
   
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
