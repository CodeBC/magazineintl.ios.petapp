//
//  AdoptionsListViewController.h
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdoptionsListViewController : PetBasedViewController
@property int selectedTypeId;
@property int selectedBreedId;
@property int selectedOrganastionId;
@end
