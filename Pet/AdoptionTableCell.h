//
//  AdoptionTableCell.h
//  Pet
//
//  Created by Zayar on 6/12/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAdoption.h"
@interface AdoptionTableCell : UITableViewCell
{
    IBOutlet UIImageView * imgView;
    IBOutlet UILabel * lblTypeAndBreed;
    IBOutlet UILabel * lblOrgaization;
}
- (void)loadTheCellWith:(ObjAdoption *)objAdo;
@end
