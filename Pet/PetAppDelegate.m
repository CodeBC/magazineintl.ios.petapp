//
//  PetAppDelegate.m
//  Pet
//
//  Created by Zayar on 4/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "PetAppDelegate.h"
#import "DBManager.h"
#import "Reachability.h"
#import "SOAPRequest.h"
#import "ObjGKey.h"
#import "ObjUser.h"
#import "StringTable.h"
#import "PetLoginViewController.h"
#import "Facebook.h"
#import "PetLoginViewController.h"
#import "PetRegisterViewController.h"
#import "petAPIClient.h"
#import "ObjLostFound.h"
#import <CommonCrypto/CommonDigest.h>
#import "PetViewController.h"
#import "ADSlidingViewController.h"
#import "ObjReminder.h"
#import "Utility.h"
#import "NSDate-Utilities.h"
#import "SBJson4.h"

//bfid:167071500119006
//skey:307bfdbe6d0f87c4d9f747bc5b05be4
static NSString* kAppId = @"167071500119006";
NSString *const FBSessionStateChangedNotification =@"com.bc.pet:FBSessionStateChangedNotification";
@implementation PetAppDelegate
@synthesize header_image,isNowInLogin;
@synthesize facebook,objAPhoto;

@synthesize userPermissions;
@synthesize uiPickerView,menu,selectedCountry,arrCountry;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    if ([Utility isGreaterOSVersion:@"7.0"])  self.header_image=@"NavF7.png";
    else self.header_image=@"Navbar.png";

    self.db = [[DBManager alloc] init];
	[self.db checkAndCreateDatabase];

    //UIDevice *device = [UIDevice currentDevice];
    NSUUID * udid = [[NSUUID alloc]init];
	self.strUdid =  [udid UUIDString];
    
    self.isFromMain = TRUE;
    self.isFromLogin = FALSE;

    //Add registration for remote notifications
    //[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];

    //Clear application badge when app launches
    //application.applicationIconBadgeNumber = 0;

    //[[UIApplication sharedApplication] unregisterForRemoteNotifications];
    //Override point for customization after application launch.

    NSLog(@"str Udid %@",self.strUdid);

    facebook = [[Facebook alloc] initWithAppId:kAppId andDelegate:nil];
    NSLog(@"facebook active access token %@",facebook.accessToken);
    // Check and retrieve authorization information
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];

    }
    // Initialize user permissions
    userPermissions = [[NSMutableDictionary alloc] initWithCapacity:1];

    // Check App ID:
    // This is really a warning for the developer, this should not
    // happen in a completed app
    if (!kAppId) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Setup Error"
                                  message:@"Missing app ID. You cannot run the app until you provide this in the code."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil,
                                  nil];
        [alertView show];
    }
    else {
        // Now check that the URL scheme fb[app_id]://authorize is in the .plist and can
        // be opened, doing a simple check without local app id factored in here
        NSString *url = [NSString stringWithFormat:@"fb%@://authorize",kAppId];
        BOOL bSchemeInPlist = NO; // find out if the sceme is in the plist file.
        NSArray* aBundleURLTypes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleURLTypes"];
        if ([aBundleURLTypes isKindOfClass:[NSArray class]] &&
            ([aBundleURLTypes count] > 0)) {
            NSDictionary* aBundleURLTypes0 = [aBundleURLTypes objectAtIndex:0];
            if ([aBundleURLTypes0 isKindOfClass:[NSDictionary class]]) {
                NSArray* aBundleURLSchemes = [aBundleURLTypes0 objectForKey:@"CFBundleURLSchemes"];
                if ([aBundleURLSchemes isKindOfClass:[NSArray class]] &&
                    ([aBundleURLSchemes count] > 0)) {
                    NSString *scheme = [aBundleURLSchemes objectAtIndex:0];
                    if ([scheme isKindOfClass:[NSString class]] &&
                        [url hasPrefix:scheme]) {
                        bSchemeInPlist = YES;
                    }
                }
            }
        }
        // Check if the authorization callback will work
        BOOL bCanOpenUrl = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: url]];
        if (!bSchemeInPlist || !bCanOpenUrl) {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Setup Error"
                                      message:@"Invalid or missing URL scheme. You cannot run the app until you set up a valid URL scheme in your .plist."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
        }
    }
    
    isFromFBRegister = FALSE;
    isFromFBLogin = FALSE;

    [self locationManager];


    [self hardCodeGenderList];

    [self loadForUIActionView];

    Class cls = NSClassFromString(@"UILocalNotification");
	if (cls) {
		UILocalNotification *notification = [launchOptions objectForKey:
                                             UIApplicationLaunchOptionsLocalNotificationKey];

		if (notification) {
			NSString *reminderID = [notification.userInfo
									  objectForKey:@"ID"];
            NSArray * commands = nil;
            NSString * notiId = @"";
            NSString * notiName = @"";
            if( [reminderID rangeOfString:@","].location != NSNotFound ){

                commands = [reminderID componentsSeparatedByString:@","];
                notiId = [commands objectAtIndex:0];
                notiName = [commands objectAtIndex:1];
            }

            [self showReminder:notiName andID:notiId];
		}
	}
    
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:0.89f green:0.43f blue:0.12f alpha:1.00f]];

	application.applicationIconBadgeNumber = 0;
    [NSThread sleepForTimeInterval:3.0];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)[application setStatusBarStyle:UIStatusBarStyleLightContent];
    [self windowViewAdjust];
    
    if (![Utility isGreaterOREqualOSVersion:@"7.0"]) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Navbar"] forBarMetrics:UIBarMetricsDefault];
    }
   
    
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent; // Set status bar color to white
}

#pragma mark UIActionSheet Setup
- (void)loadForUIActionView{
    
    self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
    
    self.uiPickerView.delegate = self;
    self.uiPickerView.showsSelectionIndicator = YES;// note this is default to NO
    
    
    self.selectedCountry = 0;
    
    CGRect toolbarFrame = CGRectMake(0, 0, self.menu.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    self.menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    // Do any additional setup after loading the view.
    if ([Utility isGreaterOSVersion:@"7.0"]) {
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40,320,260)];
        
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    else{
        
        self.uiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                        animated:NO];
        
    }
    
    
    [self.menu addSubview:controlToolbar];
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}

- (void) windowViewAdjust{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        self.window.clipsToBounds =YES;
        if ([Utility isScreenTall]) {
            [UIView animateWithDuration:0.2 animations:^{
            self.window.frame =  CGRectMake(0,20,self.window.frame.size.width,568-20);
            
            //Added on 19th Sep 2013
            self.window.bounds = CGRectMake(0, 20, self.window.frame.size.width, self.window.frame.size.height);
            }];
        }
        else{
            [UIView animateWithDuration:0.2 animations:^{
            self.window.frame =  CGRectMake(0,20,self.window.frame.size.width,480-20);
            
            //Added on 19th Sep 2013
            self.window.bounds = CGRectMake(0, 20, self.window.frame.size.width, self.window.frame.size.height);
            }];
        }
        
    }
}

- (void) windowViewReAdjust{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        self.window.clipsToBounds =YES;
        if ([Utility isScreenTall]) {
            [UIView animateWithDuration:0.2 animations:^{
                self.window.frame =  CGRectMake(0,0,self.window.frame.size.width,568);
                
                //Added on 19th Sep 2013
                self.window.bounds = CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height);
            }];
            
        }
        else{
            [UIView animateWithDuration:0.2 animations:^{
                self.window.frame =  CGRectMake(0,0,self.window.frame.size.width,480);
                
                //Added on 19th Sep 2013
                self.window.bounds = CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height);
            }];
            
        }
        
    }
}

- (void) showReminder:(NSString *)text andID:(NSString *)strID{

	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Pet Reminder"
                                                        message:text delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
    NSLog(@"Pet Reminder show");
    
    //ObjReminder * objR = [self.db getReminderById:[strID intValue]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshReminderView" object:nil];
    
}

- (void) hardCodeGenderList{

    self.arrCountry = [[NSMutableArray alloc]init];
    NSArray * commands = nil;
    if( [COUNTRY_STRING_ARRAY rangeOfString:@","].location != NSNotFound ){

        commands = [COUNTRY_STRING_ARRAY componentsSeparatedByString:@","];

        for(NSString * strWifiName in commands){
            [self.arrCountry addObject:strWifiName];
        }
    }
}

//fb connect
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"came back from fb!!");
    return [self.facebook handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [self.facebook handleOpenURL:url];
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {

	application.applicationIconBadgeNumber = 0;
	NSString *reminderID = [notification.userInfo
                            objectForKey:@"ID"];
    NSArray * commands = nil;
    NSString * notiId = @"";
    NSString * notiName = @"";
    if( [reminderID rangeOfString:@","].location != NSNotFound ){

        commands = [reminderID componentsSeparatedByString:@","];
        notiId = [commands objectAtIndex:0];
        notiName = [commands objectAtIndex:1];
    }
    
    [self showReminder:notiName andID:notiId];
}

- (void)updateStatus
{
	// Query the SystemConfiguration framework for the state of the device's network connections.
	self.remoteHostStatus           = [[Reachability sharedReachability] remoteHostStatus];
	self.internetConnectionStatus	= [[Reachability sharedReachability] internetConnectionStatus];
	self.localWiFiConnectionStatus	= [[Reachability sharedReachability] localWiFiConnectionStatus];
	[self updateCarrierDataNetworkWarning];
}

- (void)updateCarrierDataNetworkWarning
{
	NSString *msg;

	if (self.internetConnectionStatus == NotReachable) {
		msg = @"Sorry, SMATHS is unable to connect to the server. Please check your internet connection or try later";
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		getConnection = FALSE;
	}
    else getConnection = TRUE;
}

- (void)reachabilityChanged:(NSNotification *)note
{
    [self updateStatus];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    /*NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    //[prefs setObject:@"" forKey:GENR_KEY_LINK];
    ObjUser * obj= [self.db getUserObj];
    NSLog(@"obj.strSession %@",obj.strSession);
    if ([obj.strSession isEqualToString:@""] || obj.strSession == nil) {
        NSLog(@"login here");
        isNowInLogin = TRUE;
    }
    else {
        NSLog(@"view here");
        [self syncAndCheckTheSession];
        isNowInLogin = FALSE;
    }*/

    //self.window.rootViewController = self.viewController;
    [self.db removeCahcedImage];
    [self.window makeKeyAndVisible];
    /*NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    if ([strKey isEqualToString:@""] || strKey == nil) {
        [self performSelector:@selector(syncGenerateKey) withObject:nil afterDelay:0.1];
    }*/
    self.isFromMain = TRUE;
    // Although the SDK attempts to refresh its access tokens when it makes API calls,
    // it's a good practice to refresh the access token also when the app becomes active.
    // This gives apps that seldom make api calls a higher chance of having a non expired
    // access token.
    [[self facebook] extendAccessTokenIfNeeded];

    if ([[self facebook] isSessionValid]) {
        //[self showLoggedIn];
        if (isFromFBLogin) {
            [self apiFQLIMe];
        }

        if (isFromFBRegister) {
            [self apiFQLIMeForRegister];
        }

        if (isFromFBShare) {
            NSLog(@"facebook active access token %@",facebook.accessToken);
            NSLog(@"here is show dialog!!");
            [self showDialogWith:objSelectedLF];
        }
    }
    
    [self.db removeExpiredReminder];

    strCountry = @"";
    selectedCountry = 0;

    [self startLocationCounter];
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void) clearTheBudgeIcon{
    [UIApplication sharedApplication].applicationIconBadgeNumber= 0;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

////// SYNC FUNCTION //////////
- (void)syncGenerateKey{
    generateKeyRequest = [[SOAPRequest alloc] initWithOwner:self];
    generateKeyRequest.processId = 1;
    [generateKeyRequest syncGenerateKey];
    [SVProgressHUD show];
}

- (void)syncAndCheckTheSession{
    checkSessionRequest = [[SOAPRequest alloc] initWithOwner:self];
    checkSessionRequest.processId = 2;
    [checkSessionRequest syncAndCheckTheSessionKey];
    [SVProgressHUD show];
}

- (void) onErrorLoad: (int) processId{
    [self hideLoadingScreen];
    NSLog(@"Error loaded %d",processId);
    [SVProgressHUD showErrorWithStatus:@"Connection Error!"];
}

- (void) onJsonLoaded:(NSMutableDictionary *) dics{

}

- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId{
    if (processId == 1) {
        NSLog(@"arr count %d",[dics count]);
        ObjGKey * objKey = [[ObjGKey alloc]init];
        objKey.status = [[dics objectForKey:@"status"] intValue];
        objKey.strKey = [dics objectForKey:@"key"];
        objKey.strMessage = [dics objectForKey:@"message"];

        if (objKey.status == 1) {
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objKey.strKey forKey:GENR_KEY_LINK];

            ObjUser * obj= [self.db getUserObj];
            if (![obj.strSession isEqualToString:@""]) {
                [prefs setObject:obj.strSession forKey:LOGIN_LINK];
                self.strGenKey = obj.strSession;
            }
            [SVProgressHUD dismiss];
        }
        else {
           /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Field required." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];*/
            [SVProgressHUD showErrorWithStatus:objKey.strMessage];

        }
        /*for(NSInteger i=0;i<[arrFeedNews count];i++){
         NSMutableDictionary * dicNews = [arrFeedNews objectAtIndex:i];
         // NSMutableDictionary * dicNews2 = [dicNews objectForKey:@"node"];
         NSMutableDictionary * dicNews2 = dicNews;
         ObjectETNews * objNews = [[ObjectETNews alloc]init];
         objNews.nid = [[dicNews2 objectForKey:@"nid"] intValue];
         objNews.strTitle = [dicNews2 objectForKey:@"title"];
         [objNews.strTitle UTF8String];
         objNews.strBody = [dicNews2 objectForKey:@"body"];
         [objNews.strBody UTF8String];
         objNews.strPhotoLink = [dicNews2 objectForKey:@"photo"];
         [objNews.strPhotoLink UTF8String];
         objNews.intTimetick = [[dicNews2 objectForKey:@"updatetime"] intValue];
         NSLog(@"news title %@",objNews.strTitle);
         }*/
    }
    if (processId == 2) {
        NSLog(@"arr count %d",[dics count]);
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMessage = [dics objectForKey:@"message"];

        if (status == STATUS_ACTION_SUCCESS) {
            NSLog(@"session checking is successed %@",strMessage);
            //[self synUserPoint];
            ObjUser * objUser = [self.db getUserObj];
            objUser.strSession = [dics objectForKey:@"session_id"];

            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objUser.strSession forKey:LOGIN_LINK];
            NSLog(@"user session id %@",objUser.strSession);
            self.isNowInLogin = FALSE;
            [self.db updateUser:objUser];
            [SVProgressHUD dismiss];
        }
        else if(status == STATUS_SESSION_EXPIRED){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [SVProgressHUD showErrorWithStatus:strMessage];
            PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
            //nav.ownner = self;
            [self.window.rootViewController presentModalViewController:nav animated:YES];
            //self.window.rootViewController
        }
    }
}

//FB connect
/**
 * Show the logged in menu
 */
- (void)fbLogin:(PetLoginViewController *)pLoginViewController{
    petLoginViewController = pLoginViewController;
    current_View_Controller = pLoginViewController;
    isFromFBLogin = TRUE;
    isFromFBRegister = FALSE;
    isFromFBShare = FALSE;
    if (![[self facebook] isSessionValid]) {
        //[[self facebook] authorize:permissions];
        NSArray *permis = [[NSArray alloc] initWithObjects:@"publish_stream", @"email",@"user_birthday",@"user_about_me", nil];
        [[self facebook] authorize:permis];
    } else {

        [self showLoggedIn];
    }
    isFBLoginRequest = TRUE;
}

- (void)fbRegister:(PetRegisterViewController *)pRegisterViewController{
    petRegisterViewController = pRegisterViewController;
    current_View_Controller = pRegisterViewController;
    isFromFBRegister = TRUE;
    isFromFBShare = FALSE;
    isFromFBLogin = FALSE;
    if (![[self facebook] isSessionValid]) {
        NSArray *permis = [[NSArray alloc] initWithObjects:@"publish_stream", @"email",@"user_birthday",@"user_about_me", nil];
        //,@"first_name",@"last_name",@"gender"
        [[self facebook] authorize:permis];
    } else {
        [self apiFQLIMeForRegister];
    }
}

- (void)fbShare:(ObjLostFound *)objLF{
    isFromFBRegister = FALSE;
    isFromFBLogin = FALSE;
    isFromFBShare = TRUE;
    objSelectedLF = objLF;
    if (![[self facebook] isSessionValid]) {
       NSArray *permis = [NSArray arrayWithObjects:@"publish_actions",@"user_photos", nil];
        //,@"first_name",@"last_name",@"gender"
        [[self facebook] authorize:permis];
    } else {
        [self showDialogWith:objSelectedLF];
    }
}

- (void)fbAutoShare:(NSString *)strName{

    isFromFBRegister = FALSE;
    isFromFBLogin = FALSE;
    isFromFBShare = TRUE;
    if (![[self facebook] isSessionValid]) {
        NSArray *permis = [NSArray arrayWithObjects:@"publish_actions",@"user_photos", nil];
        //,@"first_name",@"last_name",@"gender"
        [[self facebook] authorize:permis];
    } else {
        [self shareAfterRegister:strName];
    }
}

- (void) logout:(UIViewController *)viewController{

    ObjUser * user = [self.db getUserObj];
    user.strSession = @"";
    [self.db updateUser:user];
    /*PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
     //nav.ownner = self;
     [self presentModalViewController:nav animated:YES];*/
    [self fbSessionKill];
    PetViewController * nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PetViewController"];
    ADSlidingViewController *slidingViewController = [viewController slidingViewController];
    [slidingViewController setMainViewController:nav];
    [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
}

- (void)showLoggedIn {
    [self apiFQLIMe];
}

- (void)apiFQLIMe {
    // Using the "pic" picture since this currently has a maximum width of 100 pixels
    // and since the minimum profile picture size is 180 pixels wide we should be able
    // to get a 100 pixel wide version of the profile picture
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"SELECT uid, name, pic, email FROM user WHERE uid=me()", @"query",
                                   nil];
    PetAppDelegate *delegate = (PetAppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate facebook] requestWithMethodName:@"fql.query"
                                     andParams:params
                                 andHttpMethod:@"POST"
                                   andDelegate:self];
}

- (void)apiFQLIMeForRegister{
    // Using the "pic" picture since this currently has a maximum width of 100 pixels
    // and since the minimum profile picture size is 180 pixels wide we should be able
    // to get a 100 pixel wide version of the profile picture
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"SELECT uid, name, pic, email FROM user WHERE uid=me()", @"query",
                                   nil];
    PetAppDelegate *delegate = (PetAppDelegate *)[[UIApplication sharedApplication] delegate];
    [[delegate facebook] requestWithMethodName:@"fql.query"
                                     andParams:params
                                 andHttpMethod:@"POST"
                                   andDelegate:self];
    isFBLoginRequest = TRUE;
}

- (void)apiGraphUserPermissions {
    [[self facebook] requestWithGraphPath:@"me" andDelegate:self];
    isFBParamRequest = TRUE;
}

#pragma mark - FBRequestDelegate Methods
/**
 * Called when the Facebook API request has returned a response.
 *
 * This callback gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    //NSLog(@"received response");
    NSLog(@"here is call back for received response");
}

/**
 * Called when a request returns and its response has been parsed into
 * an object.
 *
 * The resulting object may be a dictionary, an array or a string, depending
 * on the format of the API response. If you need access to the raw response,
 * use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
    NSLog(@"here is call back for fb success");

    if ([result isKindOfClass:[NSArray class]]) {
        result = [result objectAtIndex:0];
    }

    if (isFromFBLogin) {
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            //NSString * strId = [result objectForKey:@"id"];
            NSLog(@"Login result %@",result);
            if (objUser == nil) {
                objUser = [[ObjUser alloc]init];
            }
            NSString * strName = [result objectForKey:@"name"];
            NSString * strEmail = [result objectForKey:@"email"];
            NSString * strId = [result objectForKey:@"uid"];
            NSString * strPicLink = [result objectForKey:@"pic"];
            strPicLink = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", strId];
            objUser.strName = strName;
            objUser.strEmail = strEmail;
            objUser.strFbID = strId;
            objUser.strProfileImgLink = strPicLink;
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objUser.strFbID forKey:@"fbid"];
            [prefs synchronize];
            [self syncFBLoginWith:objUser];
        }
        else {
            // Processing permissions information
            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }
        isFromFBLogin = FALSE;
    }

    else if (isFromFBRegister){
        [SVProgressHUD show];
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            //NSString * strId = [result objectForKey:@"id"];
            NSLog(@"Register result %@",result);
            if (objUser == nil) {
                objUser = [[ObjUser alloc]init];
            }
            NSString * strName = [result objectForKey:@"name"];
            NSString * strEmail = [result objectForKey:@"email"];
            NSString * strId = [result objectForKey:@"uid"];
            NSString * strPicLink = [result objectForKey:@"pic"];
            strPicLink = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", strId];
            objUser.strName = strName;
            objUser.strEmail = strEmail;
            objUser.strFbID = strId;
            objUser.strProfileImgLink = strPicLink;
            isFromFBRegister = FALSE;
            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:objUser.strFbID forKey:@"fbid"];
            [prefs synchronize];
            [self apiGraphUserPermissions];

        }
        else {
            // Processing permissions information

            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }

        isFromFBRegister = FALSE;
    }
    else if(isFBParamRequest){
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            NSLog(@"fb result json %@",result);
            NSString * strFName = [result objectForKey:@"last_name"];
            NSString * strLName = [result objectForKey:@"first_name"];
            NSString * strGender = [result objectForKey:@"gender"];
            NSString * strFBLink = [result objectForKey:@"link"];
            NSDictionary * dicLocation = [result objectForKey:@"location"];
            NSString * strAddress = [dicLocation objectForKey:@"name"];
            NSString * strBirthday = [result objectForKey:@"birthday"];
            //birthday
            NSLog(@"str FB Birthday %@ and fb register ",strBirthday);
            if (objUser != nil) {
                objUser.strFName = strFName;
                objUser.strLName = strLName;
                objUser.strGender = strGender;
                objUser.strFbLink = strFBLink;
                objUser.strAddress = strAddress;
                objUser.strDob = strBirthday;
                //[self syncFBRegisterWith:objUser];
                NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
                [prefs setObject:objUser.strFbID forKey:@"fbid"];
                [prefs synchronize];
                [SVProgressHUD dismiss];
                [self showZDialog];
            }
        }
        else {
            // Processing permissions information

            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }
        isFBParamRequest = FALSE;
    }
    // This callback can be a result of getting the user's basic
    // information or getting the user's permissions.
    /*if (isFBLoginRequest) {
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            //NSString * strId = [result objectForKey:@"id"];
            NSLog(@"first result %@",result);

            // Get the profile image
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[result objectForKey:@"pic"]]]];


            // Resize, crop the image to make sure it is square and renders
            // well on Retina display
            float ratio;
            float delta;
            float px = 100; // Double the pixels of the UIImageView (to render on Retina)
            CGPoint offset;
            CGSize size = image.size;
            if (size.width > size.height) {
                ratio = px / size.width;
                delta = (ratio*size.width - ratio*size.height);
                offset = CGPointMake(delta/2, 0);
            } else {
                ratio = px / size.height;
                delta = (ratio*size.height - ratio*size.width);
                offset = CGPointMake(0, delta/2);
            }
            CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                         (ratio * size.width) + delta,
                                         (ratio * size.height) + delta);
            UIGraphicsBeginImageContext(CGSizeMake(px, px));
            UIRectClip(clipRect);
            [image drawInRect:clipRect];
            //UIImage *imgThumb = UIGraphicsGetImageFromCurrentImageContext();
            //UIGraphicsEndImageContext();
            //[profilePhotoImageView setImage:imgThumb];
            [self apiGraphUserPermissions];
        }
        else {
            // Processing permissions information

            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }
        isFBLoginRequest = FALSE;
    }

    if (isFBParamRequest) {
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            NSString * strName = [result objectForKey:@"name"];
            NSString * strEmail = [result objectForKey:@"email"];
            NSString * strFName = [result objectForKey:@"last_name"];
            NSString * strLName = [result objectForKey:@"first_name"];
            NSString * strGender = [result objectForKey:@"gender"];
            NSString * strId = [result objectForKey:@"uid"];

            NSString * accessToken = [[FBSession activeSession] accessToken];

            NSLog(@"str fb name %@ and email %@ and fname %@ and strLname %@ and gender %@ and token %@ and id %@",strName,strEmail,strFName,strLName,strGender,accessToken,strId);
            // Get the profile image
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[result objectForKey:@"pic"]]]];


            // Resize, crop the image to make sure it is square and renders
            // well on Retina display
            float ratio;
            float delta;
            float px = 100; // Double the pixels of the UIImageView (to render on Retina)
            CGPoint offset;
            CGSize size = image.size;
            if (size.width > size.height) {
                ratio = px / size.width;
                delta = (ratio*size.width - ratio*size.height);
                offset = CGPointMake(delta/2, 0);
            } else {
                ratio = px / size.height;
                delta = (ratio*size.height - ratio*size.width);
                offset = CGPointMake(0, delta/2);
            }
            CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                         (ratio * size.width) + delta,
                                         (ratio * size.height) + delta);
            UIGraphicsBeginImageContext(CGSizeMake(px, px));
            UIRectClip(clipRect);
            [image drawInRect:clipRect];
            //UIImage *imgThumb = UIGraphicsGetImageFromCurrentImageContext();
            //UIGraphicsEndImageContext();
            //[profilePhotoImageView setImage:imgThumb];
        }
        else {
            // Processing permissions information

            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }
        isFBParamRequest = FALSE;
    }
    else{
        if ([result objectForKey:@"name"]) {
            // If basic information callback, set the UI objects to
            // display this.
            NSString * strName = [result objectForKey:@"name"];
            NSString * strEmail = [result objectForKey:@"email"];
            NSString * strFName = [result objectForKey:@"last_name"];
            NSString * strLName = [result objectForKey:@"first_name"];
            NSString * strGender = [result objectForKey:@"gender"];
            NSString * strFBLink = [result objectForKey:@"link"];
            NSDictionary * dicLocation = [result objectForKey:@"location"];
            NSString * strAddress = [dicLocation objectForKey:@"name"];
            NSString * strBirthday = [result objectForKey:@"birthday"];
            NSString * strId = [result objectForKey:@"uid"];
            //birthday


            NSString * accessToken = [facebook.session accessToken];

            NSLog(@"str fb name %@ and email %@ and fname %@ and strLname %@ and gender %@ and token %@, fb link %@ and address %@ and birthday %@ and id %@",strName,strEmail,strFName,strLName,strGender,accessToken,strFBLink,strAddress,strBirthday,strId);
            if (petRegisterViewController != nil) {
                [petRegisterViewController hideRegister];
            }
            // Get the profile image
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[result objectForKey:@"pic"]]]];

            // Resize, crop the image to make sure it is square and renders
            // well on Retina display
            float ratio;
            float delta;
            float px = 100; // Double the pixels of the UIImageView (to render on Retina)
            CGPoint offset;
            CGSize size = image.size;
            if (size.width > size.height) {
                ratio = px / size.width;
                delta = (ratio*size.width - ratio*size.height);
                offset = CGPointMake(delta/2, 0);
            } else {
                ratio = px / size.height;
                delta = (ratio*size.height - ratio*size.width);
                offset = CGPointMake(0, delta/2);
            }
            CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                         (ratio * size.width) + delta,
                                         (ratio * size.height) + delta);
            UIGraphicsBeginImageContext(CGSizeMake(px, px));
            UIRectClip(clipRect);
            [image drawInRect:clipRect];
            //UIImage *imgThumb = UIGraphicsGetImageFromCurrentImageContext();
            //UIGraphicsEndImageContext();
            //[profilePhotoImageView setImage:imgThumb];
        }
        else {
            // Processing permissions information

            [self setUserPermissions:[[result objectForKey:@"data"] objectAtIndex:0]];
        }
        isFBParamRequest = FALSE;
    }*/
}

- (NSString *)convertServerDbFormat:(NSString *)fbDate{
    NSDateFormatter * fbDateFormat = [[NSDateFormatter alloc]init];
    [fbDateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate * date = [fbDateFormat dateFromString:fbDate];
    NSDateFormatter * serverDateFormat = [[NSDateFormatter alloc]init];
    [serverDateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString * strServerDate = @"";
    strServerDate = [serverDateFormat stringFromDate:date];
    return strServerDate;
}

- (void)syncFBRegisterWith:(ObjUser *)objU{
    NSLog(@"objU user fb id %@",objU.strFbID);
        [SVProgressHUD show];
        NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
        NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSString * strServerDob = [self convertServerDbFormat:objU.strDob];
    if ([Utility stringIsEmpty:objU.strAddress shouldCleanWhiteSpace:YES])
    objU.strAddress = @"";
    
    NSLog(@"formatted dob date %@",strServerDob);
        NSDictionary* params = @{@"user[first_name]":objU.strFName,@"user[last_name]":objU.strLName,@"user[email]":objU.strEmail,@"user[gender]":objU.strGender,@"user[username]":objU.strName,@"user[facebook]":objU.strFbLink,@"user[profile_image]":objU.strProfileImgLink,@"user[profile_image]":objU.strProfileImgLink,@"user[address]":objU.strAddress,@"user[dob]":strServerDob,@"user[password]":[self generatePasswordSHA1With:objU],@"user[password_confirmation]":[self generatePasswordSHA1With:objU],@"user[device_token]":self.strUdid,@"user[fb_id]":objU.strFbID,@"user[postal_code]":objUser.strPostalCode,@"user[country_name]":objUser.strCountry};
    //s
        NSLog(@"params %@ and link %@",params,FB_REGISTER_LINK);//user[password_confirmation]
        //[SVProgressHUD show];
        [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@",FB_REGISTER_LINK] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * dics = (NSDictionary *)json;
            int status = [[dics objectForKey:@"status"] intValue];
            NSString * strMsg = [dics objectForKey:@"message"];
            if (status == STATUS_ACTION_SUCCESS) {
                //[SVProgressHUD showSuccessWithStatus:@"Sent"];
                ObjUser * obj = [[ObjUser alloc]init];
                obj.strSession = [dics objectForKey:@"auth_token"];

                NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
                [prefs setObject:obj.strSession forKey:LOGIN_LINK];
                obj.strEmail = [dics objectForKey:@"email"];
                obj.strPassword = objUser.strPassword;
                obj.userId = [[dics objectForKey:@"user_id"] intValue];
                obj.strProfileImgLink = [dics objectForKey:@"profile_image"];
                obj.strName = objUser.strName;
                obj.isFBLogin = 1;
                NSLog(@"user session id %@",objUser.strSession);
                NSLog(@"auth_token %@",obj.strSession);
                [self.db updateUser:obj];

                NSLog(@"complete login!!");
                if (petRegisterViewController != nil) {
                    [petRegisterViewController hideRegister];
                }
                if (petLoginViewController != nil) {
                    [petLoginViewController hideLogin];
                }

                [self fbAutoShare:@"Now start using Pet Finder App in my phone."];
                
                NSDate *installDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"installDate"];
                if (!installDate) {
                    //no date is present
                    //this app has not run before
                    NSDate *todaysDate = [NSDate date];
                    [[NSUserDefaults standardUserDefaults]setObject:todaysDate forKey:@"installDate"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    //nothing more to do?
                }
                [self preSetReminders];
            }
            else if(status == 2){
                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strMsg];
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:strMsg delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil] show];
            }
            [SVProgressHUD dismiss];

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error %@",error);
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil] show];
        }];
}

- (void) preSetReminders{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"My default%d", [defaults boolForKey:@"notFirstTime"]);
    PetAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    if([defaults boolForKey:@"notFirstTime"] == 0) {
        NSDate *intallDate2 =(NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"installDate"];
        NSDate *intallDate = [NSDate dateToTomorrow:intallDate2];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        NSString * strDate2 = [dateFormatter stringFromDate:intallDate];
        ObjReminder *firstReminder = [[ObjReminder alloc] init];
        firstReminder.idx = 123456;
        firstReminder.strName = @"Annual Jab";
        firstReminder.isDone = 0;
        firstReminder.strRepeatType = @"Every Day";
        firstReminder.strReminderDate = @"At time of event";
        firstReminder.date_timetick = [intallDate timeIntervalSince1970];
        firstReminder.reminder_timetick = [intallDate timeIntervalSince1970];
        firstReminder.strDate = strDate2;
        firstReminder.isSwitch = 0;
        [delegate.db insertReminder:firstReminder];
        
        ObjReminder *secondReminder = [[ObjReminder alloc] init];
        secondReminder.idx = 123457;
        secondReminder.strName = @"Annual Vet Visit";
        secondReminder.isDone = 0;
        secondReminder.strRepeatType = @"Every Day";
        secondReminder.strReminderDate = @"At time of event";
        secondReminder.date_timetick = [intallDate timeIntervalSince1970];
        secondReminder.reminder_timetick = [intallDate timeIntervalSince1970];
        secondReminder.strDate = strDate2;
        secondReminder.isSwitch = 0;
        [delegate.db insertReminder:secondReminder];
        
        ObjReminder *thirdReminder = [[ObjReminder alloc] init];
        thirdReminder.idx = 123458;
        thirdReminder.strName = @"Flea & Tick";
        thirdReminder.isDone = 0;
        thirdReminder.strRepeatType = @"Every Day";
        thirdReminder.strReminderDate = @"At time of event";
        thirdReminder.date_timetick = [intallDate timeIntervalSince1970];
        thirdReminder.reminder_timetick = [intallDate timeIntervalSince1970];
        thirdReminder.strDate = strDate2;
        thirdReminder.isSwitch = 0;
        [delegate.db insertReminder:thirdReminder];
        
        [defaults setBool:YES forKey:@"notFirstTime"];
        
        [defaults synchronize];
    }
}

- (void)syncFBLoginWith:(ObjUser *)objU{
    NSLog(@"objU user fb id %@",objU.strFbID);
    [SVProgressHUD show];
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"user_login[fb_id]":objU.strFbID,@"user_login[password]":[self generatePasswordSHA1With:objU]};
    //s
    NSLog(@"params %@",params);
    //user[password_confirmation]
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",LOGIN_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            ObjUser * obj = [[ObjUser alloc]init];
            obj.strSession = [dics objectForKey:@"auth_token"];

            NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:obj.strSession forKey:LOGIN_LINK];
            obj.strEmail = [dics objectForKey:@"email"];
            obj.strPassword = objUser.strPassword;
            obj.userId = [[dics objectForKey:@"user_id"] intValue];

            obj.strName = objUser.strName;
            obj.isFBLogin = 1;
            NSLog(@"user session id %@",objUser.strSession);
            self.isNowInLogin = FALSE;
            ObjUser * objUser2 = [self.db getUserObj];
            obj.strProfileImgLink = objUser2.strProfileImgLink;
            [self.db updateUser:obj];
            [SVProgressHUD dismiss];
            NSLog(@"complete login!!");
            if (petLoginViewController != nil) {
                [petLoginViewController hideLogin];
            }
            
            NSDate *installDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"installDate"];
            if (!installDate) {
                //no date is present
                //this app has not run before
                NSDate *todaysDate = [NSDate date];
                [[NSUserDefaults standardUserDefaults]setObject:todaysDate forKey:@"installDate"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                //nothing more to do?
            }
            [self preSetReminders];
        }
        else if(status == 2){
            NSLog(@"incompleted login!!");
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
            [self apiGraphUserPermissions];
        }
        else if(status == 3){
            NSLog(@"email confirm frist!!");
            //[self textValidateAlertShow:strErrorMsg];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            [Utility showAlert:APP_TITLE message:strMsg];
            //[self apiGraphUserPermissions];
        }
        [SVProgressHUD dismiss];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

// Handle the publish feed call back
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {

    NSArray *permis = [NSArray arrayWithObjects:@"publish_actions",@"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permis
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];

}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    NSLog(@"sessionStateChanged");
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            /*UIViewController *topViewController = [self.navController topViewController];
             if ([[topViewController modalViewController] isKindOfClass:[SCLoginViewController class]]) {
             [topViewController dismissModalViewControllerAnimated:YES];
             }*/
            // Initiate a Facebook instance and properties
            if (nil == self.facebook) {
                self.facebook = [[Facebook alloc]
                                 initWithAppId:FBSession.activeSession.appID
                                 andDelegate:nil];

                // Store the Facebook session information
                NSLog(@"accesstoken %@ expirationDate %@ appid %@",FBSession.activeSession.accessToken,FBSession.activeSession.expirationDate,FBSession.activeSession.appID);
                self.facebook.accessToken = FBSession.activeSession.accessToken;
                self.facebook.expirationDate = FBSession.activeSession.expirationDate;
            }
            NSLog(@"Great Back!!!<<<<>>>");

            //[self showDialog];

            // FBSample logic
            // Pre-fetch and cache the friends for the friend picker as soon as possible to improve
            // responsiveness when the user tags their friends.
            FBCacheDescriptor *cacheDescriptor = [FBFriendPickerViewController cacheDescriptor];
            [cacheDescriptor prefetchAndCacheForSession:session];
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];

            [FBSession.activeSession closeAndClearTokenInformation];

            //[self showLoginView];
            break;
        default:
            break;
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification
                                                        object:session];

    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }


}

/**
 * A function for parsing URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

- (void) dialogCompleteWithUrl:(NSURL *)url {

    NSLog(@"dialogCompleteWithUrl");
    NSDictionary *params = [self parseURLParams:[url query]];
    NSString *msg = [NSString stringWithFormat:
                     @"Posted story, id: %@",
                     [params valueForKey:@"post_id"]];
    NSLog(@"%@", msg);
    // Show the result in an alert

    //    if(![msg isEqualToString:@""] || msg!=nil || (NSNull *)msg != [NSNull null]){
    //    }
    /*   [[[UIAlertView alloc] initWithTitle:@"Result"
     message:msg
     delegate:nil
     cancelButtonTitle:@"OK!"
     otherButtonTitles:nil]
     show];*/

}

- (void)fbSessionKill{
    if(self.facebook != nil) [self.facebook.session closeAndClearTokenInformation];
}

- (void) shareAfterRegister:(NSString *)strDes{

    SBJson4Writer *jsonWriter = [SBJson4Writer new];
    NSDictionary *propertyvalue = [NSDictionary dictionaryWithObjectsAndKeys:@"Pet App", @"text", @"http://www.petfinder.com", @"href", nil];
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:propertyvalue, @"Via", nil];
    NSString *finalactions = [jsonWriter stringWithObject:properties];

    NSMutableDictionary *params;
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              @"Pet", @"name", @"" ,@"caption",
              strDes,@"description",
              @"http://www.petsmagazine.com.sg/" , @"link",
              @"" , @"picture",
              finalactions,@"properties",
              nil];
    // Put together the dialog parameters

    // Invoke the dialog
    //[self.facebook dialog:@"feed" andParams:params andDelegate:self];
    //[self.facebook re]
    //[self.facebook di]
    //[self.facebook dialog:@"post" andParams:params andDelegate:self];
    [self.facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:nil];
}

- (void) showDialogWith:(ObjLostFound *)objLF{
    NSLog(@"show dialog for obj lost and found!!");
    NSLog(@"objlf name %@ and obj Caption %@",objLF.strPetName,objLF.strPetName);
    SBJson4Writer *jsonWriter = [SBJson4Writer new];
    NSDictionary *propertyvalue = [NSDictionary dictionaryWithObjectsAndKeys:@"Pet App", @"text", @"http://www.petfinder.com", @"href", nil];
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:propertyvalue, @"Via", nil];
    NSString *finalactions = [jsonWriter stringWithObject:properties];

    NSMutableDictionary *params;
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              objLF.strPetName, @"name", @"" ,@"caption",
             [NSString stringWithFormat:@"%@, Reward: %0.2f",objLF.strDescription,objLF.reward] ,@"description",
              @"http://www.petsmagazine.com.sg/" , @"link",
              objLF.strPetImgLink , @"picture",
              finalactions,@"properties",
              nil];
    // Put together the dialog parameters

    // Invoke the dialog
    [self.facebook dialog:@"feed" andParams:params andDelegate:self];
}

- (NSString *)generatePasswordSHA1With:(ObjUser *)objU{
    if (objU != nil) {
        NSString * strEncoded = [self encodePasswordWithSHA1:[NSString stringWithFormat:@"%@%@",objU.strFbID,objU.strEmail]];
        return strEncoded;
    }
    return nil;
}

- (NSString *)encodePasswordWithSHA1:(NSString *)str{
    NSString *hashkey = str;
    // PHP uses ASCII encoding, not UTF
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];

    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);

    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4 bytes
    NSString *hash = [out description];
    hash = [hash stringByReplacingOccurrencesOfString:@"Not specified" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];

    return hash;
}

- (void)fbDialogLogin:(NSString *)token expirationDate:(NSDate *)expirationDate{
    NSLog(@"fb token %@",token);
}

/*
 * ------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * ------------------------------------------------------------------------------------------
 */
/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */



    NSString *deviceToken = [[devToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @">" withString: @""] ;
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @"Not specified" withString: @""];
    
    NSLog(@"Device Token : %@", deviceToken);

    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strKey = [prefs objectForKey:GENR_KEY_LINK];
    if ([strKey isEqualToString:@""] || strKey == nil) {
        NSLog(@"Device Token : %@", deviceToken);

        [self showLoadingScreen];
        generateKeyRequest = [[SOAPRequest alloc] initWithOwner:self];
        generateKeyRequest.processId = 1;
        [generateKeyRequest syncGenerateKeyWithToken: deviceToken];
    }
    //#endif
}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

    //#if !TARGET_IPHONE_SIMULATOR

	NSLog(@"Error in registration. Error: %@", error);

    //#endif
}

/**
 * Remote Notification Received while application was open.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {

#if !TARGET_IPHONE_SIMULATOR

	NSLog(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];

	NSString *alert = [apsInfo objectForKey:@"alert"];
	NSLog(@"Received Push Alert: %@", alert);

	NSString *sound = [apsInfo objectForKey:@"sound"];
	NSLog(@"Received Push Sound: %@", sound);
	//AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

	NSString *badge = [apsInfo objectForKey:@"badge"];
	NSLog(@"Received Push Badge: %@", badge);

	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];

#endif
}

/*
 * ------------------------------------------------------------------------------------------
 *  END APNS CODE
 * ------------------------------------------------------------------------------------------
 */

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Err message: %@", [[error userInfo] objectForKey:@"error_msg"]);
    NSLog(@"Err code: %d", [error code]);
}


/**
 *	zDialog
 */
- (void)showZDialog{
    NSLog(@"show z dialog!!");
    if( zDialogCustomView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ZDialogView" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[ZDialogView class]]){
				zDialogCustomView = (ZDialogView *) currentObject;

				[current_View_Controller.view addSubview: zDialogCustomView];
				break;
			}
		}
	}
    [self showPopOverlay];
    zDialogCustomView.hidden = FALSE;
    zDialogCustomView.owner = self;
    zDialogCustomView.frame = CGRectMake(20, 480, zDialogCustomView.frame.size.width, zDialogCustomView.frame.size.height);
    [current_View_Controller.view bringSubviewToFront: zDialogCustomView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    zDialogCustomView.frame = CGRectMake(20, 80, zDialogCustomView.frame.size.width, zDialogCustomView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onCancel{
    zDialogCustomView.frame = CGRectMake(20, 80, zDialogCustomView.frame.size.width, zDialogCustomView.frame.size.height);
    [current_View_Controller.view bringSubviewToFront: zDialogCustomView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration: 0.4];
    [UIView setAnimationDelegate: self];
    zDialogCustomView.frame = CGRectMake(20, 480, zDialogCustomView.frame.size.width, zDialogCustomView.frame.size.height);
    [UIView commitAnimations];
    [self performSelector:(@selector(closezDialogCustomView)) withObject:nil afterDelay:0.4];
    //smqView.hidden = TRUE;
}

-(void) closezDialogCustomView{
    zDialogCustomView.hidden = TRUE;
    [self hidePopOverlay];
}

- (void)showPopOverlay{
    if( popUpOverlayView == nil ){
		NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PopUpOverlay" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[PopUpOverlay class]]){
				popUpOverlayView = (PopUpOverlay *) currentObject;
				[popUpOverlayView popupOverlayViewDidLoad];
				[current_View_Controller.view addSubview: popUpOverlayView];
				break;
			}
		}
	}
    popUpOverlayView.hidden = FALSE;
}

- (void)hidePopOverlay{
    popUpOverlayView.hidden = TRUE;
}

-(void)onCountry:(id)sender{
    [self.menu setTitle:@"Select Country"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.uiPickerView.tag = 1;
    self.uiPickerView.delegate = self;
    [self.uiPickerView reloadAllComponents];
    [self.uiPickerView selectRow:self.selectedCountry inComponent:0 animated:YES];
    [self.menu addSubview:self.uiPickerView];
    [self.menu showInView:current_View_Controller.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if (pickerView.tag == 1) {
        NSString *strName = [self.arrCountry objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);

        return strName;

    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        return [self.arrCountry count];
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 1) {
        //PutetDelegate * delegate = [[UIApplication sharedApplication] delegate];
        self.selectedCountry = row;
        //ObjectCity * objCity = [arrCity objectAtIndex:selectedCity];
        ///[self.btnDropDown setTitle:[NSString stringWithFormat:@"Building %d",row+1] forState:normal];
    }

    /*else if(pickerView.tag == 1){
     intFromIndex = row;
     }
     else if(pickerView.tag == 2){
     intToIndex = row;
     }
     else if(pickerView.tag == 3){
     intTimeIndex = row;
     }*/

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];

    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }

    else if (buttonIndex == 1) {


        if (self.uiPickerView.tag == 1){
            //Date picker click
            //ObjectCity * objTwn = [arrCity objectAtIndex:selectedCity];
            NSString * strName = [self.arrCountry objectAtIndex:self.selectedCountry];
            strCountry = strName;
            if (zDialogCustomView != nil) {
                [zDialogCustomView setCountryButtonTitle:strName];
            }

        }
    }
}

- (void) onOk:(NSString *)strPostalCode{
    strCountry = [strCountry stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([strCountry isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG10
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([strCountry isEqualToString:@"Singapore"] && [strPostalCode isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: APP_TITLE
                              message: ERRMSG11
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        if (objUser != nil) {
            if ([Utility stringIsEmpty:strPostalCode shouldCleanWhiteSpace:YES]) {
                strPostalCode = @"";
            }
            else if ([Utility stringIsEmpty:objUser.strAddress shouldCleanWhiteSpace:YES]) {
                objUser.strAddress = @"";
            }
            else if ([Utility stringIsEmpty:objUser.strDescription shouldCleanWhiteSpace:YES]) {
                objUser.strDescription = @"";
            }
            else if ([Utility stringIsEmpty:objUser.strDob shouldCleanWhiteSpace:YES]) {
                objUser.strDob = @"";
            }
            else if ([Utility stringIsEmpty:objUser.strFName shouldCleanWhiteSpace:YES]) {
                objUser.strFName = @"";
            }
            objUser.strCountry = strCountry;
            objUser.strPostalCode = strPostalCode;
            [self syncFBRegisterWith:objUser];
        }
    }
}

- (void) onCountry{
    [self onCountry:nil];
}

- (void) syncUserLocation:(float)late andlong:(float)lng{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];

    ObjUser * user = [self.db getUserObj];
    if (user != nil) {
        NSDictionary* params = @{@"user[latitude]":[NSString stringWithFormat:@"%f",late],@"user[longitude]":[NSString stringWithFormat:@"%f",lng]};
        [[petAPIClient sharedClient] putPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",PROFILE_EDIT_LINK,user.userId,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
            NSLog(@"successfully return!!! %@",json);
            NSDictionary * dics = (NSDictionary *)json;
            int status = [[dics objectForKey:@"status"] intValue];
            NSString * strMsg = [dics objectForKey:@"message"];
            if (status == STATUS_ACTION_SUCCESS) {
                //[SVProgressHUD showSuccessWithStatus:strMsg];

            }
            else if(status == STATUS_ACTION_FAILED){

                //[self textValidateAlertShow:strErrorMsg];
                [SVProgressHUD showErrorWithStatus:strMsg];
            }
            [SVProgressHUD dismiss];

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error %@",error);
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
        }];
    }
}

- (void) startLocationCounter{
    if (myLocationUpdateTimer == nil) {
         myLocationUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:(60*5) target:self selector:@selector(updateLocation) userInfo:nil repeats:YES];
    }
}

- (void) updateLocation{
    if (self.currentLat != .0 && self.currentLng != .0) {
        [self syncUserLocation:self.currentLat andlong:self.currentLng];
    }
}

- (void) deleteLocalNotification:(ObjReminder *)obj{
    NSString *myIDToCancel = [NSString stringWithFormat:@"%d",obj.idx];
    UILocalNotification *notificationToCancel=nil;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        NSArray * commands = nil;
        NSString * notiId = @"";
        NSString * notiName = @"";
        if( [[aNotif.userInfo objectForKey:@"ID"] rangeOfString:@","].location != NSNotFound ){
            
            commands = [[aNotif.userInfo objectForKey:@"ID"] componentsSeparatedByString:@","];
            notiId = [commands objectAtIndex:0];
            notiName = [commands objectAtIndex:1];
        }
        NSLog(@"notid id %@ and obj id %@",notiId,myIDToCancel);
        if([notiId isEqualToString:myIDToCancel]) {
            notificationToCancel=aNotif;
            break;
        }
        
    }
    if (notificationToCancel != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
    }
}
/**
 *	endzDialog
 */

// ************************ location Manager *******************
- (void)locationManager {

    geocoder = [[CLGeocoder alloc] init];

	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
	[locationManager setDelegate:self];
    NSLog(@"here to load place");
	[locationManager startUpdatingLocation];
	//return locationManager;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {

    CLLocation *currentLocation = newLocation;


    currentGeo = [newLocation coordinate];
    self.currentLat = currentGeo.latitude;
    self.currentLng = currentGeo.longitude;
    NSLog(@"lat: %+.6f, lng: %+.6f ", currentGeo.latitude, currentGeo.longitude);
    strCurrentPlace=@"";
    // Reverse Geocoding
    /*
     NSLog(@"Resolving the Address");
     [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
     NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
     if (error == nil && [placemarks count] > 0) {
     placemark = [placemarks lastObject];
     strCurrentPlace = [NSString stringWithFormat:@"%@, %@, %@",
     placemark.locality,
     placemark.administrativeArea,
     placemark.country];
     NSLog(@"lat: %+.6f, lng: %+.6f add:%@", currentGeo.latitude, currentGeo.longitude,strCurrentPlace);
     //            NSLog(@"%@",placemark.subThoroughfare);
     //            NSLog(@"%@",placemark.thoroughfare);
     //            NSLog(@"%@",placemark.postalCode);
     //            NSLog(@"%@",placemark.locality);
     //            NSLog(@"%@",placemark.administrativeArea);
     //            NSLog(@"%@",placemark.country);
     } else {
     NSLog(@"%@", error.debugDescription);
     }
     } ];
     */


}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"location error %@",error);
}
@end
