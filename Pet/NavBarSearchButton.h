//
//  NavBarButton.h
//  PropertyGuru
//
//  Created by Tonytoons on 3/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NavBarSearchButton : UIButton {
	BOOL landscape;
    
}

-(id)init;

@property BOOL landscape;

@end
