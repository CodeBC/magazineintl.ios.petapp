//
//  AnimatePetsViewController.h
//  Pet
//
//  Created by Zayar on 6/14/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjAlbumPhoto.h"

@interface AnimatePetsViewController : PetBasedViewController
{
}
- (IBAction) leftBarButton:(UIBarButtonItem *)sender;
- (IBAction) rightBarButton:(id)sender;
@end
