//
//  DetailLostFoundViewController.m
//  Pet
//
//  Created by Zayar on 6/11/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DetailFoundViewController.h"
#import <Twitter/Twitter.h>
#import "PetAppDelegate.h"
#import "StringTable.h"
#import "NavBarButton1.h"
#import "NavBarButton6.h"
#import "BrowseLFTableCell.h"
#import "ObjLostFound.h"
#import <QuartzCore/QuartzCore.h>
#import "ObjPet.h"
#import "UIImageView+AFNetworking.h"
#import "petAPIClient.h"
#import "TSActionSheet.h"
#import "ProfileMapViewController.h"
#import "Utility.h"
@interface DetailFoundViewController ()
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIImageView * imgPetView;
    TKLabelTextViewCell * lblDescripCell;
    TKLabelFieldCell * lblGenderCell;
    TKLabelFieldCell * lblBreedCell;
    TKLabelFieldCell * lblLastSeenCell;
    TKLabelFieldCell * lblRewardCell;
    TKButtonCell * btnLocationCell;
    UITextView * txtParentPassword;
    IBOutlet UITableView * tbl;
    UILabel * lblName;
    BOOL isFromMap;
    IBOutlet UIButton *btnIFoundThisPet;
    IBOutlet UIButton *btnReport;
}
@property (nonatomic,strong) NSArray *cells;
@property (strong, nonatomic) NSString *imageString;
@property (strong, nonatomic) NSString *urlString;
@end

@implementation DetailFoundViewController
@synthesize objSelectedLf;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadTheRequiredCell{
    /*self.btnPet = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
     UILabel * lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
     lblbtnCap2.text = @"Select Pet";
     lblbtnCap2.textColor = [UIColor blackColor];
     lblbtnCap2.textAlignment = UITextAlignmentLeft;
     lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
     lblbtnCap2.backgroundColor = [UIColor clearColor];
     lblbtnCap2.tag = 1;
     self.btnPet.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     self.btnPet.selectionStyle = UITableViewCellSelectionStyleNone;
     [self.btnPet addSubview:lblbtnCap2];
     
     lastSeenTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
     lastSeenTextCell.label.text = @"Last Seen:";
     lastSeenTextCell.label.textAlignment = UITextAlignmentLeft;
     lastSeenTextCell.label.font = [UIFont boldSystemFontOfSize:13];
     lastSeenTextCell.label.textColor = [UIColor blackColor];
     
     lastSeenTextCell.label.frame = CGRectMake(30 + lastSeenTextCell.label.frame.origin.x, lastSeenTextCell.label.frame.origin.y, cell3.frame.size.width+10, lastSeenTextCell.frame.size.height);
     lastSeenTextCell.field.text = @"";
     lastSeenTextCell.field.tag = 0;
     lastSeenTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
     
     areaTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
     areaTextCell.label.text = @"Area:";
     areaTextCell.label.textAlignment = UITextAlignmentLeft;
     areaTextCell.label.font = [UIFont boldSystemFontOfSize:13];
     areaTextCell.label.textColor = [UIColor blackColor];
     
     areaTextCell.label.frame = CGRectMake(30 + areaTextCell.label.frame.origin.x, areaTextCell.label.frame.origin.y, areaTextCell.frame.size.width+10, areaTextCell.frame.size.height);
     areaTextCell.field.text = @"";
     areaTextCell.field.tag = 0;
     areaTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
     
     rewardTextCell = [[TKLabelTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
     rewardTextCell.label.text = @"Reward:";
     rewardTextCell.label.textAlignment = UITextAlignmentLeft;
     rewardTextCell.label.font = [UIFont boldSystemFontOfSize:13];
     rewardTextCell.label.textColor = [UIColor blackColor];
     
     rewardTextCell.label.frame = CGRectMake(30 + rewardTextCell.label.frame.origin.x, rewardTextCell.label.frame.origin.y, rewardTextCell.frame.size.width+10, rewardTextCell.frame.size.height);
     rewardTextCell.field.text = @"";
     rewardTextCell.field.tag = 0;
     rewardTextCell.selectionStyle = UITableViewCellSelectionStyleNone;
     
     cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
     cell2.label.text = @"Description";
     cell2.label.textColor = [UIColor blackColor];
     cell2.label.font = [UIFont boldSystemFontOfSize:13];
     cell2.label.textAlignment = UITextAlignmentLeft;
     //cell2.selectionStyle = UI;
     cell2.textView.text = @"";
     cell2.textView.delegate = self;
     cell2.selectionStyle = UITableViewCellSelectionStyleNone;
     self.cells = @[self.btnPet,areaTextCell,lastSeenTextCell,rewardTextCell,cell2];
    
    self.btnBreed = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap2.text = @"Select Breed";
    lblbtnCap2.textColor = [UIColor blackColor];
    lblbtnCap2.textAlignment = UITextAlignmentLeft;
    lblbtnCap2.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap2.backgroundColor = [UIColor clearColor];
    lblbtnCap2.tag = 1;
    self.btnBreed.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnBreed.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnBreed addSubview:lblbtnCap2];
    
    self.btnGender = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap3.text = @"Select Gender";
    lblbtnCap3.textColor = [UIColor blackColor];
    lblbtnCap3.textAlignment = UITextAlignmentLeft;
    lblbtnCap3.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap3.backgroundColor = [UIColor clearColor];
    lblbtnCap3.tag = 1;
    self.btnGender.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnGender.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnGender addSubview:lblbtnCap3];
    
    self.btnArea = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap4.text = @"Select Area";
    lblbtnCap4.textColor = [UIColor blackColor];
    lblbtnCap4.textAlignment = UITextAlignmentLeft;
    lblbtnCap4.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap4.backgroundColor = [UIColor clearColor];
    lblbtnCap4.tag = 1;
    self.btnArea.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnArea.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnArea addSubview:lblbtnCap4];
    
    self.btnDate = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"button"];
    UILabel * lblbtnCap5 = [[UILabel alloc]initWithFrame:CGRectMake(20, 7, 200, 30)];
    lblbtnCap5.text = @"Lastseen Date";
    lblbtnCap5.textColor = [UIColor blackColor];
    lblbtnCap5.textAlignment = UITextAlignmentLeft;
    lblbtnCap5.font = [UIFont boldSystemFontOfSize:13];
    lblbtnCap5.backgroundColor = [UIColor clearColor];
    lblbtnCap5.tag = 1;
    self.btnDate.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.btnDate.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.btnDate addSubview:lblbtnCap5];
    
    cell2 = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	cell2.label.text = @"Description";
    cell2.label.textColor = [UIColor blackColor];
    cell2.label.font = [UIFont boldSystemFontOfSize:13];
    cell2.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	cell2.textView.text = @"";
    cell2.textView.delegate = self;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITableViewCell *cell4 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	UILabel * lblCap=[[UILabel alloc]initWithFrame:CGRectMake(20, 7, 70, 60)];
    lblCap.textColor = [UIColor blackColor];
    lblCap.backgroundColor = [UIColor clearColor];
    lblCap.text=@"Take Photo:";
    lblCap.font = [UIFont boldSystemFontOfSize:14];
    lblCap.numberOfLines = 0;
    imgProfile = [[UIImageView alloc]initWithFrame:CGRectMake(210, 7, 90, 90)];
    imgProfile.backgroundColor = [UIColor darkGrayColor];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell4 addSubview:lblCap];
    [cell4 addSubview:imgProfile];
    
    self.cells = @[self.btnBreed,self.btnGender,cell2,self.btnArea,self.btnDate,cell4];*/

    lblDescripCell = [[TKLabelTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblDescripCell.label.text = @"Description:";
    lblDescripCell.label.textColor = [UIColor blackColor];
    lblDescripCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblDescripCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
    lblDescripCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblGenderCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblGenderCell.label.text = @"Gender:";
    lblGenderCell.label.textColor = [UIColor blackColor];
    lblGenderCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblGenderCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblGenderCell.field.text = @"";
    lblGenderCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblBreedCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblBreedCell.label.text = @"Breed:";
    lblBreedCell.label.textColor = [UIColor blackColor];
    lblBreedCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblBreedCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblBreedCell.field.text = @"";
    lblBreedCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblLastSeenCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblLastSeenCell.label.text = @"Last Seen:";
    lblLastSeenCell.label.textColor = [UIColor blackColor];
    lblLastSeenCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblLastSeenCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblLastSeenCell.field.text = @"";
    lblLastSeenCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    lblRewardCell = [[TKLabelFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	lblRewardCell.label.text = @"Reward:";
    lblRewardCell.label.textColor = [UIColor blackColor];
    lblRewardCell.label.font = [UIFont boldSystemFontOfSize:13];
    lblRewardCell.label.textAlignment = UITextAlignmentLeft;
    //cell2.selectionStyle = UI;
	lblRewardCell.field.text = @"";
    lblRewardCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    btnLocationCell = [[TKButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    btnLocationCell.textLabel.text = @"Show Location";
    self.cells = @[lblDescripCell,lblGenderCell,lblBreedCell,lblLastSeenCell,lblRewardCell,btnLocationCell];
    //tbl.frame = CGRectMake(tbl.frame.origin.x, tbl.frame.origin.y, 320, [self.cells count]*44);
    
    [Utility makeCornerRadius:btnIFoundThisPet andRadius:5];
    [Utility makeCornerRadius:btnReport andRadius:5];
}

- (void) viewWillAppear:(BOOL)animated{
    [self loadTheViewWith:objSelectedLf];
}

-(void) loadTheViewWith:(ObjLostFound *)obj{
    lblDescripCell.textView.text = obj.strDescription;
    lblBreedCell.field.text = [NSString stringWithFormat:@"%d",obj.intBreedType];
    lblGenderCell.field.text = obj.strPetGender;
    //NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    //dateFormatter.dateStyle = NSDat;
    lblDescripCell.textView.editable = FALSE;
    NSLog(@"int lastseenTimestamp %d",obj.intLastSeenTimestamp);
    
    if (obj.intLastSeenTimestamp == 0) {
        lblLastSeenCell.field.text = @"-";
    }
    else{
        NSDate * dobDate = [NSDate dateWithTimeIntervalSince1970:obj.intLastSeenTimestamp];
        NSLog(@"last seen date %@",obj.strLastSeenDate);
        NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"dd MMM yyyy"];
        lblLastSeenCell.field.text = [dateFormatter1 stringFromDate:dobDate];
    }

    lblRewardCell.field.text = [NSString stringWithFormat:@"%.0f",obj.reward];
    [imgPetView setImageWithURL:[NSURL URLWithString:obj.strPetImgLink]placeholderImage:nil];
    //self.navigationItem.title = obj.strPetName;
    if (lblName == nil) {
        lblName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    }
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName.font = [UIFont fontWithName:strValueFontName size:19];
    lblName.text=  obj.strPetName;
    lblName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName;
}

- (IBAction)onThisIsMyPet:(id)sender{
    //[self syncPushMessagesWith:@"This is my pet!" andSenderId:objSelectedLf.intOwnerId];
    [self showDialog];
}

- (IBAction)onIFoundThisPet:(id)sender{
    //[self syncPushMessagesWith:@"I found this pet!" andSenderId:objSelectedLf.intOwnerId];
    [self showDialog];
}

- (void)showDialog{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"                        \n " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
    if (txtParentPassword == nil) {
        txtParentPassword = [[UITextView alloc]initWithFrame:CGRectMake(30, 45, 215, 40)];
    }
    txtParentPassword.text = @"";
    txtParentPassword.textAlignment = UITextAlignmentLeft;
    alert.tag = 1;
    [txtParentPassword becomeFirstResponder];
    txtParentPassword.backgroundColor = [UIColor whiteColor];
    
    [alert addSubview:txtParentPassword];
    alert.frame =  CGRectMake(10, 100, 310, 360);
    alert.delegate = self;
    [alert show];
}

- (void)syncPushMessagesWith:(NSString *)strMessage andSenderId:(int)intSenderId{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"msg[receiver_id]":[NSString stringWithFormat:@"%d", intSenderId],@"msg[message]":strMessage};
    [SVProgressHUD show];
    NSLog(@"params %@",params);
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",MESSAGE_PUSH_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            /*UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Info"
                                      message:strMsg
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];*/
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (IBAction)onAbuse:(id)sender{
    [self syncLostPetAduse:objSelectedLf];
}

- (void)syncLostPetAduse:(ObjLostFound *)obj{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"lost_id":[NSString stringWithFormat:@"%d", obj.idx]};
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",REPORT_ABUSE_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:@"Sent"];
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Info"
                                      message:strMsg
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            [SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return self.cells[indexPath.row];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 5) {
        [self onLocation:objSelectedLf.lat andLong:objSelectedLf.lon];
    }
}

- (void)onLocation:(float)lat andLong:(float)lon{
    ProfileMapViewController * mapViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"profileMap"];
    mapViewController.owner = self;
    mapViewController.seletedLat = lat;
    mapViewController.seletedLon = lon;
    mapViewController.isEditable = FALSE;
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:mapViewController];
    nav.navigationBar.tintColor = [UIColor blackColor];
    
    [self.navigationController presentModalViewController:nav animated:YES];
    NSLog(@"here is on map click");
    isFromMap = TRUE;
}

- (void) finishWithCoordinate:(float)late andlong:(float)lng{
    NSLog(@"home town location %f and long %f",late,lng);
    isFromMap = TRUE;
}

- (void) closeMap{
    isFromMap = TRUE;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    }*/
    
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];
    
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    
    NavBarButton6 *btn = [[NavBarButton6 alloc] init];
	[btn addTarget:self action:@selector(showActionSheet:forEvent:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * shareButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.rightBarButtonItem = shareButton;
    [self loadTheRequiredCell];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([tbl respondsToSelector:@selector(separatorInset)]) {
            [tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

-(void)showActionSheet:(id)sender forEvent:(UIEvent*)event
{
    TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@"Select Source"];
    //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
    [actionSheet addButtonWithTitle:@"Facebook" block:^{
        NSLog(@"Facebook");
        //[self showCamera];
        [self onFacebook];
    }];
    [actionSheet addButtonWithTitle:@"Twitter" block:^{
        NSLog(@"Twitter");
        [self onTwitter];
    }];
    //[actionSheet cancelButtonWithTitle:@"Cancel" block:nil];
    actionSheet.cornerRadius = 5;
    
    [actionSheet showWithTouch:event];
}

-(void)onFacebook{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate fbShare:objSelectedLf];
}

-(void)onTwitter{
    self.imageString = objSelectedLf.strPetImgLink;
    self.urlString = objSelectedLf.strPetImgLink;
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"Share from Pet App!"];
        
        if (self.imageString)
        {
            [tweetSheet addImage:[UIImage imageNamed:self.imageString]];
        }
        
        if (self.urlString)
        {
            [tweetSheet addURL:[NSURL URLWithString:self.urlString]];
        }
        
	    [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
        if(buttonIndex == 1) {
            //NSLog(@"here is custom internal");
            //[self showAndLoadCustomWebView:strMessageURL];
            //ObjUser * objUser = [self.db getUserObj];
            //objUser.strParentPassword = txtParentPassword.text;
            //[self syncFreezeWith:objUser];
            [self syncPushMessagesWith:txtParentPassword.text andSenderId:objSelectedLf.intOwnerId];
		}else if (buttonIndex == 0){
            
            NSLog(@"button index tag1");
        }
	}
}

- (void) syncDetailPet:(int)idx{
    
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
