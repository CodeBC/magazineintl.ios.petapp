//
//  ReporLostViewController.h
//  Pet
//
//  Created by Zayar on 5/1/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReporLostViewController : PetBasedViewController
-(IBAction)onPet:(id)sender;
- (IBAction)onAdd:(id)sender;
@end
