//
//  ObjAlbumPhoto.h
//  Pet
//
//  Created by Zayar on 6/13/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjAlbumPhoto : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSString * strImgURL;
@property (nonatomic, strong) NSString * strThumbImgURL;
@property (nonatomic, strong) UIImage * imgPetView;
@property (nonatomic, strong) UIImage * imgPetThumbView;
@end
