//
//  ObjType.h
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjPetType : NSObject
@property int idx;
@property (nonatomic, strong) NSString * strName;
@property (nonatomic, strong) NSMutableArray * arrBreed;
@end
