//
//  VideoTableCell.m
//  Pet
//
//  Created by Zayar on 5/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DealTableCell.h"
#import "ObjDeal.h"
#import "UIImageView+AFNetworking.h"

@implementation DealTableCell
@synthesize imgView,lblName,lblPrice;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadTheCellWith:(ObjDeal *)obj{
    NSLog(@"deal image link %@",obj.strImgLink);
    NSURL * url = [NSURL URLWithString:obj.strImgLink];
    [self.imgView setImageWithURL:url placeholderImage:nil];
    self.lblName.text = obj.strName;
    self.lblPrice.text = [NSString stringWithFormat:@"($%@)",obj.strPrice];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
