//
//  PetViewController.m
//  Pet
//
//  Created by Zayar on 4/18/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "PetViewController.h"
#import "PetLoginViewController.h"
#import "ADSlidingViewController.h"
#import "PetLoginViewController.h"
#import "PetAppDelegate.h"
#import "SideMenuViewController.h"
#import "MFSideMenuManager.h"
#import "StringTable.h"
#import "petAPIClient.h"
@interface PetViewController ()
{
    IBOutlet UIImageView * imgBgView;
}

@end

@implementation PetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIViewController *mainvc = [[UIStoryboard storyboardWithName:@"SlidingViews" bundle:nil] instantiateViewControllerWithIdentifier:@"mainViewController"];
    [[mainvc view] addGestureRecognizer:[self panGesture]];
    [self setMainViewController:mainvc];
	[self setLeftViewController:[[UIStoryboard storyboardWithName:@"SlidingViews" bundle:nil] instantiateViewControllerWithIdentifier:@"leftViewController"]];
    //[self.slidingViewController setLeftViewAnchorWidth:200];
    //[self.slidingViewController setLeftViewAnchorWidthType:ADAnchorWidthTypePeek];
	//[self setRightViewController:[[UIStoryboard storyboardWithName:@"SlidingViews" bundle:nil] instantiateViewControllerWithIdentifier:@"rightViewController"]];
	//[self setShowTopViewShadow:YES];
	//[self setRightMainAnchorType:ADMainAnchorTypeResize];
    
    //[self.slidingViewController setLeftViewAnchorWidth:200];
}

- (void)viewWillAppear:(BOOL)animated{
    /*PetLoginViewController* newpost = [[PetLoginViewController alloc] initWithNibName:@"PetLoginViewController" bundle:nil];
    //newpost.delegate = self;
    //UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:newpost];
    
    [self presentModalViewController:newpost animated:YES];
    NSLog(@"view will load");*/
    NSLog(@"view will appear Main.....");
    /*PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    if (delegate.isFromLogin) {
        
        [slidingViewController anchorTopViewTo:ADAnchorSideCenter];
    }*/
    //NSLog(@"is from login %d",delegate.isFromLogin);
}

- (void)syncNewsFeed{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    [SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@?auth_token=%@",NEWS_FEED_LINK,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [SVProgressHUD showSuccessWithStatus:strMsg];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            [SVProgressHUD showErrorWithStatus:strMsg];
        }
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)viewDidAppear:(BOOL)animated{
//// is offline comment ///
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * user = [delegate.db getUserObj];
    
    if([user.strSession isEqualToString:@""]){
        ///PetLoginViewController *loginvc =
        
        PetLoginViewController* nav = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
        //nav.ownner = self;
        [self presentModalViewController:nav animated:YES];
    }
    NSLog(@"viewDidAppear pet view controller");
}

-(void)completedWithViewController:(PetLoginViewController *)viewcontroller{
    //[viewcontroller dismissModalViewControllerAnimated:YES];
    NSLog(@"completed login!! in parent");
}

- (void)completedWithViewController{
     NSLog(@"completed login!! in parent");
}

-(void)dismissWithViewController:(PetLoginViewController *)viewcontroller{
    [viewcontroller dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
