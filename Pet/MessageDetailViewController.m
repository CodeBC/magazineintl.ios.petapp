//
//  MessageDetailViewController.m
//  Pet
//
//  Created by Zayar on 6/7/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "MessageDetailViewController.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "petAPIClient.h"
#import "StringTable.h"
#import "ObjUser.h"
#import "PetAppDelegate.h"
#import "CustomPullToRefresh.h"
#import "WebImageOperations.h"
#import "Utility.h"
#import "NavBarButton1.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Category.h"
#import "TSActionSheet.h"
@interface MessageDetailViewController ()
{
    IBOutlet UIBubbleTableView *bubbleTable;
    IBOutlet UIView *textInputView;
    IBOutlet UITextField *textField;
    
    NSMutableArray *bubbleData;
    NSMutableArray *arrDetailMessages;
    CustomPullToRefresh *_ptr;
    UILabel * lblTitleName;
    int sentToId;
    BOOL isDeleteDialogShow;
    int current_index;
    NSIndexPath * selectedIndexPath;
    NSTimer * myTimer;
}
@end

@implementation MessageDetailViewController
@synthesize objConv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
    // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
    // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
    // Groups are delimited with header which contains date and time for the first message in the group.
    if(bubbleData == nil){
        bubbleData = [[NSMutableArray alloc] init];
    }
    
    bubbleTable.bubbleDataSource = self;
    bubbleTable.snapInterval = 120;
    
    // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
    // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
    
    bubbleTable.showAvatars = YES;
    
    // Uncomment the line below to add "Now typing" bubble
    // Possible values are
    //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
    //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
    //    - NSBubbleTypingTypeNone - no "now typing" bubble
    
    bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
    
    // Keyboard events
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            [bubbleTable setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-textInputView.frame.size.height)];
            [textInputView setFrame:CGRectMake(0, bubbleTable.frame.origin.x + bubbleTable.frame.size.height-64, 320, self.view.frame.size.height)];
        }
        else {
            [bubbleTable setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-textInputView.frame.size.height)];
            [textInputView setFrame:CGRectMake(0, bubbleTable.frame.origin.y + bubbleTable.frame.size.height-44, 320, self.view.frame.size.height)];
        }
    }else{
        
        if ([Utility isGreaterOREqualOSVersion:@"7"]) {
            [bubbleTable setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-textInputView.frame.size.height)];
            [textInputView setFrame:CGRectMake(0, bubbleTable.frame.origin.x + bubbleTable.frame.size.height-64, 320, self.view.frame.size.height)];
        }
        else {
            [bubbleTable setFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-textInputView.frame.size.height)];
            [textInputView setFrame:CGRectMake(0, bubbleTable.frame.origin.x + bubbleTable.frame.size.height-44, 320, self.view.frame.size.height)];
        }
        
    }
    NSLog(@"bubble table height %f",bubbleTable.frame.size.height);
    
    
    _ptr = [[CustomPullToRefresh alloc] initWithScrollView:bubbleTable delegate:self];
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CustomPullToRefresh Delegate Methods
- (void) customPullToRefreshShouldRefresh:(CustomPullToRefresh *)ptr {
    //[self performSelectorInBackground:@selector(findNextPrime) withObject:nil];
    //[self performSelector:@selector(endRefresh) withObject:nil afterDelay:2];
    [self syncDetailMessagesWith:objConv];
}

- (void) endRefresh{
    [_ptr endRefresh];
}

- (void) startRefresh{
    
}

- (void)viewWillAppear:(BOOL)animated{
    if (lblTitleName == nil) {
        lblTitleName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    }
    
    lblTitleName.backgroundColor = [UIColor clearColor];
    lblTitleName.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblTitleName.font = [UIFont fontWithName:strValueFontName size:19];
    lblTitleName.text= objConv.strReciverName;
    lblTitleName.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblTitleName;
    [self syncDetailMessagesWith:objConv];
    //sentToId = -1;
    sentToId = objConv.receiverId;
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    lpgr.delegate = self;
    [bubbleTable addGestureRecognizer:lpgr];
    isDeleteDialogShow = FALSE;
    
    if(myTimer ==nil){
         myTimer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(refreshMessage) userInfo:nil repeats:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
}

- (void)refreshMessage{
    [self syncDetailMessagesWith:objConv];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:bubbleTable];
    
    NSIndexPath *indexPath = [bubbleTable indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else
    {
        //UITableViewCell * cell = [bubbleTable cellForRowAtIndexPath:indexPath];
        NSLog(@"long press on table view at row %d and section %d", indexPath.row,indexPath.section);
        current_index = indexPath.row-1;
        
        for (int i = indexPath.section-1; i >-1; i --) {
            NSLog(@"Previous cell count %d and i %d",[bubbleTable numberOfRowsInSection:i],i);
            current_index += [bubbleTable numberOfRowsInSection:i] - 1;
        }
        NSLog(@"Current index for arrMessages %d",current_index);
        //[self showActionSheet:cell forEvent:nil andIndex:indexPath];
        selectedIndexPath = indexPath;
        if (!isDeleteDialogShow) {
            isDeleteDialogShow = TRUE;
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_TITLE message:@"Are you sure want to delete this message?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            alert.tag = 1;
            
            [alert show];
        }
    }
}

- (void)deleteTheMessage:(int)index andIndexPath:(NSIndexPath *)indexPath{
    ObjConversation * obj = [arrDetailMessages  objectAtIndex:index];
    NSLog(@"obj message id %d",obj.message_id);
    [self syncDeleteConversation:obj.message_id and:indexPath andCurrentIndex:current_index];
}

- (void)syncDeleteConversation:(int)msg_id and:(NSIndexPath *)indexPath andCurrentIndex:(int)current_id{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    //@"user[user_id]=%d&user[first_name]=%@&user[last_name]=%@&user[gender]=%@&user[username]=%@&user[profile_image]=%@&user[email]=%@&user[facebook]=%@&user[website]=%@&user[phone]=%@&user[address]=%@&user[dob]=%@"
    //NSDictionary* params = @{@"user[first_name]":obj.strFName,@"user[last_name]":obj.strLName,@"user[gender]":user.strGender,@"user[username]":obj.strName,@"user[profile_image]":obj.strProfileImgLink,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[address]":obj.strAddress,@"user[phone]":obj.strPhone,@"user[dob]":obj.strDob};
    
    //,@"user[email]":obj.strEmail,@"user[facebook]":obj.strFbLink,@"user[website]":obj.strWebLink,@"user[phone]":obj.strPhone,@"user[address]":obj.strAddress,@"user[dob]":obj.strDob
    //NSLog(@"edited params %@",params);
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] deletePath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",MESSAGE_DELETE_LINK,msg_id,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@ and message link %@",json,[NSString stringWithFormat:@"%@/%d?auth_token=%@",MESSAGE_DELETE_LINK,msg_id,strSession]);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            //[SVProgressHUD showSuccessWithStatus:strMsg];
            [arrDetailMessages removeObjectAtIndex:current_id];
            //[bubbleTable deleteRowsAtIndexPaths:@[indexPath]  withRowAnimation:UITableViewRowAnimationFade];
            //[bubbleTable reloadData];
            [self loadTheTableViewWith:arrDetailMessages];
        }
        else if(status == STATUS_ACTION_FAILED){
            //[self textValidateAlertShow:strErrorMsg];
            //[SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        //[SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
       // [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    if( [alertView tag] == 1 ){
		if(buttonIndex == 1 ){
            NSLog(@"yes delete it!");
            isDeleteDialogShow = FALSE;
            [self deleteTheMessage:current_index andIndexPath:selectedIndexPath];
        }
        else if(buttonIndex == 0 ){
            isDeleteDialogShow = FALSE;
        }
    }
}

-(void) showActionSheet:(id)sender forEvent:(UIEvent*)event andIndex:(NSIndexPath *)indexPath
{
    if (!isDeleteDialogShow) {
        [self.view endEditing:YES];
        UITableViewCell * cell = (UITableViewCell *)sender;
        TSActionSheet *actionSheet = [[TSActionSheet alloc] initWithTitle:@""];
        //[actionSheet destructiveButtonWithTitle:@"hoge" block:nil];
        [actionSheet addButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            [self deleteTheIndex:indexPath];
        }];

        actionSheet.cornerRadius = 5;
        //actionSheet.
    
        [actionSheet showWithCell:cell];
        isDeleteDialogShow = TRUE;
    }
}

- (void)deleteTheIndex:(NSIndexPath *)indexPath{
    //NSIndexPath * indexPath = [NSIndexPath indexPathWithIndex:cell.tag];
    //[self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    //[self.bubbleSection removeObjectAtIndex:indexPath.section];
    NSLog(@"deleted bubble section %d",indexPath.row);
    //[self reloadData];
    [arrDetailMessages removeObjectAtIndex:indexPath.row-1];
    [self loadTheTableViewWith:arrDetailMessages];
    isDeleteDialogShow = FALSE;
    
}

- (void)loadTheTableViewWith:(NSMutableArray *)arrMessages{
    NSLog(@"arr message count %d",[arrMessages count]);
    /*NSBubbleData *heyBubble = [NSBubbleData dataWithText:@"Hey, halloween is soon" date:[NSDate dateWithTimeIntervalSinceNow:-300] type:BubbleTypeSomeoneElse];
    heyBubble.avatar = [UIImage imageNamed:@"avatar1.png"];
    
    NSBubbleData *photoBubble = [NSBubbleData dataWithImage:[UIImage imageNamed:@"halloween.jpg"] date:[NSDate dateWithTimeIntervalSinceNow:-290] type:BubbleTypeSomeoneElse];
    photoBubble.avatar = [UIImage imageNamed:@"avatar1.png"];
    
    NSBubbleData *replyBubble = [NSBubbleData dataWithText:@"Wow.. Really cool picture out there. iPhone 5 has really nice camera, yeah?" date:[NSDate dateWithTimeIntervalSinceNow:-5] type:BubbleTypeMine];
    replyBubble.avatar = nil;
    
    bubbleData = [[NSMutableArray alloc] initWithObjects:heyBubble, photoBubble, replyBubble, nil];*/
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    [bubbleData removeAllObjects];
   
    
    for (ObjConversation * objC in arrMessages) {
        NSBubbleData *cellBubble;
        objC.strSenderImgLink = objUser.strProfileImgLink;
        if (objUser.userId == objC.userId) {
            NSTimeInterval timeInterval = objC.timetick;
            cellBubble = [NSBubbleData dataWithText:objC.strMessage date:[NSDate dateWithTimeIntervalSince1970:timeInterval] type:BubbleTypeMine];
            
            /*[WebImageOperations processImageDataWithURLString:objC.strSenderImgLink andBlock:^(NSData *imageData) {
                if (self.view) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(60,60) resizeFrame:NO];
                    cellBubble.avatar = cropedImage;
                    NSLog(@"image loaded at userId %d",objC.userId);
                    [bubbleTable reloadData];
                }
            }];*/
            
            NSString * imagePath = [delegate.db getFilePath:objC.strSenderImgLink];
            if( ![self stringIsEmpty:imagePath shouldCleanWhiteSpace:YES] ){
                //[cell.imgThumb setImage: [[UIImage alloc] initWithContentsOfFile: imagePath]];
                UIImage * image = [[UIImage alloc] initWithContentsOfFile: imagePath];
                cellBubble.avatar = image;
                NSLog(@"here is log the image!! and img path %@",imagePath);
            }
            else{
                NSLog(@"here is need to load from url!!");
                [WebImageOperations processImageDataWithURLString:objC.strSenderImgLink andBlock:^(NSData *imageData) {
                    if (self.view) {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *tempPath = [paths objectAtIndex:0];
                        UIImage *image = [UIImage imageWithData:imageData];
                        
                        NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_profile_thumb.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
                        [delegate.db insertCachedImage:objC.strSenderImgLink  path:imgPath];
                        UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(60,60) resizeFrame:NO];
                        NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(cropedImage)];
                        [imageDetailData writeToFile:imgPath atomically:YES];
                        cellBubble.avatar = cropedImage;
                        [bubbleTable reloadData];
                        NSLog(@"here is need to load from url!! loaded");
                    }
                }];
            }
        }
        else{
            NSTimeInterval timeInterval = objC.timetick;
            cellBubble = [NSBubbleData dataWithText:objC.strMessage date:[NSDate dateWithTimeIntervalSince1970:timeInterval] type:BubbleTypeSomeoneElse];
            /*[WebImageOperations processImageDataWithURLString:objC.strReciverImgLink andBlock:^(NSData *imageData) {
                if (self.view) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(60,60) resizeFrame:NO];
                    cellBubble.avatar = cropedImage;
                    [bubbleTable reloadData];
                }
            }];*/
            
            NSString * imagePath = [delegate.db getFilePath:objC.strReciverImgLink];
            if( ![self stringIsEmpty:imagePath shouldCleanWhiteSpace:YES] ){
                //[cell.imgThumb setImage: [[UIImage alloc] initWithContentsOfFile: imagePath]];
                UIImage * image = [[UIImage alloc] initWithContentsOfFile: imagePath];
                cellBubble.avatar = image;
                NSLog(@"here is log the image!! and img path %@",imagePath);
            }
            else{
                NSLog(@"here is need to load from url!!");
                [WebImageOperations processImageDataWithURLString:objC.strReciverImgLink andBlock:^(NSData *imageData) {
                    if (self.view) {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *tempPath = [paths objectAtIndex:0];
                        UIImage *image = [UIImage imageWithData:imageData];
                        
                        NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_profile_thumb.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
                        [delegate.db insertCachedImage:objC.strReciverImgLink  path:imgPath];
                        UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(60,60) resizeFrame:NO];
                        NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(cropedImage)];
                        [imageDetailData writeToFile:imgPath atomically:YES];
                        cellBubble.avatar = cropedImage;
                        [bubbleTable reloadData];
                        NSLog(@"here is need to load from url!! loaded");
                    }
                }];
            }
        }
        [bubbleData addObject:cellBubble];
    }
    // NSLog(@"bubbleTable count %d and user id %d and objC user id %d",[bubbleData count],objUser.userId,objC.userId);
    [bubbleTable reloadData];
}

#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

- (void)syncDetailMessagesWith:(ObjConversation *)objC{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] getPath:[NSString stringWithFormat:@"%@/%d?auth_token=%@",MESSAGE_LINK,objConv.idx,strSession] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@ and message link %@",json,MESSAGE_LINK);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
            [self endRefresh];
            NSMutableArray *  arr = [dics objectForKey:@"messages"];
            sentToId = [[dics objectForKey:@"sendto"] intValue];
            NSMutableArray * arrResultMessages = [[NSMutableArray alloc]initWithCapacity:[arr count]];
            for(NSInteger i=0;i<[arr count];i++){
                NSDictionary * dicNewsFeed = [arr objectAtIndex:i];
                ObjConversation * obj = [[ObjConversation alloc]init];
                obj.idx = [[dicNewsFeed objectForKey:@"conversation_id"] intValue];
                obj.strMessage = [dicNewsFeed objectForKey:@"message"];
                obj.message_id = [[dicNewsFeed objectForKey:@"message_id"] intValue];
                obj.strReciverImgLink = [dicNewsFeed objectForKey:@"profile_image"];
                obj.strReciverName = [dicNewsFeed objectForKey:@"timestamp"];
                obj.userId = [[dicNewsFeed objectForKey:@"user_id"]intValue];
                obj.timetick = [[dicNewsFeed objectForKey:@"timestamp"]intValue];
                [arrResultMessages addObject:obj];
            }
            NSLog(@"arrMessages count %d",[arrResultMessages count]);
            [self loadTheTableViewWith:arrResultMessages];
            [self tblScrollToEnd];
            arrDetailMessages = arrResultMessages;
            [_ptr endRefresh];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            [_ptr endRefresh];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
            [_ptr endRefresh];
        }
        //[SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (void)syncPushMessagesWith:(NSString *)strMessage{
    NSUserDefaults * prefs=[NSUserDefaults standardUserDefaults];
    NSString * strSession = [prefs objectForKey:LOGIN_LINK];
    NSDictionary* params = @{@"msg[receiver_id]":[NSString stringWithFormat:@"%d", sentToId],@"msg[message]":strMessage};
    NSLog(@"params %@",params);
    //[SVProgressHUD show];
    [[petAPIClient sharedClient] postPath:[NSString stringWithFormat:@"%@?auth_token=%@",MESSAGE_PUSH_LINK,strSession] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"successfully return!!! %@",json);
        NSDictionary * dics = (NSDictionary *)json;
        int status = [[dics objectForKey:@"status"] intValue];
        NSString * strMsg = [dics objectForKey:@"message"];
        if (status == STATUS_ACTION_SUCCESS) {
           // [SVProgressHUD showSuccessWithStatus:@"Sent"];
            if (objConv == nil) {
                objConv = [[ObjConversation alloc]init];
            }
            objConv.idx = [[dics objectForKey:@"conversation_id"] intValue];
            objConv.strMessage = strMessage;
            objConv.receiverId = sentToId;
            objConv.message_id = [[dics objectForKey:@"message_id"] intValue];
            /*if (![self stringIsEmpty:[dics objectForKey:@"conversation_id"] shouldCleanWhiteSpace:YES]){
            }*/
            if (arrDetailMessages == nil) {
                arrDetailMessages = [[NSMutableArray alloc]init];
            }
            [arrDetailMessages addObject:objConv];
            //[self loadTheTableViewWith:arrDetailMessages];
        }
        else if(status == STATUS_ACTION_FAILED){
            
            //[self textValidateAlertShow:strErrorMsg];
            //[SVProgressHUD showErrorWithStatus:strMsg];
        }
        else if(status == STATUS_INVALID_AUTH){
            //[self textValidateAlertShow:strErrorMsg];
            NSString * strMsg = [dics objectForKey:@"message"];
            //[SVProgressHUD showErrorWithStatus:strMsg];
            PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
            [delegate logout:self];
        }
        
        //[SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        //[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
    }];
}

- (BOOL) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    }
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Keyboard events
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y -= kbSize.height;
        textInputView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height -= kbSize.height;
        bubbleTable.frame = frame;
    }];
    [self tblScrollToEnd];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y += kbSize.height;
        textInputView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height += kbSize.height;
        bubbleTable.frame = frame;
    }];
    [self tblScrollToEnd];
}

#pragma mark - Actions

- (IBAction)sayPressed:(id)sender
{
    bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjUser * objUser = [delegate.db getUserObj];
    NSBubbleData *sayBubble = [NSBubbleData dataWithText:textField.text date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine];
    NSString * imagePath = [delegate.db getFilePath:objUser.strProfileImgLink];
    NSLog(@"obj user img %@ and image path %@",objUser.strProfileImgLink,imagePath);
    if( ![self stringIsEmpty:imagePath shouldCleanWhiteSpace:YES] ){
        //[cell.imgThumb setImage: [[UIImage alloc] initWithContentsOfFile: imagePath]];
        UIImage * image = [[UIImage alloc] initWithContentsOfFile: imagePath];
        sayBubble.avatar = image;
        
    }
    else{
        //NSLog(@"here is need to load from url!!");
        [WebImageOperations processImageDataWithURLString:objUser.strProfileImgLink andBlock:^(NSData *imageData) {
            if (self.view) {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *tempPath = [paths objectAtIndex:0];
                UIImage *image = [UIImage imageWithData:imageData];
                
                NSString * imgPath = [tempPath stringByAppendingPathComponent: [NSString stringWithFormat: @"%.0f_profile_thumb.png", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
                [delegate.db insertCachedImage:objUser.strProfileImgLink  path:imgPath];
                UIImage * cropedImage = [Utility imageByScalingProportionallyToSize:image newsize:CGSizeMake(60,60) resizeFrame:NO];
                NSData *imageDetailData = [NSData dataWithData:UIImagePNGRepresentation(cropedImage)];
                [imageDetailData writeToFile:imgPath atomically:YES];
                sayBubble.avatar = cropedImage;
                [bubbleTable reloadData];
                NSLog(@"here is need to load from url!! loaded");
            }
        }];
    }
    
    [bubbleData addObject:sayBubble];
    [bubbleTable reloadData];
    [self tblScrollToEnd];
    [self syncPushMessagesWith:textField.text];
    
    textField.text = @"";
    [textField resignFirstResponder];

}

- (void)tblScrollToEnd{
    if ([bubbleData count]>0) {
       /* NSIndexPath* ipath = [NSIndexPath indexPathForRow: [bubbleData count]-1 inSection: [bubbleData count]-1];
        [bubbleTable scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];*/
       // [bubbleTable scr]
       NSLog(@"table height %f and table content height %f",bubbleTable.frame.size.height,bubbleTable.contentSize.height);
        if (bubbleTable.contentSize.height > bubbleTable.frame.size.height) {
            
             CGPoint offset = CGPointMake(0, bubbleTable.contentSize.height -     bubbleTable.frame.size.height);
             [bubbleTable setContentOffset:offset animated:YES];
        }
        else{
            NSLog(@"here is end!!");
            [bubbleTable scrollToTop];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    PetAppDelegate * delegate = [[UIApplication sharedApplication]delegate];
    [delegate.db removeCahcedImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
