//
//  WebImageOperations.h
//  Pet
//
//  Created by Zayar on 5/20/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebImageOperations : NSObject
+ (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage;
@end
