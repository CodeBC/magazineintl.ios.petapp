//
//  DealDetailViewController.m
//  Pet
//
//  Created by Zayar on 5/19/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import "DealDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ObjDeal.h"
#import "SVModalWebViewController.h"
#import "NavBarButton1.h"

@interface DealDetailViewController ()
{
    IBOutlet UIImageView * imgView;
    IBOutlet UILabel * lblName;
    IBOutlet UILabel * lblInfo;
    IBOutlet UILabel * lblDescrip;
    
    IBOutlet UIImageView * imgBgView;
}
@end

@implementation DealDetailViewController
@synthesize objDeal;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    NavBarButton1 *btnBack = [[NavBarButton1 alloc] init];
	[btnBack addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
	
	UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = backButton;
        
    UILabel * lblName1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
    lblName1.backgroundColor = [UIColor clearColor];
    lblName1.textColor = [UIColor whiteColor];
    NSString * strValueFontName=@"AvenirNext-Regular";
    lblName1.font = [UIFont fontWithName:strValueFontName size:19];
    lblName1.text= @"Pet Deals";
    lblName1.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = lblName1;
    
    /*CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        [imgBgView setFrame:CGRectMake(0, 0, 320, 504)];
    }else{
        [imgBgView setFrame:CGRectMake(0, 0, 320, 460)];
    }
    [imgBgView setImage:[UIImage imageNamed:@"img_main_bg"]];*/
}

- (void)loadTheDealViewWith:(ObjDeal *)obj{
    NSLog(@"deal img link %@",obj.strImgLink);
    NSURL * url = [NSURL URLWithString:obj.strImgLink];
    [imgView setImageWithURL:url placeholderImage:nil];
    
    lblName.text = obj.strName;
    lblInfo.text = obj.strInfo;
    lblDescrip.text = obj.strDescription;
}

- (void)goBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBuy:(id)sender{
    //NSString * strLink = @"https://www.paypal.com";
    NSString * strLink = [NSString stringWithFormat:@"http://pets-app.herokuapp.com/deals/%d",objDeal.idx];
    NSURL *URL = [NSURL URLWithString:strLink];
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
    webViewController.barsTintColor = [UIColor colorWithRed:0.89f green:0.43f blue:0.12f alpha:1.00f];

    [self presentModalViewController:webViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTheDealViewWith:objDeal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
