//
//  MyPetCell.h
//  Pet
//
//  Created by Zayar on 4/30/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPetCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView * imgPetView;
@property (nonatomic, strong) IBOutlet UILabel * lblName;
@property (nonatomic, strong) IBOutlet UILabel * lblType;
@end
