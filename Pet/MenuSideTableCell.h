//
//  MenuSideTableCell.h
//  Feel
//
//  Created by Zayar on 12/12/12.
//
//

#import <UIKit/UIKit.h>

@interface MenuSideTableCell : UITableViewCell
{
    UIImageView * imgView;
    UILabel * lblName;
}
@property (nonatomic,retain) IBOutlet UIImageView * imgView;
@property (nonatomic,retain) IBOutlet UILabel * lblName;
@end
