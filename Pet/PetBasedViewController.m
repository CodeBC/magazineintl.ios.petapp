//
//  PetBasedViewController.m
//  Pet
//
//  Created by Zayar on 3/11/14.
//  Copyright (c) 2014 bc. All rights reserved.
//

#import "PetBasedViewController.h"
#import "Utility.h"
@interface PetBasedViewController ()

@end

@implementation PetBasedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.colorWithHexString:@"e4e4e4"
    NSLog(@"base view!!!");
    
}

- (void)viewDidAppear:(BOOL)animated{
    //
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"e4e4e4"]];
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"e95401"];
    }
    else {
        //self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"Navbar"];
        
    }
    
}

- (void)hereloadBgView{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
