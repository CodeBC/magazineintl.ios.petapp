//
//  SOAPRequest.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjUser.h"
#import "ObjPet.h"
#import "ObjReminder.h"
@protocol SOAPRequestDelegate
- (void) onDataLoad: (int) processId withStatus:(int) status;
- (void) onErrorLoad: (int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId;
- (void) onJsonLoaded:(NSMutableDictionary *) dics withProcessId:(int) processId andResponsestatus:(int)status;
@end

typedef enum {
	REQUEST_TYPE_GENR_KEY = 1,
    REQUEST_TYPE_CHECK_SESSION = 2,
    REQUEST_TYPE_LOGIN = 3,
    REQUEST_TYPE_REGISTER = 4,
    REQUEST_TYPE_PROFILE = 5,
    REQUEST_TYPE_PROFILE_EDIT = 6,
    REQUEST_TYPE_MYPET = 7,
    REQUEST_TYPE_ADDPET = 8,
    REQUEST_TYPE_EDITPET = 9,
    REQUEST_TYPE_REMINDER = 10,
    REQUEST_TYPE_LOST_FOUND = 11,
    REQUEST_TYPE_REPORT_LOST = 12,
    REQUEST_TYPE_VEDIO = 13,
    REQUEST_TYPE_DEAL = 14
} RequestType;

@interface SOAPRequest : NSObject<SOAPRequestDelegate> {
    NSMutableData * responseData;
    
    int processId;
    id <SOAPRequestDelegate> owner;
    
    RequestType rType;
    int intResponseStatusCode;
}

@property RequestType rType;
@property (nonatomic, retain) NSMutableData * responseData;
@property id<SOAPRequestDelegate> owner;
@property int processId;

- (id)initWithOwner:(id)del;
- (NSString *) urlencode: (NSString *) url;
- (void) syncGenerateKey;
- (void) syncGenerateKeyWithToken:(NSString*) token;
- (void) syncAndCheckTheSessionKey;
- (void) syncLoginWithUser:(ObjUser *)objUser;
- (NSString *)encodePasswordWithSHA1:(NSString *)str;
- (NSString*)base64forData:(NSData*)theData;
- (void) syncRegisterWithUser:(ObjUser *)objUser;
- (void) syncProfile;
- (void) syncEditProfileWithUser:(ObjUser *)objUser;
- (void) syncMyPet;
- (void) syncAddPet:(ObjPet *)objRemind;
- (void) syncEditPet:(ObjPet *)objPet;
- (void) syncAddRemind:(ObjReminder *)objRemind;
- (void) syncLostAndFound;
- (void) syncReportLostPet:(ObjPet *)objRemind;
- (void) syncVideo;
- (void) syncDeal;
@end
