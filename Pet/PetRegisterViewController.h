//
//  PetRegisterViewController.h
//  Pet
//
//  Created by Zayar on 4/27/13.
//  Copyright (c) 2013 bc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOAPRequest.h"

@interface PetRegisterViewController : UIViewController
{
    IBOutlet UIImageView * imgBgView;
    IBOutlet UIScrollView * scrollView;
    IBOutlet UITextField * txtLoginName;
    IBOutlet UITextField * txtPassword;
    IBOutlet UITextField * txtFName;
    IBOutlet UITextField * txtLName;
    IBOutlet UITextField * txtEmail;
    IBOutlet UITextField * txtRePassword;
    IBOutlet UITextField * txtPostal;
    IBOutlet UIButton * btnSignin;
    NSString * strLoginName;
    NSString * strPassword;
    NSString * strFName;
    NSString * strLName;
    NSString * strEmail;
    NSString * strGender;
    NSString * strCountry;
    NSString * strPostalCode;

    NSMutableArray * entryFields;
    SOAPRequest * registerRequest;
}
- (void)hideRegister;


@end
